import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:flutter/material.dart';

class ExpandableBottomSheet extends StatefulWidget {
  /// Creates the [ExpandableBottomSheet].
  ///
  /// [persistentContentHeight] has to be greater 0.
  const ExpandableBottomSheet(
      {Key? key,
      required this.expandableContent,
      required this.persistentHeader,
      required this.persistentContentHeight,
      this.onExtendedCallback,
      this.onContractedCallback,
      this.maxHeight,
      this.controller})
      : assert(persistentContentHeight >= 0),
        super(key: key);

  final Widget expandableContent;

  /// [persistentHeader] is a Widget which is on top of the [expandableContent]
  /// and will never be hidden. It is made for a widget which indicates the
  /// user he can expand the content by dragging.
  final Widget persistentHeader;

  /// [persistentContentHeight] is the height of the content when expanded.
  final double persistentContentHeight;

  /// [onExtendedCallback] will be executed if the extend reaches its maximum.
  final VoidCallback? onExtendedCallback;

  /// [onContractedCallback] will be executed if the extend reaches its minimum.
  final VoidCallback? onContractedCallback;

  final ExpansionController? controller;

  final double? maxHeight;

  @override
  ExpandableBottomSheetState createState() => ExpandableBottomSheetState();
}

class ExpandableBottomSheetState extends State<ExpandableBottomSheet>
    with TickerProviderStateMixin {
  final GlobalKey _headerKey = GlobalKey(debugLabel: 'headerKey');

  AnimationController? _controller;
  late ExpansionController _expansionController;

  bool _callCallbacks = false;
  AnimationStatus _oldStatus = AnimationStatus.dismissed;
  late double _startHeightAtDragDown;

  /// The status of the expansion.
  ExpansionStatus get expansionStatus {
    if (_controller?.isCompleted == true) return ExpansionStatus.expanded;

    if (_controller?.isAnimating == true) return ExpansionStatus.middle;

    return ExpansionStatus.contracted;
  }

  /// Expands the content of the widget.
  void expand() {
    _callCallbacks = true;
    _animateExpand();
  }

  void _animateExpand() {
    final currentAnimatedValue = _controller?.value;
    _stopAnimation();
    if (currentAnimatedValue != _controller?.upperBound) {
      _oldStatus = AnimationStatus.forward;
      _controller?.forward(from: currentAnimatedValue);
    } else {
      _oldStatus = AnimationStatus.forward;
      _handleAnimationStatusUpdate(AnimationStatus.completed);
    }
  }

  void _stopAnimation() {
    if (_controller?.isAnimating == true) {
      _controller?.stop();
    }
  }

  /// Contracts the content of the widget.
  void contract() {
    _callCallbacks = true;
    _animateContract();
  }

  void _animateContract() {
    final currentAnimatedValue = _controller?.value;
    _stopAnimation();
    if (currentAnimatedValue != _controller?.lowerBound) {
      _oldStatus = AnimationStatus.reverse;
      _controller?.reverse(from: currentAnimatedValue);
    } else {
      _oldStatus = AnimationStatus.reverse;
      _handleAnimationStatusUpdate(AnimationStatus.completed);
    }
  }

  @override
  void initState() {
    _expansionController = widget.controller ?? ExpansionController();
    _expansionController.addListener(_onExpansionRequested);
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback(_initAnimationController);
  }

  void _onExpansionRequested() {
    if ((_controller?.value ?? 0) > (_controller?.lowerBound ?? 0)) {
      contract();
    } else {
      expand();
    }
  }

  void _initAnimationController(Duration timestamp) {
    final media = MediaQuery.of(context);
    final safeAreaBottomPadding = media.padding.bottom;
    final safeAreaTopPadding = media.padding.top;
    final headerHeight = _headerKey.currentContext?.size?.height ?? 0;
    final desiredHeight = widget.persistentContentHeight;

    final availableAreaHeight = (widget.maxHeight ?? 0.0) > 0
        ? (widget.maxHeight ?? 0.0)
        : media.size.height;

    final maxHeight = availableAreaHeight -
        headerHeight -
        safeAreaBottomPadding -
        safeAreaTopPadding;

    _controller = AnimationController(
      vsync: this,
      upperBound: desiredHeight > maxHeight ? desiredHeight : maxHeight,
      duration: const Duration(milliseconds: 250),
    );

    _controller?.addStatusListener(_handleAnimationStatusUpdate);
    _refreshUi();
  }

  void _handleAnimationStatusUpdate(AnimationStatus status) {
    if (status == AnimationStatus.completed) {
      if (_oldStatus == AnimationStatus.forward &&
          widget.onExtendedCallback != null &&
          _callCallbacks) {
        widget.onExtendedCallback?.call();
      }

      if (_oldStatus == AnimationStatus.reverse &&
          widget.onContractedCallback != null &&
          _callCallbacks) {
        widget.onContractedCallback?.call();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onVerticalDragDown: _dragDown,
      onVerticalDragUpdate: _dragUpdate,
      onVerticalDragEnd: _dragEnd,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(key: _headerKey, child: widget.persistentHeader),
          if (_controller != null)
            AnimatedBuilder(
                animation: _controller!,
                builder: (BuildContext context, Widget? child) {
                  return Container(
                    constraints: _controller?.value != null
                        ? BoxConstraints.expand(height: _controller!.value)
                        : null,
                    child: child,
                  );
                },
                child: widget.expandableContent)
          else
            Container(),
          Container(
            color: ThemeColors.buttonsGreyFullyVisibleColor,
            constraints: BoxConstraints.expand(
                height: MediaQuery.of(context).padding.bottom),
          ),
        ],
      ),
    );
  }

  void _dragDown(DragDownDetails details) {
    if (_controller?.isAnimating != true) {
      _startHeightAtDragDown = _controller?.value ?? 0;
    }
  }

  void _refreshUi() {
    setState(() {});
  }

  void _dragUpdate(DragUpdateDetails details) {
    if (_controller?.isAnimating == true) return;

    final offset = details.localPosition.dy;
    final newOffset = _startHeightAtDragDown - offset;
    if ((_controller?.lowerBound ?? 0) <= newOffset &&
        (_controller?.upperBound ?? 0) >= newOffset) {
      _controller?.value = newOffset;
      _refreshUi();
    } else {
      if ((_controller?.lowerBound ?? 0) > newOffset) {
        _controller?.value = _controller?.lowerBound ?? 0;
        _refreshUi();
      }

      if ((_controller?.upperBound ?? 0) < newOffset) {
        _controller?.value = _controller?.upperBound ?? 0;
        _refreshUi();
      }
    }
  }

  void _dragEnd(DragEndDetails details) {
    if (_startHeightAtDragDown == _controller?.value ||
        _controller?.isAnimating == true) return;

    if ((details.primaryVelocity ?? 0) < -250) {
      //drag up ended with high speed
      _callCallbacks = true;
      _animateExpand();
    } else {
      if ((details.primaryVelocity ?? 0) > 250) {
        //drag down ended with high speed
        _callCallbacks = true;
        _animateContract();
      } else {
        if (_controller?.value == _controller?.upperBound &&
            widget.onContractedCallback != null) {
          widget.onContractedCallback?.call();
        }
        if (_controller?.value == _controller?.lowerBound &&
            widget.onExtendedCallback != null) {
          widget.onExtendedCallback?.call();
        }

        if ((_controller?.value ?? 0) < (_controller?.upperBound ?? 0) &&
            (_controller?.value ?? 0) > (_controller?.lowerBound ?? 0)) {
          if (_startHeightAtDragDown > (_controller?.value ?? 0)) {
            _animateContract();
          } else {
            _animateExpand();
          }
        }
      }
    }
  }

  @override
  void dispose() {
    _expansionController.removeListener(_onExpansionRequested);
    if (widget.controller == null) {
      _expansionController.dispose();
    }

    _controller?.dispose();
    super.dispose();
  }
}

/// The status of the expandable content.
enum ExpansionStatus {
  expanded,
  middle,
  contracted,
}

class ExpansionController extends ValueNotifier<bool> {
  ExpansionController({bool? expanded}) : super(expanded ?? false);

  bool get expanded => value;

  void toggle() {
    expanded = !expanded;
  }

  set expanded(bool expanded) {
    value = expanded;
  }

  bool get isAttached => hasListeners;
}
