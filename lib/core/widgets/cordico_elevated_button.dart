import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:flutter/material.dart';

class CordicoElevatedButton extends StatelessWidget {
  const CordicoElevatedButton({
    Key? key,
    this.text,
    required this.onPressed,
    this.icon,
    this.child,
    this.enabled = true,
    this.isFullyRoundedButton = false,
    this.height,
    this.width,
    this.color,
    this.textStyle,
    this.hasBorder = false,
    this.isLoading = false,
  }) : super(key: key);

  final String? text;
  final VoidCallback? onPressed;
  final Widget? icon;
  final Widget? child;
  final bool enabled;
  final bool isFullyRoundedButton;
  final double? width;
  final double? height;
  final Color? color;
  final TextStyle? textStyle;
  final bool hasBorder;
  final bool isLoading;

  @override
  Widget build(BuildContext context) {
    final buttonStyle = hasBorder
        ? ThemeStyles.outlinedButtonStyle(
            context,
            color: enabled
                ? color
                : (color ?? ThemeColors.blue.shade500).withOpacity(0.5),
            height: height,
            width: width,
            isFullyRounded: isFullyRoundedButton,
          )
        : ThemeStyles.elevatedButtonStyle(
            context,
            color: enabled
                ? color
                : (color ?? ThemeColors.blue.shade500).withOpacity(0.5),
            height: height,
            width: width,
            isFullyRounded: isFullyRoundedButton,
          );

    final buttonChild = child ??
        Text(
          text!,
          textAlign: TextAlign.center,
          style: textStyle ?? ThemeStyles.elevatedButtonTextStyle(context),
        );

    if (icon == null) {
      return SizedBox(
        height: height,
        child: /*Stack(
          fit: StackFit.expand,
          children: [
            Positioned(
              left: 0,
              right: 0,
              child: */
            ElevatedButton(
          style: buttonStyle,
          onPressed: enabled ? onPressed : () {},
          child: buttonChild,
        ),
        /* ),
            if (isLoading)
              const Align(
                child: SizedBox(
                  height: 20,
                  width: 20,
                  child: CircularProgressIndicator(
                    strokeWidth: 3,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  ),
                ),
              )
          ],
        ),*/
      );
    } else {
      return SizedBox(
        height: height,
        child: ElevatedButton.icon(
          style: buttonStyle,
          label: buttonChild,
          icon: icon!,
          onPressed: enabled ? onPressed : null,
        ),
      );
    }
  }
}
