// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'delete_one_user_mutation.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeleteOneUserMutation$MutationRoot$DeleteOneUser
    _$DeleteOneUserMutation$MutationRoot$DeleteOneUserFromJson(
        Map<String, dynamic> json) {
  return DeleteOneUserMutation$MutationRoot$DeleteOneUser()
    ..id = json['id'] as String;
}

Map<String, dynamic> _$DeleteOneUserMutation$MutationRoot$DeleteOneUserToJson(
        DeleteOneUserMutation$MutationRoot$DeleteOneUser instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

DeleteOneUserMutation$MutationRoot _$DeleteOneUserMutation$MutationRootFromJson(
    Map<String, dynamic> json) {
  return DeleteOneUserMutation$MutationRoot()
    ..deleteOneUser = json['deleteOneUser'] == null
        ? null
        : DeleteOneUserMutation$MutationRoot$DeleteOneUser.fromJson(
            json['deleteOneUser'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DeleteOneUserMutation$MutationRootToJson(
        DeleteOneUserMutation$MutationRoot instance) =>
    <String, dynamic>{
      'deleteOneUser': instance.deleteOneUser?.toJson(),
    };

DeleteOneUserMutationArguments _$DeleteOneUserMutationArgumentsFromJson(
    Map<String, dynamic> json) {
  return DeleteOneUserMutationArguments(
    id: json['id'] as String?,
  );
}

Map<String, dynamic> _$DeleteOneUserMutationArgumentsToJson(
        DeleteOneUserMutationArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
