// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'create_one_collection_content.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateOneCollectionContent$MutationRoot$CreateOneCollectionContent
    _$CreateOneCollectionContent$MutationRoot$CreateOneCollectionContentFromJson(
        Map<String, dynamic> json) {
  return CreateOneCollectionContent$MutationRoot$CreateOneCollectionContent()
    ..id = json['id'] as String
    ..contentId = json['contentId'] as String
    ..collectionId = json['collectionId'] as String;
}

Map<String, dynamic>
    _$CreateOneCollectionContent$MutationRoot$CreateOneCollectionContentToJson(
            CreateOneCollectionContent$MutationRoot$CreateOneCollectionContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'contentId': instance.contentId,
          'collectionId': instance.collectionId,
        };

CreateOneCollectionContent$MutationRoot
    _$CreateOneCollectionContent$MutationRootFromJson(
        Map<String, dynamic> json) {
  return CreateOneCollectionContent$MutationRoot()
    ..createOneCollectionContent =
        CreateOneCollectionContent$MutationRoot$CreateOneCollectionContent
            .fromJson(
                json['createOneCollectionContent'] as Map<String, dynamic>);
}

Map<String, dynamic> _$CreateOneCollectionContent$MutationRootToJson(
        CreateOneCollectionContent$MutationRoot instance) =>
    <String, dynamic>{
      'createOneCollectionContent':
          instance.createOneCollectionContent.toJson(),
    };

CreateOneCollectionContentArguments
    _$CreateOneCollectionContentArgumentsFromJson(Map<String, dynamic> json) {
  return CreateOneCollectionContentArguments(
    collectionId: json['collectionId'] as String,
    contentId: json['contentId'] as String,
  );
}

Map<String, dynamic> _$CreateOneCollectionContentArgumentsToJson(
        CreateOneCollectionContentArguments instance) =>
    <String, dynamic>{
      'collectionId': instance.collectionId,
      'contentId': instance.contentId,
    };
