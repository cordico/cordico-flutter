// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'user_by_identifier.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class UserByIdentifier$QueryRoot$User extends JsonSerializable
    with EquatableMixin {
  UserByIdentifier$QueryRoot$User();

  factory UserByIdentifier$QueryRoot$User.fromJson(Map<String, dynamic> json) =>
      _$UserByIdentifier$QueryRoot$UserFromJson(json);

  dynamic? data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  late String id;

  late String identifier;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @JsonKey(name: 'user_service_id')
  String? userServiceId;

  @JsonKey(name: 'user_type_id')
  late String userTypeId;

  @override
  List<Object?> get props =>
      [data, createdAt, id, identifier, updatedAt, userServiceId, userTypeId];
  @override
  Map<String, dynamic> toJson() =>
      _$UserByIdentifier$QueryRoot$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UserByIdentifier$QueryRoot extends JsonSerializable with EquatableMixin {
  UserByIdentifier$QueryRoot();

  factory UserByIdentifier$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$UserByIdentifier$QueryRootFromJson(json);

  late List<UserByIdentifier$QueryRoot$User> user;

  @override
  List<Object?> get props => [user];
  @override
  Map<String, dynamic> toJson() => _$UserByIdentifier$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UserByIdentifierArguments extends JsonSerializable with EquatableMixin {
  UserByIdentifierArguments({this.identifier});

  @override
  factory UserByIdentifierArguments.fromJson(Map<String, dynamic> json) =>
      _$UserByIdentifierArgumentsFromJson(json);

  final String? identifier;

  @override
  List<Object?> get props => [identifier];
  @override
  Map<String, dynamic> toJson() => _$UserByIdentifierArgumentsToJson(this);
}

final USER_BY_IDENTIFIER_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'UserByIdentifier'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'identifier')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'user'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'identifier'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: '_eq'),
                              value: VariableNode(
                                  name: NameNode(value: 'identifier')))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'identifier'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'user_service_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'user_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class UserByIdentifierQuery extends GraphQLQuery<UserByIdentifier$QueryRoot,
    UserByIdentifierArguments> {
  UserByIdentifierQuery({required this.variables});

  @override
  final DocumentNode document = USER_BY_IDENTIFIER_QUERY_DOCUMENT;

  @override
  final String operationName = 'UserByIdentifier';

  @override
  final UserByIdentifierArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  UserByIdentifier$QueryRoot parse(Map<String, dynamic> json) =>
      UserByIdentifier$QueryRoot.fromJson(json);
}
