import 'package:cordico_mobile/core/providers/global_providers.dart';
import 'package:cordico_mobile/features/department/logic/department_page_holder.dart';
import 'package:cordico_mobile/features/department/logic/department_state_notifier.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final departmentPageNotifierProvider = Provider<DepartmentNotifier>((ref) {
  final notifier = DepartmentNotifier(
    organizationRepository: ref.watch(organizationRepositoryProvider),
  )..getContent();
  return notifier;
});

// * Logic holder / StateNotifier
final departmentPageStateNotifierProvider =
    StateNotifierProvider<DepartmentNotifier, DepartmentPageHolder>(
  (ref) => ref.watch(departmentPageNotifierProvider),
);
