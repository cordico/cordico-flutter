// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'category_by_name_api.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CategoryByName$QueryRoot$Category extends JsonSerializable
    with EquatableMixin {
  CategoryByName$QueryRoot$Category();

  factory CategoryByName$QueryRoot$Category.fromJson(
          Map<String, dynamic> json) =>
      _$CategoryByName$QueryRoot$CategoryFromJson(json);

  late String id;

  late String name;

  late String description;

  late String key;

  @override
  List<Object?> get props => [id, name, description, key];
  @override
  Map<String, dynamic> toJson() =>
      _$CategoryByName$QueryRoot$CategoryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CategoryByName$QueryRoot extends JsonSerializable with EquatableMixin {
  CategoryByName$QueryRoot();

  factory CategoryByName$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$CategoryByName$QueryRootFromJson(json);

  late List<CategoryByName$QueryRoot$Category> category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() => _$CategoryByName$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CategoryByNameArguments extends JsonSerializable with EquatableMixin {
  CategoryByNameArguments({required this.name});

  @override
  factory CategoryByNameArguments.fromJson(Map<String, dynamic> json) =>
      _$CategoryByNameArgumentsFromJson(json);

  late String name;

  @override
  List<Object?> get props => [name];
  @override
  Map<String, dynamic> toJson() => _$CategoryByNameArgumentsToJson(this);
}

final CATEGORY_BY_NAME_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'CategoryByName'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'name')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'category'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'name'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: '_eq'),
                              value:
                                  VariableNode(name: NameNode(value: 'name')))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'description'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class CategoryByNameQuery
    extends GraphQLQuery<CategoryByName$QueryRoot, CategoryByNameArguments> {
  CategoryByNameQuery({required this.variables});

  @override
  final DocumentNode document = CATEGORY_BY_NAME_QUERY_DOCUMENT;

  @override
  final String operationName = 'CategoryByName';

  @override
  final CategoryByNameArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  CategoryByName$QueryRoot parse(Map<String, dynamic> json) =>
      CategoryByName$QueryRoot.fromJson(json);
}
