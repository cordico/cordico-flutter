// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'configuration_type_query.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class ConfigurationTypeQuery$QueryRoot$ConfigurationType
    extends JsonSerializable with EquatableMixin {
  ConfigurationTypeQuery$QueryRoot$ConfigurationType();

  factory ConfigurationTypeQuery$QueryRoot$ConfigurationType.fromJson(
          Map<String, dynamic> json) =>
      _$ConfigurationTypeQuery$QueryRoot$ConfigurationTypeFromJson(json);

  late String id;

  late String name;

  late String key;

  late dynamic schema;

  @override
  List<Object?> get props => [id, name, key, schema];
  @override
  Map<String, dynamic> toJson() =>
      _$ConfigurationTypeQuery$QueryRoot$ConfigurationTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ConfigurationTypeQuery$QueryRoot extends JsonSerializable
    with EquatableMixin {
  ConfigurationTypeQuery$QueryRoot();

  factory ConfigurationTypeQuery$QueryRoot.fromJson(
          Map<String, dynamic> json) =>
      _$ConfigurationTypeQuery$QueryRootFromJson(json);

  @JsonKey(name: 'configuration_type')
  late List<ConfigurationTypeQuery$QueryRoot$ConfigurationType>
      configurationType;

  @override
  List<Object?> get props => [configurationType];
  @override
  Map<String, dynamic> toJson() =>
      _$ConfigurationTypeQuery$QueryRootToJson(this);
}

final CONFIGURATION_TYPE_QUERY_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'ConfigurationTypeQuery'),
      variableDefinitions: [],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'configuration_type'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class ConfigurationTypeQueryQuery
    extends GraphQLQuery<ConfigurationTypeQuery$QueryRoot, JsonSerializable> {
  ConfigurationTypeQueryQuery();

  @override
  final DocumentNode document = CONFIGURATION_TYPE_QUERY_QUERY_DOCUMENT;

  @override
  final String operationName = 'ConfigurationTypeQuery';

  @override
  List<Object?> get props => [document, operationName];
  @override
  ConfigurationTypeQuery$QueryRoot parse(Map<String, dynamic> json) =>
      ConfigurationTypeQuery$QueryRoot.fromJson(json);
}
