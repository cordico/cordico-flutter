// ignore_for_file: avoid_function_literals_in_foreach_calls

import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/app_bar_provider.dart';
import 'package:cordico_mobile/core/widgets/cordico_elevated_button.dart';
import 'package:cordico_mobile/core/widgets/cordico_linear_progress_bar.dart';
import 'package:cordico_mobile/core/widgets/message.dart';
import 'package:cordico_mobile/features/content/logic/content_provider.dart';
import 'package:cordico_mobile/features/content/views/assessment/assessment_question_page.dart';
import 'package:cordico_mobile/features/content/views/assessment/model/assessment_results.dart';
import 'package:core/core.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/graphql/content_part/content_part_by_content_id.dart';
import 'package:core/utils/map_extension.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
// ignore: implementation_imports
import 'package:vrouter/src/core/extended_context.dart';
import 'package:vrouter/vrouter.dart';

class AssessmentQuestionnairePage extends ConsumerStatefulWidget {
  const AssessmentQuestionnairePage({
    Key? key,
    this.questions,
  }) : super(key: key);

  final ContentPartByContentID$QueryRoot? questions;

  @override
  ConsumerState createState() => AssessmentQuestionnaireState();
}

class AssessmentQuestionnaireState
    extends ConsumerState<AssessmentQuestionnairePage> {
  late PageController _controller;
  bool _allowNextQuestion = false;
  int _currentPage = 0;
  int totalScore = 0;

  late ContentQuery$QueryRoot$Content? content;
  late ContentSchema? contentSchema;

  AssessmentResult result = AssessmentResult();

  final List<ContentQuery$QueryRoot$Content> _questions = [];
  Map<String, bool> assessmentQuestions = {};

  @override
  void initState() {
    _currentPage = 0;
    _controller = PageController();
    _allowNextQuestion = false;
    _controller.addListener(_onPageChanged);
    super.initState();
  }

  void _onPageChanged() {
    setState(() {
      _currentPage = (_controller.page ?? 0).round().toInt();
    });
  }

  @override
  void dispose() {
    _controller.removeListener(_onPageChanged);
    //_controller.dispose();
    super.dispose();
  }

  void _onAnswerCheckedChanged(bool checked) {
    setState(() {
      _allowNextQuestion = checked;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarProvider.getAppBarWithBackIcon(
        context,
        title: Text(
          'Self Assessment',
          style: ThemeStyles.appBarTitleStyle(context),
        ),
      ),
      body: SafeArea(
        child: _buildContent(context),
      ),
    );
  }

  Widget _buildContent(BuildContext context) {
    final contentValue = ref.watch(contentByIdProvider);
    return contentValue.when(
      data: (data) {
        final contentByID = data['content'] as ContentModel?;
        if (contentByID != null) {
          content =
              ContentQuery$QueryRoot$Content.fromJson(contentByID.toJson());

          result.assessmentID = content?.id;

          removeNullAndEmptyParams(content?.data as Map<String, dynamic>);

          contentSchema =
              ContentSchema.fromJson(content?.data as Map<String, dynamic>);
        }

        final contentParts = data['content_parts'] as List<ContentPartModel>?;
        if (content != null &&
            contentParts != null &&
            contentParts.isNotEmpty) {
          contentParts.sort((a, b) {
            return a.index < b.index ? 0 : 1;
          });

          _questions.clear();

          for (final e in contentParts) {
            _questions.add(ContentQuery$QueryRoot$Content.fromJson(
                e.contentPart.toJson()));
          }

          return Column(
            children: [
              _buildProgressDialog(context),
              Expanded(
                child: _buildQuestion(context),
              ),
              _buildButton(context),
            ],
          );
        }
        return const Message('Content not found!');
      },
      loading: () => const Center(child: CircularProgressIndicator()),
      error: (e, stackTrace) => Message(e.toString()),
    );
  }

  Widget _buildProgressDialog(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(15, 8, 15, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Question ${_currentPage + 1} of ${_questions.length}',
              style: ThemeStyles.cardContentTitle(context)?.copyWith(
                color: ThemeColors.blue.shade50,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5),
              child: CordicoLinearProgressBar(
                progress: (_currentPage + 1) / (_questions.length),
              ),
            )
          ],
        ));
  }

  Widget _buildQuestion(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 22),
      child: PageView(
        physics: const NeverScrollableScrollPhysics(),
        controller: _controller,
        children: _questions
            .map(
              (question) => AssessmentQuestionPage(
                question: question,
                onCheckedChanged: (answer) {
                  //print('question.id: ${question.id}score: ${answer.score}');

                  //print('answer.scaleScores: ${answer.scaleScores}');

                  // ignore: prefer_conditional_assignment
                  if (result.assessmentScoresResult == null) {
                    result.assessmentScoresResult = [];
                  }

                  assessmentQuestions[question.id] = true;

                  if (answer.scaleScores != null) {
                    answer.scaleScores?.forEach((element) {
                      final scores = result.assessmentScoresResult?.where(
                          (score) => element.scaleKey == score.scaleKey);

                      if (scores == null || scores.isEmpty) {
                        final singleScoreResult = AssessmentScoresResult()
                          ..scaleKey = element.scaleKey?.toLowerCase()
                          ..scores = {};

                        singleScoreResult.scores?.putIfAbsent(
                          question.id,
                          () => element.score?.toInt() ?? 0,
                        );

                        result.assessmentScoresResult?.add(singleScoreResult);
                      } else {
                        final scoreToEdit = scores.first;
                        scoreToEdit.scores?[question.id] =
                            element.score?.toInt() ?? 0;
                      }
                    });
                  } /*else {
                    AssessmentScoresResult scoreResult;

                    if (result.assessmentScoresResult!.isEmpty) {
                      final singleScoreResult = AssessmentScoresResult();

                      scoreResult = singleScoreResult;
                      result.assessmentScoresResult?.add(scoreResult);
                    } else {
                      scoreResult = result.assessmentScoresResult!.first;
                    }

                    scoreResult
                      ..scaleKey = 'single_outcome'
                      ..scores = {};

                    scoreResult.scores![question.id] = answer.getScore();
                  }*/

                  // result.assessmentScoresResult?.forEach((e) {
                  //   print('element_ ${e.scaleKey}: ${e.getFinalScore}');
                  // });

                  setState(() {
                    _allowNextQuestion =
                        assessmentQuestions[question.id] == true;
                  });
                },
              ),
            )
            .toList(),
      ),
    );
  }

  Widget _buildButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 25, left: 27, right: 27),
      child: CordicoElevatedButton(
        onPressed: _onNextQuestionClicked,
        enabled: _allowNextQuestion,
        height: 38,
        text:
            _currentPage == _questions.length - 1 ? 'Finish' : 'Next Question',
        textStyle: ThemeStyles.elevatedButtonTextStyle(context)?.copyWith(
          color: ThemeColors.blue.shade50,
          fontSize: 14,
        ),
      ),
    );
  }

  void _onNextQuestionClicked() {
    if (_currentPage == _questions.length - 1) {
      //print('object');
      //context.vRouter.historyGo(-2);
      totalScore = 0;

      result.assessmentScoresResult?.forEach((element) {
        final outcomes = contentSchema?.outcomes
            ?.where((outcome) => element.scaleKey == outcome.scoreKey);

        if (outcomes != null && outcomes.isNotEmpty) {
          final outcome = outcomes.first;
          element.outcome = outcome;
        }
      });

      global.assessmentsCompleted.removeWhere(
          (element) => element.assessmentID == result.assessmentID);

      global.assessmentsCompleted.add(result);

      context.vRouter.to('assessment_result/${result.assessmentID}');
    } else {
      _onAnswerCheckedChanged(false);
      _controller.nextPage(
          duration: const Duration(milliseconds: 250), curve: Curves.linear);
    }
  }
}
