import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class CordicoLinkableLabel extends StatefulWidget {
  const CordicoLinkableLabel(
      {Key? key,
      required this.text,
      required this.links,
      required this.onTap,
      this.style,
      this.linkStyle})
      : super(key: key);

  final String text;
  final List<String> links;
  final Function(String) onTap;
  final TextStyle? style;
  final TextStyle? linkStyle;

  @override
  State createState() => _CordicoLinkableLabelState();
}

class _CordicoLinkableLabelState extends State<CordicoLinkableLabel> {
  late List<TapGestureRecognizer> recognizers;

  @override
  void initState() {
    recognizers = widget.links.map((link) {
      final recognizer = TapGestureRecognizer()
        ..onTap = () {
          widget.onTap(link);
        };

      return recognizer;
    }).toList();

    super.initState();
  }

  @override
  void dispose() {
    for (final recognizer in recognizers) {
      recognizer.dispose();
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final spans = <TextSpan>[];
    var previousLinkIndex = 0;
    for (var i = 0; i < widget.links.length; i++) {
      final link = widget.links[i];
      final linkIndex = widget.text.indexOf(link);
      if (linkIndex > previousLinkIndex) {
        final textBeforeLink =
            widget.text.substring(previousLinkIndex, linkIndex);
        spans.add(TextSpan(
            text: textBeforeLink,
            style: widget.style ?? ThemeStyles.textFieldTextStyle(context)));
      }

      spans.add(TextSpan(
        text: link,
        style: widget.linkStyle ??
            ThemeStyles.textFieldTextStyle(context)?.copyWith(
              color: ThemeColors.blue.shade500,
              decoration: TextDecoration.underline,
            ),
        recognizer: recognizers[i],
      ));

      previousLinkIndex += linkIndex + link.length;
    }

    if (previousLinkIndex < widget.text.length) {
      final textAfterLink = widget.text.substring(previousLinkIndex);
      spans.add(TextSpan(
          text: textAfterLink,
          style: widget.style ?? ThemeStyles.textFieldTextStyle(context)));
    }

    return RichText(
      text: TextSpan(
        children: spans,
      ),
    );
  }
}
