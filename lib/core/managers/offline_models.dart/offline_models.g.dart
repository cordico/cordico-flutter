// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'offline_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrganizationContactOffline _$OrganizationContactOfflineFromJson(
    Map<String, dynamic> json) {
  return OrganizationContactOffline()
    ..data = (json['data'] as List<dynamic>?)
        ?.map((e) =>
            OrganizationContactById$QueryRoot$OrganizationContactsByPk.fromJson(
                e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$OrganizationContactOfflineToJson(
        OrganizationContactOffline instance) =>
    <String, dynamic>{
      'data': instance.data?.map((e) => e.toJson()).toList(),
    };

ContentOffline _$ContentOfflineFromJson(Map<String, dynamic> json) {
  return ContentOffline()
    ..data = (json['data'] as List<dynamic>?)
        ?.map((e) => ContentById$QueryRoot$ContentByPk.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$ContentOfflineToJson(ContentOffline instance) =>
    <String, dynamic>{
      'data': instance.data?.map((e) => e.toJson()).toList(),
    };

CategoriesOffline _$CategoriesOfflineFromJson(Map<String, dynamic> json) {
  return CategoriesOffline()
    ..data = (json['data'] as List<dynamic>?)
        ?.map((e) => CategoryById$QueryRoot$CategoryByPk.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$CategoriesOfflineToJson(CategoriesOffline instance) =>
    <String, dynamic>{
      'data': instance.data?.map((e) => e.toJson()).toList(),
    };

CollectionsOffline _$CollectionsOfflineFromJson(Map<String, dynamic> json) {
  return CollectionsOffline()
    ..data = (json['data'] as List<dynamic>?)
        ?.map((e) => CollectionById$QueryRoot$CollectionByPk.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$CollectionsOfflineToJson(CollectionsOffline instance) =>
    <String, dynamic>{
      'data': instance.data?.map((e) => e.toJson()).toList(),
    };
