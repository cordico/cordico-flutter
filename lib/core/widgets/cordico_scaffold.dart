import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/images.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/app_bar_provider.dart';
import 'package:cordico_mobile/core/widgets/bottom_navigation_bar/cordico_bottom_app_bar.dart';
import 'package:cordico_mobile/core/widgets/bottom_navigation_bar/cordico_bottom_app_bar_item.dart';
import 'package:cordico_mobile/features/connect/views/connect_page.dart';
import 'package:cordico_mobile/features/department/views/department_page.dart';
import 'package:cordico_mobile/features/discover/logic/content_types_provider.dart';
import 'package:cordico_mobile/features/discover/views/discover_page.dart';
import 'package:cordico_mobile/features/hotlines/views/hotlines_page.dart';
import 'package:cordico_mobile/features/toolkit/logic/categories_state_notifier.dart';
import 'package:cordico_mobile/features/toolkit/logic/organization_provider.dart';
import 'package:cordico_mobile/features/toolkit/views/toolkit_page.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_segment/flutter_segment.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final currentIndexProvider = StateProvider((ref) => 0);

class CordicoScaffold extends ConsumerStatefulWidget {
  const CordicoScaffold({Key? key}) : super(key: key);

  static const _toolkitPageIndex = 0;
  static const _discoverPageIndex = 1;
  static const _getHelpPageIndex = 2;
  static const _contactsPageIndex = 3;
  //static const _departmentPageIndex = 4;

  @override
  ConsumerState<CordicoScaffold> createState() => _CordicoScaffoldState();
}

class _CordicoScaffoldState extends ConsumerState<CordicoScaffold>
    with AutomaticKeepAliveClientMixin {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      ref.read(categoriesNotifierProvider.notifier).getCategories();
      ref.watch(organizationNotifierProvider).getContent();
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final currentIndex = ref.watch<int>(currentIndexProvider);
    final contentTypes = ref.watch(contentTypesStateNotifierProvider);

    return Builder(
      builder: (context) {
        global.contentTypeIds = contentTypes.filterContentTypesIds ?? [];
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1),
          child: Scaffold(
            appBar: _buildAppBar(context, currentIndex),
            body: _buildContent(context, currentIndex, ref: ref),
            bottomNavigationBar: _buildBottomNavigationBar(context, ref),
          ),
        );
      },
    );
  }

  Widget _buildContent(BuildContext context, int currentIndex,
      {required WidgetRef ref}) {
    return IndexedStack(
      index: currentIndex,
      children: const [
        ToolkitPage(),
        DiscoverPage(),
        HotlinesPage(),
        ConnectPage(),
        DepartmentPage(),
      ],
    );
  }

  PreferredSizeWidget? _buildAppBar(BuildContext context, int currentIndex) {
    switch (currentIndex) {
      case CordicoScaffold._toolkitPageIndex:
        return null;

      case CordicoScaffold._discoverPageIndex:
        return _buildAppBarForDiscover(context);

      case CordicoScaffold._getHelpPageIndex:
        return _buildAppBarForHotlines(context);

      case CordicoScaffold._contactsPageIndex:
        return _buildAppBarForConnect(context);

      default:
        return null;
    }
  }

  PreferredSizeWidget _buildAppBarForDiscover(BuildContext context) {
    return AppBarProvider.getAppBarWithTitle(context,
        title: Text('Discover', style: ThemeStyles.appBarTitleStyle(context)));
  }

  PreferredSizeWidget _buildAppBarForHotlines(BuildContext context) {
    return AppBarProvider.getAppBarWithTitle(
      context,
      title: Row(
        children: [
          Container(
            height: 48,
            width: 34,
            margin: const EdgeInsets.only(right: 2),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Image.asset(
                Images.icHeadset,
                fit: BoxFit.contain,
                height: 24,
                width: 24,
                color: const Color(0xFFCF1010),
              ),
            ),
          ),
          Text('Crisis Hotline', style: ThemeStyles.appBarTitleStyle(context)),
        ],
      ),
    );
  }

  PreferredSizeWidget _buildAppBarForConnect(BuildContext context) {
    return AppBarProvider.getAppBarWithTitle(
      context,
      title: Row(
        children: [
          Container(
            height: 48,
            width: 34,
            margin: const EdgeInsets.only(right: 2),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Image.asset(
                Images.icSupportDirectoryIcon,
                fit: BoxFit.contain,
                height: 25,
                width: 25,
              ),
            ),
          ),
          Text('Support Directory',
              style: ThemeStyles.appBarTitleStyle(context)),
        ],
      ),
    );
  }

  Widget _buildBottomNavigationBar(BuildContext context, WidgetRef ref) {
    return CordicoBottomAppBar(
        selectedIndex: ref.watch(currentIndexProvider),
        items: [
          CordicoBottomAppBarItem(
            icon: Image.asset('images/ic_home.png'),
            text: 'Home',
          ),
          CordicoBottomAppBarItem(
            icon: Image.asset('images/ic_search.png'),
            text: 'Discover',
          ),
          CordicoBottomAppBarItem(
            icon: Image.asset(Images.getHelpTabIcon),
            text: 'GET HELP',
            textColor: ThemeColors.red.shade200,
          ),
          CordicoBottomAppBarItem(
            icon: Image.asset(Images.directoryTabIcon),
            text: 'Directory',
          ),
          CordicoBottomAppBarItem(
            icon: Image.asset(Images.departmentTabIcon),
            text: 'Department',
          ),
        ],
        onTabSelected: (int index) {
          //if (index != 2) {
          ref.read(currentIndexProvider.state).state = index;
          /*} else {
            showGetHelpModal(context, ref);
          }*/
        });
  }

  Future<void> showGetHelpModal(BuildContext context, WidgetRef ref) {
    return showModalBottomSheet(
      context: context,
      useRootNavigator: true,
      builder: (ctx) {
        return StatefulBuilder(builder: (context, stateSet) {
          return Padding(
            padding: EdgeInsets.only(
              top: 12,
              bottom: MediaQuery.of(ctx).viewPadding.bottom + 12,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                buildModalHeader(context),
                buildTile(context, color: const Color(0xFFCF1010), onTap: () {
                  ref.read(currentIndexProvider.state).state = 2;
                  Navigator.maybeOf(context)?.maybePop();
                }, title: 'Crisis Hotline', image: Images.icHeadset),
                buildTile(context, color: const Color(0xFF3459B8), onTap: () {
                  ref.read(currentIndexProvider.state).state = 3;
                  Navigator.maybeOf(context)?.maybePop();
                }, title: 'Support Network', image: Images.icPersonPin),
                buildTile(context, color: const Color(0xFFD2780E), onTap: () {
                  ref.read(currentIndexProvider.state).state = 1;
                  Navigator.maybeOf(context)?.maybePop();
                }, title: 'Guides & Assessment', image: Images.icMenuBook),
              ],
            ),
          );
        });
      },
      elevation: 10,
      barrierColor: Colors.white10,
      backgroundColor: const Color(0xFF17171A),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(18),
          topRight: Radius.circular(18),
        ),
      ),
    );
  }

  Widget buildModalHeader(BuildContext context) {
    return InkWell(
        onTap: () => Navigator.of(context).pop(),
        child: Container(
          padding: const EdgeInsets.only(bottom: 20, left: 20, right: 20),
          color: const Color(0xFF17171A),
          child: Row(
            children: [
              Expanded(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    FittedBox(
                      fit: BoxFit.fitWidth,
                      child: Text(
                        'Get Help',
                        style: ThemeStyles.sectionTitle(context)
                            ?.copyWith(fontSize: 27),
                      ),
                    ),
                  ],
                ),
              ),
              Container(width: 12),
              Container(
                padding: const EdgeInsets.all(8),
                child: Image.asset(
                  Images.icChevronDown,
                  fit: BoxFit.contain,
                  width: 30,
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ));
  }

  Widget buildTile(
    BuildContext context, {
    required Color color,
    required Function()? onTap,
    required String title,
    required String image,
  }) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 64,
        margin: const EdgeInsets.only(bottom: 17, left: 20, right: 20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Row(
          children: [
            Container(
              height: 48,
              width: 48,
              margin: const EdgeInsets.only(left: 8, right: 20),
              decoration: BoxDecoration(
                color: color.withOpacity(0.3),
                shape: BoxShape.circle,
              ),
              child: Align(
                child: Image.asset(
                  image,
                  fit: BoxFit.contain,
                  height: 24,
                  width: 24,
                  color: color,
                ),
              ),
            ),
            Text(
              title,
              style: ThemeStyles.sectionTitle(context)
                  ?.copyWith(fontSize: 17)
                  .copyWith(color: Colors.black),
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
