import 'package:core/core.dart';
import 'package:core/graphql/login/login_mutation.dart';
import 'package:core/repositories/graphql_repository.dart';
import 'package:core/utils/failure.dart';
import 'package:core/utils/map_extension.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

abstract class ILoginRepository {
  Future<Either<Failure, LoginMutationModel?>> loginMutation(
      final String identifier, final String password);
}

class LoginRepository extends GraphQLRepository implements ILoginRepository {
  LoginRepository({required String url, required String userUrl})
      : super(url: url, userUrl: userUrl);

  @override
  Future<Either<Failure, LoginMutationModel?>> loginMutation(
      final String identifier, final String password) async {
    try {
      final query = LoginMutationMutation(
          variables: LoginMutationArguments(
              input: AuthInput(identifier: identifier, password: password)));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await userGraphQLClient.query<LoginMutationModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = LoginMutation$MutationRoot.fromJson(response.data!);
        return Right(root.login);
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }
}
