import 'package:json_annotation/json_annotation.dart';
part 'organization_schema.g.dart';

@JsonSerializable(explicitToJson: true)
class OrganizationSchema extends JsonSerializable {
  OrganizationSchema();

  factory OrganizationSchema.fromJson(Map<String, dynamic> json) =>
      _$OrganizationSchemaFromJson(json);

  String? zip;
  String? city;
  String? state;
  String? address;
  num? latitude;
  num? longitude;

  String? name;
  String? description;
  String? phone;

  @JsonKey(name: 'image_url')
  String? imageUrl;

  @JsonKey(name: 'long_name')
  String? longName;

  @JsonKey(name: 'cover_image_url')
  String? coverImageUrl;

  @override
  Map<String, dynamic> toJson() => _$OrganizationSchemaToJson(this);

  static OrganizationSchema fromJSON(Map<String, dynamic> json) {
    return OrganizationSchema.fromJson(json);
  }
}
