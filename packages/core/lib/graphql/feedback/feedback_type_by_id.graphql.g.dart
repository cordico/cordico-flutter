// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'feedback_type_by_id.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedbackTypeById$QueryRoot$FeedbackTypeByPk
    _$FeedbackTypeById$QueryRoot$FeedbackTypeByPkFromJson(
        Map<String, dynamic> json) {
  return FeedbackTypeById$QueryRoot$FeedbackTypeByPk()
    ..id = json['id'] as String
    ..key = json['key'] as String
    ..name = json['name'] as String
    ..schema = json['schema'];
}

Map<String, dynamic> _$FeedbackTypeById$QueryRoot$FeedbackTypeByPkToJson(
        FeedbackTypeById$QueryRoot$FeedbackTypeByPk instance) =>
    <String, dynamic>{
      'id': instance.id,
      'key': instance.key,
      'name': instance.name,
      'schema': instance.schema,
    };

FeedbackTypeById$QueryRoot _$FeedbackTypeById$QueryRootFromJson(
    Map<String, dynamic> json) {
  return FeedbackTypeById$QueryRoot()
    ..feedbackTypeByPk = json['feedback_type_by_pk'] == null
        ? null
        : FeedbackTypeById$QueryRoot$FeedbackTypeByPk.fromJson(
            json['feedback_type_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$FeedbackTypeById$QueryRootToJson(
        FeedbackTypeById$QueryRoot instance) =>
    <String, dynamic>{
      'feedback_type_by_pk': instance.feedbackTypeByPk?.toJson(),
    };

FeedbackTypeByIdArguments _$FeedbackTypeByIdArgumentsFromJson(
    Map<String, dynamic> json) {
  return FeedbackTypeByIdArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$FeedbackTypeByIdArgumentsToJson(
        FeedbackTypeByIdArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
