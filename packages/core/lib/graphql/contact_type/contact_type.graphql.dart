// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'contact_type.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class ContactTypeQuery$QueryRoot$ContactType extends JsonSerializable
    with EquatableMixin {
  ContactTypeQuery$QueryRoot$ContactType();

  factory ContactTypeQuery$QueryRoot$ContactType.fromJson(
          Map<String, dynamic> json) =>
      _$ContactTypeQuery$QueryRoot$ContactTypeFromJson(json);

  late String name;

  late String id;

  @JsonKey(name: 'display_name')
  late String displayName;

  dynamic? schema;

  @override
  List<Object?> get props => [name, id, displayName, schema];
  @override
  Map<String, dynamic> toJson() =>
      _$ContactTypeQuery$QueryRoot$ContactTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContactTypeQuery$QueryRoot extends JsonSerializable with EquatableMixin {
  ContactTypeQuery$QueryRoot();

  factory ContactTypeQuery$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$ContactTypeQuery$QueryRootFromJson(json);

  @JsonKey(name: 'contact_type')
  late List<ContactTypeQuery$QueryRoot$ContactType> contactType;

  @override
  List<Object?> get props => [contactType];
  @override
  Map<String, dynamic> toJson() => _$ContactTypeQuery$QueryRootToJson(this);
}

final CONTACT_TYPE_QUERY_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'ContactTypeQuery'),
      variableDefinitions: [],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'contact_type'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'display_name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class ContactTypeQueryQuery
    extends GraphQLQuery<ContactTypeQuery$QueryRoot, JsonSerializable> {
  ContactTypeQueryQuery();

  @override
  final DocumentNode document = CONTACT_TYPE_QUERY_QUERY_DOCUMENT;

  @override
  final String operationName = 'ContactTypeQuery';

  @override
  List<Object?> get props => [document, operationName];
  @override
  ContactTypeQuery$QueryRoot parse(Map<String, dynamic> json) =>
      ContactTypeQuery$QueryRoot.fromJson(json);
}
