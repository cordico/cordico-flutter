// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'collection_type_query.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CollectionTypeQuery$QueryRoot$CollectionType extends JsonSerializable
    with EquatableMixin {
  CollectionTypeQuery$QueryRoot$CollectionType();

  factory CollectionTypeQuery$QueryRoot$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionTypeQuery$QueryRoot$CollectionTypeFromJson(json);

  @JsonKey(name: 'updater_id')
  String? updaterId;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  dynamic? schema;

  late String name;

  late String key;

  late String id;

  String? description;

  @JsonKey(name: 'creator_id')
  String? creatorId;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @override
  List<Object?> get props => [
        updaterId,
        updatedAt,
        schema,
        name,
        key,
        id,
        description,
        creatorId,
        createdAt
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionTypeQuery$QueryRoot$CollectionTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CollectionTypeQuery$QueryRoot extends JsonSerializable
    with EquatableMixin {
  CollectionTypeQuery$QueryRoot();

  factory CollectionTypeQuery$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$CollectionTypeQuery$QueryRootFromJson(json);

  @JsonKey(name: 'collection_type')
  late List<CollectionTypeQuery$QueryRoot$CollectionType> collectionType;

  @override
  List<Object?> get props => [collectionType];
  @override
  Map<String, dynamic> toJson() => _$CollectionTypeQuery$QueryRootToJson(this);
}

final COLLECTION_TYPE_QUERY_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'CollectionTypeQuery'),
      variableDefinitions: [],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'collection_type'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'updater_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'description'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'creator_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class CollectionTypeQueryQuery
    extends GraphQLQuery<CollectionTypeQuery$QueryRoot, JsonSerializable> {
  CollectionTypeQueryQuery();

  @override
  final DocumentNode document = COLLECTION_TYPE_QUERY_QUERY_DOCUMENT;

  @override
  final String operationName = 'CollectionTypeQuery';

  @override
  List<Object?> get props => [document, operationName];
  @override
  CollectionTypeQuery$QueryRoot parse(Map<String, dynamic> json) =>
      CollectionTypeQuery$QueryRoot.fromJson(json);
}
