import 'package:cordico_mobile/core/providers/global_providers.dart';
import 'package:cordico_mobile/features/connect/logic/organization_contacts_state.dart';
import 'package:core/core.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class OrganizationContactsStateNotifier
    extends StateNotifier<OrganizationContactsState> {
  OrganizationContactsStateNotifier({
    required IOrganizationRepository repository,
    OrganizationContactsState? initialState,
  })  : _repository = repository,
        super(initialState ?? const OrganizationContactsState.initial());

  final IOrganizationRepository _repository;

  void reset() => state = const OrganizationContactsState.initial();

  Future<List<OrganizationContactModel>?> getContacts() async {
    state = const OrganizationContactsState.loading();

    final result1 = await _repository.getOrganizationContactsByOrganization(
        environmentConfig.organizationId!);
    List<OrganizationContactModel>? result;
    result1.fold(
      (error) {
        state = OrganizationContactsState.error(error.toString());
      },
      (content) {
        state = OrganizationContactsState.data(content: content);
        result = content;
      },
    );

    return result;
  }
}
