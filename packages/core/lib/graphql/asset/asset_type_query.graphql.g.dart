// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'asset_type_query.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AssetTypeQuery$QueryRoot$AssetType _$AssetTypeQuery$QueryRoot$AssetTypeFromJson(
    Map<String, dynamic> json) {
  return AssetTypeQuery$QueryRoot$AssetType()
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..creatorId = json['creator_id'] as String
    ..description = json['description'] as String?
    ..id = json['id'] as String
    ..key = json['key'] as String
    ..mimeTypes = json['mime_types'] as String
    ..name = json['name'] as String
    ..schema = json['schema']
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..updaterId = json['updater_id'] as String?;
}

Map<String, dynamic> _$AssetTypeQuery$QueryRoot$AssetTypeToJson(
        AssetTypeQuery$QueryRoot$AssetType instance) =>
    <String, dynamic>{
      'created_at': instance.createdAt.toIso8601String(),
      'creator_id': instance.creatorId,
      'description': instance.description,
      'id': instance.id,
      'key': instance.key,
      'mime_types': instance.mimeTypes,
      'name': instance.name,
      'schema': instance.schema,
      'updated_at': instance.updatedAt?.toIso8601String(),
      'updater_id': instance.updaterId,
    };

AssetTypeQuery$QueryRoot _$AssetTypeQuery$QueryRootFromJson(
    Map<String, dynamic> json) {
  return AssetTypeQuery$QueryRoot()
    ..assetType = (json['asset_type'] as List<dynamic>)
        .map((e) => AssetTypeQuery$QueryRoot$AssetType.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$AssetTypeQuery$QueryRootToJson(
        AssetTypeQuery$QueryRoot instance) =>
    <String, dynamic>{
      'asset_type': instance.assetType.map((e) => e.toJson()).toList(),
    };
