// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'content_query.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContentQuery$QueryRoot$Content$ContentType
    _$ContentQuery$QueryRoot$Content$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic> _$ContentQuery$QueryRoot$Content$ContentTypeToJson(
        ContentQuery$QueryRoot$Content$ContentType instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'is_shared': instance.isShared,
      'description': instance.description,
    };

ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentType
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentTypeToJson(
            ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'is_shared': instance.isShared,
          'description': instance.description,
        };

ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ChildContent
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ChildContentFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ChildContent()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ChildContentToJson(
            ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ChildContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ParentContent
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ParentContentFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic>
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ParentContentToJson(
            ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ParentContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'index': instance.index,
          'part_id': instance.partId,
        };

ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$CategoryToJson(
            ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentCategories
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentCategories()
    ..category =
        ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category
            .fromJson(json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentCategoriesToJson(
            ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentCategories
                instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentState
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentStateFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentStateToJson(
            ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentState
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
        };

ContentQuery$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
            ContentQuery$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

ContentQuery$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPart$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : ContentQuery$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPart$Collections$CollectionToJson(
            ContentQuery$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

ContentQuery$QueryRoot$Content$ChildContent$ContentPart$Collections
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPart$CollectionsFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$ChildContent$ContentPart$Collections()
    ..collection =
        ContentQuery$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection
            .fromJson(json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPart$CollectionsToJson(
            ContentQuery$QueryRoot$Content$ChildContent$ContentPart$Collections
                instance) =>
        <String, dynamic>{
          'collection': instance.collection.toJson(),
        };

ContentQuery$QueryRoot$Content$ChildContent$ContentPart
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPartFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$ChildContent$ContentPart()
    ..contentType =
        ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentType
            .fromJson(json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) =>
            ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ChildContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) =>
            ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ParentContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentCategories
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentState =
        ContentQuery$QueryRoot$Content$ChildContent$ContentPart$ContentState
            .fromJson(json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) =>
            ContentQuery$QueryRoot$Content$ChildContent$ContentPart$Collections
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic>
    _$ContentQuery$QueryRoot$Content$ChildContent$ContentPartToJson(
            ContentQuery$QueryRoot$Content$ChildContent$ContentPart instance) =>
        <String, dynamic>{
          'content_type': instance.contentType.toJson(),
          'updated_at': instance.updatedAt?.toIso8601String(),
          'name': instance.name,
          'is_shared': instance.isShared,
          'id': instance.id,
          'data': instance.data,
          'created_at': instance.createdAt.toIso8601String(),
          'child_content':
              instance.childContent.map((e) => e.toJson()).toList(),
          'parent_content':
              instance.parentContent.map((e) => e.toJson()).toList(),
          'content_categories':
              instance.contentCategories.map((e) => e.toJson()).toList(),
          'content_state': instance.contentState.toJson(),
          'collections': instance.collections.map((e) => e.toJson()).toList(),
        };

ContentQuery$QueryRoot$Content$ChildContent
    _$ContentQuery$QueryRoot$Content$ChildContentFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$ChildContent()
    ..contentPart =
        ContentQuery$QueryRoot$Content$ChildContent$ContentPart.fromJson(
            json['content_part'] as Map<String, dynamic>)
    ..id = json['id'] as String
    ..index = json['index'] as int;
}

Map<String, dynamic> _$ContentQuery$QueryRoot$Content$ChildContentToJson(
        ContentQuery$QueryRoot$Content$ChildContent instance) =>
    <String, dynamic>{
      'content_part': instance.contentPart.toJson(),
      'id': instance.id,
      'index': instance.index,
    };

ContentQuery$QueryRoot$Content$ParentContent
    _$ContentQuery$QueryRoot$Content$ParentContentFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic> _$ContentQuery$QueryRoot$Content$ParentContentToJson(
        ContentQuery$QueryRoot$Content$ParentContent instance) =>
    <String, dynamic>{
      'id': instance.id,
      'index': instance.index,
      'part_id': instance.partId,
    };

ContentQuery$QueryRoot$Content$ContentCategories$Category
    _$ContentQuery$QueryRoot$Content$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String,
    dynamic> _$ContentQuery$QueryRoot$Content$ContentCategories$CategoryToJson(
        ContentQuery$QueryRoot$Content$ContentCategories$Category instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'description': instance.description,
    };

ContentQuery$QueryRoot$Content$ContentCategories
    _$ContentQuery$QueryRoot$Content$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$ContentCategories()
    ..category =
        ContentQuery$QueryRoot$Content$ContentCategories$Category.fromJson(
            json['category'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ContentQuery$QueryRoot$Content$ContentCategoriesToJson(
        ContentQuery$QueryRoot$Content$ContentCategories instance) =>
    <String, dynamic>{
      'category': instance.category.toJson(),
    };

ContentQuery$QueryRoot$Content$ContentState
    _$ContentQuery$QueryRoot$Content$ContentStateFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic> _$ContentQuery$QueryRoot$Content$ContentStateToJson(
        ContentQuery$QueryRoot$Content$ContentState instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };

ContentQuery$QueryRoot$Content$Collections$Collection$CollectionType
    _$ContentQuery$QueryRoot$Content$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$ContentQuery$QueryRoot$Content$Collections$Collection$CollectionTypeToJson(
            ContentQuery$QueryRoot$Content$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

ContentQuery$QueryRoot$Content$Collections$Collection
    _$ContentQuery$QueryRoot$Content$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : ContentQuery$QueryRoot$Content$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentQuery$QueryRoot$Content$Collections$CollectionToJson(
            ContentQuery$QueryRoot$Content$Collections$Collection instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

ContentQuery$QueryRoot$Content$Collections
    _$ContentQuery$QueryRoot$Content$CollectionsFromJson(
        Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content$Collections()
    ..collection =
        ContentQuery$QueryRoot$Content$Collections$Collection.fromJson(
            json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ContentQuery$QueryRoot$Content$CollectionsToJson(
        ContentQuery$QueryRoot$Content$Collections instance) =>
    <String, dynamic>{
      'collection': instance.collection.toJson(),
    };

ContentQuery$QueryRoot$Content _$ContentQuery$QueryRoot$ContentFromJson(
    Map<String, dynamic> json) {
  return ContentQuery$QueryRoot$Content()
    ..contentType = ContentQuery$QueryRoot$Content$ContentType.fromJson(
        json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) => ContentQuery$QueryRoot$Content$ChildContent.fromJson(
            e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) => ContentQuery$QueryRoot$Content$ParentContent.fromJson(
            e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) => ContentQuery$QueryRoot$Content$ContentCategories.fromJson(
            e as Map<String, dynamic>))
        .toList()
    ..contentState = ContentQuery$QueryRoot$Content$ContentState.fromJson(
        json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) => ContentQuery$QueryRoot$Content$Collections.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$ContentQuery$QueryRoot$ContentToJson(
        ContentQuery$QueryRoot$Content instance) =>
    <String, dynamic>{
      'content_type': instance.contentType.toJson(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'name': instance.name,
      'is_shared': instance.isShared,
      'id': instance.id,
      'data': instance.data,
      'created_at': instance.createdAt.toIso8601String(),
      'child_content': instance.childContent.map((e) => e.toJson()).toList(),
      'parent_content': instance.parentContent.map((e) => e.toJson()).toList(),
      'content_categories':
          instance.contentCategories.map((e) => e.toJson()).toList(),
      'content_state': instance.contentState.toJson(),
      'collections': instance.collections.map((e) => e.toJson()).toList(),
    };

ContentQuery$QueryRoot _$ContentQuery$QueryRootFromJson(
    Map<String, dynamic> json) {
  return ContentQuery$QueryRoot()
    ..content = (json['content'] as List<dynamic>)
        .map((e) =>
            ContentQuery$QueryRoot$Content.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$ContentQuery$QueryRootToJson(
        ContentQuery$QueryRoot instance) =>
    <String, dynamic>{
      'content': instance.content.map((e) => e.toJson()).toList(),
    };

ContentQueryArguments _$ContentQueryArgumentsFromJson(
    Map<String, dynamic> json) {
  return ContentQueryArguments(
    contentStateNames: (json['contentStateNames'] as List<dynamic>?)
        ?.map((e) => e as String?)
        .toList(),
    offset: json['offset'] as int?,
    limit: json['limit'] as int?,
  );
}

Map<String, dynamic> _$ContentQueryArgumentsToJson(
        ContentQueryArguments instance) =>
    <String, dynamic>{
      'contentStateNames': instance.contentStateNames,
      'offset': instance.offset,
      'limit': instance.limit,
    };
