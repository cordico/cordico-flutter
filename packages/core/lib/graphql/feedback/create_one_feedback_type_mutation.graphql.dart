// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'create_one_feedback_type_mutation.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CreateOneFeedbackTypeMutation$MutationRoot$InsertFeedbackTypeOne
    extends JsonSerializable with EquatableMixin {
  CreateOneFeedbackTypeMutation$MutationRoot$InsertFeedbackTypeOne();

  factory CreateOneFeedbackTypeMutation$MutationRoot$InsertFeedbackTypeOne.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneFeedbackTypeMutation$MutationRoot$InsertFeedbackTypeOneFromJson(
          json);

  late String id;

  late String key;

  late String name;

  dynamic? schema;

  @override
  List<Object?> get props => [id, key, name, schema];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneFeedbackTypeMutation$MutationRoot$InsertFeedbackTypeOneToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneFeedbackTypeMutation$MutationRoot extends JsonSerializable
    with EquatableMixin {
  CreateOneFeedbackTypeMutation$MutationRoot();

  factory CreateOneFeedbackTypeMutation$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneFeedbackTypeMutation$MutationRootFromJson(json);

  @JsonKey(name: 'insert_feedback_type_one')
  CreateOneFeedbackTypeMutation$MutationRoot$InsertFeedbackTypeOne?
      insertFeedbackTypeOne;

  @override
  List<Object?> get props => [insertFeedbackTypeOne];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneFeedbackTypeMutation$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneFeedbackTypeMutationArguments extends JsonSerializable
    with EquatableMixin {
  CreateOneFeedbackTypeMutationArguments(
      {required this.key, required this.name, required this.schema});

  @override
  factory CreateOneFeedbackTypeMutationArguments.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneFeedbackTypeMutationArgumentsFromJson(json);

  late String key;

  late String name;

  late dynamic schema;

  @override
  List<Object?> get props => [key, name, schema];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneFeedbackTypeMutationArgumentsToJson(this);
}

final CREATE_ONE_FEEDBACK_TYPE_MUTATION_MUTATION_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'CreateOneFeedbackTypeMutation'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'key')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'name')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'schema')),
            type:
                NamedTypeNode(name: NameNode(value: 'jsonb'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'insert_feedback_type_one'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'object'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'key'),
                        value: VariableNode(name: NameNode(value: 'key'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'name'),
                        value: VariableNode(name: NameNode(value: 'name'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'schema'),
                        value: VariableNode(name: NameNode(value: 'schema')))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class CreateOneFeedbackTypeMutationMutation extends GraphQLQuery<
    CreateOneFeedbackTypeMutation$MutationRoot,
    CreateOneFeedbackTypeMutationArguments> {
  CreateOneFeedbackTypeMutationMutation({required this.variables});

  @override
  final DocumentNode document =
      CREATE_ONE_FEEDBACK_TYPE_MUTATION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'CreateOneFeedbackTypeMutation';

  @override
  final CreateOneFeedbackTypeMutationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  CreateOneFeedbackTypeMutation$MutationRoot parse(Map<String, dynamic> json) =>
      CreateOneFeedbackTypeMutation$MutationRoot.fromJson(json);
}
