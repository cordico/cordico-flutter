// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'create_one_content_category.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CreateOneContentCategoryMutation$MutationRoot$CreateOneContentCategory
    extends JsonSerializable with EquatableMixin {
  CreateOneContentCategoryMutation$MutationRoot$CreateOneContentCategory();

  factory CreateOneContentCategoryMutation$MutationRoot$CreateOneContentCategory.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneContentCategoryMutation$MutationRoot$CreateOneContentCategoryFromJson(
          json);

  late String id;

  late String contentId;

  late String categoryId;

  @override
  List<Object?> get props => [id, contentId, categoryId];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneContentCategoryMutation$MutationRoot$CreateOneContentCategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneContentCategoryMutation$MutationRoot extends JsonSerializable
    with EquatableMixin {
  CreateOneContentCategoryMutation$MutationRoot();

  factory CreateOneContentCategoryMutation$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneContentCategoryMutation$MutationRootFromJson(json);

  late CreateOneContentCategoryMutation$MutationRoot$CreateOneContentCategory
      createOneContentCategory;

  @override
  List<Object?> get props => [createOneContentCategory];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneContentCategoryMutation$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneContentCategoryMutationArguments extends JsonSerializable
    with EquatableMixin {
  CreateOneContentCategoryMutationArguments(
      {required this.categoryId, required this.contentId});

  @override
  factory CreateOneContentCategoryMutationArguments.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneContentCategoryMutationArgumentsFromJson(json);

  late String categoryId;

  late String contentId;

  @override
  List<Object?> get props => [categoryId, contentId];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneContentCategoryMutationArgumentsToJson(this);
}

final CREATE_ONE_CONTENT_CATEGORY_MUTATION_MUTATION_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'CreateOneContentCategoryMutation'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'categoryId')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentId')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'createOneContentCategory'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'data'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'category'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'connect'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'id'),
                                    value: VariableNode(
                                        name: NameNode(value: 'categoryId')))
                              ]))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'content'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'connect'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'id'),
                                    value: VariableNode(
                                        name: NameNode(value: 'contentId')))
                              ]))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'contentId'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'categoryId'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class CreateOneContentCategoryMutationMutation extends GraphQLQuery<
    CreateOneContentCategoryMutation$MutationRoot,
    CreateOneContentCategoryMutationArguments> {
  CreateOneContentCategoryMutationMutation({required this.variables});

  @override
  final DocumentNode document =
      CREATE_ONE_CONTENT_CATEGORY_MUTATION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'CreateOneContentCategoryMutation';

  @override
  final CreateOneContentCategoryMutationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  CreateOneContentCategoryMutation$MutationRoot parse(
          Map<String, dynamic> json) =>
      CreateOneContentCategoryMutation$MutationRoot.fromJson(json);
}
