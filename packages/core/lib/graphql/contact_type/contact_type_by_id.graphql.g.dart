// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'contact_type_by_id.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContactTypeById$QueryRoot$ContactTypeByPk
    _$ContactTypeById$QueryRoot$ContactTypeByPkFromJson(
        Map<String, dynamic> json) {
  return ContactTypeById$QueryRoot$ContactTypeByPk()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..displayName = json['display_name'] as String
    ..schema = json['schema'];
}

Map<String, dynamic> _$ContactTypeById$QueryRoot$ContactTypeByPkToJson(
        ContactTypeById$QueryRoot$ContactTypeByPk instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'display_name': instance.displayName,
      'schema': instance.schema,
    };

ContactTypeById$QueryRoot _$ContactTypeById$QueryRootFromJson(
    Map<String, dynamic> json) {
  return ContactTypeById$QueryRoot()
    ..contactTypeByPk = json['contact_type_by_pk'] == null
        ? null
        : ContactTypeById$QueryRoot$ContactTypeByPk.fromJson(
            json['contact_type_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ContactTypeById$QueryRootToJson(
        ContactTypeById$QueryRoot instance) =>
    <String, dynamic>{
      'contact_type_by_pk': instance.contactTypeByPk?.toJson(),
    };

ContactTypeByIdArguments _$ContactTypeByIdArgumentsFromJson(
    Map<String, dynamic> json) {
  return ContactTypeByIdArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$ContactTypeByIdArgumentsToJson(
        ContactTypeByIdArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
