// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'delete_one_organization_contact_mutation.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeleteOneOrganizationContactMutation$MutationRoot$DeleteOneOrganizationContact
    _$DeleteOneOrganizationContactMutation$MutationRoot$DeleteOneOrganizationContactFromJson(
        Map<String, dynamic> json) {
  return DeleteOneOrganizationContactMutation$MutationRoot$DeleteOneOrganizationContact()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$DeleteOneOrganizationContactMutation$MutationRoot$DeleteOneOrganizationContactToJson(
            DeleteOneOrganizationContactMutation$MutationRoot$DeleteOneOrganizationContact
                instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

DeleteOneOrganizationContactMutation$MutationRoot
    _$DeleteOneOrganizationContactMutation$MutationRootFromJson(
        Map<String, dynamic> json) {
  return DeleteOneOrganizationContactMutation$MutationRoot()
    ..deleteOneOrganizationContact = json['deleteOneOrganizationContact'] ==
            null
        ? null
        : DeleteOneOrganizationContactMutation$MutationRoot$DeleteOneOrganizationContact
            .fromJson(
                json['deleteOneOrganizationContact'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DeleteOneOrganizationContactMutation$MutationRootToJson(
        DeleteOneOrganizationContactMutation$MutationRoot instance) =>
    <String, dynamic>{
      'deleteOneOrganizationContact':
          instance.deleteOneOrganizationContact?.toJson(),
    };

DeleteOneOrganizationContactMutationArguments
    _$DeleteOneOrganizationContactMutationArgumentsFromJson(
        Map<String, dynamic> json) {
  return DeleteOneOrganizationContactMutationArguments(
    id: json['id'] as String?,
  );
}

Map<String, dynamic> _$DeleteOneOrganizationContactMutationArgumentsToJson(
        DeleteOneOrganizationContactMutationArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
