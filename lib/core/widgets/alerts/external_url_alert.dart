// ignore_for_file: unawaited_futures, use_build_context_synchronously, lines_longer_than_80_chars

import 'package:cordico_mobile/core/managers/cordico_url.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/cordico_elevated_button.dart';
import 'package:flutter/material.dart';

class ExternalUrlAlert extends StatefulWidget {
  const ExternalUrlAlert({
    Key? key,
    this.dismissable = true,
    required this.url,
  }) : super(key: key);

  final bool dismissable;
  final String url;

  @override
  _ExternalUrlAlertState createState() => _ExternalUrlAlertState();
}

class _ExternalUrlAlertState extends State<ExternalUrlAlert> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => widget.dismissable,
      child: AlertDialog(
        backgroundColor: ThemeColors.gray.shade900,
        content: Container(
          clipBehavior: Clip.hardEdge,
          width: MediaQuery.of(context).size.width,
          decoration: const BoxDecoration(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'Alert',
                style: ThemeStyles.sectionTitle(context),
              ),
              const SizedBox(height: 4),
              Text(
                'You are about to leave the Cordico app to a 3rd party site.  Please be aware that while Cordico Wellness does not track your activities an external 3rd party site may have different privacy policies.',
                style: ThemeStyles.smallParagraph(context),
              ),
              const SizedBox(height: 16),
              buildUrlButton(),
              const SizedBox(height: 16),
              CordicoElevatedButton(
                color: ThemeColors.gray.shade900,
                hasBorder: true,
                height: 44,
                child: Text(
                  'Cancel',
                  textAlign: TextAlign.center,
                  style: ThemeStyles.smallParagraph(context)
                      ?.copyWith(fontSize: 14),
                ),
                onPressed: () {
                  Navigator.of(context).maybePop();
                },
              ),
            ],
          ),
        ),
        contentPadding:
            const EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
        insetPadding: const EdgeInsets.only(left: 12, right: 12),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6),
        ),
      ),
    );
  }

  Widget buildUrlButton() {
    return CordicoElevatedButton(
      color: ThemeColors.gray.shade900,
      hasBorder: true,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const SizedBox(height: 10),
          Text(
            'continue to',
            style: ThemeStyles.smallParagraph(context)?.copyWith(fontSize: 14),
          ),
          const SizedBox(height: 4),
          Text(
            widget.url,
            style: ThemeStyles.smallParagraph(context)
                ?.copyWith(fontSize: 12, color: const Color(0xFF5FA1DB)),
          ),
          const SizedBox(height: 10),
        ],
      ),
      onPressed: () async {
        final canLaunch = await CordicoUrl.canLaunchUri(uri: widget.url);

        if (canLaunch) {
          Navigator.of(context).maybePop();
          CordicoUrl.openUrl(url: widget.url);
        }
      },
    );
  }
}
