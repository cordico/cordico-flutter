import 'package:core/core.dart';
import 'package:core/graphql/content/content_by_category.dart';
import 'package:core/graphql/content/content_by_content_type.dart';
import 'package:core/graphql/content/content_by_id_api.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/graphql/content/content_state_api.dart';
import 'package:core/graphql/content/content_type_api.dart';
import 'package:core/graphql/content/content_type_by_id.dart';
import 'package:core/graphql/content/organization_content_by_collection_type_key.dart';
import 'package:core/repositories/graphql_repository.dart';
import 'package:core/utils/failure.dart';
import 'package:core/utils/map_extension.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

abstract class IContentRepository {
  //TYPES
  Future<Either<Failure, List<ContentTypeModel>?>> listContentType();
  Future<Either<Failure, ContentTypeModel?>> getContentTypeById(String id,
      {FetchPolicy? fetchPolicy});

  //STATES
  Future<Either<Failure, List<ContentStateModel>?>> listContentStates();

  //CONTENT
  Future<Either<Failure, ContentModel?>> getContentById(String id,
      {FetchPolicy? fetchPolicy});
  Future<Either<Failure, List<ContentModel>?>> listContent({
    List<String>? contentStateNames,
    int? offset,
    int? limit,
  });
  Future<Either<Failure, List<ContentModel>?>> getContentByCategory(
    String categoryID, {
    List<String>? contentStateNames,
    int? offset,
    int? limit,
  });
  Future<Either<Failure, List<ContentModel>?>> getContentByContentType(
    List<String> contentTypeIDs, {
    List<String>? contentStateNames,
    int? offset,
    int? limit,
  });

  Future<Either<Failure, List<ContentModel>?>>
      getOrganizationContentByCollectionTypeKey({
    required String organizationId,
    required String collectionTypeKey,
    List<String>? contentStateNames,
    int? offset,
    int? limit,
  });

  //CONTENT CATEGORY
  Future<Either<Failure, ContentModel?>> createOneContentCategory(
      CreateOneContentCategoryMutationArguments arguments,
      {String? authorizationToken,
      bool returnModel = true});
  Future<Either<Failure, int?>> deleteManyContentCategory(
      DeleteManyContentCategoryMutationArguments arguments,
      {String? authorizationToken});

  //COLLECTION CONTENT
  Future<Either<Failure, ContentModel?>> createOneCollectionContent(
      CreateOneCollectionContentArguments arguments,
      {String? authorizationToken});

  //CRUD CONTENT
  Future<Either<Failure, ContentModel?>> createOneContent(
      CreateOneContentMutationArguments arguments,
      {String? authorizationToken});
  Future<Either<Failure, ContentModel?>> updateOneContent(
      UpdateOneContentMutationArguments arguments,
      {String? authorizationToken});
  Future<Either<Failure, ContentModel?>> deleteOneContent(
      DeleteOneContentMutationArguments arguments,
      {String? authorizationToken});
}

class ContentRepository extends GraphQLRepository
    implements IContentRepository {
  ContentRepository({required String url, required String userUrl,
    String? userToken,})
      : super(url: url, userUrl: userUrl, token: userToken,);

  //TYPES
  @override
  Future<Either<Failure, List<ContentTypeModel>?>> listContentType() async {
    try {
      final query = ContentTypeQueryQuery();

      final response = await graphqlClient.query<ContentTypeModel>(QueryOptions(
          document: query.document, fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = ContentTypeQuery$QueryRoot.fromJson(response.data!);
        return Right(root.contentType
            .map((e) => ContentTypeModel.fromJson(e.toJson()))
            .toList());
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, ContentTypeModel?>> getContentTypeById(String id,
      {FetchPolicy? fetchPolicy}) async {
    try {
      final query =
          ContentTypeByIdQuery(variables: ContentTypeByIdArguments(id: id));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<ContentTypeModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = ContentTypeById$QueryRoot.fromJson(response.data!);
        if (root.contentTypeByPk != null) {
          return Right(root.contentTypeByPk);
        }
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  //STATES
  @override
  Future<Either<Failure, List<ContentStateModel>?>> listContentStates() async {
    try {
      final query = ContentStateQuery();

      final response = await graphqlClient.query<ContentStateModel>(QueryOptions(
          document: query.document, fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = ContentState$QueryRoot.fromJson(response.data!);
        return Right(root.contentState);
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  //CONTENT
  @override
  Future<Either<Failure, ContentModel?>> getContentById(String id,
      {FetchPolicy? fetchPolicy}) async {
    try {
      final query = ContentByIdQuery(variables: ContentByIdArguments(id: id));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<ContentModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = ContentById$QueryRoot.fromJson(response.data!);
        if (root.contentByPk != null) {
          return Right(root.contentByPk);
        }
      }
      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<ContentModel>?>> listContent({
    List<String>? contentStateNames,
    int? offset,
    int? limit,
  }) async {
    try {
      final query = ContentQueryQuery(
          variables: ContentQueryArguments(
              contentStateNames: contentStateNames,
              limit: limit,
              offset: offset));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<ContentModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = ContentQuery$QueryRoot.fromJson(response.data!);
        return Right(root.content
            .map((e) => ContentModel.fromJson(e.toJson()))
            .toList());
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<ContentModel>?>> getContentByCategory(
    String categoryID, {
    List<String>? contentStateNames,
    int? offset,
    int? limit,
  }) async {
    try {
      final query = ContentByCategoryQuery(
          variables: ContentByCategoryArguments(
              categoryID: categoryID,
              contentStateNames: contentStateNames,
              limit: limit,
              offset: offset));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<ContentModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = ContentByCategory$QueryRoot.fromJson(response.data!);
        return Right(root.content
            .map((e) => ContentModel.fromJson(e.toJson()))
            .toList());
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<ContentModel>?>> getContentByContentType(
    List<String> contentTypeIDs, {
    List<String>? contentStateNames,
    int? offset,
    int? limit,
  }) async {
    try {
      final query = ContentByContentTypeQuery(
          variables: ContentByContentTypeArguments(
              contentTypeIDs: contentTypeIDs,
              contentStateNames: contentStateNames,
              offset: offset,
              limit: limit));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<ContentModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = ContentByContentType$QueryRoot.fromJson(response.data!);
        return Right(root.content
            .map((e) => ContentModel.fromJson(e.toJson()))
            .toList());
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<ContentModel>?>>
      getOrganizationContentByCollectionTypeKey({
    required String organizationId,
    required String collectionTypeKey,
    List<String>? contentStateNames,
    int? offset,
    int? limit,
  }) async {
    try {
      final query = OrganizationContentByCollectionTypeKeyQuery(
          variables: OrganizationContentByCollectionTypeKeyArguments(
              organizationId: organizationId,
              collectionTypeKey: collectionTypeKey,
              contentStateNames: contentStateNames,
              limit: limit,
              offset: offset));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<ContentModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = OrganizationContentByCollectionTypeKey$QueryRoot.fromJson(
            response.data!);
        return Right(root.content
            .map((e) => ContentModel.fromJson(e.toJson()))
            .toList());
      }
      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  //CONTENT CATEGORY
  @override
  Future<Either<Failure, ContentModel?>> createOneContentCategory(
      CreateOneContentCategoryMutationArguments arguments,
      {String? authorizationToken,
      bool returnModel = true}) async {
    try {
      final query =
          CreateOneContentCategoryMutationMutation(variables: arguments);

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await userGraphQLClient.query<ContentModel>(QueryOptions(
          document: query.document,
          variables: variables,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          fetchPolicy: FetchPolicy.noCache));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = CreateOneContentCategoryMutation$MutationRoot.fromJson(
            response.data!);
        if (returnModel) {
          final element = await getContentById(
              root.createOneContentCategory.contentId,
              fetchPolicy: FetchPolicy.noCache);
          return element;
        } else {
          return const Right(null);
        }
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, int?>> deleteManyContentCategory(
      DeleteManyContentCategoryMutationArguments arguments,
      {String? authorizationToken}) async {
    try {
      final query =
          DeleteManyContentCategoryMutationMutation(variables: arguments);

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await userGraphQLClient.query<int>(QueryOptions(
          document: query.document,
          variables: variables,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          fetchPolicy: FetchPolicy.noCache));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = DeleteManyContentCategoryMutation$MutationRoot.fromJson(
            response.data!);
        return Right(root.deleteManyContentCategory.count);
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  //COLLECTION CONTENT
  @override
  Future<Either<Failure, ContentModel?>> createOneCollectionContent(
      CreateOneCollectionContentArguments arguments,
      {String? authorizationToken}) async {
    try {
      final query = CreateOneCollectionContentMutation(variables: arguments);

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await userGraphQLClient.query<ContentModel>(QueryOptions(
          document: query.document,
          variables: variables,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          fetchPolicy: FetchPolicy.noCache));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root =
            CreateOneCollectionContent$MutationRoot.fromJson(response.data!);
        final element = await getContentById(
            root.createOneCollectionContent.contentId,
            fetchPolicy: FetchPolicy.noCache);
        return element;
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  //CRUD CONTENT
  @override
  Future<Either<Failure, ContentModel?>> createOneContent(
      CreateOneContentMutationArguments arguments,
      {String? authorizationToken}) async {
    try {
      final query = CreateOneContentMutationMutation(variables: arguments);

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await userGraphQLClient.query<ContentModel>(QueryOptions(
          document: query.document,
          variables: variables,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          fetchPolicy: FetchPolicy.noCache));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root =
            CreateOneContentMutation$MutationRoot.fromJson(response.data!);
        final element = await getContentById(root.createOneContent.id,
            fetchPolicy: FetchPolicy.noCache);
        return element;
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, ContentModel?>> updateOneContent(
      UpdateOneContentMutationArguments arguments,
      {String? authorizationToken}) async {
    try {
      final query = UpdateOneContentMutationMutation(variables: arguments);

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await userGraphQLClient.query<ContentModel>(QueryOptions(
          document: query.document,
          variables: variables,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          fetchPolicy: FetchPolicy.noCache));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root =
            UpdateOneContentMutation$MutationRoot.fromJson(response.data!);
        if (root.updateOneContent != null) {
          final element = await getContentById(root.updateOneContent!.id,
              fetchPolicy: FetchPolicy.noCache);
          return element;
        }
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, ContentModel?>> deleteOneContent(
      DeleteOneContentMutationArguments arguments,
      {String? authorizationToken}) async {
    try {
      final query = DeleteOneContentMutationMutation(variables: arguments);

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await userGraphQLClient.query<ContentModel>(QueryOptions(
          document: query.document,
          variables: variables,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          fetchPolicy: FetchPolicy.noCache));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        return const Right(null);
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }
}
