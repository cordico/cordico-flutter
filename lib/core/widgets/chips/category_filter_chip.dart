import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:flutter/material.dart';

class CategoryFilterChip extends StatefulWidget {
  const CategoryFilterChip({
    Key? key,
    required this.categoryName,
    required this.leading,
    this.onTap,
  }) : super(key: key);

  final String categoryName;
  final Widget leading;
  final Function()? onTap;

  @override
  State createState() => CategoryFilterChipState();
}

class CategoryFilterChipState extends State<CategoryFilterChip> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(50)),
          color: ThemeColors.gray.shade600,
        ),
        height: 33,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            widget.leading,
            Container(
              width: 8,
            ),
            Text(widget.categoryName,
                style: ThemeStyles.contentDescription(context))
          ],
        ),
      ),
    );
  }
}
