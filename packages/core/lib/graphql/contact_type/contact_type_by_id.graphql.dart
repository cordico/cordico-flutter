// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'contact_type_by_id.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class ContactTypeById$QueryRoot$ContactTypeByPk extends JsonSerializable
    with EquatableMixin {
  ContactTypeById$QueryRoot$ContactTypeByPk();

  factory ContactTypeById$QueryRoot$ContactTypeByPk.fromJson(
          Map<String, dynamic> json) =>
      _$ContactTypeById$QueryRoot$ContactTypeByPkFromJson(json);

  late String name;

  late String id;

  @JsonKey(name: 'display_name')
  late String displayName;

  dynamic? schema;

  @override
  List<Object?> get props => [name, id, displayName, schema];
  @override
  Map<String, dynamic> toJson() =>
      _$ContactTypeById$QueryRoot$ContactTypeByPkToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContactTypeById$QueryRoot extends JsonSerializable with EquatableMixin {
  ContactTypeById$QueryRoot();

  factory ContactTypeById$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$ContactTypeById$QueryRootFromJson(json);

  @JsonKey(name: 'contact_type_by_pk')
  ContactTypeById$QueryRoot$ContactTypeByPk? contactTypeByPk;

  @override
  List<Object?> get props => [contactTypeByPk];
  @override
  Map<String, dynamic> toJson() => _$ContactTypeById$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContactTypeByIdArguments extends JsonSerializable with EquatableMixin {
  ContactTypeByIdArguments({required this.id});

  @override
  factory ContactTypeByIdArguments.fromJson(Map<String, dynamic> json) =>
      _$ContactTypeByIdArgumentsFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$ContactTypeByIdArgumentsToJson(this);
}

final CONTACT_TYPE_BY_ID_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'ContactTypeById'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'contact_type_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'id'),
                  value: VariableNode(name: NameNode(value: 'id')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'display_name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class ContactTypeByIdQuery
    extends GraphQLQuery<ContactTypeById$QueryRoot, ContactTypeByIdArguments> {
  ContactTypeByIdQuery({required this.variables});

  @override
  final DocumentNode document = CONTACT_TYPE_BY_ID_QUERY_DOCUMENT;

  @override
  final String operationName = 'ContactTypeById';

  @override
  final ContactTypeByIdArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  ContactTypeById$QueryRoot parse(Map<String, dynamic> json) =>
      ContactTypeById$QueryRoot.fromJson(json);
}
