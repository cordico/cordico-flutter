// ignore_for_file: avoid_function_literals_in_foreach_calls

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:cordico_mobile/core/managers/offlline_manager.dart';
import 'package:cordico_mobile/core/providers/global_providers.dart';
import 'package:cordico_mobile/features/department/logic/department_page_holder.dart';
import 'package:core/core.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/utils/constants.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class DepartmentNotifier extends StateNotifier<DepartmentPageHolder> {
  DepartmentNotifier({
    required IOrganizationRepository organizationRepository,
  })  : _organizationRepository = organizationRepository,
        super(DepartmentPageHolder());

  final IOrganizationRepository _organizationRepository;

  bool _isLoading = false;
  bool get isLoading => _isLoading;

  bool _contentLoaded = false;
  bool get contentLoaded => _contentLoaded;

  void reset() {
    state = DepartmentPageHolder();
    _contentLoaded = false;
  }

  Future<void> getContent() async {
    if (_contentLoaded == false) {
      var valueHolder = DepartmentPageHolder();
      final connectityCheck = await Connectivity().checkConnectivity();

      if (connectityCheck == ConnectivityResult.none) {
        final dep = OfflineManager.getData(key: ModelsKeys.departmentHolder);
        if (dep != null) {
          valueHolder = DepartmentPageHolder.fromJson(dep);
        }
      } else {
        _isLoading = true;
        final result2 = await _organizationRepository
            .getOrganizationByID(environmentConfig.organizationId!);
        result2.fold(
          (error) {},
          (content) {
            valueHolder.organization = content;
          },
        );

        final result1 = await _organizationRepository
            .listOrganizationCollectionByOrganizationIdAndCollectionType(
          organizationId: environmentConfig.organizationId,
          collectionType: kCollectionTypeBulletin,
        );
        result1.fold(
          (error) {},
          (content) {
            if (content != null && content.isNotEmpty) {
              final res = <ContentQuery$QueryRoot$Content>[];
              content.forEach((collectionContents) {
                collectionContents.collectionContents.forEach((contents) {
                  res.add(ContentQuery$QueryRoot$Content.fromJson(
                      contents.content.toJson()));
                });
              });

              valueHolder.bulletin = List.from(res);
            }
          },
        );

        final result3 = await _organizationRepository
            .listOrganizationCollectionByOrganizationIdAndCollectionType(
          organizationId: environmentConfig.organizationId,
          collectionType: kCollectionTypeDepartmentLink,
        );
        result3.fold((error) {}, (content) {
          if (content != null && content.isNotEmpty) {
            final res = <ContentQuery$QueryRoot$Content>[];
            content.forEach((collectionContents) {
              collectionContents.collectionContents.forEach((contents) {
                res.add(ContentQuery$QueryRoot$Content.fromJson(
                    contents.content.toJson()));
              });
            });

            valueHolder.departmentResources = List.from(res);
          }
        });

        final result4 = await _organizationRepository
            .listOrganizationCollectionByOrganizationIdAndCollectionType(
          organizationId: environmentConfig.organizationId,
          collectionType: kCollectionTypeEAP,
        );
        result4.fold((error) {}, (content) {
          if (content != null && content.isNotEmpty) {
            final res = <ContentQuery$QueryRoot$Content>[];
            content.forEach((collectionContents) {
              collectionContents.collectionContents.forEach((contents) {
                res.add(ContentQuery$QueryRoot$Content.fromJson(
                    contents.content.toJson()));
              });
            });

            valueHolder.eapResources = List.from(res);

            valueHolder.eapResources?.removeWhere((element) {
              //print('NAME: ${element.contentType.name} - ${element.id}');
              return element.contentType.id != kContentTypeEAPId;
            });
          }
        });

        _isLoading = false;

        await OfflineManager.persistData(
          key: ModelsKeys.departmentHolder,
          value: valueHolder.toJson(),
        );
      }

      _contentLoaded = true;
      state = valueHolder;
    }
  }
}
