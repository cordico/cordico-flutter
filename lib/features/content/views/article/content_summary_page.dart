import 'package:collection/collection.dart';
import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/article_item_list_item.dart';
import 'package:cordico_mobile/core/widgets/content_widget.dart';
import 'package:cordico_mobile/features/content/logic/content_provider.dart';
import 'package:cordico_mobile/features/content/views/article/content_summary_header.dart';
import 'package:cordico_mobile/features/department/logic/department_provider.dart';
import 'package:cordico_mobile/features/discover/logic/content_filter_state.dart';
import 'package:core/core.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/utils/constants.dart';
import 'package:core/utils/map_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_segment/flutter_segment.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:vrouter/vrouter.dart';

class ContentSummaryPage extends ConsumerStatefulWidget {
  const ContentSummaryPage({Key? key}) : super(key: key);

  @override
  ConsumerState<ContentSummaryPage> createState() => _ContentSummaryPageState();
}

class _ContentSummaryPageState extends ConsumerState<ContentSummaryPage> {
  late ContentQuery$QueryRoot$Content? content;
  late ContentSchema contentSchema;

  @override
  Widget build(BuildContext context) {
    //final contentValue = ref.watch(contentByIdProvider);

    final contentID = ref.watch(contentIdProvider);
    final source = context.vRouter.pathParameters['source'];

    if (source == 'eap') {
      final state = ref.watch(departmentPageStateNotifierProvider);
      final id = context.vRouter.pathParameters['id'];
      content =
          state.eapResources?.firstWhereOrNull((element) => element.id == id);
    } else {
      final state = ref.watch(allContentProvider);
      content = state.fromAllCategoriesContent
          .firstWhereOrNull((element) => element.id == contentID);
    }

    Segment.screen(screenName: 'View Content', properties: <String, dynamic>{
      'type': 'screen',
      'name': 'Content Summary'
    });

    if (content == null) {
      return buildEmptyWidget('Content not available');
    }

    Segment.track(eventName: 'View Content', properties: <String, dynamic>{
      'content_id': content?.id,
      'type': content?.contentType.name,
      'name': content?.name
    });

    removeNullAndEmptyParams(content?.data as Map<String, dynamic>);
    contentSchema =
        ContentSchema.fromJson(content?.data as Map<String, dynamic>);

    var readContentParts = <String>[];

    readContentParts = global.prefs.getStringList(content?.id ?? '') ?? [];

    final contentParts = content?.childContent.map((e) {
      final map = e.toJson();
      return ContentPartModel.fromJson(map);
    }).toList();

    if (contentParts != null && contentParts.isNotEmpty) {
      contentParts.sort((a, b) {
        return a.index < b.index ? 0 : 1;
      });

      return Scaffold(
        body: CustomScrollView(
          slivers: [
            SliverPersistentHeader(
              pinned: true,
              delegate: ContentSummaryHeader(
                imageUrl: contentSchema.imageUrl,
                minExtent: 80 + MediaQuery.of(context).viewPadding.top,
                maxExtent: 210 + MediaQuery.of(context).viewPadding.top,
              ),
            ),

            SliverToBoxAdapter(
              child: _buildTitle(context, contentSchema.title ?? '', false),
            ),
            SliverToBoxAdapter(
              child: _buildDescription(context, content!),
            ),
            SliverToBoxAdapter(
              child: ListView.builder(
                shrinkWrap: true,
                primary: false,
                padding: EdgeInsets.zero,
                itemBuilder: (BuildContext context, int index) {
                  final part = contentParts[index];

                  final isRead = readContentParts.contains(part.contentPart.id);
                  return _buildArticleItem(
                    context,
                    part: ContentQuery$QueryRoot$Content.fromJson(
                        part.contentPart.toJson()),
                    isRead: isRead,
                    index: index,
                  );
                },
                itemCount: contentParts.length,
              ),
            ),
            //_buildRelatedArticlesHeader(context),
          ],
        ),
      );
    }
    return buildEmptyWidget('Content is not formatted correctly');
  }

  Widget buildEmptyWidget(String message) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Text(
                  message,
                  style: ThemeStyles.contentDescription(context)
                      ?.copyWith(fontSize: 16),
                ),
              ),
            ),
            _buildBackIcon(context),
          ],
        ),
      ),
    );
  }

  Widget _buildBackIcon(BuildContext context) {
    return IconButton(
      onPressed: () {
        context.vRouter.historyBack();
      },
      iconSize: 40,
      splashRadius: 20,
      icon: CircleAvatar(
        radius: 20,
        backgroundColor: ThemeColors.buttonsGreyColor,
        child: Padding(
          padding: const EdgeInsets.only(left: 8),
          child: Icon(Icons.arrow_back_ios, color: ThemeColors.blue.shade100),
        ),
      ),
    );
  }

  Widget _buildTitle(BuildContext context, String title, bool isFavorite) {
    final likedContentIds =
        global.prefs.getStringList(kSharedPrefContentId) ?? [];
    final isLiked = likedContentIds.contains(content?.id ?? '');
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(24), topRight: Radius.circular(24)),
        color: Colors.black,
      ),
      padding: const EdgeInsets.fromLTRB(20, 0, 12, 0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 4),
              child: Text(
                title,
                style: ThemeStyles.bulletinCardTitle(context)
                    ?.copyWith(fontSize: 27),
              ),
            ),
          ),
          const SizedBox(width: 12),
          IconButton(
            onPressed: () {
              if (isLiked) {
                likedContentIds.remove(content?.id);
              } else {
                likedContentIds.add(content?.id ?? '');
              }
              global.prefs.setStringList(kSharedPrefContentId, likedContentIds);

              setState(() {});
            },
            icon: Icon(
              isLiked ? Icons.thumb_up_alt : Icons.thumb_up_alt_outlined,
              color: Colors.white,
              size: 26,
            ),
            padding: EdgeInsets.zero,
          ),
        ],
      ),
    );
  }

  Widget _buildDescription(
      BuildContext context, ContentQuery$QueryRoot$Content content) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(20, 3, 20, 0),
          child: ContentWidget(
            contentType: MimeType.markdown,
            content: contentSchema.description ?? '',
          ),
        ),
        const SizedBox(height: 17),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Container(
            width: double.infinity,
            height: 1,
            color: ThemeColors.blue.shade100,
          ),
        ),
        const SizedBox(height: 26),
      ],
    );
  }

  Widget _buildArticleItem(
    BuildContext context, {
    ContentQuery$QueryRoot$Content? part,
    int index = 0,
    bool isRead = false,
  }) {
    removeNullAndEmptyParams(part?.data as Map<String, dynamic>);
    final partContentSchema =
        ContentSchema.fromJson(part?.data as Map<String, dynamic>);

    var type = 'Text';
    if ((partContentSchema.videoUrl1 != null &&
            partContentSchema.videoUrl1!.isNotEmpty) ||
        (partContentSchema.videoUrl != null &&
            partContentSchema.videoUrl!.isNotEmpty)) {
      type = 'Video';
    } else if (partContentSchema.audioUrl != null &&
        partContentSchema.audioUrl!.isNotEmpty) {
      type = 'Audio';
    }

    return ArticleItemListItem(
      content: part,
      type: type,
      contentSchema: partContentSchema,
      isRead: isRead,
      onItemTap: () {
        context.vRouter.to('content/${content!.id}/$index/general');

        Future.delayed(const Duration(milliseconds: 1000), () {
          if (mounted) {
            setState(() {});
          }
        });
      },
    );
  }
}
