// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'create_one_feedback_mutation.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CreateOneFeedbackMutation$MutationRoot$InsertFeedbackOne
    extends JsonSerializable with EquatableMixin {
  CreateOneFeedbackMutation$MutationRoot$InsertFeedbackOne();

  factory CreateOneFeedbackMutation$MutationRoot$InsertFeedbackOne.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneFeedbackMutation$MutationRoot$InsertFeedbackOneFromJson(json);

  late String body;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  dynamic? data;

  @JsonKey(name: 'feedback_type_id')
  late String feedbackTypeId;

  late String id;

  String? subject;

  @JsonKey(name: 'updated_at')
  late DateTime updatedAt;

  @override
  List<Object?> get props =>
      [body, createdAt, data, feedbackTypeId, id, subject, updatedAt];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneFeedbackMutation$MutationRoot$InsertFeedbackOneToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneFeedbackMutation$MutationRoot extends JsonSerializable
    with EquatableMixin {
  CreateOneFeedbackMutation$MutationRoot();

  factory CreateOneFeedbackMutation$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneFeedbackMutation$MutationRootFromJson(json);

  @JsonKey(name: 'insert_feedback_one')
  CreateOneFeedbackMutation$MutationRoot$InsertFeedbackOne? insertFeedbackOne;

  @override
  List<Object?> get props => [insertFeedbackOne];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneFeedbackMutation$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneFeedbackMutationArguments extends JsonSerializable
    with EquatableMixin {
  CreateOneFeedbackMutationArguments(
      {required this.body,
      required this.data,
      required this.feedback_type_id,
      required this.subject,
      required this.creatorId});

  @override
  factory CreateOneFeedbackMutationArguments.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneFeedbackMutationArgumentsFromJson(json);

  late String body;

  late dynamic data;

  late String feedback_type_id;

  late String subject;

  late String creatorId;

  @override
  List<Object?> get props => [body, data, feedback_type_id, subject, creatorId];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneFeedbackMutationArgumentsToJson(this);
}

final CREATE_ONE_FEEDBACK_MUTATION_MUTATION_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'CreateOneFeedbackMutation'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'body')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'data')),
            type:
                NamedTypeNode(name: NameNode(value: 'jsonb'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'feedback_type_id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'subject')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'creatorId')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'insert_feedback_one'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'object'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'body'),
                        value: VariableNode(name: NameNode(value: 'body'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'data'),
                        value: VariableNode(name: NameNode(value: 'data'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'feedback_type_id'),
                        value: VariableNode(
                            name: NameNode(value: 'feedback_type_id'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'subject'),
                        value: VariableNode(name: NameNode(value: 'subject'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'creator_id'),
                        value: VariableNode(name: NameNode(value: 'creatorId')))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'body'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'feedback_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'subject'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class CreateOneFeedbackMutationMutation extends GraphQLQuery<
    CreateOneFeedbackMutation$MutationRoot,
    CreateOneFeedbackMutationArguments> {
  CreateOneFeedbackMutationMutation({required this.variables});

  @override
  final DocumentNode document = CREATE_ONE_FEEDBACK_MUTATION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'CreateOneFeedbackMutation';

  @override
  final CreateOneFeedbackMutationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  CreateOneFeedbackMutation$MutationRoot parse(Map<String, dynamic> json) =>
      CreateOneFeedbackMutation$MutationRoot.fromJson(json);
}
