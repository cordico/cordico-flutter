// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'organization_by_id.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class OrganizationByID$QueryRoot$OrganizationByPk extends JsonSerializable
    with EquatableMixin {
  OrganizationByID$QueryRoot$OrganizationByPk();

  factory OrganizationByID$QueryRoot$OrganizationByPk.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationByID$QueryRoot$OrganizationByPkFromJson(json);

  late String name;

  late String id;

  String? description;

  late dynamic data;

  @JsonKey(name: 'organization_type_id')
  late String organizationTypeId;

  @JsonKey(name: 'portal_fqdn')
  String? portalFqdn;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @override
  List<Object?> get props => [
        name,
        id,
        description,
        data,
        organizationTypeId,
        portalFqdn,
        createdAt,
        updatedAt
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationByID$QueryRoot$OrganizationByPkToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationByID$QueryRoot extends JsonSerializable with EquatableMixin {
  OrganizationByID$QueryRoot();

  factory OrganizationByID$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$OrganizationByID$QueryRootFromJson(json);

  @JsonKey(name: 'organization_by_pk')
  OrganizationByID$QueryRoot$OrganizationByPk? organizationByPk;

  @override
  List<Object?> get props => [organizationByPk];
  @override
  Map<String, dynamic> toJson() => _$OrganizationByID$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationByIDArguments extends JsonSerializable with EquatableMixin {
  OrganizationByIDArguments({required this.id});

  @override
  factory OrganizationByIDArguments.fromJson(Map<String, dynamic> json) =>
      _$OrganizationByIDArgumentsFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$OrganizationByIDArgumentsToJson(this);
}

final ORGANIZATION_BY_I_D_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'OrganizationByID'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'organization_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'id'),
                  value: VariableNode(name: NameNode(value: 'id')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'description'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'organization_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'portal_fqdn'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class OrganizationByIDQuery extends GraphQLQuery<OrganizationByID$QueryRoot,
    OrganizationByIDArguments> {
  OrganizationByIDQuery({required this.variables});

  @override
  final DocumentNode document = ORGANIZATION_BY_I_D_QUERY_DOCUMENT;

  @override
  final String operationName = 'OrganizationByID';

  @override
  final OrganizationByIDArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  OrganizationByID$QueryRoot parse(Map<String, dynamic> json) =>
      OrganizationByID$QueryRoot.fromJson(json);
}
