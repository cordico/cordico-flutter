// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'user_by_id.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserById$QueryRoot$UserByPk _$UserById$QueryRoot$UserByPkFromJson(
    Map<String, dynamic> json) {
  return UserById$QueryRoot$UserByPk()
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..id = json['id'] as String
    ..identifier = json['identifier'] as String
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..userServiceId = json['user_service_id'] as String?
    ..userTypeId = json['user_type_id'] as String;
}

Map<String, dynamic> _$UserById$QueryRoot$UserByPkToJson(
        UserById$QueryRoot$UserByPk instance) =>
    <String, dynamic>{
      'data': instance.data,
      'created_at': instance.createdAt.toIso8601String(),
      'id': instance.id,
      'identifier': instance.identifier,
      'updated_at': instance.updatedAt?.toIso8601String(),
      'user_service_id': instance.userServiceId,
      'user_type_id': instance.userTypeId,
    };

UserById$QueryRoot _$UserById$QueryRootFromJson(Map<String, dynamic> json) {
  return UserById$QueryRoot()
    ..userByPk = json['user_by_pk'] == null
        ? null
        : UserById$QueryRoot$UserByPk.fromJson(
            json['user_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UserById$QueryRootToJson(UserById$QueryRoot instance) =>
    <String, dynamic>{
      'user_by_pk': instance.userByPk?.toJson(),
    };

UserByIdArguments _$UserByIdArgumentsFromJson(Map<String, dynamic> json) {
  return UserByIdArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$UserByIdArgumentsToJson(UserByIdArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
