// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'organization_by_ids.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrganizationByIDs$QueryRoot$Organization
    _$OrganizationByIDs$QueryRoot$OrganizationFromJson(
        Map<String, dynamic> json) {
  return OrganizationByIDs$QueryRoot$Organization()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?
    ..data = json['data']
    ..organizationTypeId = json['organization_type_id'] as String
    ..portalFqdn = json['portal_fqdn'] as String?
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String);
}

Map<String, dynamic> _$OrganizationByIDs$QueryRoot$OrganizationToJson(
        OrganizationByIDs$QueryRoot$Organization instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'description': instance.description,
      'data': instance.data,
      'organization_type_id': instance.organizationTypeId,
      'portal_fqdn': instance.portalFqdn,
      'created_at': instance.createdAt.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
    };

OrganizationByIDs$QueryRoot _$OrganizationByIDs$QueryRootFromJson(
    Map<String, dynamic> json) {
  return OrganizationByIDs$QueryRoot()
    ..organization = (json['organization'] as List<dynamic>)
        .map((e) => OrganizationByIDs$QueryRoot$Organization.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$OrganizationByIDs$QueryRootToJson(
        OrganizationByIDs$QueryRoot instance) =>
    <String, dynamic>{
      'organization': instance.organization.map((e) => e.toJson()).toList(),
    };

OrganizationByIDsArguments _$OrganizationByIDsArgumentsFromJson(
    Map<String, dynamic> json) {
  return OrganizationByIDsArguments(
    ids: (json['ids'] as List<dynamic>?)?.map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$OrganizationByIDsArgumentsToJson(
        OrganizationByIDsArguments instance) =>
    <String, dynamic>{
      'ids': instance.ids,
    };
