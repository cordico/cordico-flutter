// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'delete_many_content_category.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeleteManyContentCategoryMutation$MutationRoot$DeleteManyContentCategory
    _$DeleteManyContentCategoryMutation$MutationRoot$DeleteManyContentCategoryFromJson(
        Map<String, dynamic> json) {
  return DeleteManyContentCategoryMutation$MutationRoot$DeleteManyContentCategory()
    ..count = json['count'] as int;
}

Map<String, dynamic>
    _$DeleteManyContentCategoryMutation$MutationRoot$DeleteManyContentCategoryToJson(
            DeleteManyContentCategoryMutation$MutationRoot$DeleteManyContentCategory
                instance) =>
        <String, dynamic>{
          'count': instance.count,
        };

DeleteManyContentCategoryMutation$MutationRoot
    _$DeleteManyContentCategoryMutation$MutationRootFromJson(
        Map<String, dynamic> json) {
  return DeleteManyContentCategoryMutation$MutationRoot()
    ..deleteManyContentCategory =
        DeleteManyContentCategoryMutation$MutationRoot$DeleteManyContentCategory
            .fromJson(
                json['deleteManyContentCategory'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DeleteManyContentCategoryMutation$MutationRootToJson(
        DeleteManyContentCategoryMutation$MutationRoot instance) =>
    <String, dynamic>{
      'deleteManyContentCategory': instance.deleteManyContentCategory.toJson(),
    };

DeleteManyContentCategoryMutationArguments
    _$DeleteManyContentCategoryMutationArgumentsFromJson(
        Map<String, dynamic> json) {
  return DeleteManyContentCategoryMutationArguments(
    contentIds: (json['contentIds'] as List<dynamic>?)
        ?.map((e) => e as String?)
        .toList(),
  );
}

Map<String, dynamic> _$DeleteManyContentCategoryMutationArgumentsToJson(
        DeleteManyContentCategoryMutationArguments instance) =>
    <String, dynamic>{
      'contentIds': instance.contentIds,
    };
