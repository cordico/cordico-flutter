// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'configuration_type_query.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConfigurationTypeQuery$QueryRoot$ConfigurationType
    _$ConfigurationTypeQuery$QueryRoot$ConfigurationTypeFromJson(
        Map<String, dynamic> json) {
  return ConfigurationTypeQuery$QueryRoot$ConfigurationType()
    ..id = json['id'] as String
    ..name = json['name'] as String
    ..key = json['key'] as String
    ..schema = json['schema'];
}

Map<String, dynamic> _$ConfigurationTypeQuery$QueryRoot$ConfigurationTypeToJson(
        ConfigurationTypeQuery$QueryRoot$ConfigurationType instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'key': instance.key,
      'schema': instance.schema,
    };

ConfigurationTypeQuery$QueryRoot _$ConfigurationTypeQuery$QueryRootFromJson(
    Map<String, dynamic> json) {
  return ConfigurationTypeQuery$QueryRoot()
    ..configurationType = (json['configuration_type'] as List<dynamic>)
        .map((e) => ConfigurationTypeQuery$QueryRoot$ConfigurationType.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$ConfigurationTypeQuery$QueryRootToJson(
        ConfigurationTypeQuery$QueryRoot instance) =>
    <String, dynamic>{
      'configuration_type':
          instance.configurationType.map((e) => e.toJson()).toList(),
    };
