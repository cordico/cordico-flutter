import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:flutter/material.dart';

class UrgentBadge extends StatelessWidget {
  const UrgentBadge({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 1, horizontal: 2),
      color: ThemeColors.orange.shade400,
      child: Text(
        'URGENT',
        style: ThemeStyles.extraSmallParagraph(context)?.copyWith(
          color: ThemeColors.gray.shade900,
          fontWeight: FontWeight.w700,
          fontSize: 9.5,
        ),
      ),
    );
  }
}
