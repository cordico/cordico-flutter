import 'package:core/core.dart';
import 'package:core/graphql/content_part/content_part_by_content_id.dart';
import 'package:core/repositories/graphql_repository.dart';
import 'package:core/utils/failure.dart';
import 'package:core/utils/map_extension.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

abstract class IContentPartRepository {
  Future<Either<Failure, List<ContentPartModel>?>> getContentPartByContentID({
    required String contentID,
    List<String>? contentStateNames,
    int? offset,
    int? limit,
  });
}

class ContentPartRepository extends GraphQLRepository
    implements IContentPartRepository {
  ContentPartRepository({required String url, required String userUrl,
    String? userToken}) :
        super(
          url: url,
          userUrl: userUrl,
        token: userToken,
      );

  @override
  Future<Either<Failure, List<ContentPartModel>?>> getContentPartByContentID({
    required String contentID,
    List<String>? contentStateNames,
    int? offset,
    int? limit,
  }) async {
    try {
      final query = ContentPartByContentIDQuery(
          variables: ContentPartByContentIDArguments(
        contentID: contentID,
        contentStateNames: contentStateNames,
        offset: offset,
        limit: limit,
      ));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<ContentPartModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = ContentPartByContentID$QueryRoot.fromJson(response.data!);
        return Right(root.contentPart);
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }
}
