// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'user_type_query.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserTypeQuery$QueryRoot$UserType _$UserTypeQuery$QueryRoot$UserTypeFromJson(
    Map<String, dynamic> json) {
  return UserTypeQuery$QueryRoot$UserType()
    ..id = json['id'] as String
    ..identitySchema = json['identity_schema'] as String
    ..key = json['key'] as String
    ..name = json['name'] as String
    ..schema = json['schema'];
}

Map<String, dynamic> _$UserTypeQuery$QueryRoot$UserTypeToJson(
        UserTypeQuery$QueryRoot$UserType instance) =>
    <String, dynamic>{
      'id': instance.id,
      'identity_schema': instance.identitySchema,
      'key': instance.key,
      'name': instance.name,
      'schema': instance.schema,
    };

UserTypeQuery$QueryRoot _$UserTypeQuery$QueryRootFromJson(
    Map<String, dynamic> json) {
  return UserTypeQuery$QueryRoot()
    ..userType = (json['user_type'] as List<dynamic>)
        .map((e) => UserTypeQuery$QueryRoot$UserType.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$UserTypeQuery$QueryRootToJson(
        UserTypeQuery$QueryRoot instance) =>
    <String, dynamic>{
      'user_type': instance.userType.map((e) => e.toJson()).toList(),
    };
