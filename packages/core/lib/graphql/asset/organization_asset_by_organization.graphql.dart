// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'organization_asset_by_organization.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$Asset
    extends JsonSerializable with EquatableMixin {
  OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$Asset();

  factory OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$Asset.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$AssetFromJson(
          json);

  @JsonKey(name: 'asset_type_id')
  late String assetTypeId;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'creator_id')
  late String creatorId;

  late dynamic data;

  @JsonKey(name: 'file_name')
  late String fileName;

  String? hash;

  late String id;

  @JsonKey(name: 'mime_type')
  late String mimeType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @JsonKey(name: 'updater_id')
  String? updaterId;

  String? url;

  @override
  List<Object?> get props => [
        assetTypeId,
        createdAt,
        creatorId,
        data,
        fileName,
        hash,
        id,
        mimeType,
        updatedAt,
        updaterId,
        url
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$AssetToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$Organization
    extends JsonSerializable with EquatableMixin {
  OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$Organization();

  factory OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$Organization.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$OrganizationFromJson(
          json);

  late String name;

  late String id;

  String? description;

  late dynamic data;

  @JsonKey(name: 'organization_type_id')
  late String organizationTypeId;

  @JsonKey(name: 'portal_fqdn')
  String? portalFqdn;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @override
  List<Object?> get props => [
        name,
        id,
        description,
        data,
        organizationTypeId,
        portalFqdn,
        createdAt,
        updatedAt
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$OrganizationToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationAssetByOrganization$QueryRoot$OrganizationAsset
    extends JsonSerializable with EquatableMixin {
  OrganizationAssetByOrganization$QueryRoot$OrganizationAsset();

  factory OrganizationAssetByOrganization$QueryRoot$OrganizationAsset.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationAssetByOrganization$QueryRoot$OrganizationAssetFromJson(
          json);

  late OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$Asset asset;

  late OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$Organization
      organization;

  @override
  List<Object?> get props => [asset, organization];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationAssetByOrganization$QueryRoot$OrganizationAssetToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationAssetByOrganization$QueryRoot extends JsonSerializable
    with EquatableMixin {
  OrganizationAssetByOrganization$QueryRoot();

  factory OrganizationAssetByOrganization$QueryRoot.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationAssetByOrganization$QueryRootFromJson(json);

  @JsonKey(name: 'organization_asset')
  late List<OrganizationAssetByOrganization$QueryRoot$OrganizationAsset>
      organizationAsset;

  @override
  List<Object?> get props => [organizationAsset];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationAssetByOrganization$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationAssetByOrganizationArguments extends JsonSerializable
    with EquatableMixin {
  OrganizationAssetByOrganizationArguments({this.ids});

  @override
  factory OrganizationAssetByOrganizationArguments.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationAssetByOrganizationArgumentsFromJson(json);

  final List<String>? ids;

  @override
  List<Object?> get props => [ids];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationAssetByOrganizationArgumentsToJson(this);
}

final ORGANIZATION_ASSET_BY_ORGANIZATION_QUERY_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'OrganizationAssetByOrganization'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'ids')),
            type: ListTypeNode(
                type: NamedTypeNode(
                    name: NameNode(value: 'uuid'), isNonNull: true),
                isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'organization_asset'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'organization_id'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: '_in'),
                              value: VariableNode(name: NameNode(value: 'ids')))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'asset'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'asset_type_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'created_at'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'creator_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'data'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'file_name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'hash'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'mime_type'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'updated_at'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'updater_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'url'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'organization'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'description'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'data'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'organization_type_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'portal_fqdn'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'created_at'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'updated_at'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ]))
            ]))
      ]))
]);

class OrganizationAssetByOrganizationQuery extends GraphQLQuery<
    OrganizationAssetByOrganization$QueryRoot,
    OrganizationAssetByOrganizationArguments> {
  OrganizationAssetByOrganizationQuery({required this.variables});

  @override
  final DocumentNode document =
      ORGANIZATION_ASSET_BY_ORGANIZATION_QUERY_DOCUMENT;

  @override
  final String operationName = 'OrganizationAssetByOrganization';

  @override
  final OrganizationAssetByOrganizationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  OrganizationAssetByOrganization$QueryRoot parse(Map<String, dynamic> json) =>
      OrganizationAssetByOrganization$QueryRoot.fromJson(json);
}
