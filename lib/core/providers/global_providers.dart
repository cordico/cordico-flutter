import 'dart:async';

import 'package:core/core.dart';
import 'package:core/repositories/content_part_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

const secureStorage = FlutterSecureStorage();
final authenticationService =
    AuthenticationService(EnvironmentConfig.kratosUrl);

final userRepository = UserRepository(
    url: EnvironmentConfig.contentGraphqlUrl,
    userUrl: EnvironmentConfig.userGraphqlUrl,
  kratosUrl: EnvironmentConfig.kratosUrl,
);

final environmentConfig = EnvironmentConfig();

final sharedPreferencesProvider = Provider<SharedPreferences>((ref) {
  throw UnimplementedError();
});

final packageInfoProvider = FutureProvider<PackageInfo>(
  (_) async => PackageInfo.fromPlatform(),
);

final organizationRepositoryProvider = Provider<IOrganizationRepository>(
        (_) {
  return OrganizationRepository(
      url: EnvironmentConfig.contentGraphqlUrl,
      userUrl: EnvironmentConfig.userGraphqlUrl,
    kratosUrl: EnvironmentConfig.kratosUrl,
  );
});

final collectionRepositoryProvider = Provider<ICollectionRepository>((_) =>
    CollectionRepository(
        url: EnvironmentConfig.contentGraphqlUrl,
        userUrl: EnvironmentConfig.userGraphqlUrl,),);

final categoryRepositoryProvider = Provider<ICategoryRepository>((_) =>
    CategoryRepository(
        url: EnvironmentConfig.contentGraphqlUrl,
        userUrl: EnvironmentConfig.userGraphqlUrl,),);

final contentRepositoryProvider = Provider<IContentRepository>((_) =>
    ContentRepository(
        url: EnvironmentConfig.contentGraphqlUrl,
        userUrl: EnvironmentConfig.userGraphqlUrl,),);

final feedbackRepositoryProvider = Provider<IFeedbackRepository>((_) =>
    FeedbackRepository(
        url: EnvironmentConfig.contentGraphqlUrl,
        userUrl: EnvironmentConfig.userGraphqlUrl,),);

final contentPartsRepositoryProvider = Provider<IContentPartRepository>(
  (_) => ContentPartRepository(
      url: EnvironmentConfig.contentGraphqlUrl,
      userUrl: EnvironmentConfig.userGraphqlUrl,),
);
