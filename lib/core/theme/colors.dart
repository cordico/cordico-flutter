import 'package:flutter/material.dart';

abstract class ThemeColors {
  static const green = MaterialColor(
    _greenShade500Value,
    <int, Color>{
      100: Color(_greenShade100Value),
      200: Color(_greenShade200Value),
      300: Color(_greenShade300Value),
      400: Color(_greenShade400Value),
      500: Color(_greenShade500Value),
      600: Color(_greenShade600Value),
      700: Color(_greenShade700Value),
    },
  );

  static const int _greenShade100Value = 0xFFDCFFD2;
  static const int _greenShade200Value = 0xFFB0F99B;
  static const int _greenShade300Value = 0xFF7ACB6F;
  static const int _greenShade400Value = 0xFF52BF3E;
  static const int _greenShade500Value = 0xFF2BB311;
  static const int _greenShade600Value = 0xFF68A75F;
  static const int _greenShade700Value = 0xFF3B7F2D;

  static const blue = MaterialColor(
    _blueShade500Value,
    <int, Color>{
      50: Color(_blueShade50Value),
      100: Color(_blueShade100Value),
      200: Color(_blueShade200Value),
      300: Color(_blueShade300Value),
      400: Color(_blueShade400Value),
      500: Color(_blueShade500Value),
      600: Color(_blueShade600Value),
      700: Color(_blueShade700Value),
    },
  );

  static const int _blueShade50Value = 0xFFF1FAFF;
  static const int _blueShade100Value = 0xFFD9F2FF;
  static const int _blueShade200Value = 0xFF97D2FF;
  static const int _blueShade300Value = 0xFF5598D9;
  static const int _blueShade400Value = 0xFF0D68CC;
  static const int _blueShade500Value = 0xFF004FCE;
  static const int _blueShade600Value = 0xFF174D81;
  static const int _blueShade700Value = 0xFF003666;

  static const gray = MaterialColor(
    _grayShade500Value,
    <int, Color>{
      100: Color(_grayShade100Value),
      200: Color(_grayShade200Value),
      300: Color(_grayShade300Value),
      400: Color(_grayShade400Value),
      500: Color(_grayShade500Value),
      600: Color(_grayShade600Value),
      700: Color(_grayShade700Value),
      800: Color(_grayShade800Value),
      900: Color(_grayShade900Value),
    },
  );

  static const int _grayShade100Value = 0xFFF5F5F5;
  static const int _grayShade200Value = 0xFFDEDEE0;
  static const int _grayShade300Value = 0xFF95959C;
  static const int _grayShade400Value = 0xFF5B5B5E;
  static const int _grayShade500Value = 0xFF414245;
  static const int _grayShade600Value = 0xFF303234;
  static const int _grayShade700Value = 0xFF272829;
  static const int _grayShade800Value = 0xFF1D1F20;
  static const int _grayShade900Value = 0xFF17171A;

  static const red = MaterialColor(
    _redShade500Value,
    <int, Color>{
      100: Color(_redShade100Value),
      200: Color(_redShade200Value),
      300: Color(_redShade300Value),
      400: Color(_redShade400Value),
      500: Color(_redShade500Value),
      600: Color(_redShade600Value),
      700: Color(_redShade700Value),
    },
  );

  static const int _redShade100Value = 0xFFFFD5CC;
  static const int _redShade200Value = 0xFFFFAC99;
  static const int _redShade300Value = 0xFFFF785A;
  static const int _redShade400Value = 0xFFFF6D4D;
  static const int _redShade500Value = 0xFFFF451C;
  static const int _redShade600Value = 0xFFA73117;
  static const int _redShade700Value = 0xFF721B07;

  static const yellow = MaterialColor(
    _yellowShade500Value,
    <int, Color>{
      100: Color(_yellowShade100Value),
      200: Color(_yellowShade200Value),
      300: Color(_yellowShade300Value),
      400: Color(_yellowShade400Value),
      500: Color(_yellowShade500Value),
      600: Color(_yellowShade600Value),
      700: Color(_yellowShade700Value),
    },
  );

  static const int _yellowShade100Value = 0xFFFFF4AF;
  static const int _yellowShade200Value = 0xFFFFE964;
  static const int _yellowShade300Value = 0xFFF7D64D;
  static const int _yellowShade400Value = 0xFFF9CC12;
  static const int _yellowShade500Value = 0xFFFFBC00;
  static const int _yellowShade600Value = 0xFFC5A318;
  static const int _yellowShade700Value = 0xFF8E7407;

  static const orange = MaterialColor(
    _orangeShade500Value,
    <int, Color>{
      100: Color(_orangeShade100Value),
      200: Color(_orangeShade200Value),
      300: Color(_orangeShade300Value),
      400: Color(_orangeShade400Value),
      500: Color(_orangeShade500Value),
      600: Color(_orangeShade600Value),
      700: Color(_orangeShade700Value),
    },
  );

  static const int _orangeShade100Value = 0xFFFFE7CC;
  static const int _orangeShade200Value = 0xFFFFD199;
  static const int _orangeShade300Value = 0xFFFFB75A;
  static const int _orangeShade400Value = 0xFFFF921C;
  static const int _orangeShade500Value = 0xFFFF7D1C;
  static const int _orangeShade600Value = 0xFFA76817;
  static const int _orangeShade700Value = 0xFF723A07;

  static const textError = Color(0xFFC23934);
  static const focusedColor = Color(0xFF1589EE);
  static const buttonsGreyColor = Color(0x800C0C0B);
  static const categoryNameColor = Color(0xFFEAEAF4);
  static const buttonsGreyFullyVisibleColor = Color(0xFF0C0C0B);
  static const progressBarColor = Color(0xFF5598D9);
}
