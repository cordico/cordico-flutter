// ignore_for_file: cascade_invocations

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:cordico_mobile/core/managers/offline_models.dart/offline_models.dart';
import 'package:cordico_mobile/core/managers/offlline_manager.dart';
import 'package:cordico_mobile/core/providers/global_providers.dart';
import 'package:core/core.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class GetHelpState extends StateNotifier<List<ContentModel>> {
  GetHelpState(this.ref) : super([]);

  bool _isLoading = false;
  bool get isLoading => _isLoading;

  bool _contentLoaded = false;
  bool get contentLoaded => _contentLoaded;

  final StateNotifierProviderRef ref;

  void reset() {
    state = [];
    _contentLoaded = false;
  }

  Future<void> getContent() async {
    final connectityCheck = await Connectivity().checkConnectivity();

    if (contentLoaded == false) {
      if (connectityCheck == ConnectivityResult.none) {
        final list = OfflineManager.getData(key: ModelsKeys.getHelp);
        if (list != null) {
          final contents = ContentOffline.fromJson(list).data ?? [];
          state =
              contents.map((e) => ContentModel.fromJson(e.toJson())).toList();
        } else {
          state = [];
        }
      } else {
        _isLoading = true;
        final repository = ref.watch(contentRepositoryProvider);
        final result =
            await repository.getOrganizationContentByCollectionTypeKey(
                collectionTypeKey: 'get_help',
                organizationId: environmentConfig.organizationId!);

        List<ContentModel>? resultContent;
        result.fold((error) {}, (state) => resultContent = state);

        _isLoading = false;

        if (resultContent != null) {
          final content = ContentOffline();
          content.data = resultContent;

          await OfflineManager.persistData(
            key: ModelsKeys.getHelp,
            value: content.toJson(),
          );

          state = resultContent!
              .map((e) => ContentModel.fromJson(e.toJson()))
              .toList();
        }
      }
      _contentLoaded = true;
    }
  }
}

final getHelpContentProvider =
    StateNotifierProvider<GetHelpState, List<ContentModel>>((ref) =>
        GetHelpState(ref));

/*final getHelpContentFuture = FutureProvider<List<ContentModel>?>((ref) async {
  final repository = ref.watch(contentRepositoryProvider);

  final result =
      await repository.getContentByContentType([kContentTypeGetHelpId]);
  List<ContentModel>? resultContent;
  result.fold((error) {}, (state) {
    resultContent = state;
  });
  return resultContent;
});*/

/*final getHelpForOrganizationContentFuture =
    FutureProvider<List<ContentModel>?>((ref) async {
  final repository = ref.watch(contentRepositoryProvider);

  final result = await repository.getOrganizationContentByCollectionTypeKey(
      collectionTypeKey: 'get_help',
      organizationId: environmentConfig.organizationId!);
  List<ContentModel>? resultContent;
  result.fold((error) {}, (state) {
    resultContent = state;
  });
  return resultContent;
});
*/