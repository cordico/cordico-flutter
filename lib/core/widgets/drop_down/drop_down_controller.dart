import 'package:flutter/material.dart';

class DropdownController extends ValueNotifier<List<String>> {
  DropdownController(List<String> positions) : super(positions);

  List<String> get positions => value;

  set positions(List<String> positions) {
    value = positions;
  }
}
