// ignore_for_file: lines_longer_than_80_chars

import 'package:core/core.dart';
import 'package:core/repositories/graphql_repository.dart';
import 'package:core/utils/failure.dart';
import 'package:core/utils/map_extension.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

abstract class IFeedbackRepository {
  //FEEDBACK TYPE
  Future<Either<Failure, List<FeedbackTypeModel>?>> listFeedbackType(
      {FeedbackTypeQueryArguments? arguments, FetchPolicy? fetchPolicy});
  Future<Either<Failure, FeedbackTypeModel?>> getFeedbackType(
      {required FeedbackTypeByIdArguments arguments, FetchPolicy? fetchPolicy});
  Future<Either<Failure, FeedbackTypeModel?>> createFeedbackType(
      {required CreateOneFeedbackTypeMutationArguments arguments,
      FetchPolicy? fetchPolicy});
  Future<Either<Failure, FeedbackTypeModel?>> updateFeedbackType(
      {required UpdateOneFeedbackTypeMutationArguments arguments,
      FetchPolicy? fetchPolicy});
  Future<Either<Failure, FeedbackTypeModel?>> deleteFeedbackType(
      {required DeleteOneFeedbackTypeMutationArguments arguments,
      FetchPolicy? fetchPolicy});

  //FEEDBACK
  Future<Either<Failure, List<FeedbackModel>?>> listFeedback(
      {FeedbackQueryArguments? arguments, FetchPolicy? fetchPolicy});
  Future<Either<Failure, FeedbackModel?>> getFeedback(
      {required FeedbackByIdArguments arguments, FetchPolicy? fetchPolicy});
  Future<Either<Failure, FeedbackModel?>> createFeedback(
      {required CreateOneFeedbackMutationArguments arguments,
      FetchPolicy? fetchPolicy});
  Future<Either<Failure, FeedbackModel?>> deleteFeedback(
      {required DeleteOneFeedbackMutationArguments arguments,
      FetchPolicy? fetchPolicy});
}

class FeedbackRepository extends GraphQLRepository
    implements IFeedbackRepository {
  FeedbackRepository({required String url, required String userUrl,
    String? userToken,})
      : super(url: url, userUrl: userUrl, token: userToken,);

  //FEEDBACK TYPE
  @override
  Future<Either<Failure, List<FeedbackTypeModel>?>> listFeedbackType(
      {FeedbackTypeQueryArguments? arguments, FetchPolicy? fetchPolicy}) async {
    try {
      final query = FeedbackTypeQueryQuery(
          variables: arguments ?? FeedbackTypeQueryArguments());
      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);
      final response = await graphqlClient.query<FeedbackTypeModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: fetchPolicy ?? FetchPolicy.cacheAndNetwork));
      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }
      if (response.data != null) {
        final root = FeedbackTypeQuery$QueryRoot.fromJson(response.data!);
        return Right(root.feedbackType
            .map((e) => FeedbackTypeModel.fromJson(e.toJson()))
            .toList());
      }
      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, FeedbackTypeModel?>> getFeedbackType(
      {required FeedbackTypeByIdArguments arguments,
      FetchPolicy? fetchPolicy}) async {
    try {
      final query = FeedbackTypeByIdQuery(variables: arguments);
      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);
      final response = await graphqlClient.query<FeedbackTypeModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: fetchPolicy ?? FetchPolicy.cacheAndNetwork));
      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }
      if (response.data != null) {
        final root = FeedbackTypeById$QueryRoot.fromJson(response.data!);
        if (root.feedbackTypeByPk != null) {
          return Right(root.feedbackTypeByPk);
        }
      }
      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, FeedbackTypeModel?>> createFeedbackType(
      {required CreateOneFeedbackTypeMutationArguments arguments,
      FetchPolicy? fetchPolicy}) async {
    try {
      final query = CreateOneFeedbackTypeMutationMutation(variables: arguments);
      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);
      final response = await graphqlClient.query<FeedbackTypeModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: fetchPolicy ?? FetchPolicy.cacheAndNetwork));
      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }
      if (response.data != null) {
        final root =
            CreateOneFeedbackTypeMutation$MutationRoot.fromJson(response.data!);
        if (root.insertFeedbackTypeOne != null) {
          return Right(
              FeedbackTypeModel.fromJson(root.insertFeedbackTypeOne!.toJson()));
        }
      }
      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, FeedbackTypeModel?>> updateFeedbackType(
      {required UpdateOneFeedbackTypeMutationArguments arguments,
      FetchPolicy? fetchPolicy}) async {
    try {
      final query = UpdateOneFeedbackTypeMutationMutation(variables: arguments);
      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);
      final response = await graphqlClient.query<FeedbackTypeModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: fetchPolicy ?? FetchPolicy.cacheAndNetwork));
      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }
      if (response.data != null) {
        final root =
            UpdateOneFeedbackTypeMutation$MutationRoot.fromJson(response.data!);
        if (root.updateFeedbackTypeByPk != null) {
          return Right(FeedbackTypeModel.fromJson(
              root.updateFeedbackTypeByPk!.toJson()));
        }
      }
      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, FeedbackTypeModel?>> deleteFeedbackType(
      {required DeleteOneFeedbackTypeMutationArguments arguments,
      FetchPolicy? fetchPolicy}) async {
    try {
      final query = DeleteOneFeedbackTypeMutationMutation(variables: arguments);
      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);
      final response = await graphqlClient.query<FeedbackTypeModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: fetchPolicy ?? FetchPolicy.cacheAndNetwork));
      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }
      if (response.data != null) {
        final root =
            DeleteOneFeedbackTypeMutation$MutationRoot.fromJson(response.data!);
        if (root.deleteFeedbackTypeByPk != null) {
          return Right(FeedbackTypeModel.fromJson(
              root.deleteFeedbackTypeByPk!.toJson()));
        }
      }
      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  //FEEDBACK
  @override
  Future<Either<Failure, List<FeedbackModel>?>> listFeedback(
      {FeedbackQueryArguments? arguments, FetchPolicy? fetchPolicy}) async {
    try {
      final query =
          FeedbackQueryQuery(variables: arguments ?? FeedbackQueryArguments());
      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);
      final response = await graphqlClient.query<FeedbackModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: fetchPolicy ?? FetchPolicy.cacheAndNetwork));
      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }
      if (response.data != null) {
        final root = FeedbackQuery$QueryRoot.fromJson(response.data!);
        return Right(root.feedback
            .map((e) => FeedbackModel.fromJson(e.toJson()))
            .toList());
      }
      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, FeedbackModel?>> getFeedback(
      {required FeedbackByIdArguments arguments,
      FetchPolicy? fetchPolicy}) async {
    try {
      final query = FeedbackByIdQuery(variables: arguments);
      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);
      final response = await graphqlClient.query<FeedbackModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: fetchPolicy ?? FetchPolicy.cacheAndNetwork));
      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }
      if (response.data != null) {
        final root = FeedbackById$QueryRoot.fromJson(response.data!);
        if (root.feedbackByPk != null) {
          return Right(root.feedbackByPk);
        }
      }
      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, FeedbackModel?>> createFeedback(
      {required CreateOneFeedbackMutationArguments arguments,
      FetchPolicy? fetchPolicy}) async {
    try {
      final query = CreateOneFeedbackMutationMutation(variables: arguments);
      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);
      final response = await graphqlClient.query<FeedbackModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: fetchPolicy ?? FetchPolicy.cacheAndNetwork));
      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }
      if (response.data != null) {
        final root =
            CreateOneFeedbackMutation$MutationRoot.fromJson(response.data!);
        if (root.insertFeedbackOne != null) {
          return Right(
              FeedbackModel.fromJson(root.insertFeedbackOne!.toJson()));
        }
      }
      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, FeedbackModel?>> deleteFeedback(
      {required DeleteOneFeedbackMutationArguments arguments,
      FetchPolicy? fetchPolicy}) async {
    try {
      final query = DeleteOneFeedbackMutationMutation(variables: arguments);
      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);
      final response = await graphqlClient.query<FeedbackModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: fetchPolicy ?? FetchPolicy.cacheAndNetwork));
      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }
      if (response.data != null) {
        final root =
            DeleteOneFeedbackMutation$MutationRoot.fromJson(response.data!);
        if (root.deleteFeedbackByPk != null) {
          return Right(
              FeedbackModel.fromJson(root.deleteFeedbackByPk!.toJson()));
        }
      }
      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }
}
