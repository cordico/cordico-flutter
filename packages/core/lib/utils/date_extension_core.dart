import 'package:intl/intl.dart';

class DateExtensionCore {
  static DateTime toLocalTimeZone(DateTime date) {
    final hours = DateTime.now().timeZoneOffset.inHours;
    return date.subtract(Duration(hours: hours));
  }

  static DateTime formatDBDate(String dateString) {
    final dateFormat = DateFormat('yyyy-MM-ddTHH:mm:ss.SSSZ');
    final date = dateFormat.parse(dateString, true).toLocal();
    return date;
  }

  static DateTime? getDate(String dateString, {String format = 'dd/MM/yyyy'}) {
    final dateFormat = DateFormat(format);
    try {
      final date = dateFormat.parse(dateString, true).toLocal();
      return date;
    } catch (err) {
      return null;
    }
  }

  static String toDBString(
      {required DateTime date, String format = 'yyyy-MM-dd HH:mm:ss'}) {
    final dateFormat = DateFormat(format);
    return dateFormat.format(date);
  }

  static String toDBPerfectString(
      {required DateTime date, String format = 'yyyy-MM-ddTHH:mm:ss'}) {
    final dateFormat = DateFormat(format);
    return dateFormat.format(date);
  }

  static String? formatDate(
      {DateTime? date, String format = 'dd-MM-yyyy', String? stringDate}) {
    if (stringDate != null) {
      return DateExtensionCore.formatDate(
          date: DateExtensionCore.formatDBDate(stringDate), format: format);
    }
    if (date != null) {
      return DateFormat(format).format(date);
    }
    return null;
  }
}
