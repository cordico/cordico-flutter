// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'organization_by_id.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrganizationByID$QueryRoot$OrganizationByPk
    _$OrganizationByID$QueryRoot$OrganizationByPkFromJson(
        Map<String, dynamic> json) {
  return OrganizationByID$QueryRoot$OrganizationByPk()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?
    ..data = json['data']
    ..organizationTypeId = json['organization_type_id'] as String
    ..portalFqdn = json['portal_fqdn'] as String?
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String);
}

Map<String, dynamic> _$OrganizationByID$QueryRoot$OrganizationByPkToJson(
        OrganizationByID$QueryRoot$OrganizationByPk instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'description': instance.description,
      'data': instance.data,
      'organization_type_id': instance.organizationTypeId,
      'portal_fqdn': instance.portalFqdn,
      'created_at': instance.createdAt.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
    };

OrganizationByID$QueryRoot _$OrganizationByID$QueryRootFromJson(
    Map<String, dynamic> json) {
  return OrganizationByID$QueryRoot()
    ..organizationByPk = json['organization_by_pk'] == null
        ? null
        : OrganizationByID$QueryRoot$OrganizationByPk.fromJson(
            json['organization_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$OrganizationByID$QueryRootToJson(
        OrganizationByID$QueryRoot instance) =>
    <String, dynamic>{
      'organization_by_pk': instance.organizationByPk?.toJson(),
    };

OrganizationByIDArguments _$OrganizationByIDArgumentsFromJson(
    Map<String, dynamic> json) {
  return OrganizationByIDArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$OrganizationByIDArgumentsToJson(
        OrganizationByIDArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
