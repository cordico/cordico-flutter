import 'package:flutter/material.dart';

class CordicoBottomAppBarItem {
  const CordicoBottomAppBarItem({
    required this.icon,
    this.text,
    this.textColor,
  });

  final Widget icon;
  final String? text;
  final Color? textColor;
}
