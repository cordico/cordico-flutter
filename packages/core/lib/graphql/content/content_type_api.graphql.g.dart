// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'content_type_api.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContentTypeQuery$QueryRoot$ContentType
    _$ContentTypeQuery$QueryRoot$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return ContentTypeQuery$QueryRoot$ContentType()
    ..id = json['id'] as String
    ..name = json['name'] as String
    ..description = json['description'] as String?
    ..schema = json['schema']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..creatorId = json['creator_id'] as String
    ..updaterId = json['updater_id'] as String?
    ..parentTypeId = json['parent_type_id'] as String?
    ..isShared = json['is_shared'] as bool
    ..supportsCategory = json['supports_category'] as bool
    ..key = json['key'] as String;
}

Map<String, dynamic> _$ContentTypeQuery$QueryRoot$ContentTypeToJson(
        ContentTypeQuery$QueryRoot$ContentType instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'schema': instance.schema,
      'created_at': instance.createdAt.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'creator_id': instance.creatorId,
      'updater_id': instance.updaterId,
      'parent_type_id': instance.parentTypeId,
      'is_shared': instance.isShared,
      'supports_category': instance.supportsCategory,
      'key': instance.key,
    };

ContentTypeQuery$QueryRoot _$ContentTypeQuery$QueryRootFromJson(
    Map<String, dynamic> json) {
  return ContentTypeQuery$QueryRoot()
    ..contentType = (json['content_type'] as List<dynamic>)
        .map((e) => ContentTypeQuery$QueryRoot$ContentType.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$ContentTypeQuery$QueryRootToJson(
        ContentTypeQuery$QueryRoot instance) =>
    <String, dynamic>{
      'content_type': instance.contentType.map((e) => e.toJson()).toList(),
    };
