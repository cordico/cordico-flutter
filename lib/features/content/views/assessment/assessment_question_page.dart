import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/assessment/assessment_answer.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/models/schema/content_schema.dart';
import 'package:core/utils/map_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_segment/flutter_segment.dart';

class AssessmentQuestionPage extends StatefulWidget {
  const AssessmentQuestionPage({
    Key? key,
    required this.question,
    required this.onCheckedChanged,
  }) : super(key: key);

  final ContentQuery$QueryRoot$Content question;
  final Function(AnswerSchema) onCheckedChanged;

  @override
  State createState() => AssessmentQuestionPageState();
}

class AssessmentQuestionPageState extends State<AssessmentQuestionPage> {
  int? _checkedAnswerIndex;

  List<AnswerSchema> _answersList = [];
  ContentSchema? contentSchema;

  @override
  void initState() {
    super.initState();
    _checkedAnswerIndex = null;

    removeNullAndEmptyParams(widget.question.data as Map<String, dynamic>);

    contentSchema =
        ContentSchema.fromJson(widget.question.data as Map<String, dynamic>);

    if (contentSchema != null) {
      contentSchema?.answers?.sort((a, b) {
        return a.getIndex().compareTo(b.getIndex());
      });

      _answersList = List.from(contentSchema?.answers ?? <AnswerSchema>[]);

      //_answersList.forEach((element) {});
    }
  }

  @override
  Widget build(BuildContext context) {
    Segment.screen(
        screenName: 'Assessment Question',
        properties: <String, dynamic>{
          'type': contentSchema?.type,
          'title': contentSchema?.title
        });

    return contentSchema != null
        ? ListView.builder(
            itemCount: _answersList.length + 1,
            itemBuilder: (context, index) {
              if (index == 0) {
                return _buildQuestionText(context);
              }

              return _buildAnswerOption(context, _answersList[index - 1]);
            },
          )
        : Container();
  }

  Widget _buildQuestionText(BuildContext context) {
    Segment.track(eventName: 'View Question', properties: <String, dynamic>{
      'type': contentSchema?.questionType,
      'text': contentSchema?.questionText
    });

    return Padding(
      padding: const EdgeInsets.only(bottom: 42),
      child: Text(
        contentSchema?.questionText ?? '',
        style: ThemeStyles.sectionTitle(context),
      ),
    );
  }

  Widget _buildAnswerOption(
    BuildContext context,
    AnswerSchema answer,
  ) {
    final index = answer.getIndex();
    return AssessmentAnswer(
      onTap: () {
        setState(() {
          _checkedAnswerIndex = index;
          widget.onCheckedChanged(answer);
        });
      },
      isChecked: _checkedAnswerIndex == index,
      labelText: answer.text ?? '',
    );
  }
}
