// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'user_type_by_id.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserTypeById$QueryRoot$UserTypeByPk
    _$UserTypeById$QueryRoot$UserTypeByPkFromJson(Map<String, dynamic> json) {
  return UserTypeById$QueryRoot$UserTypeByPk()
    ..id = json['id'] as String
    ..identitySchema = json['identity_schema'] as String
    ..key = json['key'] as String
    ..name = json['name'] as String
    ..schema = json['schema'];
}

Map<String, dynamic> _$UserTypeById$QueryRoot$UserTypeByPkToJson(
        UserTypeById$QueryRoot$UserTypeByPk instance) =>
    <String, dynamic>{
      'id': instance.id,
      'identity_schema': instance.identitySchema,
      'key': instance.key,
      'name': instance.name,
      'schema': instance.schema,
    };

UserTypeById$QueryRoot _$UserTypeById$QueryRootFromJson(
    Map<String, dynamic> json) {
  return UserTypeById$QueryRoot()
    ..userTypeByPk = json['user_type_by_pk'] == null
        ? null
        : UserTypeById$QueryRoot$UserTypeByPk.fromJson(
            json['user_type_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UserTypeById$QueryRootToJson(
        UserTypeById$QueryRoot instance) =>
    <String, dynamic>{
      'user_type_by_pk': instance.userTypeByPk?.toJson(),
    };

UserTypeByIdArguments _$UserTypeByIdArgumentsFromJson(
    Map<String, dynamic> json) {
  return UserTypeByIdArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$UserTypeByIdArgumentsToJson(
        UserTypeByIdArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
