// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'organization_asset_by_organization.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$Asset
    _$OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$AssetFromJson(
        Map<String, dynamic> json) {
  return OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$Asset()
    ..assetTypeId = json['asset_type_id'] as String
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..creatorId = json['creator_id'] as String
    ..data = json['data']
    ..fileName = json['file_name'] as String
    ..hash = json['hash'] as String?
    ..id = json['id'] as String
    ..mimeType = json['mime_type'] as String
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..updaterId = json['updater_id'] as String?
    ..url = json['url'] as String?;
}

Map<String, dynamic>
    _$OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$AssetToJson(
            OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$Asset
                instance) =>
        <String, dynamic>{
          'asset_type_id': instance.assetTypeId,
          'created_at': instance.createdAt.toIso8601String(),
          'creator_id': instance.creatorId,
          'data': instance.data,
          'file_name': instance.fileName,
          'hash': instance.hash,
          'id': instance.id,
          'mime_type': instance.mimeType,
          'updated_at': instance.updatedAt?.toIso8601String(),
          'updater_id': instance.updaterId,
          'url': instance.url,
        };

OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$Organization
    _$OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$OrganizationFromJson(
        Map<String, dynamic> json) {
  return OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$Organization()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?
    ..data = json['data']
    ..organizationTypeId = json['organization_type_id'] as String
    ..portalFqdn = json['portal_fqdn'] as String?
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String);
}

Map<String, dynamic>
    _$OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$OrganizationToJson(
            OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$Organization
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
          'data': instance.data,
          'organization_type_id': instance.organizationTypeId,
          'portal_fqdn': instance.portalFqdn,
          'created_at': instance.createdAt.toIso8601String(),
          'updated_at': instance.updatedAt?.toIso8601String(),
        };

OrganizationAssetByOrganization$QueryRoot$OrganizationAsset
    _$OrganizationAssetByOrganization$QueryRoot$OrganizationAssetFromJson(
        Map<String, dynamic> json) {
  return OrganizationAssetByOrganization$QueryRoot$OrganizationAsset()
    ..asset = OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$Asset
        .fromJson(json['asset'] as Map<String, dynamic>)
    ..organization =
        OrganizationAssetByOrganization$QueryRoot$OrganizationAsset$Organization
            .fromJson(json['organization'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$OrganizationAssetByOrganization$QueryRoot$OrganizationAssetToJson(
            OrganizationAssetByOrganization$QueryRoot$OrganizationAsset
                instance) =>
        <String, dynamic>{
          'asset': instance.asset.toJson(),
          'organization': instance.organization.toJson(),
        };

OrganizationAssetByOrganization$QueryRoot
    _$OrganizationAssetByOrganization$QueryRootFromJson(
        Map<String, dynamic> json) {
  return OrganizationAssetByOrganization$QueryRoot()
    ..organizationAsset = (json['organization_asset'] as List<dynamic>)
        .map((e) => OrganizationAssetByOrganization$QueryRoot$OrganizationAsset
            .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$OrganizationAssetByOrganization$QueryRootToJson(
        OrganizationAssetByOrganization$QueryRoot instance) =>
    <String, dynamic>{
      'organization_asset':
          instance.organizationAsset.map((e) => e.toJson()).toList(),
    };

OrganizationAssetByOrganizationArguments
    _$OrganizationAssetByOrganizationArgumentsFromJson(
        Map<String, dynamic> json) {
  return OrganizationAssetByOrganizationArguments(
    ids: (json['ids'] as List<dynamic>?)?.map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$OrganizationAssetByOrganizationArgumentsToJson(
        OrganizationAssetByOrganizationArguments instance) =>
    <String, dynamic>{
      'ids': instance.ids,
    };
