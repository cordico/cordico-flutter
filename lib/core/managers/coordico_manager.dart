import 'package:cordico_mobile/core/managers/cordico_url.dart';
import 'package:cordico_mobile/core/managers/cordico_utils.dart';
import 'package:cordico_mobile/core/widgets/bottom_navigation_bar/cordico_bottom_app_bar.dart';
import 'package:cordico_mobile/core/widgets/drop_down/cordico_filters_widget.dart';
import 'package:cordico_mobile/features/connect/logic/directory_state.dart';
import 'package:cordico_mobile/features/content/views/assessment/model/assessment_results.dart';
import 'package:cordico_mobile/features/department/logic/department_provider.dart';
import 'package:cordico_mobile/features/hotlines/logic/get_help_state.dart';
import 'package:cordico_mobile/features/toolkit/logic/organization_provider.dart';
import 'package:core/core.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/graphql/content/content_type_api.dart';
import 'package:core/graphql/user/user_by_id.graphql.dart';
import 'package:core/utils/constants.dart';
import 'package:core/utils/map_extension.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';
// ignore: implementation_imports
import 'package:vrouter/src/core/extended_context.dart';

CordicoManager global = CordicoManager();

CordicoUtils utils = CordicoUtils();

class CordicoManager {
  late CordicoAppBarState tabBarState;
  late SharedPreferences prefs;

  LocationPermission? locationPermission;
  OrganizationSchema? organizationSchema;
  UserSchema? user;
  UserById$QueryRoot$UserByPk? globalUser;

  TextEditingController discoverTextController = TextEditingController();

  Filters discoverFilters = Filters(contentTypes: [], categoriesTypes: []);

  List<AssessmentResult> assessmentsCompleted = [];
  List<String> contentTypeIds = [];
  List<ContentTypeQuery$QueryRoot$ContentType>? contentTypes = [];

  void resetAll(WidgetRef ref) {
    // CLEARING ALL SHARED PREFERENCES
    prefs.clear();
    ref.watch(organizationStateNotifierProvider.notifier).reset();
    ref.watch(organizationContactsStateNotifierProvider.notifier).reset();
    ref.watch(allContactsProvider.notifier).reset();
    ref.watch(departmentPageStateNotifierProvider.notifier).reset();
    ref.watch(getHelpContentProvider.notifier).reset();
  }

  Color colorFromCategory(
      ContentQuery$QueryRoot$Content$ContentCategories$Category category) {
    if (category.id == 'e4e3394e-2902-4ce0-a495-48c50c9c8962') {
      //Mental Wellness
      return const Color(0xFF96C122);
    } else if (category.id == '19d5527b-abcb-424d-bf93-55b414166c06') {
      //Healthy Relationships
      return const Color(0xFF2D3184);
    } else if (category.id == '350600f0-cd9c-4325-a14f-cb4c27df124d') {
      //Personal finance
      return const Color(0xFF008D39);
    } else if (category.id == '53c91452-4d58-4cd7-af4e-cea3483d2e95') {
      //Mindfulness
      return const Color(0xFF52BBB5);
    } else if (category.id == 'd3fb457e-b4a0-4801-b6f0-ed5f208a53db') {
      //Health/fitness
      return const Color(0xFFF39300);
    } else if (category.id == '3881818d-5d0c-4d8e-b276-37e74614f858') {
      //Substance dependancy
      return const Color(0xFF9E1820);
    } else if (category.id == 'ae22e761-b597-4fe9-a532-26999370b32f') {
      //Career managment
      return const Color(0xFF672783);
    } else if (category.id == '142385ff-185c-406f-81ca-0ccba39750d0') {
      //Dr Gilmartin
      return const Color(0xFFBD1822);
    } else if (category.id == '35c80756-4579-48f2-82bb-232fe1d7fc05') {
      //Additional Resourcer
      return const Color(0xFFFFFFFF);
    }
    return const Color(0xFF008D39);
  }

  Color colorFromCategoryModel(CategoryModel category) {
    if (category.id == 'e4e3394e-2902-4ce0-a495-48c50c9c8962') {
      //Mental Wellness
      return const Color(0xFF96C122);
    } else if (category.id == '19d5527b-abcb-424d-bf93-55b414166c06') {
      //Healthy Relationships
      return const Color(0xFF2D3184);
    } else if (category.id == '350600f0-cd9c-4325-a14f-cb4c27df124d') {
      //Personal finance
      return const Color(0xFF008D39);
    } else if (category.id == '53c91452-4d58-4cd7-af4e-cea3483d2e95') {
      //Mindfulness
      return const Color(0xFF52BBB5);
    } else if (category.id == 'd3fb457e-b4a0-4801-b6f0-ed5f208a53db') {
      //Health/fitness
      return const Color(0xFFF39300);
    } else if (category.id == '3881818d-5d0c-4d8e-b276-37e74614f858') {
      //Substance dependancy
      return const Color(0xFF9E1820);
    } else if (category.id == 'ae22e761-b597-4fe9-a532-26999370b32f') {
      //Career managment
      return const Color(0xFF672783);
    } else if (category.id == '142385ff-185c-406f-81ca-0ccba39750d0') {
      //Dr Gilmartin
      return const Color(0xFFBD1822);
    } else if (category.id == '35c80756-4579-48f2-82bb-232fe1d7fc05') {
      //Additional Resourcer
      return const Color(0xFFFFFFFF);
    }
    return const Color(0xFF008D39);
  }

  String imageFromCategory(CategoryModel category) {
    if (category.id == 'e4e3394e-2902-4ce0-a495-48c50c9c8962') {
      //Mental Wellness
      return 'images/categories/mental_welness.png';
    } else if (category.id == '19d5527b-abcb-424d-bf93-55b414166c06') {
      //Healthy Relationships
      return 'images/categories/healthy_relationship.png';
    } else if (category.id == '350600f0-cd9c-4325-a14f-cb4c27df124d') {
      //Personal finance
      return 'images/categories/personal_finance.png';
    } else if (category.id == '53c91452-4d58-4cd7-af4e-cea3483d2e95') {
      //Mindfulness
      return 'images/categories/mindfulness.png';
    } else if (category.id == 'd3fb457e-b4a0-4801-b6f0-ed5f208a53db') {
      //Health/fitness
      return 'images/categories/health_fitness.png';
    } else if (category.id == '3881818d-5d0c-4d8e-b276-37e74614f858') {
      //Substance dependancy
      return 'images/categories/substance_dependency.png';
    } else if (category.id == 'ae22e761-b597-4fe9-a532-26999370b32f') {
      //Career managment
      return 'images/categories/career_management.png';
    } else if (category.id == '142385ff-185c-406f-81ca-0ccba39750d0') {
      //Dr Gilmartin
      return 'images/categories/dr_gilmartin_category.png';
    } else if (category.id == '35c80756-4579-48f2-82bb-232fe1d7fc05') {
      //Additional Resourcer
      return 'images/categories/additional_resources.png';
    }
    return 'images/categories/dr_gilmartin_category.png';
  }

  void goTo(
    BuildContext context, {
    required ContentQuery$QueryRoot$Content content,
    int? index,
    String? bulletinSource = 'department',
    String? contentSource = 'general',
  }) {
    if (content.contentType.id == kContentTypeGuideId) {
      context.vRouter.to('content_summary/${content.id}/$contentSource');
    } else if (content.contentType.id == kContentTypeAssessmentId) {
      context.vRouter.to('assessment/${content.id}');
    } else if (content.contentType.id == kContentTypeBulletinId) {
      context.vRouter.to('bulletin/${content.id}/$bulletinSource');
    } else if (content.contentType.id == kContentTypeCovidId) {
      removeNullAndEmptyParams(content.data as Map<String, dynamic>);
      final schema =
          ContentSchema.fromJson(content.data as Map<String, dynamic>);
      CordicoUrl.openUrl(url: schema.link ?? '');
    } else if (content.contentType.id == kContentTypeEAPId) {
      context.vRouter.to('content/${content.id}/0/$contentSource');
    }
  }
}
