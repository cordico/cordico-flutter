// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'collection_content.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CollectionContentQuery$QueryRoot$CollectionContent$Collection$CollectionType
    _$CollectionContentQuery$QueryRoot$CollectionContent$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Collection$CollectionTypeToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionContentQuery$QueryRoot$CollectionContent$Collection
    _$CollectionContentQuery$QueryRoot$CollectionContent$CollectionFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Collection()
    ..id = json['id'] as String
    ..name = json['name'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : CollectionContentQuery$QueryRoot$CollectionContent$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$CollectionToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Collection
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
          'collection_type': instance.collectionType?.toJson(),
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentType
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentTypeToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'is_shared': instance.isShared,
          'description': instance.description,
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentType
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentTypeToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'is_shared': instance.isShared,
          'description': instance.description,
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ChildContent
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ChildContentFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ChildContent()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ChildContentToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ChildContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ParentContent
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ParentContentFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ParentContentToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ParentContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'index': instance.index,
          'part_id': instance.partId,
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories$Category
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories$CategoryToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories()
    ..category =
        CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories$Category
            .fromJson(json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategoriesToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories
                instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentState
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentStateFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentStateToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentState
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$CollectionToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$CollectionsFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections()
    ..collection =
        CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection
            .fromJson(json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$CollectionsToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections
                instance) =>
        <String, dynamic>{
          'collection': instance.collection.toJson(),
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPartFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart()
    ..contentType =
        CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentType
            .fromJson(json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) =>
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ChildContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) =>
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ParentContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentState =
        CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentState
            .fromJson(json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) =>
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPartToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart
                instance) =>
        <String, dynamic>{
          'content_type': instance.contentType.toJson(),
          'updated_at': instance.updatedAt?.toIso8601String(),
          'name': instance.name,
          'is_shared': instance.isShared,
          'id': instance.id,
          'data': instance.data,
          'created_at': instance.createdAt.toIso8601String(),
          'child_content':
              instance.childContent.map((e) => e.toJson()).toList(),
          'parent_content':
              instance.parentContent.map((e) => e.toJson()).toList(),
          'content_categories':
              instance.contentCategories.map((e) => e.toJson()).toList(),
          'content_state': instance.contentState.toJson(),
          'collections': instance.collections.map((e) => e.toJson()).toList(),
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContentFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent()
    ..contentPart =
        CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart
            .fromJson(json['content_part'] as Map<String, dynamic>)
    ..id = json['id'] as String
    ..index = json['index'] as int;
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContentToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent
                instance) =>
        <String, dynamic>{
          'content_part': instance.contentPart.toJson(),
          'id': instance.id,
          'index': instance.index,
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$ParentContent
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ParentContentFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ParentContentToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ParentContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'index': instance.index,
          'part_id': instance.partId,
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories$Category
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories$CategoryToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories()
    ..category =
        CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories$Category
            .fromJson(json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategoriesToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories
                instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentState
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentStateFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentStateToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentState
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection$CollectionType
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection$CollectionTypeToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$CollectionToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$CollectionsFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections()
    ..collection =
        CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection
            .fromJson(json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionContentQuery$QueryRoot$CollectionContent$Content$CollectionsToJson(
            CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections
                instance) =>
        <String, dynamic>{
          'collection': instance.collection.toJson(),
        };

CollectionContentQuery$QueryRoot$CollectionContent$Content
    _$CollectionContentQuery$QueryRoot$CollectionContent$ContentFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent$Content()
    ..contentType =
        CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentType
            .fromJson(json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) =>
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) =>
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ParentContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentState =
        CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentState
            .fromJson(json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) =>
            CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String,
    dynamic> _$CollectionContentQuery$QueryRoot$CollectionContent$ContentToJson(
        CollectionContentQuery$QueryRoot$CollectionContent$Content instance) =>
    <String, dynamic>{
      'content_type': instance.contentType.toJson(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'name': instance.name,
      'is_shared': instance.isShared,
      'id': instance.id,
      'data': instance.data,
      'created_at': instance.createdAt.toIso8601String(),
      'child_content': instance.childContent.map((e) => e.toJson()).toList(),
      'parent_content': instance.parentContent.map((e) => e.toJson()).toList(),
      'content_categories':
          instance.contentCategories.map((e) => e.toJson()).toList(),
      'content_state': instance.contentState.toJson(),
      'collections': instance.collections.map((e) => e.toJson()).toList(),
    };

CollectionContentQuery$QueryRoot$CollectionContent
    _$CollectionContentQuery$QueryRoot$CollectionContentFromJson(
        Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot$CollectionContent()
    ..collection =
        CollectionContentQuery$QueryRoot$CollectionContent$Collection.fromJson(
            json['collection'] as Map<String, dynamic>)
    ..id = json['id'] as String
    ..content =
        CollectionContentQuery$QueryRoot$CollectionContent$Content.fromJson(
            json['content'] as Map<String, dynamic>);
}

Map<String, dynamic> _$CollectionContentQuery$QueryRoot$CollectionContentToJson(
        CollectionContentQuery$QueryRoot$CollectionContent instance) =>
    <String, dynamic>{
      'collection': instance.collection.toJson(),
      'id': instance.id,
      'content': instance.content.toJson(),
    };

CollectionContentQuery$QueryRoot _$CollectionContentQuery$QueryRootFromJson(
    Map<String, dynamic> json) {
  return CollectionContentQuery$QueryRoot()
    ..collectionContent = (json['collection_content'] as List<dynamic>)
        .map((e) => CollectionContentQuery$QueryRoot$CollectionContent.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$CollectionContentQuery$QueryRootToJson(
        CollectionContentQuery$QueryRoot instance) =>
    <String, dynamic>{
      'collection_content':
          instance.collectionContent.map((e) => e.toJson()).toList(),
    };

CollectionContentQueryArguments _$CollectionContentQueryArgumentsFromJson(
    Map<String, dynamic> json) {
  return CollectionContentQueryArguments(
    contentStateNames: (json['contentStateNames'] as List<dynamic>?)
        ?.map((e) => e as String?)
        .toList(),
    contentOffset: json['contentOffset'] as int?,
    contentLimit: json['contentLimit'] as int?,
  );
}

Map<String, dynamic> _$CollectionContentQueryArgumentsToJson(
        CollectionContentQueryArguments instance) =>
    <String, dynamic>{
      'contentStateNames': instance.contentStateNames,
      'contentOffset': instance.contentOffset,
      'contentLimit': instance.contentLimit,
    };
