// ignore_for_file: lines_longer_than_80_chars, cascade_invocations

import 'package:built_value/json_object.dart';
import 'package:core/core.dart';
import 'package:core/graphql/user/create_one_user_mutation.dart';
import 'package:core/graphql/user/delete_one_user_mutation.dart';
import 'package:core/graphql/user/update_one_user_mutation.dart';
import 'package:core/graphql/user/user_by_id.dart';
import 'package:core/graphql/user/user_by_identifier.dart';
import 'package:core/graphql/user/user_by_user_type.dart';
import 'package:core/graphql/user/user_query.dart';
import 'package:core/graphql/user/user_type_by_id.dart';
import 'package:core/graphql/user/user_type_query.dart';
import 'package:core/repositories/graphql_repository.dart';
import 'package:core/utils/failure.dart';
import 'package:core/utils/map_extension.dart';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:kratos_api/kratos_api.dart';

abstract class IUserRepository {
  Future<Either<Failure, UserModel?>> getUserById(String id,
      {FetchPolicy? fetchPolicy});
  Future<Either<Failure, UserModel?>> getUserByIdentifier(String identifier,
      {FetchPolicy? fetchPolicy});
  Future<Either<Failure, List<UserModel>?>> getUsersByUserTypes(
      List<String> userTypes);
  Future<Either<Failure, List<UserModel>?>> userQuery();

  Future<Either<Failure, UserTypeModel?>> getUserTypeById(String id,
      {FetchPolicy? fetchPolicy});
  Future<Either<Failure, List<UserTypeModel>?>> userTypeQuery();

  //CRUD USER
  Future<Either<Failure, UserModel?>> createOneUser(
      {String? userTypeId,
      required String identifier,
      dynamic data,
      String? authorizationToken});
  Future<Either<Failure, UserModel?>> updateOneUser(
      {String? id,
      required String identifier,
      String? userTypeId,
      dynamic data,
      String? authorizationToken});
  Future<Either<Failure, UserModel?>> deleteOneUser(
      {String? id, String? authorizationToken});
}

class UserRepository extends GraphQLRepository implements IUserRepository {
  UserRepository({required String url, required String userUrl, required String
    kratosUrl,
    String? userToken,})
      : super(url: url, userUrl: userUrl, token: userToken,) {
    final dio = Dio(BaseOptions(baseUrl: kratosUrl));
    api = KratosApi(dio: dio, serializers: standardSerializers);
    vAlphaApi = api.getV0alpha2Api();
  }

  late KratosApi api;
  late V0alpha2Api vAlphaApi;

  @override
  Future<Either<Failure, UserModel?>> getUserById(String id,
      {FetchPolicy? fetchPolicy}) async {
    try {
      final query = UserByIdQuery(variables: UserByIdArguments(id: id));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<UserModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = UserById$QueryRoot.fromJson(response.data!);
        if (root.userByPk != null) {
          return Right(root.userByPk);
        }
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, UserModel?>> getUserByIdentifier(String identifier,
      {FetchPolicy? fetchPolicy}) async {
    try {
      final query = UserByIdentifierQuery(
          variables: UserByIdentifierArguments(identifier: identifier));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<UserModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = UserByIdentifier$QueryRoot.fromJson(response.data!);
        if (root.user.isNotEmpty) {
          return Right(UserModel.fromJson(root.user.first.toJson()));
        }
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<UserModel>?>> getUsersByUserTypes(
      List<String> userTypes) async {
    try {
      final query = UserByUserTypeQuery(
          variables: UserByUserTypeArguments(userTypes: userTypes));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<UserModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = UserByUserType$QueryRoot.fromJson(response.data!);
        return Right(
            root.user.map((e) => UserModel.fromJson(e.toJson())).toList());
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<UserModel>?>> userQuery() async {
    try {
      final query = UserQueryQuery();

      final response = await graphqlClient.query<UserModel>(QueryOptions(
          document: query.document, fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = UserQuery$QueryRoot.fromJson(response.data!);
        return Right(
            root.user.map((e) => UserModel.fromJson(e.toJson())).toList());
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, UserTypeModel?>> getUserTypeById(String id,
      {FetchPolicy? fetchPolicy}) async {
    try {
      final query = UserTypeByIdQuery(variables: UserTypeByIdArguments(id: id));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<UserTypeModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = UserTypeById$QueryRoot.fromJson(response.data!);
        if (root.userTypeByPk != null) {
          return Right(root.userTypeByPk);
        }
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<UserTypeModel>?>> userTypeQuery() async {
    try {
      final query = UserTypeQueryQuery();

      final response = await graphqlClient.query<UserTypeModel>(QueryOptions(
          document: query.document, fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = UserTypeQuery$QueryRoot.fromJson(response.data!);
        return Right(root.userType
            .map((e) => UserTypeModel.fromJson(e.toJson()))
            .toList());
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  //CRUD USER

  @override
  Future<Either<Failure, UserModel?>> createOneUser(
      {String? userTypeId,
      required String identifier,
      dynamic data,
      String? authorizationToken}) async {
    try {

      final query = CreateOneUserMutationMutation(
          variables: CreateOneUserMutationArguments(
        userTypeId: userTypeId,
        identifier: identifier,
        data: data,
      ));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      /*final userTypeEither = await getUserTypeById(userTypeId!);
      UserTypeById$QueryRoot$UserTypeByPk? userType;
      Failure? failure;
      userTypeEither.fold((l) => failure = l, (r) => userType = r);
      if (failure != null) {
        return Left(failure!);
      }

      final userSchema = userType!.identitySchema;
      String? linkEmail;
      AdminCreateIdentityBody? adminCreateIdentityBody;
      final dataMap = cast<Map>(data);
      if (userType!.key == 'organization') {
        final organizationIds = (dataMap['organization_ids'] as List).map((dynamic e) => e as String).toList();

        if (organizationIds.isEmpty) {
          // cannot create this here...
          return Left(EmptyFailure());
        }
        linkEmail = 'tjames@lexipol.com';
        adminCreateIdentityBody = AdminCreateIdentityBody((b) {
          b.schemaId = userSchema;
          b.traits = MapJsonObject({
            'email':linkEmail,
            'identifier': identifier
          });
        });
      } else {
        adminCreateIdentityBody = AdminCreateIdentityBody((b) {
          b.schemaId = userSchema;
          b.traits = MapJsonObject({
            'email':cast<String>(dataMap['email']),
            'identifier': identifier,
            'name': MapJsonObject({
              'first_name': cast<String>(dataMap['first_name']),
              'last_name': cast<String>(dataMap['last_name'])
            })
          });
        });
      }

      final createResponse = await vAlphaApi.adminCreateIdentity(
        adminCreateIdentityBody: adminCreateIdentityBody,
      );

      String? userServiceId;
      if (createResponse.data != null) {
        userServiceId = createResponse.data!.id;
        query.getVariablesMap().putIfAbsent('userServiceId', () => userServiceId);
        variables = query.variables.toJson();
        removeNullAndEmptyParams(variables);
      } else {
        // failed
        return Left(EmptyFailure());
      }*/

      final response = await userGraphQLClient.query<UserModel>(QueryOptions(
          document: query.document,
          variables: variables,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          fetchPolicy: FetchPolicy.noCache));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root =
            CreateOneUserMutation$MutationRoot.fromJson(response.data!);
        return getUserById(root.createOneUser.id,
            fetchPolicy: FetchPolicy.noCache);
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, UserModel?>> updateOneUser(
      {String? id,
      required String identifier,
      String? userTypeId,
      dynamic data,
      String? authorizationToken}) async {
    try {
      final query = UpdateOneUserMutationMutation(
          variables: UpdateOneUserMutationArguments(
        id: id,
        userTypeId: userTypeId,
        identifier: identifier,
        data: JsonFieldUpdateOperationsInput(kw$set: data),
      ));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await userGraphQLClient.query<UserModel>(QueryOptions(
          document: query.document,
          variables: variables,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          fetchPolicy: FetchPolicy.noCache));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root =
            UpdateOneUserMutation$MutationRoot.fromJson(response.data!);
        if (root.updateOneUser != null) {
          return getUserById(root.updateOneUser!.id,
              fetchPolicy: FetchPolicy.noCache);
        }
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, UserModel?>> deleteOneUser(
      {String? id, String? authorizationToken}) async {
    try {
      final query = DeleteOneUserMutationMutation(
          variables: DeleteOneUserMutationArguments(id: id));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await userGraphQLClient.query<UserModel>(QueryOptions(
          document: query.document,
          variables: variables,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          fetchPolicy: FetchPolicy.noCache));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        return const Right(null);
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }
}
