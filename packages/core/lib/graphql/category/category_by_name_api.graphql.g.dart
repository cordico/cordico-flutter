// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'category_by_name_api.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoryByName$QueryRoot$Category _$CategoryByName$QueryRoot$CategoryFromJson(
    Map<String, dynamic> json) {
  return CategoryByName$QueryRoot$Category()
    ..id = json['id'] as String
    ..name = json['name'] as String
    ..description = json['description'] as String
    ..key = json['key'] as String;
}

Map<String, dynamic> _$CategoryByName$QueryRoot$CategoryToJson(
        CategoryByName$QueryRoot$Category instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'key': instance.key,
    };

CategoryByName$QueryRoot _$CategoryByName$QueryRootFromJson(
    Map<String, dynamic> json) {
  return CategoryByName$QueryRoot()
    ..category = (json['category'] as List<dynamic>)
        .map((e) => CategoryByName$QueryRoot$Category.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$CategoryByName$QueryRootToJson(
        CategoryByName$QueryRoot instance) =>
    <String, dynamic>{
      'category': instance.category.map((e) => e.toJson()).toList(),
    };

CategoryByNameArguments _$CategoryByNameArgumentsFromJson(
    Map<String, dynamic> json) {
  return CategoryByNameArguments(
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$CategoryByNameArgumentsToJson(
        CategoryByNameArguments instance) =>
    <String, dynamic>{
      'name': instance.name,
    };
