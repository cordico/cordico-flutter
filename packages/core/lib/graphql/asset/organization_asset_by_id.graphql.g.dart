// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'organization_asset_by_id.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrganizationAssetById$QueryRoot$OrganizationAssetByPk$Asset
    _$OrganizationAssetById$QueryRoot$OrganizationAssetByPk$AssetFromJson(
        Map<String, dynamic> json) {
  return OrganizationAssetById$QueryRoot$OrganizationAssetByPk$Asset()
    ..assetTypeId = json['asset_type_id'] as String
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..creatorId = json['creator_id'] as String
    ..data = json['data']
    ..fileName = json['file_name'] as String
    ..hash = json['hash'] as String?
    ..id = json['id'] as String
    ..mimeType = json['mime_type'] as String
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..updaterId = json['updater_id'] as String?
    ..url = json['url'] as String?;
}

Map<String, dynamic>
    _$OrganizationAssetById$QueryRoot$OrganizationAssetByPk$AssetToJson(
            OrganizationAssetById$QueryRoot$OrganizationAssetByPk$Asset
                instance) =>
        <String, dynamic>{
          'asset_type_id': instance.assetTypeId,
          'created_at': instance.createdAt.toIso8601String(),
          'creator_id': instance.creatorId,
          'data': instance.data,
          'file_name': instance.fileName,
          'hash': instance.hash,
          'id': instance.id,
          'mime_type': instance.mimeType,
          'updated_at': instance.updatedAt?.toIso8601String(),
          'updater_id': instance.updaterId,
          'url': instance.url,
        };

OrganizationAssetById$QueryRoot$OrganizationAssetByPk$Organization
    _$OrganizationAssetById$QueryRoot$OrganizationAssetByPk$OrganizationFromJson(
        Map<String, dynamic> json) {
  return OrganizationAssetById$QueryRoot$OrganizationAssetByPk$Organization()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?
    ..data = json['data']
    ..organizationTypeId = json['organization_type_id'] as String
    ..portalFqdn = json['portal_fqdn'] as String?
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String);
}

Map<String, dynamic>
    _$OrganizationAssetById$QueryRoot$OrganizationAssetByPk$OrganizationToJson(
            OrganizationAssetById$QueryRoot$OrganizationAssetByPk$Organization
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
          'data': instance.data,
          'organization_type_id': instance.organizationTypeId,
          'portal_fqdn': instance.portalFqdn,
          'created_at': instance.createdAt.toIso8601String(),
          'updated_at': instance.updatedAt?.toIso8601String(),
        };

OrganizationAssetById$QueryRoot$OrganizationAssetByPk
    _$OrganizationAssetById$QueryRoot$OrganizationAssetByPkFromJson(
        Map<String, dynamic> json) {
  return OrganizationAssetById$QueryRoot$OrganizationAssetByPk()
    ..asset =
        OrganizationAssetById$QueryRoot$OrganizationAssetByPk$Asset.fromJson(
            json['asset'] as Map<String, dynamic>)
    ..organization =
        OrganizationAssetById$QueryRoot$OrganizationAssetByPk$Organization
            .fromJson(json['organization'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$OrganizationAssetById$QueryRoot$OrganizationAssetByPkToJson(
            OrganizationAssetById$QueryRoot$OrganizationAssetByPk instance) =>
        <String, dynamic>{
          'asset': instance.asset.toJson(),
          'organization': instance.organization.toJson(),
        };

OrganizationAssetById$QueryRoot _$OrganizationAssetById$QueryRootFromJson(
    Map<String, dynamic> json) {
  return OrganizationAssetById$QueryRoot()
    ..organizationAssetByPk = json['organization_asset_by_pk'] == null
        ? null
        : OrganizationAssetById$QueryRoot$OrganizationAssetByPk.fromJson(
            json['organization_asset_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$OrganizationAssetById$QueryRootToJson(
        OrganizationAssetById$QueryRoot instance) =>
    <String, dynamic>{
      'organization_asset_by_pk': instance.organizationAssetByPk?.toJson(),
    };

OrganizationAssetByIdArguments _$OrganizationAssetByIdArgumentsFromJson(
    Map<String, dynamic> json) {
  return OrganizationAssetByIdArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$OrganizationAssetByIdArgumentsToJson(
        OrganizationAssetByIdArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
