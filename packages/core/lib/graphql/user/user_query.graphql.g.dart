// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'user_query.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserQuery$QueryRoot$User _$UserQuery$QueryRoot$UserFromJson(
    Map<String, dynamic> json) {
  return UserQuery$QueryRoot$User()
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..id = json['id'] as String
    ..identifier = json['identifier'] as String
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..userServiceId = json['user_service_id'] as String?
    ..userTypeId = json['user_type_id'] as String;
}

Map<String, dynamic> _$UserQuery$QueryRoot$UserToJson(
        UserQuery$QueryRoot$User instance) =>
    <String, dynamic>{
      'data': instance.data,
      'created_at': instance.createdAt.toIso8601String(),
      'id': instance.id,
      'identifier': instance.identifier,
      'updated_at': instance.updatedAt?.toIso8601String(),
      'user_service_id': instance.userServiceId,
      'user_type_id': instance.userTypeId,
    };

UserQuery$QueryRoot _$UserQuery$QueryRootFromJson(Map<String, dynamic> json) {
  return UserQuery$QueryRoot()
    ..user = (json['user'] as List<dynamic>)
        .map(
            (e) => UserQuery$QueryRoot$User.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$UserQuery$QueryRootToJson(
        UserQuery$QueryRoot instance) =>
    <String, dynamic>{
      'user': instance.user.map((e) => e.toJson()).toList(),
    };
