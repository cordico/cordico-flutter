// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'content_by_category.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$ContentType extends JsonSerializable
    with EquatableMixin {
  ContentByCategory$QueryRoot$Content$ContentType();

  factory ContentByCategory$QueryRoot$Content$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$ContentTypeFromJson(json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$ContentTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentType
    extends JsonSerializable with EquatableMixin {
  ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentType();

  factory ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentTypeFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ChildContent
    extends JsonSerializable with EquatableMixin {
  ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ChildContent();

  factory ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ChildContentFromJson(
          json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ChildContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ParentContent
    extends JsonSerializable with EquatableMixin {
  ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ParentContent();

  factory ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ParentContentFromJson(
          json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ParentContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category();

  factory ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories
    extends JsonSerializable with EquatableMixin {
  ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories();

  factory ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategoriesFromJson(
          json);

  late ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category
      category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategoriesToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentState
    extends JsonSerializable with EquatableMixin {
  ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentState();

  factory ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentStateFromJson(
          json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentStateToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType();

  factory ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection();

  factory ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$CollectionFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections
    extends JsonSerializable with EquatableMixin {
  ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections();

  factory ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$CollectionsFromJson(
          json);

  late ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection
      collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$CollectionsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$ChildContent$ContentPart
    extends JsonSerializable with EquatableMixin {
  ContentByCategory$QueryRoot$Content$ChildContent$ContentPart();

  factory ContentByCategory$QueryRoot$Content$ChildContent$ContentPart.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPartFromJson(
          json);

  @JsonKey(name: 'content_type')
  late ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentType
      contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<
          ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ChildContent>
      childContent;

  @JsonKey(name: 'parent_content')
  late List<
          ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ParentContent>
      parentContent;

  @JsonKey(name: 'content_categories')
  late List<
          ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentState
      contentState;

  late List<
          ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections>
      collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPartToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$ChildContent extends JsonSerializable
    with EquatableMixin {
  ContentByCategory$QueryRoot$Content$ChildContent();

  factory ContentByCategory$QueryRoot$Content$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$ChildContentFromJson(json);

  @JsonKey(name: 'content_part')
  late ContentByCategory$QueryRoot$Content$ChildContent$ContentPart contentPart;

  late String id;

  late int index;

  @override
  List<Object?> get props => [contentPart, id, index];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$ChildContentToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$ParentContent extends JsonSerializable
    with EquatableMixin {
  ContentByCategory$QueryRoot$Content$ParentContent();

  factory ContentByCategory$QueryRoot$Content$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$ParentContentFromJson(json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$ParentContentToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  ContentByCategory$QueryRoot$Content$ContentCategories$Category();

  factory ContentByCategory$QueryRoot$Content$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$ContentCategories
    extends JsonSerializable with EquatableMixin {
  ContentByCategory$QueryRoot$Content$ContentCategories();

  factory ContentByCategory$QueryRoot$Content$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$ContentCategoriesFromJson(json);

  late ContentByCategory$QueryRoot$Content$ContentCategories$Category category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$ContentCategoriesToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$ContentState extends JsonSerializable
    with EquatableMixin {
  ContentByCategory$QueryRoot$Content$ContentState();

  factory ContentByCategory$QueryRoot$Content$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$ContentStateFromJson(json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$ContentStateToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  ContentByCategory$QueryRoot$Content$Collections$Collection$CollectionType();

  factory ContentByCategory$QueryRoot$Content$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  ContentByCategory$QueryRoot$Content$Collections$Collection();

  factory ContentByCategory$QueryRoot$Content$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$Collections$CollectionFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  ContentByCategory$QueryRoot$Content$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$Collections$CollectionToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content$Collections extends JsonSerializable
    with EquatableMixin {
  ContentByCategory$QueryRoot$Content$Collections();

  factory ContentByCategory$QueryRoot$Content$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$Content$CollectionsFromJson(json);

  late ContentByCategory$QueryRoot$Content$Collections$Collection collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$Content$CollectionsToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot$Content extends JsonSerializable
    with EquatableMixin {
  ContentByCategory$QueryRoot$Content();

  factory ContentByCategory$QueryRoot$Content.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRoot$ContentFromJson(json);

  @JsonKey(name: 'content_type')
  late ContentByCategory$QueryRoot$Content$ContentType contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<ContentByCategory$QueryRoot$Content$ChildContent> childContent;

  @JsonKey(name: 'parent_content')
  late List<ContentByCategory$QueryRoot$Content$ParentContent> parentContent;

  @JsonKey(name: 'content_categories')
  late List<ContentByCategory$QueryRoot$Content$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late ContentByCategory$QueryRoot$Content$ContentState contentState;

  late List<ContentByCategory$QueryRoot$Content$Collections> collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByCategory$QueryRoot$ContentToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategory$QueryRoot extends JsonSerializable with EquatableMixin {
  ContentByCategory$QueryRoot();

  factory ContentByCategory$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$ContentByCategory$QueryRootFromJson(json);

  late List<ContentByCategory$QueryRoot$Content> content;

  @override
  List<Object?> get props => [content];
  @override
  Map<String, dynamic> toJson() => _$ContentByCategory$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentByCategoryArguments extends JsonSerializable with EquatableMixin {
  ContentByCategoryArguments(
      {required this.categoryID,
      this.contentStateNames,
      this.offset,
      this.limit});

  @override
  factory ContentByCategoryArguments.fromJson(Map<String, dynamic> json) =>
      _$ContentByCategoryArgumentsFromJson(json);

  late String categoryID;

  final List<String?>? contentStateNames;

  final int? offset;

  final int? limit;

  @override
  List<Object?> get props => [categoryID, contentStateNames, offset, limit];
  @override
  Map<String, dynamic> toJson() => _$ContentByCategoryArgumentsToJson(this);
}

final CONTENT_BY_CATEGORY_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'ContentByCategory'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'categoryID')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentStateNames')),
            type: ListTypeNode(
                type: NamedTypeNode(
                    name: NameNode(value: 'String'), isNonNull: false),
                isNonNull: false),
            defaultValue: DefaultValueNode(
                value: ListValueNode(values: [
              StringValueNode(value: 'Published', isBlock: false)
            ])),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'offset')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '0')),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'limit')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '10000')),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'content'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'offset'),
                  value: VariableNode(name: NameNode(value: 'offset'))),
              ArgumentNode(
                  name: NameNode(value: 'limit'),
                  value: VariableNode(name: NameNode(value: 'limit'))),
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'content_state'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'name'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: '_in'),
                                    value: VariableNode(
                                        name: NameNode(
                                            value: 'contentStateNames')))
                              ]))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'content_categories'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'category_id'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: '_eq'),
                                    value: VariableNode(
                                        name: NameNode(value: 'categoryID')))
                              ]))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'content_type'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'is_shared'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'description'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'is_shared'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'child_content'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'content_part'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'content_type'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'is_shared'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'description'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'updated_at'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'is_shared'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'data'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'created_at'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'child_content'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'parent_content'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'index'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'part_id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'content_categories'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'category'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'description'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ]))
                              ])),
                          FieldNode(
                              name: NameNode(value: 'content_state'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'collections'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'collection'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(
                                              value: 'collection_type'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'name'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'description'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null)
                                          ]))
                                    ]))
                              ]))
                        ])),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'index'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'parent_content'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'index'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'part_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'content_categories'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'category'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'description'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ]))
                  ])),
              FieldNode(
                  name: NameNode(value: 'content_state'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'collections'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'collection'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'collection_type'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'description'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ]))
                        ]))
                  ]))
            ]))
      ]))
]);

class ContentByCategoryQuery extends GraphQLQuery<ContentByCategory$QueryRoot,
    ContentByCategoryArguments> {
  ContentByCategoryQuery({required this.variables});

  @override
  final DocumentNode document = CONTENT_BY_CATEGORY_QUERY_DOCUMENT;

  @override
  final String operationName = 'ContentByCategory';

  @override
  final ContentByCategoryArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  ContentByCategory$QueryRoot parse(Map<String, dynamic> json) =>
      ContentByCategory$QueryRoot.fromJson(json);
}
