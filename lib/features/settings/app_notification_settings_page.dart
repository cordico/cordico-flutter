// ignore_for_file: lines_longer_than_80_chars, implementation_imports

import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/app_bar_provider.dart';
import 'package:cordico_mobile/core/widgets/cordico_switch.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:notification_permissions/notification_permissions.dart';
import 'package:system_settings/system_settings.dart';

class AppNotficationsSettingsPage extends ConsumerStatefulWidget {
  const AppNotficationsSettingsPage({Key? key}) : super(key: key);

  @override
  ConsumerState createState() => _AppNotficationsSettingsPageState();
}

class _AppNotficationsSettingsPageState
    extends ConsumerState<AppNotficationsSettingsPage>
    with WidgetsBindingObserver {
  bool pushEnabled = false;

  bool newContent = true;
  String kNotifNewContent = 'kNotifNewContent';

  bool newAssessment = true;
  String kNotifNewAssessment = 'kNotifNewAssessment';

  bool newContact = true;
  String kNotifNewContact = 'kNotifNewContact';

  bool newBulletin = true;
  String kNotifNewBulletin = 'kNotifNewBulletin';

  bool newGallery = true;
  String kNotifNewGallery = 'kNotifNewGallery';

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addObserver(this);

    checkStatus();

    setnotificationSwitches();
  }

  void setnotificationSwitches() {
    newContent = global.prefs.getBool(kNotifNewContent) ?? true;
    newAssessment = global.prefs.getBool(kNotifNewAssessment) ?? true;
    newContact = global.prefs.getBool(kNotifNewContact) ?? true;
    newBulletin = global.prefs.getBool(kNotifNewBulletin) ?? true;
    newGallery = global.prefs.getBool(kNotifNewGallery) ?? true;
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
      checkStatus();
    }
  }

  @override
  Widget build(BuildContext context) {
    const padding = 32.0;
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBarProvider.getAppBarWithBackIcon(
          context,
          backgroundColor: ThemeColors.gray.shade900,
          title: Text(
            'App Notification Settings',
            style: ThemeStyles.cardContentTitle(context)
                ?.copyWith(color: Colors.white),
          ),
        ),
        body: Padding(
          padding:
              const EdgeInsets.only(left: 16, right: 16, top: 22, bottom: 80),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              CordicoSwitch(
                text: 'Push Notifications',
                isEnabled: pushEnabled,
                onSwitch: (val) async {
                  await handlePushTap(val);
                },
              ),
              const SizedBox(height: padding),
              Text(
                'Receive push notification to your device for the following (active) options.',
                style: ThemeStyles.smallParagraph(context)
                    ?.copyWith(color: Colors.white),
              ),
              const SizedBox(height: padding + 16),
              CordicoSwitch(
                text: 'New Content Added',
                isEnabled: newContent,
                onSwitch: (val) async {
                  await global.prefs.setBool(kNotifNewContent, val);
                  setState(() => newContent = val);
                },
              ),
              const SizedBox(height: padding),
              CordicoSwitch(
                text: 'New Assesment Added',
                isEnabled: newAssessment,
                onSwitch: (val) async {
                  await global.prefs.setBool(kNotifNewAssessment, val);
                  setState(() => newAssessment = val);
                },
              ),
              const SizedBox(height: padding),
              CordicoSwitch(
                text: 'New Contact Added',
                isEnabled: newContact,
                onSwitch: (val) async {
                  await global.prefs.setBool(kNotifNewContact, val);
                  setState(() => newContact = val);
                },
              ),
              const SizedBox(height: padding),
              CordicoSwitch(
                text: 'New Bulletin Message Posted',
                isEnabled: newBulletin,
                onSwitch: (val) async {
                  await global.prefs.setBool(kNotifNewBulletin, val);
                  setState(() => newBulletin = val);
                },
              ),
              const SizedBox(height: padding),
              CordicoSwitch(
                text: 'New Gallery Post Added',
                isEnabled: newGallery,
                onSwitch: (val) async {
                  await global.prefs.setBool(kNotifNewGallery, val);
                  setState(() => newGallery = val);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> handlePushTap(bool v) async {
    PermissionStatus? permissionStatus =
        await NotificationPermissions.getNotificationPermissionStatus();

    if (permissionStatus == PermissionStatus.granted) {
      await SystemSettings.appNotifications();
      return;
    }
    if (v) {
      if (permissionStatus == PermissionStatus.denied ||
          permissionStatus == PermissionStatus.provisional) {
        await SystemSettings.appNotifications();
        return;
      } else if (permissionStatus == PermissionStatus.unknown) {
        await NotificationPermissions.requestNotificationPermissions();
        permissionStatus =
            await NotificationPermissions.getNotificationPermissionStatus();
      }
    }
    setState(() {
      pushEnabled = permissionStatus == PermissionStatus.granted;
    });
  }

  void checkStatus() {
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async {
      final permissionStatus =
          await NotificationPermissions.getNotificationPermissionStatus();
      pushEnabled = permissionStatus == PermissionStatus.granted;
      setState(() {});
    });
  }
}
