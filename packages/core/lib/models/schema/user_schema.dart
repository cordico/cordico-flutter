import 'package:json_annotation/json_annotation.dart';
part 'user_schema.g.dart';

@JsonSerializable(explicitToJson: true)
class UserSchema extends JsonSerializable {
  UserSchema();

  factory UserSchema.fromJson(Map<String, dynamic> json) =>
      _$UserSchemaFromJson(json);

  String? email;
  String? id;
  String? identifier;
  String? sessionToken;

  String? userTypeId;

  List<UserSchemaRole>? roles;

  @JsonKey(name: 'last_name')
  String? lastName;
  @JsonKey(name: 'first_name')
  String? firstName;

  @JsonKey(name: 'organization_id')
  String? organizationId;

  @JsonKey(name: 'organization_ids')
  List<String>? organizationIds;

  @JsonKey(name: 'connected_organizations')
  List<UserSchemaConnectedOrganizations>? connectedOrganizations;

  @JsonKey(name: 'lid_identifier')
  String? lidIdentifier;

  UserSchemaCredentials? credentials;

  UserSchemaOrganization? organization;

  dynamic name;

  @override
  Map<String, dynamic> toJson() => _$UserSchemaToJson(this);

  static UserSchema fromJSON(Map<String, dynamic> json) {
    return UserSchema.fromJson(json);
  }

  List<String> allOrganizations() {
    final a = organizationIds ?? [];
    if (organizationId != null) {
      a.add(organizationId!);
    }
    if (connectedOrganizations != null) {
      a.addAll(
          connectedOrganizations!.map((e) => e.organizationId ?? '').toList());
    }
    return a.toSet().toList();
  }

  String? get rFirstName {
    if (name is UserSchemaNameSchema) {
      return firstName ?? (name as UserSchemaNameSchema).first;
    }
    return firstName ?? name as String?;
  }

  String? get rLastName {
    if (name is UserSchemaNameSchema) {
      return lastName ?? (name as UserSchemaNameSchema).last;
    }
    return lastName ?? name as String?;
  }

  String? get fullName {
    if (rFirstName == null && rLastName == null) {
      return null;
    }
    return '${rFirstName ?? ''} ${rLastName ?? ''}'.trim();
  }
}

@JsonSerializable(explicitToJson: true)
class UserSchemaNameSchema {
  UserSchemaNameSchema();

  factory UserSchemaNameSchema.fromJson(Map<String, dynamic> json) =>
      _$UserSchemaNameSchemaFromJson(json);

  String? first;
  String? last;

  Map<String, dynamic> toJson() => _$UserSchemaNameSchemaToJson(this);

  static UserSchemaNameSchema fromJSON(Map<String, dynamic> json) {
    return UserSchemaNameSchema.fromJson(json);
  }
}

@JsonSerializable(explicitToJson: true)
class UserSchemaCredentials {
  UserSchemaCredentials();

  factory UserSchemaCredentials.fromJson(Map<String, dynamic> json) =>
      _$UserSchemaCredentialsFromJson(json);

  String? identifier;
  String? password;

  Map<String, dynamic> toJson() => _$UserSchemaCredentialsToJson(this);

  static UserSchemaCredentials fromJSON(Map<String, dynamic> json) {
    return UserSchemaCredentials.fromJson(json);
  }
}

@JsonSerializable(explicitToJson: true)
class UserSchemaOrganization {
  UserSchemaOrganization();

  factory UserSchemaOrganization.fromJson(Map<String, dynamic> json) =>
      _$UserSchemaOrganizationFromJson(json);

  UserSchemaOrganizationData? data;
  String? name;

  @JsonKey(name: 'organization_type')
  String? organizationType;

  Map<String, dynamic> toJson() => _$UserSchemaOrganizationToJson(this);

  static UserSchemaOrganization fromJSON(Map<String, dynamic> json) {
    return UserSchemaOrganization.fromJson(json);
  }
}

@JsonSerializable(explicitToJson: true)
class UserSchemaOrganizationData {
  UserSchemaOrganizationData();

  factory UserSchemaOrganizationData.fromJson(Map<String, dynamic> json) =>
      _$UserSchemaOrganizationDataFromJson(json);

  String? id;

  Map<String, dynamic> toJson() => _$UserSchemaOrganizationDataToJson(this);

  static UserSchemaOrganizationData fromJSON(Map<String, dynamic> json) {
    return UserSchemaOrganizationData.fromJson(json);
  }
}

enum UserSchemaRole { admin, editor, user }

@JsonSerializable(explicitToJson: true)
class UserSchemaConnectedOrganizations {
  UserSchemaConnectedOrganizations();

  factory UserSchemaConnectedOrganizations.fromJson(
          Map<String, dynamic> json) =>
      _$UserSchemaConnectedOrganizationsFromJson(json);

  List<String>? roles;
  @JsonKey(name: 'organization_id')
  String? organizationId;

  Map<String, dynamic> toJson() =>
      _$UserSchemaConnectedOrganizationsToJson(this);

  static UserSchemaConnectedOrganizations fromJSON(Map<String, dynamic> json) {
    return UserSchemaConnectedOrganizations.fromJson(json);
  }
}
