// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'content_type_by_id.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class ContentTypeById$QueryRoot$ContentTypeByPk extends JsonSerializable
    with EquatableMixin {
  ContentTypeById$QueryRoot$ContentTypeByPk();

  factory ContentTypeById$QueryRoot$ContentTypeByPk.fromJson(
          Map<String, dynamic> json) =>
      _$ContentTypeById$QueryRoot$ContentTypeByPkFromJson(json);

  late String id;

  late String name;

  String? description;

  late dynamic schema;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @JsonKey(name: 'creator_id')
  late String creatorId;

  @JsonKey(name: 'updater_id')
  String? updaterId;

  @JsonKey(name: 'parent_type_id')
  String? parentTypeId;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  @JsonKey(name: 'supports_category')
  late bool supportsCategory;

  late String key;

  @override
  List<Object?> get props => [
        id,
        name,
        description,
        schema,
        createdAt,
        updatedAt,
        creatorId,
        updaterId,
        parentTypeId,
        isShared,
        supportsCategory,
        key
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentTypeById$QueryRoot$ContentTypeByPkToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentTypeById$QueryRoot extends JsonSerializable with EquatableMixin {
  ContentTypeById$QueryRoot();

  factory ContentTypeById$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$ContentTypeById$QueryRootFromJson(json);

  @JsonKey(name: 'content_type_by_pk')
  ContentTypeById$QueryRoot$ContentTypeByPk? contentTypeByPk;

  @override
  List<Object?> get props => [contentTypeByPk];
  @override
  Map<String, dynamic> toJson() => _$ContentTypeById$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentTypeByIdArguments extends JsonSerializable with EquatableMixin {
  ContentTypeByIdArguments({required this.id});

  @override
  factory ContentTypeByIdArguments.fromJson(Map<String, dynamic> json) =>
      _$ContentTypeByIdArgumentsFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$ContentTypeByIdArgumentsToJson(this);
}

final CONTENT_TYPE_BY_ID_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'ContentTypeById'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'content_type_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'id'),
                  value: VariableNode(name: NameNode(value: 'id')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'description'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'creator_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updater_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'parent_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'is_shared'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'supports_category'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class ContentTypeByIdQuery
    extends GraphQLQuery<ContentTypeById$QueryRoot, ContentTypeByIdArguments> {
  ContentTypeByIdQuery({required this.variables});

  @override
  final DocumentNode document = CONTENT_TYPE_BY_ID_QUERY_DOCUMENT;

  @override
  final String operationName = 'ContentTypeById';

  @override
  final ContentTypeByIdArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  ContentTypeById$QueryRoot parse(Map<String, dynamic> json) =>
      ContentTypeById$QueryRoot.fromJson(json);
}
