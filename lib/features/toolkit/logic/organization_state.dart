import 'package:core/core.dart';
import 'package:core/graphql/organization/organization_by_id.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'organization_state.freezed.dart';

extension ArticleGetters on OrganizationState {
  bool get isLoading => this is _Loading;
}

@freezed
class OrganizationState with _$OrganizationState {
  /// Data is present state
  const factory OrganizationState.data({required OrganizationModel? content}) =
      _Data;

  /// Initial/default state
  const factory OrganizationState.initial() = _Initial;

  /// Data is loading state
  const factory OrganizationState.loading() = _Loading;

  /// Error when loading data state
  const factory OrganizationState.error([String? message]) = _Error;
}
