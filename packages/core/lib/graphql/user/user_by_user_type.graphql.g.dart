// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'user_by_user_type.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserByUserType$QueryRoot$User _$UserByUserType$QueryRoot$UserFromJson(
    Map<String, dynamic> json) {
  return UserByUserType$QueryRoot$User()
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..id = json['id'] as String
    ..identifier = json['identifier'] as String
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..userServiceId = json['user_service_id'] as String?
    ..userTypeId = json['user_type_id'] as String;
}

Map<String, dynamic> _$UserByUserType$QueryRoot$UserToJson(
        UserByUserType$QueryRoot$User instance) =>
    <String, dynamic>{
      'data': instance.data,
      'created_at': instance.createdAt.toIso8601String(),
      'id': instance.id,
      'identifier': instance.identifier,
      'updated_at': instance.updatedAt?.toIso8601String(),
      'user_service_id': instance.userServiceId,
      'user_type_id': instance.userTypeId,
    };

UserByUserType$QueryRoot _$UserByUserType$QueryRootFromJson(
    Map<String, dynamic> json) {
  return UserByUserType$QueryRoot()
    ..user = (json['user'] as List<dynamic>)
        .map((e) =>
            UserByUserType$QueryRoot$User.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$UserByUserType$QueryRootToJson(
        UserByUserType$QueryRoot instance) =>
    <String, dynamic>{
      'user': instance.user.map((e) => e.toJson()).toList(),
    };

UserByUserTypeArguments _$UserByUserTypeArgumentsFromJson(
    Map<String, dynamic> json) {
  return UserByUserTypeArguments(
    userTypes:
        (json['userTypes'] as List<dynamic>?)?.map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$UserByUserTypeArgumentsToJson(
        UserByUserTypeArguments instance) =>
    <String, dynamic>{
      'userTypes': instance.userTypes,
    };
