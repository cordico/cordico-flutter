// ignore_for_file: avoid_function_literals_in_foreach_calls

import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/app_bar_provider.dart';
import 'package:cordico_mobile/core/widgets/content_widget.dart';
import 'package:cordico_mobile/core/widgets/cordico_elevated_button.dart';
import 'package:cordico_mobile/core/widgets/message.dart';
import 'package:cordico_mobile/features/content/logic/content_provider.dart';
import 'package:cordico_mobile/features/content/views/assessment/model/assessment_results.dart';
import 'package:core/core.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/utils/map_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_segment/flutter_segment.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
// ignore: implementation_imports
import 'package:vrouter/src/core/extended_context.dart';
import 'package:vrouter/vrouter.dart';

class AssessmentResultPage extends StatefulWidget {
  const AssessmentResultPage({Key? key}) : super(key: key);

  @override
  State<AssessmentResultPage> createState() => _AssessmentResultPageState();
}

class _AssessmentResultPageState extends State<AssessmentResultPage> {
  late ContentQuery$QueryRoot$Content mainContent;
  late ContentSchema contentSchema;
  late int score = 0;
  late AssessmentResult? result;
  String id = '';

  @override
  Widget build(BuildContext context) {
    //GET THE ASSESSMENT SCORE
    if (context.vRouter.pathParameters.containsKey('id')) {
      id = context.vRouter.pathParameters['id']!;

      final results = global.assessmentsCompleted
          .where((element) => element.assessmentID == id);

      if (results.isNotEmpty) {
        result = results.first;

        //print('RESULT: ${result}');
      }
    }
    return Consumer(
      builder: (BuildContext context, WidgetRef ref, Widget? child) {
        final contentValue = ref.watch(contentByIdProvider);
        return Scaffold(
          appBar: AppBarProvider.getAppBarWithBackIcon(
            context,
            onIconPressed: () {
              context.vRouter.historyGo(-3);
            },
            title: Text(
              'Self Assessment',
              style: ThemeStyles.cardContentTitle(context)
                  ?.copyWith(color: Colors.white),
            ),
          ),
          body: contentValue.when(
            data: (data) {
              final content = data['content'] as ContentModel?;

              if (content != null) {
                mainContent =
                    ContentQuery$QueryRoot$Content.fromJson(content.toJson());
                removeNullAndEmptyParams(
                    mainContent.data as Map<String, dynamic>);

                contentSchema = ContentSchema.fromJson(
                    mainContent.data as Map<String, dynamic>);

                if (result != null && result!.assessmentScoresResult != null) {
                  final assessmentResultList = result!.assessmentScoresResult!;
                  for (final score in assessmentResultList) {
                    Segment.track(
                        eventName: 'Assessment Result',
                        properties: <String, dynamic>{
                          'content_id': mainContent.id,
                          'type': mainContent.contentType.name,
                          'name': score.outcome?.scoreName,
                          'score': score.getFinalScore,
                          'scaleKey': score.scaleKey
                        });
                  }
                }

                return SafeArea(
                  child: _buildContent(context),
                );
              }
              return const Message('No content.');
            },
            loading: () => const Center(child: CircularProgressIndicator()),
            error: (e, stackTrace) => Message(e.toString()),
          ),
        );
      },
    );
  }

  Widget _buildContent(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: const EdgeInsets.all(14),
                  child: Text(
                    'Results',
                    style: ThemeStyles.cardContentTitle(context)
                        ?.copyWith(color: Colors.white),
                  ),
                ),
                if (result != null)
                  ...result!.assessmentScoresResult!.asMap().entries.map((e) {
                    return _buildAssessmentDetails(
                      context,
                      result: e.value,
                    );
                  }).toList(),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 19),
          child: CordicoElevatedButton(
            text: 'Done',
            height: 38,
            width: 140,
            color: ThemeColors.blue.shade400,
            onPressed: () {
              context.vRouter.historyGo(-3);
            },
          ),
        ),
      ],
    );
  }

  Widget _buildAssessmentDetails(
    BuildContext context, {
    required AssessmentScoresResult result,
  }) {
    final scoreRange = result.getScoreRange;

    if (scoreRange == null) {
      return Container();
    }
    return Padding(
      padding: const EdgeInsets.all(14),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 23),
          Text(
            result.outcome?.scoreName ?? '',
            style: ThemeStyles.cardContentTitle(context)
                ?.copyWith(color: Colors.white)
                .copyWith(fontSize: 22),
          ),
          const SizedBox(height: 23),
          ContentWidget(
              content: (scoreRange.interpretation ?? '')
                  .replaceAll('{{{SCORE}}}', '${result.getFinalScore}'),
              contentType: MimeType.markdown)
        ],
      ),
    );
  }
}
