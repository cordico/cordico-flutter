import 'package:flutter/material.dart';

extension TextExt on String {
  bool isExeeded({
    required int lines,
    required double width,
    required TextStyle? style,
  }) {
    final span = TextSpan(text: this, style: style);
    // Use a textpainter to determine if it will exceed max lines
    final tp = TextPainter(
      maxLines: lines,
      textAlign: TextAlign.left,
      textDirection: TextDirection.ltr,
      text: span,
    )..layout(maxWidth: width);
    // whether the text overflowed or not
    return tp.didExceedMaxLines;
  }

  Size getSize(
      {required BuildContext context,
      required TextStyle? style,
      double minWidth = 0.0,
      double maxWidth = double.infinity,
      int? maxLines = 1,
      double textScaleFactor = 1,
      TextDirection textDirection = TextDirection.ltr}) {
    return (TextPainter(
            text: TextSpan(text: this, style: style),
            maxLines: maxLines,
            textScaleFactor: textScaleFactor,
            textDirection: textDirection)
          ..layout(minWidth: minWidth, maxWidth: maxWidth))
        .size;
  }
}
