import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/images.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:flutter/material.dart';

class CordicoDropDownItem extends StatefulWidget {
  const CordicoDropDownItem({
    Key? key,
    required this.item,
    required this.isSelected,
    required this.onSelected,
    this.isRounded = false,
    this.isSingleSelect = false,
    this.colordBadge,
  }) : super(key: key);

  final String item;
  final bool isSelected;
  final bool isSingleSelect;
  final bool isRounded;
  final Function(String) onSelected;
  final Color? colordBadge;

  @override
  State createState() => CordicoDropDownItemState();
}

class CordicoDropDownItemState extends State<CordicoDropDownItem> {
  bool _isSelected = false;

  @override
  void initState() {
    super.initState();
    _isSelected = widget.isSelected;
  }

  @override
  void didUpdateWidget(covariant CordicoDropDownItem oldWidget) {
    if (_isSelected != widget.isSelected) {
      setState(() {
        _isSelected = widget.isSelected;
      });
    }

    super.didUpdateWidget(oldWidget);
  }

  void _onSelectedChanged() {
    widget.onSelected(widget.item);
    setState(() {
      _isSelected = !_isSelected;
    });
  }

  @override
  Widget build(BuildContext context) {
    String selectableIcon;
    if (widget.isSingleSelect) {
      selectableIcon = _isSelected
          ? Images.icRadioButtonWhiteChecked
          : Images.icRadioButtonWhiteUnchecked;
    } else {
      selectableIcon =
          _isSelected ? Images.icCheckboxChecked : Images.icCheckboxUnchecked;
    }

    return GestureDetector(
      onTap: _onSelectedChanged,
      child: Container(
        height: 37,
        padding: const EdgeInsets.symmetric(horizontal: 13),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: ThemeColors.gray.shade500),
        child: Row(
          children: [
            if (widget.isRounded)
              Container(
                height: 15,
                width: 15,
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(
                    color: _isSelected ? Colors.white : const Color(0xFFDADADA),
                  ),
                ),
                padding: const EdgeInsets.all(3),
                child: Container(
                  height: 8,
                  width: 8,
                  decoration: BoxDecoration(
                      color: _isSelected ? Colors.white : Colors.transparent,
                      borderRadius: BorderRadius.circular(4)),
                ),
              ),
            if (widget.isRounded == false)
              Image.asset(
                selectableIcon,
                height: 18,
              ),
            if (widget.colordBadge != null)
              Container(
                height: 10,
                width: 10,
                margin: const EdgeInsets.only(left: 8),
                decoration: BoxDecoration(
                  color: widget.colordBadge,
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
            Container(width: 7),
            Expanded(
              child: Text(
                widget.item,
                style: ThemeStyles.contentDescription(context)
                    ?.copyWith(color: ThemeColors.gray.shade200),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
