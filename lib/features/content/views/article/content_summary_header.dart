import 'package:cached_network_image/cached_network_image.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
// ignore: implementation_imports
import 'package:vrouter/src/core/extended_context.dart';

class ContentSummaryHeader implements SliverPersistentHeaderDelegate {
  ContentSummaryHeader({
    this.minExtent = 80,
    required this.maxExtent,
    required this.imageUrl,
    this.onTap,
    this.isOther = false,
    this.controller,
    this.isTabScreen = false,
  });
  final String? imageUrl;
  final bool isOther;
  final Function()? onTap;
  final ScrollController? controller;
  final bool isTabScreen;

  @override
  final double minExtent;
  @override
  double maxExtent;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    //blur(shrinkOffset);
    return GestureDetector(
      onTap: () {
        if (controller != null) {
          controller!.animateTo(0,
              duration: const Duration(milliseconds: 200),
              curve: Curves.linear);
        }
      },
      child: Container(
        decoration: const BoxDecoration(color: Colors.black),
        child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.bottomCenter,
          children: [
            _returnBookImage(context, shrinkOffset),
            Positioned(
              top: MediaQuery.of(context).viewPadding.top,
              left: 10,
              child: _buildBackIcon(context),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                height: 20,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(24),
                      topRight: Radius.circular(24)),
                  color: Colors.black,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _returnBookImage(
    BuildContext context,
    double shrinkOffset,
  ) {
    return imageUrl == null || imageUrl!.isEmpty
        ? Container(color: Colors.grey.shade100.withOpacity(0.1))
        : GestureDetector(
            onTap: onTap,
            child: CachedNetworkImage(
              fit: BoxFit.cover,
              width: MediaQuery.of(context).size.width,
              imageUrl: imageUrl ?? '',
              imageBuilder: (context, image) {
                return Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: image,
                      fit: BoxFit.cover,
                    ),
                  ),
                );
              },
              progressIndicatorBuilder: (context, text, progress) {
                return Center(
                  child: Container(),
                );
              },
            ),
          );
  }

  Widget _buildBackIcon(BuildContext context) {
    return IconButton(
      onPressed: () {
        context.vRouter.historyBack();
      },
      iconSize: 40,
      splashRadius: 20,
      icon: CircleAvatar(
        radius: 20,
        backgroundColor: ThemeColors.buttonsGreyColor,
        child: Padding(
          padding: const EdgeInsets.only(left: 8),
          child: Icon(Icons.arrow_back_ios, color: ThemeColors.blue.shade100),
        ),
      ),
    );
  }

  double titleOpacity(double shrinkOffset) {
    // simple formula: fade out text as soon as shrinkOffset > 0
    return 1;

    //return 1.0 - max(0.0, (shrinkOffset - minExtent)) / (maxExtent - minExtent);
  }

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }

  @override
  FloatingHeaderSnapConfiguration get snapConfiguration =>
      FloatingHeaderSnapConfiguration();

  @override
  OverScrollHeaderStretchConfiguration get stretchConfiguration =>
      OverScrollHeaderStretchConfiguration(
        stretchTriggerOffset: 50,
      );

  @override
  PersistentHeaderShowOnScreenConfiguration get showOnScreenConfiguration =>
      const PersistentHeaderShowOnScreenConfiguration();

  @override
  TickerProvider? get vsync => null;
}
