// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'organization_contact_query.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrganizationContactQuery$QueryRoot$OrganizationContacts$ContactType
    _$OrganizationContactQuery$QueryRoot$OrganizationContacts$ContactTypeFromJson(
        Map<String, dynamic> json) {
  return OrganizationContactQuery$QueryRoot$OrganizationContacts$ContactType()
    ..displayName = json['display_name'] as String
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$OrganizationContactQuery$QueryRoot$OrganizationContacts$ContactTypeToJson(
            OrganizationContactQuery$QueryRoot$OrganizationContacts$ContactType
                instance) =>
        <String, dynamic>{
          'display_name': instance.displayName,
          'id': instance.id,
          'name': instance.name,
        };

OrganizationContactQuery$QueryRoot$OrganizationContacts$Organization
    _$OrganizationContactQuery$QueryRoot$OrganizationContacts$OrganizationFromJson(
        Map<String, dynamic> json) {
  return OrganizationContactQuery$QueryRoot$OrganizationContacts$Organization()
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$OrganizationContactQuery$QueryRoot$OrganizationContacts$OrganizationToJson(
            OrganizationContactQuery$QueryRoot$OrganizationContacts$Organization
                instance) =>
        <String, dynamic>{
          'name': instance.name,
        };

OrganizationContactQuery$QueryRoot$OrganizationContacts
    _$OrganizationContactQuery$QueryRoot$OrganizationContactsFromJson(
        Map<String, dynamic> json) {
  return OrganizationContactQuery$QueryRoot$OrganizationContacts()
    ..data = json['data']
    ..title = json['title'] as String
    ..contactTypeId = json['contact_type_id'] as String
    ..contactType =
        OrganizationContactQuery$QueryRoot$OrganizationContacts$ContactType
            .fromJson(json['contact_type'] as Map<String, dynamic>)
    ..organizationId = json['organization_id'] as String
    ..organization =
        OrganizationContactQuery$QueryRoot$OrganizationContacts$Organization
            .fromJson(json['organization'] as Map<String, dynamic>)
    ..name = json['name'] as String
    ..isActive = json['is_active'] as bool
    ..id = json['id'] as String
    ..updated = json['updated'] == null
        ? null
        : DateTime.parse(json['updated'] as String)
    ..created = DateTime.parse(json['created'] as String);
}

Map<String, dynamic>
    _$OrganizationContactQuery$QueryRoot$OrganizationContactsToJson(
            OrganizationContactQuery$QueryRoot$OrganizationContacts instance) =>
        <String, dynamic>{
          'data': instance.data,
          'title': instance.title,
          'contact_type_id': instance.contactTypeId,
          'contact_type': instance.contactType.toJson(),
          'organization_id': instance.organizationId,
          'organization': instance.organization.toJson(),
          'name': instance.name,
          'is_active': instance.isActive,
          'id': instance.id,
          'updated': instance.updated?.toIso8601String(),
          'created': instance.created.toIso8601String(),
        };

OrganizationContactQuery$QueryRoot _$OrganizationContactQuery$QueryRootFromJson(
    Map<String, dynamic> json) {
  return OrganizationContactQuery$QueryRoot()
    ..organizationContacts = (json['organization_contacts'] as List<dynamic>)
        .map((e) =>
            OrganizationContactQuery$QueryRoot$OrganizationContacts.fromJson(
                e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$OrganizationContactQuery$QueryRootToJson(
        OrganizationContactQuery$QueryRoot instance) =>
    <String, dynamic>{
      'organization_contacts':
          instance.organizationContacts.map((e) => e.toJson()).toList(),
    };

OrganizationContactQueryArguments _$OrganizationContactQueryArgumentsFromJson(
    Map<String, dynamic> json) {
  return OrganizationContactQueryArguments(
    offset: json['offset'] as int?,
    limit: json['limit'] as int?,
  );
}

Map<String, dynamic> _$OrganizationContactQueryArgumentsToJson(
        OrganizationContactQueryArguments instance) =>
    <String, dynamic>{
      'offset': instance.offset,
      'limit': instance.limit,
    };
