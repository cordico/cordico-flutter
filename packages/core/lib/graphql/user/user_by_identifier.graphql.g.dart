// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'user_by_identifier.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserByIdentifier$QueryRoot$User _$UserByIdentifier$QueryRoot$UserFromJson(
    Map<String, dynamic> json) {
  return UserByIdentifier$QueryRoot$User()
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..id = json['id'] as String
    ..identifier = json['identifier'] as String
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..userServiceId = json['user_service_id'] as String?
    ..userTypeId = json['user_type_id'] as String;
}

Map<String, dynamic> _$UserByIdentifier$QueryRoot$UserToJson(
        UserByIdentifier$QueryRoot$User instance) =>
    <String, dynamic>{
      'data': instance.data,
      'created_at': instance.createdAt.toIso8601String(),
      'id': instance.id,
      'identifier': instance.identifier,
      'updated_at': instance.updatedAt?.toIso8601String(),
      'user_service_id': instance.userServiceId,
      'user_type_id': instance.userTypeId,
    };

UserByIdentifier$QueryRoot _$UserByIdentifier$QueryRootFromJson(
    Map<String, dynamic> json) {
  return UserByIdentifier$QueryRoot()
    ..user = (json['user'] as List<dynamic>)
        .map((e) =>
            UserByIdentifier$QueryRoot$User.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$UserByIdentifier$QueryRootToJson(
        UserByIdentifier$QueryRoot instance) =>
    <String, dynamic>{
      'user': instance.user.map((e) => e.toJson()).toList(),
    };

UserByIdentifierArguments _$UserByIdentifierArgumentsFromJson(
    Map<String, dynamic> json) {
  return UserByIdentifierArguments(
    identifier: json['identifier'] as String?,
  );
}

Map<String, dynamic> _$UserByIdentifierArgumentsToJson(
        UserByIdentifierArguments instance) =>
    <String, dynamic>{
      'identifier': instance.identifier,
    };
