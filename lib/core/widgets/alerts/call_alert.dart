// ignore_for_file: unawaited_futures, use_build_context_synchronously, lines_longer_than_80_chars

import 'package:cordico_mobile/core/managers/cordico_url.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/images.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/buttons/action_button.dart';
import 'package:core/models/schema/content_schema.dart';
import 'package:flutter/material.dart';

class CallAlert extends StatefulWidget {
  const CallAlert({
    Key? key,
    this.dismissable = true,
    required this.phone,
    required this.action,
  }) : super(key: key);

  final bool dismissable;
  final String phone;
  final ActionButtonSchema? action;

  @override
  _CallAlertState createState() => _CallAlertState();
}

class _CallAlertState extends State<CallAlert> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => widget.dismissable,
      child: AlertDialog(
        backgroundColor: ThemeColors.gray.shade900,
        content: Container(
          clipBehavior: Clip.hardEdge,
          width: MediaQuery.of(context).size.width,
          decoration: const BoxDecoration(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              Align(
                child: Container(
                  clipBehavior: Clip.hardEdge,
                  height: 112,
                  width: 112,
                  decoration: BoxDecoration(
                      color: ThemeColors.green.shade500,
                      borderRadius: BorderRadius.circular(64)),
                  child: Center(
                    child: Image.asset(
                      Images.deviceLockIcon,
                      width: 45,
                      height: 70,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 25),
              Text(
                'Your number will not be shared.',
                style: ThemeStyles.sectionTitle(context),
              ),
              const SizedBox(height: 4),
              Text(
                'Every time you make a call starting from the department app, your number will be redacted, so that every call will be anonymous.',
                style: ThemeStyles.smallParagraph(context),
              ),
              const SizedBox(height: 16),
              ActionButton(
                action: widget.action,
                content: null,
                onPressed: () async {
                  Navigator.of(context).maybePop();

                  final phoneNumber =
                      CordicoUrl.cleanPhone(phone: widget.phone, prefix: '*67');
                  CordicoUrl.call(phone: phoneNumber);
                },
              ),
            ],
          ),
        ),
        contentPadding:
            const EdgeInsets.only(left: 16, right: 16, bottom: 27, top: 34),
        insetPadding: const EdgeInsets.only(left: 25, right: 25),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16),
        ),
      ),
    );
  }
}
