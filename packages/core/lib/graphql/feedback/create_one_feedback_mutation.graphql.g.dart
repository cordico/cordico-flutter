// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'create_one_feedback_mutation.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateOneFeedbackMutation$MutationRoot$InsertFeedbackOne
    _$CreateOneFeedbackMutation$MutationRoot$InsertFeedbackOneFromJson(
        Map<String, dynamic> json) {
  return CreateOneFeedbackMutation$MutationRoot$InsertFeedbackOne()
    ..body = json['body'] as String
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..data = json['data']
    ..feedbackTypeId = json['feedback_type_id'] as String
    ..id = json['id'] as String
    ..subject = json['subject'] as String?
    ..updatedAt = DateTime.parse(json['updated_at'] as String);
}

Map<String,
    dynamic> _$CreateOneFeedbackMutation$MutationRoot$InsertFeedbackOneToJson(
        CreateOneFeedbackMutation$MutationRoot$InsertFeedbackOne instance) =>
    <String, dynamic>{
      'body': instance.body,
      'created_at': instance.createdAt.toIso8601String(),
      'data': instance.data,
      'feedback_type_id': instance.feedbackTypeId,
      'id': instance.id,
      'subject': instance.subject,
      'updated_at': instance.updatedAt.toIso8601String(),
    };

CreateOneFeedbackMutation$MutationRoot
    _$CreateOneFeedbackMutation$MutationRootFromJson(
        Map<String, dynamic> json) {
  return CreateOneFeedbackMutation$MutationRoot()
    ..insertFeedbackOne = json['insert_feedback_one'] == null
        ? null
        : CreateOneFeedbackMutation$MutationRoot$InsertFeedbackOne.fromJson(
            json['insert_feedback_one'] as Map<String, dynamic>);
}

Map<String, dynamic> _$CreateOneFeedbackMutation$MutationRootToJson(
        CreateOneFeedbackMutation$MutationRoot instance) =>
    <String, dynamic>{
      'insert_feedback_one': instance.insertFeedbackOne?.toJson(),
    };

CreateOneFeedbackMutationArguments _$CreateOneFeedbackMutationArgumentsFromJson(
    Map<String, dynamic> json) {
  return CreateOneFeedbackMutationArguments(
    body: json['body'] as String,
    data: json['data'],
    feedback_type_id: json['feedback_type_id'] as String,
    subject: json['subject'] as String,
    creatorId: json['creatorId'] as String,
  );
}

Map<String, dynamic> _$CreateOneFeedbackMutationArgumentsToJson(
        CreateOneFeedbackMutationArguments instance) =>
    <String, dynamic>{
      'body': instance.body,
      'data': instance.data,
      'feedback_type_id': instance.feedback_type_id,
      'subject': instance.subject,
      'creatorId': instance.creatorId,
    };
