# kratos_api.model.UiNodeAttributes

## Load the model package
```dart
import 'package:kratos_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disabled** | **bool** | Sets the input's disabled field to true or false. | [optional] 
**label** | [**UiText**](UiText.md) |  | [optional] 
**name** | **String** | The input's element name. | [optional] 
**nodeType** | **String** |  | [optional] 
**onclick** | **String** | OnClick may contain javascript which should be executed on click. This is primarily used for WebAuthn. | [optional] 
**pattern** | **String** | The input's pattern. | [optional] 
**required_** | **bool** | Mark this input field as required. | [optional] 
**type** | **String** | The script MIME type | [optional] 
**value** | [**JsonObject**](.md) | The input's value. | [optional] 
**id** | **String** | A unique identifier | [optional] 
**text** | [**UiText**](UiText.md) |  | [optional] 
**height** | **int** | Height of the image | [optional] 
**src** | **String** | The script source | [optional] 
**width** | **int** | Width of the image | [optional] 
**href** | **String** | The link's href (destination) URL.  format: uri | [optional] 
**title** | [**UiText**](UiText.md) |  | [optional] 
**async_** | **bool** | The script async type | [optional] 
**crossorigin** | **String** | The script cross origin policy | [optional] 
**integrity** | **String** | The script's integrity hash | [optional] 
**nonce** | **String** | Nonce for CSP  A nonce you may want to use to improve your Content Security Policy. You do not have to use this value but if you want to improve your CSP policies you may use it. You can also choose to use your own nonce value! | [optional] 
**referrerpolicy** | **String** | The script referrer policy | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


