import 'dart:ui' as ui;
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/images.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/cordico_elevated_button.dart';
import 'package:cordico_mobile/core/widgets/cordico_scaffold.dart';
import 'package:cordico_mobile/core/widgets/cordico_toolkit_header.dart';
import 'package:cordico_mobile/core/widgets/loading_widget.dart';
import 'package:cordico_mobile/features/content/logic/content_provider.dart';
import 'package:cordico_mobile/features/department/views/accordion_list.dart';
import 'package:cordico_mobile/features/discover/logic/content_types_provider.dart';
import 'package:cordico_mobile/features/discover/views/discover_page.dart';
import 'package:cordico_mobile/features/toolkit/logic/categories_state_notifier.dart';
import 'package:cordico_mobile/features/toolkit/logic/organization_content_state.dart';
import 'package:cordico_mobile/features/toolkit/logic/organization_provider.dart';
import 'package:cordico_mobile/features/toolkit/views/categories_consumer.dart';
import 'package:core/core.dart';
import 'package:core/graphql/collection/collection_by_id.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_segment/flutter_segment.dart';
// ignore: implementation_imports
import 'package:vrouter/src/core/extended_context.dart';

class ToolkitPage extends ConsumerStatefulWidget {
  const ToolkitPage({Key? key}) : super(key: key);

  @override
  ConsumerState<ToolkitPage> createState() => _ToolkitPageState();
}

class _ToolkitPageState extends ConsumerState<ToolkitPage> {
  final ScrollController _toolkitScrollController = ScrollController();

  late OrganizationSchema organizationSchema;

  bool showScrollToExplore = true;

  @override
  void initState() {
    super.initState();
    _toolkitScrollController.addListener(toolkitControllerListener);

    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      ref.watch(organizatioinContentNotifierProvider).getContent();
    });
  }

  @override
  void dispose() {
    super.dispose();

    _toolkitScrollController.removeListener(toolkitControllerListener);
  }

  @override
  Widget build(BuildContext context) {
    Segment.screen(
        screenName: 'Toolkit',
        properties: <String, dynamic>{'type': 'screen', 'name': 'Toolkit'});

    final state = ref.watch(organizationStateNotifierProvider);
    return state.when(
      initial: () => Container(),
      loading: () => const Center(child: CircularProgressIndicator()),
      data: (data) {
        organizationSchema =
            OrganizationSchema.fromJson(data?.data as Map<String, dynamic>);

        //SETTING TO A GLOBAL VARIABLE TO USE PROPERTIES
        //IN MAP VIEW AND OTHER STUFFS
        global.organizationSchema = organizationSchema;

        return NotificationListener(
          onNotification: (notification) {
            if (notification is ScrollEndNotification) {
              _snapAppbar();
            }
            return false;
          },
          child: Stack(
            children: [
              CustomScrollView(
                physics: const AlwaysScrollableScrollPhysics(),
                controller: _toolkitScrollController,
                slivers: [
                  SliverAppBar(
                    systemOverlayStyle: SystemUiOverlayStyle.light,
                    pinned: true,
                    collapsedHeight: 80,
                    elevation: 0,
                    expandedHeight: 330 - MediaQuery.of(context).padding.top,
                    backgroundColor: Colors.black,
                    stretch: true,
                    flexibleSpace: CordicoToolkitHeader(
                      organizationImageUrl: organizationSchema.imageUrl ?? '',
                      organizationCoverUrl:
                          organizationSchema.coverImageUrl ?? '',
                      organizationName: organizationSchema.name ?? '',
                      maxHeight: 320 - MediaQuery.of(context).padding.top,
                      minHeight: 130 + MediaQuery.of(context).padding.top,
                      onSettingsTap: () {
                        context.vRouter.to('settings');
                      },
                    ),
                  ),
                  _buildTookitList(context, ref),
                  const SliverFillRemaining(
                    hasScrollBody: false,
                    child: SizedBox(height: 40),
                  )
                ],
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: AnimatedOpacity(
                  opacity: showScrollToExplore ? 1 : 0,
                  duration: const Duration(milliseconds: 300),
                  child: GestureDetector(
                    onTap: () {
                      final offset = 200 - MediaQuery.of(context).padding.top;

                      _toolkitScrollController.animateTo(offset,
                          duration: const Duration(milliseconds: 300),
                          curve: Curves.linear);

                      setState(() => showScrollToExplore = false);
                    },
                    child: Container(
                      clipBehavior: Clip.antiAlias,
                      decoration:
                          const BoxDecoration(color: Colors.transparent),
                      height: 54,
                      child: BackdropFilter(
                        filter: ui.ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                        child: Container(
                          clipBehavior: Clip.antiAlias,
                          decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.2),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.arrow_upward_rounded,
                                color: ThemeColors.gray.shade300,
                              ),
                              const SizedBox(width: 8),
                              Text('Scroll to Explore',
                                  style: ThemeStyles.appBarTitleStyle(context)
                                      ?.copyWith(
                                          fontSize: 19,
                                          color: ThemeColors.gray.shade300)),
                              const SizedBox(width: 8),
                              Icon(
                                Icons.arrow_upward_rounded,
                                color: ThemeColors.gray.shade300,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
      error: (errorDetail) => Container(
        color: Colors.white,
        child: Text(
          'Error occurred with $errorDetail',
        ),
      ),
    );
  }

  void _snapAppbar() {
    final scrollDistance =
        (140 + MediaQuery.of(context).viewPadding.top).toDouble();
    if (_toolkitScrollController.offset > 0 &&
        _toolkitScrollController.offset < scrollDistance) {
      final double snapOffset;
      if (_toolkitScrollController.offset / scrollDistance > 0.5) {
        snapOffset = scrollDistance;
      } else {
        snapOffset = 0;
      }

      Future.microtask(() => _toolkitScrollController.animateTo(snapOffset,
          duration: const Duration(milliseconds: 200), curve: Curves.easeIn));
    }
  }

  void toolkitControllerListener() {
    if (_toolkitScrollController.offset >
        (200 - MediaQuery.of(context).padding.top)) {
      setState(() => showScrollToExplore = false);
      _toolkitScrollController.removeListener(toolkitControllerListener);
    }
  }

  Widget _buildTookitList(BuildContext context, WidgetRef ref) {
    final state = ref.watch(organizatioinContentNotifierProvider);
    //final notifier = ref.watch(organizatioinContentNotifierProvider.notifier);
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          if (index == 0) {
            return Container();
          }

          if (index == 1) {
            return _buildCategories(context, ref);
          }

          if (index == 2) {
            return buildCordicoFeaturedCollection(context, state);
          }

          if (index == 3) {
            return buildWhatsNewCollection(context, state);
          }

          if (index == 4) {
            return buildCordicoNewsCollection(context, state);
          }
          if (index == 5) {
            return buildCategories();
          }

          if (index == 6) {
            return Padding(
              padding: const EdgeInsets.only(
                  left: 28, right: 28, top: 50, bottom: 80),
              child: CordicoElevatedButton(
                color: Colors.black,
                hasBorder: true,
                height: 48,
                child: Text(
                  'Give feedback to Cordico',
                  textAlign: TextAlign.center,
                  style: ThemeStyles.smallParagraph(context)
                      ?.copyWith(fontSize: 14),
                ),
                onPressed: () {
                  context.vRouter.to('feedback');
                },
              ),
            );
          }

          if (index == 7) {
            return Container(height: 0);
          }

          /*if (index == 4) {
            return buildGuidesContent(context, ref);
          }

          if (index == 5) {
            return buildVideosContent(context, ref);
          }

          if (index == 3) {
            return buildallContent(ref);
          }*/
          return Container();
        },
        childCount: 8,
      ),
    );
  }

  Widget? buildCordicoFeaturedCollection(
      BuildContext context, OrganizationContentState state) {
    final data = state.cordicoFeatured;
    if (data == null) {
      return Container(height: 50);
    }

    return AccordionList<
        CollectionById$QueryRoot$CollectionByPk$CollectionContents>(
      title: data.title,
      source: data.collectionContents,
      buttonTitle: 'See All',
      showArchiveButton: true,
      onItemTap: (index) {
        final co = data.collectionContents[index];

        final finalItem =
            ContentQuery$QueryRoot$Content.fromJson(co.content.toJson());
        //SETTING THE ID TO PERFORM THE CALL
        ref.watch(contentIdProvider.state).state = finalItem.id;
        global.goTo(context, content: finalItem, index: 0);
      },
      onViewArchiveTap: () {
        ref.watch(currentIndexProvider.state).state = 1;
      },
    );
  }

  Widget? buildWhatsNewCollection(
      BuildContext context, OrganizationContentState state) {
    final data = state.whatsNew;

    if (data == null) {
      return Container(height: 50);
    }
    return AccordionList<
        CollectionById$QueryRoot$CollectionByPk$CollectionContents>(
      title: data.title,
      source: data.collectionContents,
      buttonTitle: 'See All',
      showArchiveButton: true,
      onItemTap: (index) {
        final co = data.collectionContents[index];

        final finalItem =
            ContentQuery$QueryRoot$Content.fromJson(co.content.toJson());
        //SETTING THE ID TO PERFORM THE CALL
        ref.watch(contentIdProvider.state).state = finalItem.id;
        global.goTo(context, content: finalItem, index: 0);
      },
      onViewArchiveTap: () {
        ref.watch(currentIndexProvider.state).state = 1;
      },
    );
  }

  Widget? buildCordicoNewsCollection(
      BuildContext context, OrganizationContentState state) {
    final data = state.newsFeed;

    if (data == null) {
      return Container(height: 50);
    }

    final collectionContent = data.collectionContents
        .map((e) => ContentQuery$QueryRoot$Content.fromJson(e.content.toJson()))
        .toList();
    return AccordionList<ContentQuery$QueryRoot$Content>(
      title: data.title,
      source: collectionContent,
      buttonTitle: 'See All',
      isBulletin: true,
      showArchiveButton: true,
      onItemTap: (index) {
        final co = data.collectionContents[index];

        final finalItem =
            ContentQuery$QueryRoot$Content.fromJson(co.content.toJson());
        //SETTING THE ID TO PERFORM THE CALL
        ref.watch(contentIdProvider.state).state = finalItem.id;
        global.goTo(context,
            content: finalItem, index: 0, bulletinSource: 'toolkit');
      },
      onViewArchiveTap: () {
        ref.watch(currentIndexProvider.state).state = 1;
      },
    );
  }

  Widget _buildCategories(BuildContext context, WidgetRef ref) {
    return Padding(
      padding: const EdgeInsets.only(left: 12, right: 12, bottom: 20),
      child: Row(children: [
        buildToolkitAction(
          context,
          title: 'Peer\nSupport',
          image: Images.peerSupportIcon,
          action: () {
            context.vRouter.to('contact_type/peer');

            Future.delayed(const Duration(milliseconds: 1500), () {
              ref.watch(currentIndexProvider.state).state = 3;
            });
          },
        ),
        const SizedBox(width: 9),
        buildToolkitAction(
          context,
          title: 'Find a Therapist',
          image: Images.findTherapistIcon,
          action: () {
            context.vRouter.to('contacts_map/_');
          },
        ),
        const SizedBox(width: 9),
        buildToolkitAction(
          context,
          title: 'Self Assessments',
          image: Images.selfAssessmentIcon,
          action: () {
            ref.watch(filtersTypesProvider.state).state = [kContentTypeAssessmentId];
            ref.watch(filtersQueryProvider.state).state = '';

            global.discoverTextController.value = TextEditingValue.empty;

            global.discoverFilters.clearAll();
            final items = ref
                .watch(contentTypesStateNotifierProvider)
                .contentTypes
                ?.where((element) => element.id == kContentTypeAssessmentId);

            if (items != null && items.isNotEmpty) {
              global.discoverFilters.contentTypes = [items.first];
            }

            ref.watch(currentIndexProvider.state).state = 1;
          },
        ),
      ]),
    );
  }

  Widget buildToolkitAction(
    BuildContext context, {
    required String title,
    required String image,
    required Function()? action,
  }) {
    return Flexible(
      child: GestureDetector(
        onTap: action,
        child: SizedBox(
          height: 123,
          child: Stack(
            children: [
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: 91,
                  padding: const EdgeInsets.only(top: 40, left: 2, right: 2),
                  decoration: BoxDecoration(
                    color: ThemeColors.gray.shade600,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: AutoSizeText(
                    title,
                    textAlign: TextAlign.center,
                    minFontSize: 14,
                    maxLines: 2,
                    style: ThemeStyles.cardTitle(context),
                  ),
                ),
              ),
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                child: Image.asset(image, height: 64, width: 64),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget buildCategories() {
    return Consumer(
      builder: (BuildContext context, WidgetRef ref, Widget? child) {
        final state = ref.watch(categoriesNotifierProvider);
        final notifier = ref.watch(categoriesNotifierProvider.notifier);
        return notifier.isLoading
            ? SizedBox(
                height: 183,
                child: Center(child: LoadingWidget(key: loadingIndicatorKey)),
              )
            : Padding(
                padding: const EdgeInsets.only(left: 28, right: 28, top: 36),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Image.asset(
                          'images/search_icon.png',
                          fit: BoxFit.contain,
                          width: 24,
                          height: 24,
                        ),
                        const SizedBox(width: 10),
                        Expanded(
                          child: Text(
                            'Explore Wellness Toolkits',
                            style: ThemeStyles.sectionTitle(context),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 16),
                    ...(state.categories ?? []).map((e) {
                      return GestureDetector(
                        onTap: () {
                          context.vRouter
                              .to('archive/${e.name}/${e.id}/category');
                        },
                        child: Container(
                          height: 80,
                          margin: const EdgeInsets.only(bottom: 16),
                          decoration: BoxDecoration(
                            color: ThemeColors.gray.shade600,
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Row(
                            children: [
                              const SizedBox(width: 16),
                              Image.asset(
                                global.imageFromCategory(e),
                                fit: BoxFit.contain,
                                width: 48,
                                height: 48,
                              ),
                              const SizedBox(width: 20),
                              Expanded(
                                child: AutoSizeText(
                                  e.name,
                                  maxLines: 2,
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  style: ThemeStyles.bulletinCardTitle(context)
                                      ?.copyWith(fontSize: 22),
                                ),
                              ),
                              const SizedBox(width: 16),
                            ],
                          ),
                        ),
                      );
                    }).toList()
                  ],
                ),
              );
      },
    );
  }
}
