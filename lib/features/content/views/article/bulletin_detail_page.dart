// ignore_for_file: implementation_imports
import 'package:collection/collection.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/audio_player/cordico_audio_player.dart';
import 'package:cordico_mobile/core/widgets/badge/urgent_badge.dart';
import 'package:cordico_mobile/core/widgets/brightcove_player.dart';
import 'package:cordico_mobile/core/widgets/content_widget.dart';
import 'package:cordico_mobile/core/widgets/cordico_video_player.dart';
import 'package:cordico_mobile/core/widgets/vimeo_player.dart';
import 'package:cordico_mobile/features/department/logic/department_provider.dart';
import 'package:cordico_mobile/features/toolkit/logic/organization_provider.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/models/schema/content_schema.dart';
import 'package:core/utils/map_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_segment/flutter_segment.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:vrouter/src/core/extended_context.dart';

class BulletinDetailPage extends ConsumerStatefulWidget {
  const BulletinDetailPage({Key? key}) : super(key: key);

  @override
  ConsumerState<BulletinDetailPage> createState() => _ContentPartWidgetState();
}

class _ContentPartWidgetState extends ConsumerState<BulletinDetailPage> {
  late ContentSchema mainContentSchema;

  late ContentQuery$QueryRoot$Content? mainContent;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final id = context.vRouter.pathParameters['id'] ?? '';
    final source = context.vRouter.pathParameters['source'] ?? '';

    Segment.screen(
        screenName: 'Bulletin',
        properties: <String, dynamic>{'type': 'screen', 'name': 'Bulletin'});

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarBrightness: ThemeData.estimateBrightnessForColor(Colors.white),
        statusBarIconBrightness: Brightness.light,
      ),
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          bottom: false,
          child: Stack(
            children: [
              Builder(
                builder: (context) {
                  if (source == 'department') {
                    final state =
                        ref.watch(departmentPageStateNotifierProvider);

                    mainContent = state.bulletin
                        ?.firstWhereOrNull((element) => element.id == id);
                  } else {
                    final state =
                        ref.watch(organizatioinContentNotifierProvider);
                    if (state.newsFeed != null) {
                      final bulletins =
                          state.newsFeed!.collectionContents.map((e) {
                        return ContentQuery$QueryRoot$Content.fromJson(
                            e.content.toJson());
                      }).toList();

                      mainContent = bulletins
                          .firstWhereOrNull((element) => element.id == id);
                    }
                  }

                  if (mainContent != null) {
                    Segment.track(
                        eventName: 'View Bulletin',
                        properties: <String, dynamic>{
                          'id': mainContent!.id,
                          'type': mainContent!.contentType.name,
                          'name': mainContent!.name
                        });

                    removeNullAndEmptyParams(
                        mainContent!.data as Map<String, dynamic>);

                    mainContentSchema = ContentSchema.fromJson(
                        mainContent!.data as Map<String, dynamic>);
                    return _buildContentScrollView(context,
                        content: mainContent);
                  }

                  return Container();
                },
              ),
              Positioned(
                top: 12,
                right: 12,
                child: IconButton(
                  onPressed: () {
                    context.vRouter.historyBack();
                  },
                  iconSize: 40,
                  splashRadius: 20,
                  icon: CircleAvatar(
                    radius: 20,
                    backgroundColor: ThemeColors.buttonsGreyColor,
                    child: Icon(Icons.close, color: ThemeColors.blue.shade100),
                  ),
                  padding: EdgeInsets.zero,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget getVideoPlayerWidget(String url) {
    if (url.contains('players.brightcove.net')) {
      return BrightcovePlayer(url: url);
    } else if (url.contains('player.vimeo.com') ||
        url.contains('www.youtube.com')) {
      return VimeoPlayer(url: url);
    }
    return CordicoVideoPlayer(videoUrl: url);
  }

  Widget _buildContentScrollView(BuildContext context,
      {required ContentQuery$QueryRoot$Content? content}) {
    //videoUrl; //'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4';
    //audioUrl; //'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-2.mp3';

    Widget? videoPlayerChild;
    if (mainContentSchema.videoUrl1 != null &&
        mainContentSchema.videoUrl1!.isNotEmpty) {
      final url = mainContentSchema.videoUrl1!;
      videoPlayerChild = getVideoPlayerWidget(url);
    }

    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 17, 20, 0),
              child: _buildHeader(content: content),
            ),
            if (videoPlayerChild != null)
              Padding(
                padding: const EdgeInsets.only(top: 18),
                child: videoPlayerChild,
              ),
            if (mainContentSchema.audioUrl != null &&
                mainContentSchema.audioUrl!.isNotEmpty)
              Padding(
                padding: const EdgeInsets.fromLTRB(22, 37, 22, 0),
                child:
                    CordicoAudioPlayer(url: mainContentSchema.audioUrl ?? ''),
              ),
            Padding(
              padding: mainContentSchema.videoUrl1 != null ||
                      mainContentSchema.audioUrl != null
                  ? const EdgeInsets.fromLTRB(20, 30, 20, 0)
                  : const EdgeInsets.fromLTRB(20, 56, 20, 0),
              child: ContentWidget(
                content: mainContentSchema.body ?? '',
                contentType: mainContentSchema.bodyMimeType == null ||
                        mainContentSchema.bodyMimeType == MimeType.markdown
                    ? MimeType.markdown
                    : MimeType.html,
                isDarkMode: false,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildCategoryHeader(BuildContext context) {
    return Row(
      children: [
        Flexible(
          child: Text(
            (mainContentSchema.type ?? '').toUpperCase(),
            style: ThemeStyles.cardCategory(context)!
                .copyWith(color: ThemeColors.gray.shade500),
          ),
        ),
        /*if (isNew)
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: NewBadge(),
          ),*/
        if (mainContentSchema.isUrgent == true)
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: UrgentBadge(),
          ),
      ],
    );
  }

  Widget _buildHeader({ContentQuery$QueryRoot$Content? content}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          children: [
            Expanded(child: _buildCategoryHeader(context)),
            Container(width: 8),
          ],
        ),
        const SizedBox(height: 8),
        Text(
          mainContentSchema.title ?? '',
          style: ThemeStyles.bulletinCardTitle(context)
              ?.copyWith(fontSize: 20, color: ThemeColors.gray.shade500),
        ),
      ],
    );
  }
}
