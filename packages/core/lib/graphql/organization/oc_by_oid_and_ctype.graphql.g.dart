// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'oc_by_oid_and_ctype.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentType
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentTypeToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'is_shared': instance.isShared,
          'description': instance.description,
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentTypeToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'is_shared': instance.isShared,
          'description': instance.description,
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContentFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContentToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContentFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContentToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'index': instance.index,
          'part_id': instance.partId,
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$CategoryToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories()
    ..category =
        OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category
            .fromJson(json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategoriesToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories
                instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentStateFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentStateToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$CollectionToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$CollectionsFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections()
    ..collection =
        OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection
            .fromJson(json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$CollectionsToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections
                instance) =>
        <String, dynamic>{
          'collection': instance.collection.toJson(),
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPartFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart()
    ..contentType =
        OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType
            .fromJson(json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) =>
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) =>
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentState =
        OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState
            .fromJson(json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) =>
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPartToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart
                instance) =>
        <String, dynamic>{
          'content_type': instance.contentType.toJson(),
          'updated_at': instance.updatedAt?.toIso8601String(),
          'name': instance.name,
          'is_shared': instance.isShared,
          'id': instance.id,
          'data': instance.data,
          'created_at': instance.createdAt.toIso8601String(),
          'child_content':
              instance.childContent.map((e) => e.toJson()).toList(),
          'parent_content':
              instance.parentContent.map((e) => e.toJson()).toList(),
          'content_categories':
              instance.contentCategories.map((e) => e.toJson()).toList(),
          'content_state': instance.contentState.toJson(),
          'collections': instance.collections.map((e) => e.toJson()).toList(),
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContentFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent()
    ..contentPart =
        OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart
            .fromJson(json['content_part'] as Map<String, dynamic>)
    ..id = json['id'] as String
    ..index = json['index'] as int;
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContentToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent
                instance) =>
        <String, dynamic>{
          'content_part': instance.contentPart.toJson(),
          'id': instance.id,
          'index': instance.index,
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ParentContent
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ParentContentFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ParentContentToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ParentContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'index': instance.index,
          'part_id': instance.partId,
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories$Category
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories$CategoryToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories()
    ..category =
        OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories$Category
            .fromJson(json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategoriesToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories
                instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentState
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentStateFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentStateToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentState
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection$CollectionType
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection$CollectionTypeToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$CollectionToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$CollectionsFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections()
    ..collection =
        OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection
            .fromJson(json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$CollectionsToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections
                instance) =>
        <String, dynamic>{
          'collection': instance.collection.toJson(),
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$ContentFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content()
    ..contentType =
        OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentType
            .fromJson(json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) =>
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) =>
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ParentContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentState =
        OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentState
            .fromJson(json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) =>
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$ContentToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content
                instance) =>
        <String, dynamic>{
          'content_type': instance.contentType.toJson(),
          'updated_at': instance.updatedAt?.toIso8601String(),
          'name': instance.name,
          'is_shared': instance.isShared,
          'id': instance.id,
          'data': instance.data,
          'created_at': instance.createdAt.toIso8601String(),
          'child_content':
              instance.childContent.map((e) => e.toJson()).toList(),
          'parent_content':
              instance.parentContent.map((e) => e.toJson()).toList(),
          'content_categories':
              instance.contentCategories.map((e) => e.toJson()).toList(),
          'content_state': instance.contentState.toJson(),
          'collections': instance.collections.map((e) => e.toJson()).toList(),
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContentsFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents()
    ..id = json['id'] as String
    ..content =
        OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content
            .fromJson(json['content'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContentsToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'content': instance.content.toJson(),
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$CollectionFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection()
    ..id = json['id'] as String
    ..collectionContents = (json['collection_contents'] as List<dynamic>)
        .map((e) =>
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic>
    _$OCByOIdAndCType$QueryRoot$OrganizationCollections$CollectionToJson(
            OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'collection_contents':
              instance.collectionContents.map((e) => e.toJson()).toList(),
        };

OCByOIdAndCType$QueryRoot$OrganizationCollections
    _$OCByOIdAndCType$QueryRoot$OrganizationCollectionsFromJson(
        Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot$OrganizationCollections()
    ..collection =
        OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection.fromJson(
            json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic> _$OCByOIdAndCType$QueryRoot$OrganizationCollectionsToJson(
        OCByOIdAndCType$QueryRoot$OrganizationCollections instance) =>
    <String, dynamic>{
      'collection': instance.collection.toJson(),
    };

OCByOIdAndCType$QueryRoot _$OCByOIdAndCType$QueryRootFromJson(
    Map<String, dynamic> json) {
  return OCByOIdAndCType$QueryRoot()
    ..organizationCollections = (json['organization_collections']
            as List<dynamic>)
        .map((e) => OCByOIdAndCType$QueryRoot$OrganizationCollections.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$OCByOIdAndCType$QueryRootToJson(
        OCByOIdAndCType$QueryRoot instance) =>
    <String, dynamic>{
      'organization_collections':
          instance.organizationCollections.map((e) => e.toJson()).toList(),
    };

OCByOIdAndCTypeArguments _$OCByOIdAndCTypeArgumentsFromJson(
    Map<String, dynamic> json) {
  return OCByOIdAndCTypeArguments(
    limit: json['limit'] as int?,
    offset: json['offset'] as int?,
    organization_id: json['organization_id'] as String?,
    collection_type: json['collection_type'] as String?,
    contentStateNames: (json['contentStateNames'] as List<dynamic>?)
        ?.map((e) => e as String?)
        .toList(),
  );
}

Map<String, dynamic> _$OCByOIdAndCTypeArgumentsToJson(
        OCByOIdAndCTypeArguments instance) =>
    <String, dynamic>{
      'limit': instance.limit,
      'offset': instance.offset,
      'organization_id': instance.organization_id,
      'collection_type': instance.collection_type,
      'contentStateNames': instance.contentStateNames,
    };
