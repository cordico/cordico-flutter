import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/card/cordico_card.dart';
import 'package:cordico_mobile/core/widgets/loading_widget.dart';
import 'package:cordico_mobile/features/toolkit/logic/categories_state_notifier.dart';
import 'package:core/core.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:vrouter/vrouter.dart';

final loadingIndicatorKey = UniqueKey();

class CategoriesConsumer extends ConsumerStatefulWidget {
  const CategoriesConsumer({Key? key}) : super(key: key);

  @override
  ConsumerState<CategoriesConsumer> createState() => _CategoriesConsumerState();
}

class _CategoriesConsumerState extends ConsumerState<CategoriesConsumer> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (BuildContext context, WidgetRef ref, Widget? child) {
        final state = ref.watch(categoriesNotifierProvider);
        final notifier = ref.watch(categoriesNotifierProvider.notifier);
        return notifier.isLoading
            ? SizedBox(
                height: 183,
                child: Center(child: LoadingWidget(key: loadingIndicatorKey)),
              )
            : _buildCategories(state.categories ?? []);
      },
    );
  }

  String _makeImageName(String categoryName) {
    final converted = categoryName.toLowerCase().replaceAll(' ', '_');
    return 'images/$converted.png';
  }

  Widget _buildCategories(List<CategoryModel> categories) {
    if (categories.isEmpty) {
      return const _Message('No categories!');
    }
    return SizedBox(
      height: 183,
      child: ListView.builder(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          itemCount: categories.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            final category = categories[index];
            final item = _buildCategoryItem(
              context,
              category,
              _makeImageName(category.name),
            );

            if (index < categories.length - 1) {
              return Padding(
                padding: const EdgeInsets.only(right: 8),
                child: item,
              );
            }
            return item;
          }),
    );
  }

  Widget _buildCategoryItem(
      BuildContext context, CategoryModel category, String image) {
    return GestureDetector(
      onTap: () {
        context.vRouter.to('archive/${category.name}/${category.id}/category');
      },
      child: CordicoCard(
        color: ThemeColors.gray.shade800,
        cornerRadius: 6,
        child: SizedBox(
          width: 148,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset(image),
              Expanded(
                child: Padding(
                    padding: const EdgeInsets.all(9),
                    child: Text(
                      category.name,
                      style: ThemeStyles.cardContentTitle(context)?.copyWith(
                        fontSize: 18,
                        color: ThemeColors.categoryNameColor,
                      ),
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _Message extends StatelessWidget {
  const _Message(this.message);

  final String message;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Text(
        message,
        style: Theme.of(context).textTheme.headline5,
        textAlign: TextAlign.center,
      ),
    );
  }
}
