// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'create_one_collection_content.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CreateOneCollectionContent$MutationRoot$CreateOneCollectionContent
    extends JsonSerializable with EquatableMixin {
  CreateOneCollectionContent$MutationRoot$CreateOneCollectionContent();

  factory CreateOneCollectionContent$MutationRoot$CreateOneCollectionContent.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneCollectionContent$MutationRoot$CreateOneCollectionContentFromJson(
          json);

  late String id;

  late String contentId;

  late String collectionId;

  @override
  List<Object?> get props => [id, contentId, collectionId];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneCollectionContent$MutationRoot$CreateOneCollectionContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneCollectionContent$MutationRoot extends JsonSerializable
    with EquatableMixin {
  CreateOneCollectionContent$MutationRoot();

  factory CreateOneCollectionContent$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneCollectionContent$MutationRootFromJson(json);

  late CreateOneCollectionContent$MutationRoot$CreateOneCollectionContent
      createOneCollectionContent;

  @override
  List<Object?> get props => [createOneCollectionContent];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneCollectionContent$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneCollectionContentArguments extends JsonSerializable
    with EquatableMixin {
  CreateOneCollectionContentArguments(
      {required this.collectionId, required this.contentId});

  @override
  factory CreateOneCollectionContentArguments.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneCollectionContentArgumentsFromJson(json);

  late String collectionId;

  late String contentId;

  @override
  List<Object?> get props => [collectionId, contentId];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneCollectionContentArgumentsToJson(this);
}

final CREATE_ONE_COLLECTION_CONTENT_MUTATION_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'CreateOneCollectionContent'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'collectionId')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentId')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'createOneCollectionContent'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'data'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'collection'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'connect'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'id'),
                                    value: VariableNode(
                                        name: NameNode(value: 'collectionId')))
                              ]))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'content'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'connect'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'id'),
                                    value: VariableNode(
                                        name: NameNode(value: 'contentId')))
                              ]))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'contentId'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'collectionId'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class CreateOneCollectionContentMutation extends GraphQLQuery<
    CreateOneCollectionContent$MutationRoot,
    CreateOneCollectionContentArguments> {
  CreateOneCollectionContentMutation({required this.variables});

  @override
  final DocumentNode document = CREATE_ONE_COLLECTION_CONTENT_MUTATION_DOCUMENT;

  @override
  final String operationName = 'CreateOneCollectionContent';

  @override
  final CreateOneCollectionContentArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  CreateOneCollectionContent$MutationRoot parse(Map<String, dynamic> json) =>
      CreateOneCollectionContent$MutationRoot.fromJson(json);
}
