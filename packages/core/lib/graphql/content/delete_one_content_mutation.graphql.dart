// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'delete_one_content_mutation.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class DeleteOneContentMutation$MutationRoot$DeleteOneContent
    extends JsonSerializable with EquatableMixin {
  DeleteOneContentMutation$MutationRoot$DeleteOneContent();

  factory DeleteOneContentMutation$MutationRoot$DeleteOneContent.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneContentMutation$MutationRoot$DeleteOneContentFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneContentMutation$MutationRoot$DeleteOneContentToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeleteOneContentMutation$MutationRoot extends JsonSerializable
    with EquatableMixin {
  DeleteOneContentMutation$MutationRoot();

  factory DeleteOneContentMutation$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneContentMutation$MutationRootFromJson(json);

  DeleteOneContentMutation$MutationRoot$DeleteOneContent? deleteOneContent;

  @override
  List<Object?> get props => [deleteOneContent];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneContentMutation$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeleteOneContentMutationArguments extends JsonSerializable
    with EquatableMixin {
  DeleteOneContentMutationArguments({this.id});

  @override
  factory DeleteOneContentMutationArguments.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneContentMutationArgumentsFromJson(json);

  final String? id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneContentMutationArgumentsToJson(this);
}

final DELETE_ONE_CONTENT_MUTATION_MUTATION_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'DeleteOneContentMutation'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'deleteOneContent'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'id'),
                        value: VariableNode(name: NameNode(value: 'id')))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class DeleteOneContentMutationMutation extends GraphQLQuery<
    DeleteOneContentMutation$MutationRoot, DeleteOneContentMutationArguments> {
  DeleteOneContentMutationMutation({required this.variables});

  @override
  final DocumentNode document = DELETE_ONE_CONTENT_MUTATION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'DeleteOneContentMutation';

  @override
  final DeleteOneContentMutationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  DeleteOneContentMutation$MutationRoot parse(Map<String, dynamic> json) =>
      DeleteOneContentMutation$MutationRoot.fromJson(json);
}
