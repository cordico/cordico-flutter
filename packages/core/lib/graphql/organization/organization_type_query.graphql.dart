// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'organization_type_query.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class OrganizationTypeQuery$QueryRoot$OrganizationType extends JsonSerializable
    with EquatableMixin {
  OrganizationTypeQuery$QueryRoot$OrganizationType();

  factory OrganizationTypeQuery$QueryRoot$OrganizationType.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationTypeQuery$QueryRoot$OrganizationTypeFromJson(json);

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'creator_id')
  late String creatorId;

  String? description;

  late String id;

  late String key;

  late String name;

  late dynamic schema;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @JsonKey(name: 'updater_id')
  String? updaterId;

  @override
  List<Object?> get props => [
        createdAt,
        creatorId,
        description,
        id,
        key,
        name,
        schema,
        updatedAt,
        updaterId
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationTypeQuery$QueryRoot$OrganizationTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationTypeQuery$QueryRoot extends JsonSerializable
    with EquatableMixin {
  OrganizationTypeQuery$QueryRoot();

  factory OrganizationTypeQuery$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$OrganizationTypeQuery$QueryRootFromJson(json);

  @JsonKey(name: 'organization_type')
  late List<OrganizationTypeQuery$QueryRoot$OrganizationType> organizationType;

  @override
  List<Object?> get props => [organizationType];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationTypeQuery$QueryRootToJson(this);
}

final ORGANIZATION_TYPE_QUERY_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'OrganizationTypeQuery'),
      variableDefinitions: [],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'organization_type'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'creator_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'description'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updater_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class OrganizationTypeQueryQuery
    extends GraphQLQuery<OrganizationTypeQuery$QueryRoot, JsonSerializable> {
  OrganizationTypeQueryQuery();

  @override
  final DocumentNode document = ORGANIZATION_TYPE_QUERY_QUERY_DOCUMENT;

  @override
  final String operationName = 'OrganizationTypeQuery';

  @override
  List<Object?> get props => [document, operationName];
  @override
  OrganizationTypeQuery$QueryRoot parse(Map<String, dynamic> json) =>
      OrganizationTypeQuery$QueryRoot.fromJson(json);
}
