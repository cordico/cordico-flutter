// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'collection_by_collection_type.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionType();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections$Organization
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections$Organization();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections$Organization.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections$OrganizationFromJson(
          json);

  late String name;

  late String id;

  @override
  List<Object?> get props => [name, id];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections$OrganizationToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollectionsFromJson(
          json);

  late CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections$Organization
      organization;

  @override
  List<Object?> get props => [organization];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollectionsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentType
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentType();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentTypeFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentTypeFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContentFromJson(
          json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContentFromJson(
          json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategoriesFromJson(
          json);

  late CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category
      category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategoriesToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentStateFromJson(
          json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentStateToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$CollectionFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$CollectionsFromJson(
          json);

  late CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection
      collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$CollectionsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPartFromJson(
          json);

  @JsonKey(name: 'content_type')
  late CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType
      contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<
          CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent>
      childContent;

  @JsonKey(name: 'parent_content')
  late List<
          CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent>
      parentContent;

  @JsonKey(name: 'content_categories')
  late List<
          CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState
      contentState;

  late List<
          CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections>
      collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPartToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContentFromJson(
          json);

  @JsonKey(name: 'content_part')
  late CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart
      contentPart;

  late String id;

  late int index;

  @override
  List<Object?> get props => [contentPart, id, index];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ParentContent
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ParentContent();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ParentContentFromJson(
          json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ParentContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories$Category();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategoriesFromJson(
          json);

  late CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories$Category
      category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategoriesToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentState
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentState();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentStateFromJson(
          json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentStateToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection$CollectionType();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$CollectionFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$CollectionsFromJson(
          json);

  late CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection
      collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$CollectionsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$ContentFromJson(
          json);

  @JsonKey(name: 'content_type')
  late CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentType
      contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<
          CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent>
      childContent;

  @JsonKey(name: 'parent_content')
  late List<
          CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ParentContent>
      parentContent;

  @JsonKey(name: 'content_categories')
  late List<
          CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentState
      contentState;

  late List<
          CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections>
      collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$ContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContentsFromJson(
          json);

  late String id;

  @JsonKey(name: 'content_id')
  late String contentId;

  @JsonKey(name: 'collection_id')
  late String collectionId;

  late CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content
      content;

  @override
  List<Object?> get props => [id, contentId, collectionId, content];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContentsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot$Collection
    extends JsonSerializable with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot$Collection();

  factory CollectionByCollectionTypeKey$QueryRoot$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRoot$CollectionFromJson(json);

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String name;

  late String title;

  String? key;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @JsonKey(name: 'collection_type_id')
  String? collectionTypeId;

  @JsonKey(name: 'collection_type')
  CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionType?
      collectionType;

  @JsonKey(name: 'organization_collections')
  late List<
          CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections>
      organizationCollections;

  @JsonKey(name: 'collection_contents')
  late List<
          CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents>
      collectionContents;

  @override
  List<Object?> get props => [
        id,
        isShared,
        name,
        title,
        key,
        createdAt,
        updatedAt,
        collectionTypeId,
        collectionType,
        organizationCollections,
        collectionContents
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRoot$CollectionToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKey$QueryRoot extends JsonSerializable
    with EquatableMixin {
  CollectionByCollectionTypeKey$QueryRoot();

  factory CollectionByCollectionTypeKey$QueryRoot.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKey$QueryRootFromJson(json);

  late List<CollectionByCollectionTypeKey$QueryRoot$Collection> collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKey$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByCollectionTypeKeyArguments extends JsonSerializable
    with EquatableMixin {
  CollectionByCollectionTypeKeyArguments(
      {required this.collectionTypeKey, this.contentStateNames, this.isShared});

  @override
  factory CollectionByCollectionTypeKeyArguments.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionByCollectionTypeKeyArgumentsFromJson(json);

  late String collectionTypeKey;

  final List<String?>? contentStateNames;

  final List<bool>? isShared;

  @override
  List<Object?> get props => [collectionTypeKey, contentStateNames, isShared];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionByCollectionTypeKeyArgumentsToJson(this);
}

final COLLECTION_BY_COLLECTION_TYPE_KEY_QUERY_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'CollectionByCollectionTypeKey'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'collectionTypeKey')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentStateNames')),
            type: ListTypeNode(
                type: NamedTypeNode(
                    name: NameNode(value: 'String'), isNonNull: false),
                isNonNull: false),
            defaultValue: DefaultValueNode(
                value: ListValueNode(values: [
              StringValueNode(value: 'Published', isBlock: false)
            ])),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'isShared')),
            type: ListTypeNode(
                type: NamedTypeNode(
                    name: NameNode(value: 'Boolean'), isNonNull: true),
                isNonNull: false),
            defaultValue: DefaultValueNode(
                value: ListValueNode(values: [BooleanValueNode(value: true)])),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'collection'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'collection_type'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'key'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: '_eq'),
                                    value: VariableNode(
                                        name: NameNode(
                                            value: 'collectionTypeKey')))
                              ]))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'is_shared'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: '_in'),
                              value: VariableNode(
                                  name: NameNode(value: 'isShared')))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'is_shared'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'title'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'collection_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'collection_type'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'description'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'organization_collections'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'organization'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ]))
                  ])),
              FieldNode(
                  name: NameNode(value: 'collection_contents'),
                  alias: null,
                  arguments: [
                    ArgumentNode(
                        name: NameNode(value: 'where'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'content'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'content_state'),
                                    value: ObjectValueNode(fields: [
                                      ObjectFieldNode(
                                          name: NameNode(value: 'name'),
                                          value: ObjectValueNode(fields: [
                                            ObjectFieldNode(
                                                name: NameNode(value: '_in'),
                                                value: VariableNode(
                                                    name: NameNode(
                                                        value:
                                                            'contentStateNames')))
                                          ]))
                                    ]))
                              ]))
                        ]))
                  ],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'content_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'collection_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'content'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'content_type'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'is_shared'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'description'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'updated_at'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'is_shared'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'data'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'created_at'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'child_content'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'content_part'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'content_type'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'name'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'is_shared'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'description'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null)
                                          ])),
                                      FieldNode(
                                          name: NameNode(value: 'updated_at'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'is_shared'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'data'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'created_at'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name:
                                              NameNode(value: 'child_content'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null)
                                          ])),
                                      FieldNode(
                                          name:
                                              NameNode(value: 'parent_content'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'index'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name:
                                                    NameNode(value: 'part_id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null)
                                          ])),
                                      FieldNode(
                                          name: NameNode(
                                              value: 'content_categories'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name:
                                                    NameNode(value: 'category'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: SelectionSetNode(
                                                    selections: [
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'name'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'id'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value:
                                                                  'description'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null)
                                                    ]))
                                          ])),
                                      FieldNode(
                                          name:
                                              NameNode(value: 'content_state'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'name'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null)
                                          ])),
                                      FieldNode(
                                          name: NameNode(value: 'collections'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'collection'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: SelectionSetNode(
                                                    selections: [
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'name'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'id'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value:
                                                                  'collection_type'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet:
                                                              SelectionSetNode(
                                                                  selections: [
                                                                FieldNode(
                                                                    name: NameNode(
                                                                        value:
                                                                            'name'),
                                                                    alias: null,
                                                                    arguments: [],
                                                                    directives: [],
                                                                    selectionSet:
                                                                        null),
                                                                FieldNode(
                                                                    name: NameNode(
                                                                        value:
                                                                            'id'),
                                                                    alias: null,
                                                                    arguments: [],
                                                                    directives: [],
                                                                    selectionSet:
                                                                        null),
                                                                FieldNode(
                                                                    name: NameNode(
                                                                        value:
                                                                            'description'),
                                                                    alias: null,
                                                                    arguments: [],
                                                                    directives: [],
                                                                    selectionSet:
                                                                        null)
                                                              ]))
                                                    ]))
                                          ]))
                                    ])),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'index'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'parent_content'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'index'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'part_id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'content_categories'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'category'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'description'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ]))
                              ])),
                          FieldNode(
                              name: NameNode(value: 'content_state'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'collections'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'collection'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(
                                              value: 'collection_type'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'name'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'description'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null)
                                          ]))
                                    ]))
                              ]))
                        ]))
                  ]))
            ]))
      ]))
]);

class CollectionByCollectionTypeKeyQuery extends GraphQLQuery<
    CollectionByCollectionTypeKey$QueryRoot,
    CollectionByCollectionTypeKeyArguments> {
  CollectionByCollectionTypeKeyQuery({required this.variables});

  @override
  final DocumentNode document =
      COLLECTION_BY_COLLECTION_TYPE_KEY_QUERY_DOCUMENT;

  @override
  final String operationName = 'CollectionByCollectionTypeKey';

  @override
  final CollectionByCollectionTypeKeyArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  CollectionByCollectionTypeKey$QueryRoot parse(Map<String, dynamic> json) =>
      CollectionByCollectionTypeKey$QueryRoot.fromJson(json);
}
