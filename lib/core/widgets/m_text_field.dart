import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MTextField extends StatefulWidget {
  const MTextField({
    Key? key,
    this.isEditable = true,
    this.obscureText = false,
    this.onChanged,
    this.check = true,
    this.showIcon = true,
    this.isFocusable = true,
    this.onTap,
    this.controller,
    this.isEnabled = true,
    this.focusNode,
    this.textColor,
    this.hintColor,
    this.digitsOnly = false,
    this.showCounter = false,
    this.isDate = false,
    this.lineVisible = true,
    this.hasBackground = false,
    this.cap = TextCapitalization.none,
    this.textAlign = TextAlign.start,
    this.hint,
    this.type,
    this.borderColor,
    this.suffixIcon,
    this.prefixIcon,
    this.inputFormatters,
    this.minLines = 1,
    this.maxLines = 1,
    this.borderWidth = 0,
    this.maxLength,
    this.textSize = 15,
    this.style,
    this.hintStyle,
    this.bgRadius,
    this.helperText,
    this.labelText,
    this.floatingLabelBehavior,
    this.labelStyle,
    this.autocorrect = false,
    this.tfPadding = const EdgeInsets.only(top: 12, bottom: 10),
    this.enableSuggestions = false,
    this.enableInteractiveSelection = true,
    this.validator,
    this.toolbarOptions,
    this.showCursor = true,
    this.autofocus = false,
    this.autofillHints,
    this.textInputAction,
    this.backgroundColor,
    this.showClear = false,
    this.onFocusChanged,
    this.cursorColor,
    this.errorStyle,
    this.errorText,
    this.clearIcon,
    this.initialValue,
    this.onSubmitted,
    this.fullyRounded = true,
  }) : super(key: key);

  final String? hint;
  final bool obscureText;
  final bool isEditable;
  final bool isEnabled;
  final bool isFocusable;
  final bool hasBackground;
  final bool digitsOnly;
  final bool check;
  final TextStyle? style;
  final TextStyle? hintStyle;
  final Color? textColor;
  final Color? hintColor;
  final double textSize;
  final bool lineVisible;
  final bool isDate;
  final bool showIcon;
  final bool showCounter;
  final double borderWidth;
  final Color? borderColor;
  final TextInputType? type;
  final TextCapitalization cap;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final EdgeInsets tfPadding;
  final int minLines;
  final int? maxLines;
  final int? maxLength;
  final TextAlign textAlign;
  final double? bgRadius;
  final FocusNode? focusNode;
  final TextEditingController? controller;
  final Function(String)? onChanged;
  final Function(String)? onSubmitted;
  final Function()? onTap;
  final List<TextInputFormatter>? inputFormatters;
  final String? helperText;
  final bool enableSuggestions;
  final bool autocorrect;
  final String? Function(String?)? validator;
  final bool enableInteractiveSelection;
  final ToolbarOptions? toolbarOptions;
  final bool showCursor;
  final bool autofocus;
  final Iterable<String>? autofillHints;
  final TextInputAction? textInputAction;
  final bool showClear;
  final Function(bool)? onFocusChanged;
  final Color? backgroundColor;
  final String? labelText;
  final FloatingLabelBehavior? floatingLabelBehavior;
  final TextStyle? labelStyle;
  final Color? cursorColor;
  final String? errorText;
  final TextStyle? errorStyle;
  final Widget? clearIcon;
  final String? initialValue;
  final bool fullyRounded;

  @override
  _MTextFieldState createState() => _MTextFieldState();
}

class _MTextFieldState extends State<MTextField> {
  TextEditingController controller = TextEditingController();
  FocusNode focusNode = FocusNode();

  @override
  void initState() {
    super.initState();

    if (widget.onFocusChanged != null) {
      focusNode.addListener(() {
        widget.onFocusChanged!(focusNode.hasFocus);
      });
    }
    if (widget.initialValue != null) {
      controller = TextEditingController(text: widget.initialValue);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.hasBackground) {
      return Container(
        padding: const EdgeInsets.only(top: 3),
        decoration: BoxDecoration(
          color: widget.backgroundColor ?? ThemeColors.gray.shade700,
          borderRadius: BorderRadius.circular(widget.bgRadius ?? 1),
          border: widget.borderWidth > 0
              ? Border.all(
                  color: widget.check
                      ? widget.borderColor ?? ThemeColors.blue.shade500
                      : ThemeColors.red,
                  width: widget.borderWidth,
                )
              : null,
        ),
        child: returnTF(),
      );
    } else {
      return returnTF();
    }
  }

  Widget returnTF() {
    return TextFormField(
      readOnly: !widget.isFocusable,
      focusNode: widget.focusNode ?? focusNode,
      keyboardAppearance: MediaQuery.of(context).platformBrightness,
      obscureText: widget.obscureText,
      enableInteractiveSelection: widget.enableInteractiveSelection,
      enabled: widget.isEnabled,
      textInputAction: widget.textInputAction,
      autofillHints: widget.isEnabled ? widget.autofillHints : null,
      textCapitalization: widget.cap,
      keyboardType: widget.type,
      textAlign: widget.textAlign,
      maxLength: widget.maxLength,
      minLines: widget.minLines,
      maxLines: widget.maxLines,
      autofocus: widget.autofocus,
      showCursor: widget.showCursor,
      validator: widget.validator,
      controller: widget.controller ?? controller,
      toolbarOptions: widget.toolbarOptions,
      style: widget.style ??
          ThemeStyles.textFieldTextStyle(context)?.copyWith(
              fontSize: widget.textSize, color: Colors.white, height: 1.3),
      inputFormatters: widget.digitsOnly
          ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
          : widget.inputFormatters,
      decoration: returnTextFieldInputDecoration(
        context,
        hint: widget.hint ?? '',
        check: widget.check,
        prefixIcon: widget.prefixIcon,
        suffixIcon: widget.suffixIcon,
        lineVisible: widget.lineVisible,
        padding: widget.tfPadding,
        showCounter: widget.showCounter,
      ),
      cursorColor: widget.cursorColor,
      onChanged: widget.onChanged,
      onFieldSubmitted: widget.onSubmitted,
      enableSuggestions: widget.enableSuggestions,
      autocorrect: widget.autocorrect,
      onTap: widget.onTap,
    );
  }

  InputDecoration returnTextFieldInputDecoration(
    BuildContext context, {
    bool? check,
    String hint = '',
    EdgeInsets padding = const EdgeInsets.only(right: 30, top: 8, bottom: 16),
    bool lightOnly = false,
    bool lineVisible = true,
    bool showCounter = false,
    Widget? suffixIcon,
    Widget? prefixIcon,
  }) {
    return InputDecoration(
      contentPadding: padding,
      isDense: true,
      suffixIconConstraints: const BoxConstraints(minWidth: 30, maxHeight: 26),
      suffixIcon: widget.showClear ? returnClearIcon() : suffixIcon,
      prefixIconConstraints: const BoxConstraints(minWidth: 30, maxHeight: 26),
      prefixIcon: prefixIcon,
      helperText: widget.helperText,
      helperStyle:
          ThemeStyles.textFieldTextStyle(context)?.copyWith(fontSize: 12),
      counter: showCounter ? null : const Offstage(),
      labelStyle: widget.labelStyle ??
          ThemeStyles.textFieldTextStyle(context)
              ?.copyWith(fontSize: widget.textSize),
      hintText: hint,
      helperMaxLines: 4,
      errorMaxLines: 2,
      errorText: widget.errorText,
      errorStyle: widget.errorStyle,
      labelText: widget.labelText,
      floatingLabelBehavior: widget.floatingLabelBehavior,
      hintStyle: widget.hintStyle ??
          ThemeStyles.textFieldTextStyle(context)?.copyWith(
              fontSize: widget.textSize, color: ThemeColors.gray.shade300),
      counterStyle: ThemeStyles.textFieldTextStyle(context)
          ?.copyWith(fontSize: 14, color: const Color(0xFF6A7178)),
      focusedErrorBorder: lineVisible
          ? UnderlineInputBorder(
              borderSide:
                  BorderSide(width: 0.5, color: ThemeColors.blue.shade500),
            )
          : InputBorder.none,
      errorBorder: lineVisible
          ? OutlineInputBorder(
              borderSide:
                  BorderSide(width: 0.5, color: ThemeColors.red.shade500),
            )
          : InputBorder.none,
      disabledBorder: lineVisible
          ? UnderlineInputBorder(
              borderSide:
                  BorderSide(width: 0.5, color: ThemeColors.gray.shade500),
            )
          : InputBorder.none,
      focusedBorder: lineVisible
          ? (widget.fullyRounded
              ? OutlineInputBorder(
                  borderSide: BorderSide(
                  width: 0.5,
                  color: (check == null)
                      ? ThemeColors.blue.shade500
                      : check
                          ? ThemeColors.blue.shade500
                          : Colors.red,
                ))
              : UnderlineInputBorder(
                  borderSide: BorderSide(
                      width: 0.5,
                      color: (check == null)
                          ? ThemeColors.blue.shade500
                          : check
                              ? ThemeColors.blue.shade500
                              : Colors.red),
                ))
          : InputBorder.none,
      enabledBorder: lineVisible
          ? (widget.fullyRounded
              ? OutlineInputBorder(
                  borderSide: BorderSide(
                  width: 0.5,
                  color: (check == null)
                      ? ThemeColors.blue.shade500
                      : check
                          ? ThemeColors.blue.shade500
                          : Colors.red,
                ))
              : UnderlineInputBorder(
                  borderSide: BorderSide(
                      width: 0.5,
                      color: (check == null)
                          ? ThemeColors.blue.shade500
                          : check
                              ? ThemeColors.blue.shade500
                              : Colors.red),
                ))
          : InputBorder.none,
    );
  }

  Widget returnClearIcon() {
    final textController = widget.controller ?? controller;

    return SizedBox(
      height: 30,
      width: 30,
      child: textController.text.isNotEmpty
          ? GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                textController.clear();
                if (widget.onChanged != null) {
                  widget.onChanged!('');
                }

                setState(() {});
              },
              child: widget.clearIcon ??
                  Padding(
                    padding: const EdgeInsets.only(left: 3, top: 4, right: 4),
                    child: Icon(
                      CupertinoIcons.clear_circled_solid,
                      size: 20,
                      color: ThemeColors.gray.shade500,
                    ),
                  ),
            )
          : null,
    );
  }
}
