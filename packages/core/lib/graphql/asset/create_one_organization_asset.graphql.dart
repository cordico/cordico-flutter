// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'create_one_organization_asset.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset$Returning
    extends JsonSerializable with EquatableMixin {
  CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset$Returning();

  factory CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset$Returning.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset$ReturningFromJson(
          json);

  @JsonKey(name: 'asset_id')
  late String assetId;

  late String id;

  @JsonKey(name: 'organization_id')
  late String organizationId;

  @override
  List<Object?> get props => [assetId, id, organizationId];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset$ReturningToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset
    extends JsonSerializable with EquatableMixin {
  CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset();

  factory CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAssetFromJson(
          json);

  @JsonKey(name: 'affected_rows')
  late int affectedRows;

  late List<
          CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset$Returning>
      returning;

  @override
  List<Object?> get props => [affectedRows, returning];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAssetToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneOrganizationAsset$MutationRoot extends JsonSerializable
    with EquatableMixin {
  CreateOneOrganizationAsset$MutationRoot();

  factory CreateOneOrganizationAsset$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneOrganizationAsset$MutationRootFromJson(json);

  @JsonKey(name: 'insert_organization_asset')
  CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset?
      insertOrganizationAsset;

  @override
  List<Object?> get props => [insertOrganizationAsset];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneOrganizationAsset$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneOrganizationAssetArguments extends JsonSerializable
    with EquatableMixin {
  CreateOneOrganizationAssetArguments(
      {required this.assetId, required this.organizationId});

  @override
  factory CreateOneOrganizationAssetArguments.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneOrganizationAssetArgumentsFromJson(json);

  late String assetId;

  late String organizationId;

  @override
  List<Object?> get props => [assetId, organizationId];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneOrganizationAssetArgumentsToJson(this);
}

final CREATE_ONE_ORGANIZATION_ASSET_MUTATION_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'CreateOneOrganizationAsset'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'assetId')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'organizationId')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'insert_organization_asset'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'objects'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'asset_id'),
                        value: VariableNode(name: NameNode(value: 'assetId'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'organization_id'),
                        value: VariableNode(
                            name: NameNode(value: 'organizationId')))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'affected_rows'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'returning'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'asset_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'organization_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ]))
            ]))
      ]))
]);

class CreateOneOrganizationAssetMutation extends GraphQLQuery<
    CreateOneOrganizationAsset$MutationRoot,
    CreateOneOrganizationAssetArguments> {
  CreateOneOrganizationAssetMutation({required this.variables});

  @override
  final DocumentNode document = CREATE_ONE_ORGANIZATION_ASSET_MUTATION_DOCUMENT;

  @override
  final String operationName = 'CreateOneOrganizationAsset';

  @override
  final CreateOneOrganizationAssetArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  CreateOneOrganizationAsset$MutationRoot parse(Map<String, dynamic> json) =>
      CreateOneOrganizationAsset$MutationRoot.fromJson(json);
}
