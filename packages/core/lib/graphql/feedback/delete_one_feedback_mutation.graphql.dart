// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'delete_one_feedback_mutation.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class DeleteOneFeedbackMutation$MutationRoot$DeleteFeedbackByPk
    extends JsonSerializable with EquatableMixin {
  DeleteOneFeedbackMutation$MutationRoot$DeleteFeedbackByPk();

  factory DeleteOneFeedbackMutation$MutationRoot$DeleteFeedbackByPk.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneFeedbackMutation$MutationRoot$DeleteFeedbackByPkFromJson(json);

  late String body;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  dynamic? data;

  @JsonKey(name: 'feedback_type_id')
  late String feedbackTypeId;

  late String id;

  String? subject;

  @JsonKey(name: 'updated_at')
  late DateTime updatedAt;

  @override
  List<Object?> get props =>
      [body, createdAt, data, feedbackTypeId, id, subject, updatedAt];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneFeedbackMutation$MutationRoot$DeleteFeedbackByPkToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeleteOneFeedbackMutation$MutationRoot extends JsonSerializable
    with EquatableMixin {
  DeleteOneFeedbackMutation$MutationRoot();

  factory DeleteOneFeedbackMutation$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneFeedbackMutation$MutationRootFromJson(json);

  @JsonKey(name: 'delete_feedback_by_pk')
  DeleteOneFeedbackMutation$MutationRoot$DeleteFeedbackByPk? deleteFeedbackByPk;

  @override
  List<Object?> get props => [deleteFeedbackByPk];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneFeedbackMutation$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeleteOneFeedbackMutationArguments extends JsonSerializable
    with EquatableMixin {
  DeleteOneFeedbackMutationArguments({required this.id});

  @override
  factory DeleteOneFeedbackMutationArguments.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneFeedbackMutationArgumentsFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneFeedbackMutationArgumentsToJson(this);
}

final DELETE_ONE_FEEDBACK_MUTATION_MUTATION_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'DeleteOneFeedbackMutation'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'delete_feedback_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'id'),
                  value: VariableNode(name: NameNode(value: 'id')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'body'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'feedback_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'subject'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class DeleteOneFeedbackMutationMutation extends GraphQLQuery<
    DeleteOneFeedbackMutation$MutationRoot,
    DeleteOneFeedbackMutationArguments> {
  DeleteOneFeedbackMutationMutation({required this.variables});

  @override
  final DocumentNode document = DELETE_ONE_FEEDBACK_MUTATION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'DeleteOneFeedbackMutation';

  @override
  final DeleteOneFeedbackMutationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  DeleteOneFeedbackMutation$MutationRoot parse(Map<String, dynamic> json) =>
      DeleteOneFeedbackMutation$MutationRoot.fromJson(json);
}
