// ignore_for_file: lines_longer_than_80_chars

import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/theme/images.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/accordion.dart';
import 'package:cordico_mobile/core/widgets/card/cordico_card.dart';
import 'package:cordico_mobile/core/widgets/cordico_elevated_button.dart';
import 'package:cordico_mobile/core/widgets/cordico_search_field.dart';
import 'package:cordico_mobile/features/connect/logic/directory_state.dart';
import 'package:cordico_mobile/features/department/views/accordion_list.dart';
import 'package:core/core.dart';
import 'package:core/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_segment/flutter_segment.dart';
// ignore: implementation_imports
import 'package:vrouter/src/core/extended_context.dart';

final filtersDirectoryQueryProvider = StateProvider<String>((ref) => '');

final filteredDirectoryContactsProvider =
    Provider<List<OrganizationContactModel>>((ref) {
  final filter = ref.watch(filtersDirectoryQueryProvider);
  final contacts = ref.watch(allContactsProvider);

  return utils.filterContacts(content: contacts, query: filter);
});

class ConnectPage extends ConsumerStatefulWidget {
  const ConnectPage({Key? key}) : super(key: key);

  @override
  ConsumerState<ConnectPage> createState() => _ConnectPageState();
}

class _ConnectPageState extends ConsumerState<ConnectPage> {
  final focus = FocusNode();

  final peerAccordionState = GlobalKey<AccordionState>();
  final chaplainAccordionState = GlobalKey<AccordionState>();
  final therapistAccordionState = GlobalKey<AccordionState>();

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      ref.watch(allContactsProvider.notifier).getContent();
    });
    focus.addListener(handleFocus);
  }

  @override
  void dispose() {
    super.dispose();
    focus.removeListener(handleFocus);
  }

  void handleFocus() {
    if (focus.hasFocus) {
      peerAccordionState.currentState?.expand();
      chaplainAccordionState.currentState?.expand();
      therapistAccordionState.currentState?.expand();
    }
  }

  @override
  Widget build(BuildContext context) {
    final stateP = ref.watch(filteredDirectoryContactsProvider);
    final stateNotifier = ref.watch(allContactsProvider.notifier);

    Segment.screen(
        screenName: 'Connect',
        properties: <String, dynamic>{'type': 'screen', 'name': 'Connect'});

    return stateNotifier.isLoading
        ? const Center(child: CircularProgressIndicator())
        : _buildConnectList(context, stateP);
  }

  Widget _buildConnectList(
    BuildContext context,
    List<OrganizationContactModel> allData,
  ) {
    final peerSupportList = <OrganizationContactModel>[];
    final chaplainList = <OrganizationContactModel>[];
    final therapistsContactList = <OrganizationContactModel>[];

    for (final element in allData) {
      if (element.contactType.id == kContactTypePeerSupportId) {
        peerSupportList.add(element);
      } else if (element.contactType.id == kContactTypeChaplainId) {
        chaplainList.add(element);
      } else if (element.contactType.id == kContactTypeTherapistId) {
        therapistsContactList.add(element);
      }
    }

    return Column(
      children: [
        _buildFilterInputField(context),
        Expanded(
          child: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: _buildFindContactsNearby(context),
              ),
              SliverToBoxAdapter(
                child: Offstage(
                  offstage: peerSupportList.isEmpty,
                  child: AccordionList<OrganizationContactModel>(
                    title: 'Peer Support',
                    accordionState: peerAccordionState,
                    source: peerSupportList.length > 5
                        ? peerSupportList.sublist(0, 5)
                        : peerSupportList,
                    showArchiveButton: peerSupportList.length > 5,
                    buttonTitle: 'See All',
                    onViewArchiveTap: () {
                      context.vRouter.to('contact_type/peer');
                    },
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Offstage(
                  offstage: therapistsContactList.isEmpty,
                  child: AccordionList<OrganizationContactModel>(
                    title: 'Therapists',
                    accordionState: therapistAccordionState,
                    source: therapistsContactList.length > 5
                        ? therapistsContactList.sublist(0, 5)
                        : therapistsContactList,
                    showArchiveButton: therapistsContactList.length > 5,
                    buttonTitle: 'See All',
                    onViewArchiveTap: () {
                      context.vRouter.to('contact_type/therapist');
                    },
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Offstage(
                  offstage: chaplainList.isEmpty,
                  child: AccordionList<OrganizationContactModel>(
                    title: 'Chaplain',
                    accordionState: chaplainAccordionState,
                    source: chaplainList.length > 5
                        ? chaplainList.sublist(0, 5)
                        : chaplainList,
                    showArchiveButton: chaplainList.length > 5,
                    buttonTitle: 'See All',
                    onViewArchiveTap: () {
                      context.vRouter.to('contact_type/chaplain');
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildFilterInputField(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(12, 8, 12, 19),
      child: CordicoSearchField(
        focusNode: focus,
        hintText: 'Search Contacts',
        onChanged: (value) {
          ref.watch(filtersDirectoryQueryProvider.state).state = value;
        },
      ),
    );
  }

  Widget _buildFindContactsNearby(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(12, 0, 12, 20),
      child: AspectRatio(
        aspectRatio: 16 / 7.7,
        child: CordicoCard(
          cornerRadius: 6,
          child: Stack(
            children: [
              Positioned(
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                child: Image.asset(
                  Images.mapView,
                  fit: BoxFit.cover,
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: 180,
                  padding: const EdgeInsets.only(bottom: 16),
                  child: CordicoElevatedButton(
                    onPressed: () {
                      context.vRouter.to('contacts_map/_');
                    },
                    text: 'Find Contacts Near You',
                    textStyle: ThemeStyles.fieldLabel(context),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
