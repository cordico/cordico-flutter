// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'create_one_user_mutation.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateOneUserMutation$MutationRoot$CreateOneUser
    _$CreateOneUserMutation$MutationRoot$CreateOneUserFromJson(
        Map<String, dynamic> json) {
  return CreateOneUserMutation$MutationRoot$CreateOneUser()
    ..id = json['id'] as String;
}

Map<String, dynamic> _$CreateOneUserMutation$MutationRoot$CreateOneUserToJson(
        CreateOneUserMutation$MutationRoot$CreateOneUser instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

CreateOneUserMutation$MutationRoot _$CreateOneUserMutation$MutationRootFromJson(
    Map<String, dynamic> json) {
  return CreateOneUserMutation$MutationRoot()
    ..createOneUser = CreateOneUserMutation$MutationRoot$CreateOneUser.fromJson(
        json['createOneUser'] as Map<String, dynamic>);
}

Map<String, dynamic> _$CreateOneUserMutation$MutationRootToJson(
        CreateOneUserMutation$MutationRoot instance) =>
    <String, dynamic>{
      'createOneUser': instance.createOneUser.toJson(),
    };

CreateOneUserMutationArguments _$CreateOneUserMutationArgumentsFromJson(
    Map<String, dynamic> json) {
  return CreateOneUserMutationArguments(
    userTypeId: json['userTypeId'] as String?,
    identifier: json['identifier'] as String,
    data: json['data'],
    userServiceId: json['userServiceId'] as String?,
  );
}

Map<String, dynamic> _$CreateOneUserMutationArgumentsToJson(
        CreateOneUserMutationArguments instance) =>
    <String, dynamic>{
      'userTypeId': instance.userTypeId,
      'identifier': instance.identifier,
      'data': instance.data,
      'userServiceId': instance.userServiceId,
    };
