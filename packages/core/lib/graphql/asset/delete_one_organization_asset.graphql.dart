// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'delete_one_organization_asset.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset$Returning
    extends JsonSerializable with EquatableMixin {
  DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset$Returning();

  factory DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset$Returning.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset$ReturningFromJson(
          json);

  @JsonKey(name: 'asset_id')
  late String assetId;

  late String id;

  @JsonKey(name: 'organization_id')
  late String organizationId;

  @override
  List<Object?> get props => [assetId, id, organizationId];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset$ReturningToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset
    extends JsonSerializable with EquatableMixin {
  DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset();

  factory DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAssetFromJson(
          json);

  @JsonKey(name: 'affected_rows')
  late int affectedRows;

  late List<
          DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset$Returning>
      returning;

  @override
  List<Object?> get props => [affectedRows, returning];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAssetToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class DeleteOneOrganizationAsset$MutationRoot extends JsonSerializable
    with EquatableMixin {
  DeleteOneOrganizationAsset$MutationRoot();

  factory DeleteOneOrganizationAsset$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneOrganizationAsset$MutationRootFromJson(json);

  @JsonKey(name: 'delete_organization_asset')
  DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset?
      deleteOrganizationAsset;

  @override
  List<Object?> get props => [deleteOrganizationAsset];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneOrganizationAsset$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeleteOneOrganizationAssetArguments extends JsonSerializable
    with EquatableMixin {
  DeleteOneOrganizationAssetArguments({required this.id});

  @override
  factory DeleteOneOrganizationAssetArguments.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneOrganizationAssetArgumentsFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneOrganizationAssetArgumentsToJson(this);
}

final DELETE_ONE_ORGANIZATION_ASSET_MUTATION_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'DeleteOneOrganizationAsset'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'delete_organization_asset'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'id'),
                        value: VariableNode(name: NameNode(value: 'id')))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'affected_rows'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'returning'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'asset_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'organization_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ]))
            ]))
      ]))
]);

class DeleteOneOrganizationAssetMutation extends GraphQLQuery<
    DeleteOneOrganizationAsset$MutationRoot,
    DeleteOneOrganizationAssetArguments> {
  DeleteOneOrganizationAssetMutation({required this.variables});

  @override
  final DocumentNode document = DELETE_ONE_ORGANIZATION_ASSET_MUTATION_DOCUMENT;

  @override
  final String operationName = 'DeleteOneOrganizationAsset';

  @override
  final DeleteOneOrganizationAssetArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  DeleteOneOrganizationAsset$MutationRoot parse(Map<String, dynamic> json) =>
      DeleteOneOrganizationAsset$MutationRoot.fromJson(json);
}
