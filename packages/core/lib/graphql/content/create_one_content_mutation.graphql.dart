// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'create_one_content_mutation.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CreateOneContentMutation$MutationRoot$CreateOneContent
    extends JsonSerializable with EquatableMixin {
  CreateOneContentMutation$MutationRoot$CreateOneContent();

  factory CreateOneContentMutation$MutationRoot$CreateOneContent.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneContentMutation$MutationRoot$CreateOneContentFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneContentMutation$MutationRoot$CreateOneContentToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneContentMutation$MutationRoot extends JsonSerializable
    with EquatableMixin {
  CreateOneContentMutation$MutationRoot();

  factory CreateOneContentMutation$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneContentMutation$MutationRootFromJson(json);

  late CreateOneContentMutation$MutationRoot$CreateOneContent createOneContent;

  @override
  List<Object?> get props => [createOneContent];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneContentMutation$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneContentMutationArguments extends JsonSerializable
    with EquatableMixin {
  CreateOneContentMutationArguments(
      {this.contentStateId,
      this.contentTypeId,
      required this.name,
      required this.data,
      this.isShared});

  @override
  factory CreateOneContentMutationArguments.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneContentMutationArgumentsFromJson(json);

  final String? contentStateId;

  final String? contentTypeId;

  late String name;

  late dynamic data;

  final bool? isShared;

  @override
  List<Object?> get props =>
      [contentStateId, contentTypeId, name, data, isShared];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneContentMutationArgumentsToJson(this);
}

final CREATE_ONE_CONTENT_MUTATION_MUTATION_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'CreateOneContentMutation'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentStateId')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentTypeId')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'name')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'data')),
            type: NamedTypeNode(name: NameNode(value: 'Json'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'isShared')),
            type: NamedTypeNode(
                name: NameNode(value: 'Boolean'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'createOneContent'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'data'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'contentState'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'connect'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'id'),
                                    value: VariableNode(
                                        name:
                                            NameNode(value: 'contentStateId')))
                              ]))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'contentType'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'connect'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'id'),
                                    value: VariableNode(
                                        name: NameNode(value: 'contentTypeId')))
                              ]))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'data'),
                        value: VariableNode(name: NameNode(value: 'data'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'name'),
                        value: VariableNode(name: NameNode(value: 'name'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'isShared'),
                        value: VariableNode(name: NameNode(value: 'isShared')))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class CreateOneContentMutationMutation extends GraphQLQuery<
    CreateOneContentMutation$MutationRoot, CreateOneContentMutationArguments> {
  CreateOneContentMutationMutation({required this.variables});

  @override
  final DocumentNode document = CREATE_ONE_CONTENT_MUTATION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'CreateOneContentMutation';

  @override
  final CreateOneContentMutationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  CreateOneContentMutation$MutationRoot parse(Map<String, dynamic> json) =>
      CreateOneContentMutation$MutationRoot.fromJson(json);
}
