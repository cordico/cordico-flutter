// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'organization_query.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrganizationQuery$QueryRoot$Organization
    _$OrganizationQuery$QueryRoot$OrganizationFromJson(
        Map<String, dynamic> json) {
  return OrganizationQuery$QueryRoot$Organization()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?
    ..data = json['data']
    ..organizationTypeId = json['organization_type_id'] as String
    ..portalFqdn = json['portal_fqdn'] as String?
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String);
}

Map<String, dynamic> _$OrganizationQuery$QueryRoot$OrganizationToJson(
        OrganizationQuery$QueryRoot$Organization instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'description': instance.description,
      'data': instance.data,
      'organization_type_id': instance.organizationTypeId,
      'portal_fqdn': instance.portalFqdn,
      'created_at': instance.createdAt.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
    };

OrganizationQuery$QueryRoot _$OrganizationQuery$QueryRootFromJson(
    Map<String, dynamic> json) {
  return OrganizationQuery$QueryRoot()
    ..organization = (json['organization'] as List<dynamic>)
        .map((e) => OrganizationQuery$QueryRoot$Organization.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$OrganizationQuery$QueryRootToJson(
        OrganizationQuery$QueryRoot instance) =>
    <String, dynamic>{
      'organization': instance.organization.map((e) => e.toJson()).toList(),
    };

OrganizationQueryArguments _$OrganizationQueryArgumentsFromJson(
    Map<String, dynamic> json) {
  return OrganizationQueryArguments(
    offset: json['offset'] as int?,
    limit: json['limit'] as int?,
  );
}

Map<String, dynamic> _$OrganizationQueryArgumentsToJson(
        OrganizationQueryArguments instance) =>
    <String, dynamic>{
      'offset': instance.offset,
      'limit': instance.limit,
    };
