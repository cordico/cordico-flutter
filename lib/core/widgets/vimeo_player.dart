import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';

class VimeoPlayer extends StatefulWidget {
  const VimeoPlayer({Key? key, required this.url}) : super(key: key);

  final String url;

  @override
  _VimeoPlayerState createState() => _VimeoPlayerState();
}

class _VimeoPlayerState extends State<VimeoPlayer> {
  WebViewController? webViewController;

  Future<String> _loadCSS() async {
    return rootBundle.loadString('assets/index.css');
  }

  Future<String> _loadDarkCSS() async {
    return rootBundle.loadString('assets/index_dark.css');
  }

  @override
  void initState() {
    super.initState();

    if (!kIsWeb && Platform.isAndroid) {
      WebView.platform =
          SurfaceAndroidWebView();
    }
  }

  @override
  Widget build(BuildContext context) {
    final isDarkMode =
        WidgetsBinding.instance!.window.platformBrightness == Brightness.dark;

    final width = MediaQuery.of(context).size.width -
        MediaQuery.of(context).padding.horizontal - 5;
    final height = (width * 9.0)/16.0;

    final heightString = height.toInt().toString();
    final widthString = width.toInt().toString();

    return SizedBox(
      height: height,
      width: width,
      child: WebView(
        initialUrl: 'about:blank',
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController wvc) async {
          webViewController = wvc;

          final css = isDarkMode ? await _loadDarkCSS() : await _loadCSS();

          final iframe = '''
    <html><head><meta name='viewport' content='width=device-width, initial-scale=1.0' />
    <style>$css</style></head><body>
<iframe class="mbr-embedded-video" src="${widget.url}" width="$widthString" height="$heightString" frameborder="0"></iframe>
</body></html>
''';

          await wvc.loadUrl(Uri.dataFromString(
            iframe,
            mimeType: 'text/html',
            encoding: Encoding.getByName('utf-8'),
          ).toString());
        },
        onPageFinished: (finish) {
          //debugPrint('The loaded is : $finish');
        },
      ),
    );
  }
}
