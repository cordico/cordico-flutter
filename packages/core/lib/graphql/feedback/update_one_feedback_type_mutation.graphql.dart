// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'update_one_feedback_type_mutation.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class UpdateOneFeedbackTypeMutation$MutationRoot$UpdateFeedbackTypeByPk
    extends JsonSerializable with EquatableMixin {
  UpdateOneFeedbackTypeMutation$MutationRoot$UpdateFeedbackTypeByPk();

  factory UpdateOneFeedbackTypeMutation$MutationRoot$UpdateFeedbackTypeByPk.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateOneFeedbackTypeMutation$MutationRoot$UpdateFeedbackTypeByPkFromJson(
          json);

  late String id;

  late String key;

  late String name;

  dynamic? schema;

  @override
  List<Object?> get props => [id, key, name, schema];
  @override
  Map<String, dynamic> toJson() =>
      _$UpdateOneFeedbackTypeMutation$MutationRoot$UpdateFeedbackTypeByPkToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class UpdateOneFeedbackTypeMutation$MutationRoot extends JsonSerializable
    with EquatableMixin {
  UpdateOneFeedbackTypeMutation$MutationRoot();

  factory UpdateOneFeedbackTypeMutation$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateOneFeedbackTypeMutation$MutationRootFromJson(json);

  @JsonKey(name: 'update_feedback_type_by_pk')
  UpdateOneFeedbackTypeMutation$MutationRoot$UpdateFeedbackTypeByPk?
      updateFeedbackTypeByPk;

  @override
  List<Object?> get props => [updateFeedbackTypeByPk];
  @override
  Map<String, dynamic> toJson() =>
      _$UpdateOneFeedbackTypeMutation$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateOneFeedbackTypeMutationArguments extends JsonSerializable
    with EquatableMixin {
  UpdateOneFeedbackTypeMutationArguments(
      {required this.id,
      required this.key,
      required this.name,
      required this.schema});

  @override
  factory UpdateOneFeedbackTypeMutationArguments.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateOneFeedbackTypeMutationArgumentsFromJson(json);

  late String id;

  late String key;

  late String name;

  late dynamic schema;

  @override
  List<Object?> get props => [id, key, name, schema];
  @override
  Map<String, dynamic> toJson() =>
      _$UpdateOneFeedbackTypeMutationArgumentsToJson(this);
}

final UPDATE_ONE_FEEDBACK_TYPE_MUTATION_MUTATION_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'UpdateOneFeedbackTypeMutation'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'key')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'name')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'schema')),
            type:
                NamedTypeNode(name: NameNode(value: 'jsonb'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'update_feedback_type_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: '_set'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'key'),
                        value: VariableNode(name: NameNode(value: 'key'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'name'),
                        value: VariableNode(name: NameNode(value: 'name'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'schema'),
                        value: VariableNode(name: NameNode(value: 'schema')))
                  ])),
              ArgumentNode(
                  name: NameNode(value: 'pk_columns'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'id'),
                        value: VariableNode(name: NameNode(value: 'id')))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class UpdateOneFeedbackTypeMutationMutation extends GraphQLQuery<
    UpdateOneFeedbackTypeMutation$MutationRoot,
    UpdateOneFeedbackTypeMutationArguments> {
  UpdateOneFeedbackTypeMutationMutation({required this.variables});

  @override
  final DocumentNode document =
      UPDATE_ONE_FEEDBACK_TYPE_MUTATION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'UpdateOneFeedbackTypeMutation';

  @override
  final UpdateOneFeedbackTypeMutationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  UpdateOneFeedbackTypeMutation$MutationRoot parse(Map<String, dynamic> json) =>
      UpdateOneFeedbackTypeMutation$MutationRoot.fromJson(json);
}
