// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'organization_type_by_id.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrganizationTypeByID$QueryRoot$OrganizationTypeByPk
    _$OrganizationTypeByID$QueryRoot$OrganizationTypeByPkFromJson(
        Map<String, dynamic> json) {
  return OrganizationTypeByID$QueryRoot$OrganizationTypeByPk()
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..creatorId = json['creator_id'] as String
    ..description = json['description'] as String?
    ..id = json['id'] as String
    ..key = json['key'] as String
    ..name = json['name'] as String
    ..schema = json['schema']
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..updaterId = json['updater_id'] as String?;
}

Map<String, dynamic>
    _$OrganizationTypeByID$QueryRoot$OrganizationTypeByPkToJson(
            OrganizationTypeByID$QueryRoot$OrganizationTypeByPk instance) =>
        <String, dynamic>{
          'created_at': instance.createdAt.toIso8601String(),
          'creator_id': instance.creatorId,
          'description': instance.description,
          'id': instance.id,
          'key': instance.key,
          'name': instance.name,
          'schema': instance.schema,
          'updated_at': instance.updatedAt?.toIso8601String(),
          'updater_id': instance.updaterId,
        };

OrganizationTypeByID$QueryRoot _$OrganizationTypeByID$QueryRootFromJson(
    Map<String, dynamic> json) {
  return OrganizationTypeByID$QueryRoot()
    ..organizationTypeByPk = json['organization_type_by_pk'] == null
        ? null
        : OrganizationTypeByID$QueryRoot$OrganizationTypeByPk.fromJson(
            json['organization_type_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$OrganizationTypeByID$QueryRootToJson(
        OrganizationTypeByID$QueryRoot instance) =>
    <String, dynamic>{
      'organization_type_by_pk': instance.organizationTypeByPk?.toJson(),
    };

OrganizationTypeByIDArguments _$OrganizationTypeByIDArgumentsFromJson(
    Map<String, dynamic> json) {
  return OrganizationTypeByIDArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$OrganizationTypeByIDArgumentsToJson(
        OrganizationTypeByIDArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
