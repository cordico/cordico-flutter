/// IGLU MOBILE LIBRARY DART
///
/// Copyright © 2020 - 2021 IGLU. All rights reserved.
/// Copyright © 2020 - 2021 IGLU S.r.l.s.
///

// ignore_for_file: unnecessary_raw_strings

import 'dart:io';
import 'package:url_launcher/url_launcher.dart';

class CordicoUrl {
  static Future<void> openUrl({
    required String url,
    Function? onError,
    bool forceWebView = false,
    bool forceSafariVC = false,
  }) async {
    try {
      if (await canLaunch(url)) {
        await launch(url,
            forceWebView: forceWebView, forceSafariVC: forceSafariVC);
      }
    } catch (e) {
      if (onError != null) {}
    }
  }

  static Future<void> sendEmail(
      {required String mail, String? subject, String? body}) async {
    final params = Uri(
      scheme: 'mailto',
      path: mail,
      query: 'subject=${subject ?? ''}&body=${Uri.encodeComponent(body ?? '')}',
    );

    final url = params.toString();

    try {
      if (await canLaunch(url)) {
        await launch(url);
      }
      // ignore: empty_catches
    } catch (e) {}
  }

  static Future<void> call({required String phone}) async {
    final url = 'tel://$phone';
    try {
      if (await canLaunch(url)) {
        await launch(url);
      }
      // ignore: empty_catches
    } catch (e) {}
  }

  static Future<void> sendSMS({required String phone, String body = ''}) async {
    final word = Platform.isIOS ? '&' : '?';
    final url = 'sms:$phone${word}body=$body';
    if (await canLaunch(url)) {
      await launch(url);
    }
  }

  static Future<void> openMap({double? latitude, double? longitude}) async {
    final googleUrl =
        'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    await CordicoUrl.openUrl(url: googleUrl);
  }

  static Future<void> openPlatformMap({
    double? latitude,
    double? longitude,
    String? address,
  }) async {
    var url = '';
    if (Platform.isIOS) {
      if (longitude != null && latitude != null) {
        //url = 'https://maps.apple.com/?daddr=$latitude,$longitude';
        url = 'https://maps.apple.com/?q=$latitude,$longitude';
      } else {
        url = 'https://maps.apple.com/?daddr=${Uri.encodeFull(address ?? '')}';
      }
    } else {
      if (longitude != null && latitude != null) {
        url =
            'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
      } else {
        url =
            'https://www.google.com/maps/search/${Uri.encodeFull(address ?? '')}';
      }
    }

    if (await canLaunch(url)) {
      await launch(url);
    }
  }

  static Future<void> openPlatformMapForDirection({
    double? latitude,
    double? longitude,
    String? address,
  }) async {
    var url = '';
    if (Platform.isIOS) {
      if (longitude != null && latitude != null) {
        url = 'https://maps.apple.com/?daddr=$latitude,$longitude';
      } else {
        url = 'https://maps.apple.com/?daddr=${Uri.encodeFull(address ?? '')}';
      }
    } else {
      if (longitude != null && latitude != null) {
        url =
            'https://www.google.com/maps/dir/?api=1&destination=$latitude,$longitude';
      } else {
        url =
            'https://www.google.com/maps/search/${Uri.encodeFull(address ?? '')}';
      }
    }

    if (await canLaunch(url)) {
      await launch(url);
    }
  }

  static Future<bool> canLaunchUri({required String uri}) async {
    return canLaunch(uri);
  }

  static String cleanPhone({required String phone, String? prefix}) {
    String cleanPhone;

    if (phone.startsWith('+1')) {
      cleanPhone = phone.replaceAll('+1', '').replaceAll(RegExp(r'[^0-9]'), '');
      if (prefix != null) {
        cleanPhone = prefix + cleanPhone;
      } else {
        cleanPhone = '+1$cleanPhone';
      }
    } else {
      cleanPhone = phone.replaceAll(RegExp(r'[^0-9]'), '');

      if (prefix != null) {
        cleanPhone = prefix + cleanPhone;
      }
    }

    return cleanPhone;
  }
}
