// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'content_by_id_api.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContentById$QueryRoot$ContentByPk$ContentType
    _$ContentById$QueryRoot$ContentByPk$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic> _$ContentById$QueryRoot$ContentByPk$ContentTypeToJson(
        ContentById$QueryRoot$ContentByPk$ContentType instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'is_shared': instance.isShared,
      'description': instance.description,
    };

ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentType
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentTypeToJson(
            ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'is_shared': instance.isShared,
          'description': instance.description,
        };

ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ChildContent
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ChildContentFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ChildContent()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ChildContentToJson(
            ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ChildContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ParentContent
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ParentContentFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic>
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ParentContentToJson(
            ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ParentContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'index': instance.index,
          'part_id': instance.partId,
        };

ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories$Category
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories$CategoryToJson(
            ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories()
    ..category =
        ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories$Category
            .fromJson(json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategoriesToJson(
            ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories
                instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentState
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentStateFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentStateToJson(
            ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentState
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
        };

ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection$CollectionType
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
            ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$CollectionToJson(
            ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$CollectionsFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections()
    ..collection =
        ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection
            .fromJson(json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$CollectionsToJson(
            ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections
                instance) =>
        <String, dynamic>{
          'collection': instance.collection.toJson(),
        };

ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart
    _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPartFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart()
    ..contentType =
        ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentType
            .fromJson(json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) =>
            ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ChildContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) =>
            ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ParentContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentState =
        ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentState
            .fromJson(json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) =>
            ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String,
    dynamic> _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPartToJson(
        ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart instance) =>
    <String, dynamic>{
      'content_type': instance.contentType.toJson(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'name': instance.name,
      'is_shared': instance.isShared,
      'id': instance.id,
      'data': instance.data,
      'created_at': instance.createdAt.toIso8601String(),
      'child_content': instance.childContent.map((e) => e.toJson()).toList(),
      'parent_content': instance.parentContent.map((e) => e.toJson()).toList(),
      'content_categories':
          instance.contentCategories.map((e) => e.toJson()).toList(),
      'content_state': instance.contentState.toJson(),
      'collections': instance.collections.map((e) => e.toJson()).toList(),
    };

ContentById$QueryRoot$ContentByPk$ChildContent
    _$ContentById$QueryRoot$ContentByPk$ChildContentFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$ChildContent()
    ..contentPart =
        ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart.fromJson(
            json['content_part'] as Map<String, dynamic>)
    ..id = json['id'] as String
    ..index = json['index'] as int;
}

Map<String, dynamic> _$ContentById$QueryRoot$ContentByPk$ChildContentToJson(
        ContentById$QueryRoot$ContentByPk$ChildContent instance) =>
    <String, dynamic>{
      'content_part': instance.contentPart.toJson(),
      'id': instance.id,
      'index': instance.index,
    };

ContentById$QueryRoot$ContentByPk$ParentContent
    _$ContentById$QueryRoot$ContentByPk$ParentContentFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic> _$ContentById$QueryRoot$ContentByPk$ParentContentToJson(
        ContentById$QueryRoot$ContentByPk$ParentContent instance) =>
    <String, dynamic>{
      'id': instance.id,
      'index': instance.index,
      'part_id': instance.partId,
    };

ContentById$QueryRoot$ContentByPk$ContentCategories$Category
    _$ContentById$QueryRoot$ContentByPk$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$ContentById$QueryRoot$ContentByPk$ContentCategories$CategoryToJson(
            ContentById$QueryRoot$ContentByPk$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

ContentById$QueryRoot$ContentByPk$ContentCategories
    _$ContentById$QueryRoot$ContentByPk$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$ContentCategories()
    ..category =
        ContentById$QueryRoot$ContentByPk$ContentCategories$Category.fromJson(
            json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentById$QueryRoot$ContentByPk$ContentCategoriesToJson(
            ContentById$QueryRoot$ContentByPk$ContentCategories instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

ContentById$QueryRoot$ContentByPk$ContentState
    _$ContentById$QueryRoot$ContentByPk$ContentStateFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic> _$ContentById$QueryRoot$ContentByPk$ContentStateToJson(
        ContentById$QueryRoot$ContentByPk$ContentState instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };

ContentById$QueryRoot$ContentByPk$Collections$Collection$CollectionType
    _$ContentById$QueryRoot$ContentByPk$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$ContentById$QueryRoot$ContentByPk$Collections$Collection$CollectionTypeToJson(
            ContentById$QueryRoot$ContentByPk$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

ContentById$QueryRoot$ContentByPk$Collections$Collection
    _$ContentById$QueryRoot$ContentByPk$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : ContentById$QueryRoot$ContentByPk$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String,
    dynamic> _$ContentById$QueryRoot$ContentByPk$Collections$CollectionToJson(
        ContentById$QueryRoot$ContentByPk$Collections$Collection instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'collection_type': instance.collectionType?.toJson(),
    };

ContentById$QueryRoot$ContentByPk$Collections
    _$ContentById$QueryRoot$ContentByPk$CollectionsFromJson(
        Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk$Collections()
    ..collection =
        ContentById$QueryRoot$ContentByPk$Collections$Collection.fromJson(
            json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ContentById$QueryRoot$ContentByPk$CollectionsToJson(
        ContentById$QueryRoot$ContentByPk$Collections instance) =>
    <String, dynamic>{
      'collection': instance.collection.toJson(),
    };

ContentById$QueryRoot$ContentByPk _$ContentById$QueryRoot$ContentByPkFromJson(
    Map<String, dynamic> json) {
  return ContentById$QueryRoot$ContentByPk()
    ..contentType = ContentById$QueryRoot$ContentByPk$ContentType.fromJson(
        json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) => ContentById$QueryRoot$ContentByPk$ChildContent.fromJson(
            e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) => ContentById$QueryRoot$ContentByPk$ParentContent.fromJson(
            e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            ContentById$QueryRoot$ContentByPk$ContentCategories.fromJson(
                e as Map<String, dynamic>))
        .toList()
    ..contentState = ContentById$QueryRoot$ContentByPk$ContentState.fromJson(
        json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) => ContentById$QueryRoot$ContentByPk$Collections.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$ContentById$QueryRoot$ContentByPkToJson(
        ContentById$QueryRoot$ContentByPk instance) =>
    <String, dynamic>{
      'content_type': instance.contentType.toJson(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'name': instance.name,
      'is_shared': instance.isShared,
      'id': instance.id,
      'data': instance.data,
      'created_at': instance.createdAt.toIso8601String(),
      'child_content': instance.childContent.map((e) => e.toJson()).toList(),
      'parent_content': instance.parentContent.map((e) => e.toJson()).toList(),
      'content_categories':
          instance.contentCategories.map((e) => e.toJson()).toList(),
      'content_state': instance.contentState.toJson(),
      'collections': instance.collections.map((e) => e.toJson()).toList(),
    };

ContentById$QueryRoot _$ContentById$QueryRootFromJson(
    Map<String, dynamic> json) {
  return ContentById$QueryRoot()
    ..contentByPk = json['content_by_pk'] == null
        ? null
        : ContentById$QueryRoot$ContentByPk.fromJson(
            json['content_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ContentById$QueryRootToJson(
        ContentById$QueryRoot instance) =>
    <String, dynamic>{
      'content_by_pk': instance.contentByPk?.toJson(),
    };

ContentByIdArguments _$ContentByIdArgumentsFromJson(Map<String, dynamic> json) {
  return ContentByIdArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$ContentByIdArgumentsToJson(
        ContentByIdArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
