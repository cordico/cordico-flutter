// ignore_for_file: cascade_invocations

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:cordico_mobile/core/managers/offline_models.dart/offline_models.dart';
import 'package:cordico_mobile/core/managers/offlline_manager.dart';
import 'package:cordico_mobile/features/toolkit/logic/organization_provider.dart';
import 'package:core/core.dart';
import 'package:core/utils/map_extension.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class DirectoryFilterState
    extends StateNotifier<List<OrganizationContactModel>> {
  DirectoryFilterState(this.ref) : super([]);

  bool _isLoading = false;
  bool get isLoading => _isLoading;

  bool _contentLoaded = false;
  bool get contentLoaded => _contentLoaded;

  final StateNotifierProviderRef ref;

  void reset() {
    state = [];
    _contentLoaded = false;
  }

  Future<void> getContent() async {
    if (contentLoaded == false) {
      final connectityCheck = await Connectivity().checkConnectivity();

      final list = OfflineManager.getData(key: ModelsKeys.contacts);
      if (list != null) {
        final contacts = OrganizationContactOffline.fromJson(list).data ?? [];
        state = contacts
            .map((e) => OrganizationContactModel.fromJson(e.toJson()))
            .toList();
      } else {
        state = [];
      }

      if (connectityCheck == ConnectivityResult.none) {
        final list = OfflineManager.getData(key: ModelsKeys.contacts);
        if (list != null) {
          final contacts = OrganizationContactOffline.fromJson(list).data ?? [];
          state = contacts
              .map((e) => OrganizationContactModel.fromJson(e.toJson()))
              .toList();
        } else {
          state = [];
        }
      } else {
        final contactsRef =
            ref.watch(organizationContactsStateNotifierProvider.notifier);
        _isLoading = true;
        final response = await contactsRef.getContacts();
        _isLoading = false;

        final orgContacts = OrganizationContactOffline();
        orgContacts.data = response;

        await OfflineManager.persistData(
          key: ModelsKeys.contacts,
          value: orgContacts.toJson(),
        );

        response?.forEach((element) {
          removeNullAndEmptyParams(element.data as Map<String, dynamic>);
        });

        state = response ?? [];

        _contentLoaded = true;
      }
    }
  }
}

final allContactsProvider =
    StateNotifierProvider<DirectoryFilterState, List<OrganizationContactModel>>(
        (ref) => DirectoryFilterState(ref));
