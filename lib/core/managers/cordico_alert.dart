import 'package:cordico_mobile/core/widgets/alerts/call_alert.dart';
import 'package:cordico_mobile/core/widgets/alerts/content_alert.dart';
import 'package:cordico_mobile/core/widgets/alerts/external_url_alert.dart';
import 'package:cordico_mobile/core/widgets/drop_down/cordico_filters_widget.dart';
import 'package:core/models/schema/content_schema.dart';
import 'package:flutter/material.dart';

class CordicoAlert {
  static Future<Filters?> showContentFilters(
    BuildContext context, {
    bool dismissable = true,
    bool filterByCategory = false,
    bool filterByContent = true,
    bool filterByDay = false,
    bool filterByBulletinType = false,
    bool hideReset = false,
    required Filters filters,
  }) async {
    return showDialog(
      context: context,
      barrierDismissible: dismissable,
      builder: (_) => CordicoFiltersWidget(
        dismissable: dismissable,
        filters: filters,
        filterByCategory: filterByCategory,
        filterByDay: filterByDay,
        filterByContent: filterByContent,
        hideReset: hideReset,
        filterByBulletinType: filterByBulletinType,
      ),
    );
  }

  static Future<void> showExternalLinkAlert(BuildContext context,
      {bool dismissable = true, required String url}) async {
    return showDialog(
      context: context,
      barrierColor: Colors.black.withOpacity(0.8),
      barrierDismissible: dismissable,
      builder: (_) => ExternalUrlAlert(
        dismissable: dismissable,
        url: url,
      ),
    );
  }

  static Future<void> showCallAlert(BuildContext context,
      {bool dismissable = true,
      required String phone,
      required ActionButtonSchema? action}) async {
    return showDialog(
      context: context,
      barrierColor: Colors.black.withOpacity(0.8),
      barrierDismissible: dismissable,
      builder: (_) => CallAlert(
        dismissable: dismissable,
        phone: phone,
        action: action,
      ),
    );
  }

  static Future<void> showContentAlert(
    BuildContext context, {
    bool dismissable = true,
    required String title,
    required String image,
    required String message,
    required String actionTitle,
    required Function()? action,
  }) async {
    return showDialog(
      context: context,
      barrierColor: Colors.black.withOpacity(0.8),
      barrierDismissible: dismissable,
      builder: (_) => ContentAlert(
        dismissable: dismissable,
        action: action,
        title: title,
        image: image,
        message: message,
        actionTitle: actionTitle,
      ),
    );
  }
}
