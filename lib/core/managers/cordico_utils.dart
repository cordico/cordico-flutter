// ignore_for_file: invariant_booleans, lines_longer_than_80_chars, use_build_context_synchronously

import 'dart:async';
import 'dart:ui' as ui;
import 'package:another_flushbar/flushbar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/images.dart';
import 'package:cordico_mobile/core/widgets/drop_down/cordico_filters_widget.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/graphql/organization/organization_contact_by_id.dart';
import 'package:core/models/schema/content_schema.dart';
import 'package:core/models/schema/organization_contact_schema.dart';
import 'package:core/utils/constants.dart';
import 'package:core/utils/map_extension.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:validators/validators.dart';

class CordicoUtils {
  static Flushbar? globalFlush;

  List<ContentQuery$QueryRoot$Content> filterContent({
    String query = '',
    List<String>? types,
    List<String>? categories,
    DaysFilter? dayFilter,
    List<BulletinType>? bulletinFilters,
    bool update = true,
    bool isBulletin = false,
    bool isEAP = false,
    required List<ContentQuery$QueryRoot$Content> content,
  }) {
    final filteredByContent = content
        .where((element) {
          if (isEAP) {
            return true;
          }
          if (isBulletin) {
            return element.contentType.id == kContentTypeBulletinId;
          } else {
            return global.contentTypeIds.contains(element.contentType.id);
          }
        })
        .cast<ContentQuery$QueryRoot$Content?>()
        .toList();

    var filtered =
        List<ContentQuery$QueryRoot$Content?>.from(filteredByContent);

    if (query.isEmpty &&
        (types == null || types.isEmpty) &&
        (categories == null || categories.isEmpty) &&
        (dayFilter == null || dayFilter == DaysFilter.allDays) &&
        (bulletinFilters == null || bulletinFilters.isEmpty)) {
      return List.from(filtered);
    }
    if (query.isNotEmpty) {
      filtered = filtered.where((element) {
        var title = '';
        if (element?.data != null &&
            (element?.data as Map).containsKey('title')) {
          title = (element?.data as Map)['title'] as String;
        }

        String? description = '';
        if (element?.data != null &&
            (element?.data as Map).containsKey('summary')) {
          description = (element?.data as Map)['summary'] as String?;
        }
        return title.toLowerCase().contains(query.toLowerCase()) ||
            (description != null &&
                description.toLowerCase().contains(query.toLowerCase()));
      }).toList();
    }

    if (types != null && types.isNotEmpty) {
      filtered = filtered.where((element) {
        if (element == null) {
          return false;
        }
        return types.contains(element.contentType.id);
      }).toList();
    }

    if (categories != null && categories.isNotEmpty) {
      filtered = filtered.where((element) {
        if (element == null) {
          return false;
        }

        return categories.any((catID) {
          final elements = element.contentCategories
              .where((cat) => cat.category.id == catID);
          return elements.isNotEmpty;
        });
      }).toList();
    }

    if (dayFilter != null && dayFilter != DaysFilter.allDays) {
      final now = DateTime.now();
      final filtersDays = dayFilter.getFilterDays();
      filtered = filtered.where((element) {
        if (element == null) {
          return false;
        }
        final diffDuration = element.createdAt.difference(now);
        return diffDuration.inDays.abs() < filtersDays;
      }).toList();
    }

    if (bulletinFilters != null && bulletinFilters.isNotEmpty) {
      filtered = filtered.where((element) {
        if (element == null) {
          return false;
        }

        removeNullAndEmptyParams(element.data as Map<String, dynamic>);

        final schema =
            ContentSchema.fromJson(element.data as Map<String, dynamic>);

        if (schema.type == null) {
          return false;
        }

        return bulletinFilters
            .contains(BulletinHelper.getType(schema.type ?? ''));
      }).toList();
    }

    return List.from(filtered);
  }

  List<OrganizationContactById$QueryRoot$OrganizationContactsByPk>
      filterContacts({
    String query = '',
    bool update = true,
    required List<OrganizationContactById$QueryRoot$OrganizationContactsByPk>
        content,
  }) {
    var filtered =
        List<OrganizationContactById$QueryRoot$OrganizationContactsByPk?>.from(
            content);

    List<OrganizationContactById$QueryRoot$OrganizationContactsByPk?>.from(
        content);

    if (query.isEmpty) {
      return List.from(content);
    }
    if (query.isNotEmpty) {
      filtered = content.where((element) {
        final contactSchema = OrganizationContactSchema.fromJson(
            element.data as Map<String, dynamic>);

        final zipCode = contactSchema.address?.zip ?? '';

        return (contactSchema.name?.displayName ?? '')
                .toLowerCase()
                .contains(query.toLowerCase()) ||
            (contactSchema.summary?.body ?? '')
                .toLowerCase()
                .contains(query.toLowerCase()) ||
            (zipCode.isNotEmpty &&
                zipCode.toLowerCase().contains(query.toLowerCase()));
      }).toList();
    }
    return List.from(filtered);
  }

  Future<BitmapDescriptor?> getMarkerIcon(String? imagePath,
      {double size = 50}) async {
    final pinSize = Size(
      size * WidgetsBinding.instance!.window.devicePixelRatio,
      size * WidgetsBinding.instance!.window.devicePixelRatio,
    );
    final pictureRecorder = ui.PictureRecorder();
    final canvas = Canvas(pictureRecorder);
    final radius = Radius.circular(pinSize.width / 6);
    //final Paint tagPaint = Paint()..color = Colors.blue;
    //final double tagWidth = 40.0;
    final shadowPaint = Paint()..color = const Color(0xFF2864FF);

    const shadowWidth = 8.0;

    final borderPaint = Paint()..color = Colors.white;
    //const borderWidth = 4.0;

    const imageOffset = shadowWidth;

    // Add shadow circle
    canvas.drawRRect(
        RRect.fromRectAndCorners(
          Rect.fromLTWH(0, 0, pinSize.width, pinSize.height),
          topLeft: radius,
          topRight: radius,
          bottomLeft: radius,
          bottomRight: radius,
        ),
        shadowPaint);

    // Add border circle
    // ignore: cascade_invocations
    canvas.drawRRect(
      RRect.fromRectAndCorners(
        Rect.fromLTWH(
            shadowWidth,
            shadowWidth,
            pinSize.width - (shadowWidth * 2),
            pinSize.height - (shadowWidth * 2)),
        topLeft: radius,
        topRight: radius,
        bottomLeft: radius,
        bottomRight: radius,
      ),
      borderPaint,
    );

    // // Add tag circle
    // canvas.drawRRect(
    //     RRect.fromRectAndCorners(
    //       Rect.fromLTWH(size.width - tagWidth, 0.0, tagWidth, tagWidth),
    //       topLeft: radius,
    //       topRight: radius,
    //       bottomLeft: radius,
    //       bottomRight: radius,
    //     ),
    //     tagPaint);

    // // Add tag text
    // TextPainter textPainter = TextPainter(textDirection: TextDirection.ltr);
    // textPainter.text = TextSpan(
    //   text: '1',
    //   style: TextStyle(fontSize: 20.0, color: Colors.white),
    // );

    // textPainter.layout();
    // textPainter.paint(
    //     canvas,
    //     Offset(size.width - tagWidth / 2 - textPainter.width / 2,
    //         tagWidth / 2 - textPainter.height / 2));

    // Oval for the image
    final oval = Rect.fromLTWH(imageOffset, imageOffset,
        pinSize.width - (imageOffset * 2), pinSize.height - (imageOffset * 2));

    // Add path for oval image
    canvas.clipPath(Path()..addRRect(BorderRadius.circular(20).toRRect(oval)));

    // Add image
    final image = await getImage(imagePath);

    paintImage(canvas: canvas, image: image, rect: oval, fit: BoxFit.cover);

    // Convert canvas to image
    final markerAsImage = await pictureRecorder
        .endRecording()
        .toImage(pinSize.width.toInt(), pinSize.height.toInt());

    // Convert image to bytes
    final byteData =
        await markerAsImage.toByteData(format: ui.ImageByteFormat.png);
    final uint8List = byteData?.buffer.asUint8List();

    if (uint8List != null) {
      return BitmapDescriptor.fromBytes(uint8List);
    }
    return null;
  }

  Future<ui.Image> getImage(String? url) async {
    final completer = Completer<ImageInfo>();
    ImageProvider img;

    if (url != null && isURL(url)) {
      img = CachedNetworkImageProvider(url, errorListener: () {});
    } else {
      img = AssetImage(Images.icPersonPin);
    }

    try {
      // ignore: use_named_constants
      img.resolve(const ImageConfiguration()).addListener(
        ImageStreamListener((ImageInfo info, bool _) {
          if (completer.isCompleted == false) {
            completer.complete(info);
          }
        }),
      );

      final imageInfo = await completer.future;
      return imageInfo.image;
    } catch (e) {
      img = AssetImage(Images.icPersonPin);
      final imageInfo = await completer.future;
      return imageInfo.image;
    }
  }

  double calculateAlpha(
    double _offset, {
    double fullOpacityOffset = 60,
    double zeroOpacityOffset = 50,
    bool reversed = false,
  }) {
    var val = 0.0;

    if (fullOpacityOffset > zeroOpacityOffset) {
      if (fullOpacityOffset == 0) {
        val = 1;
      } else if (fullOpacityOffset > zeroOpacityOffset) {
        // fading in
        if (_offset <= zeroOpacityOffset) {
          val = 0;
        } else if (_offset >= fullOpacityOffset) {
          val = 1;
        } else {
          val = (_offset - zeroOpacityOffset) /
              (fullOpacityOffset - zeroOpacityOffset);
        }
      } else {
        // fading out
        if (_offset <= fullOpacityOffset) {
          val = 1;
        } else if (_offset >= zeroOpacityOffset) {
          val = 0;
        } else {
          val = (_offset - fullOpacityOffset) /
              (zeroOpacityOffset - fullOpacityOffset);
        }
      }
    } else {
      if (zeroOpacityOffset == 0) {
        val = 1;
      } else if (zeroOpacityOffset > fullOpacityOffset) {
        // fading in
        if (_offset <= fullOpacityOffset) {
          val = 0;
        } else if (_offset >= zeroOpacityOffset) {
          val = 1;
        } else {
          val = (_offset - fullOpacityOffset) /
              (zeroOpacityOffset - fullOpacityOffset);
        }
      } else {
        // fading out
        if (_offset <= zeroOpacityOffset) {
          val = 1;
        } else if (_offset >= fullOpacityOffset) {
          val = 0;
        } else {
          val = (_offset - zeroOpacityOffset) /
              (fullOpacityOffset - zeroOpacityOffset);
        }
      }
    }

    if (reversed) {
      return 1 - val;
    } else {
      return val;
    }
  }

  static Future<void> showFlush(
    BuildContext context, {
    String? title,
    String? message,
    TextStyle? titleStyle,
    TextStyle? messageStyle,
    Function(Flushbar)? onTap,
    bool hasShadow = true,
    EdgeInsets? margin,
    Widget? icon,
    TextAlign? textAlign,
    FlushbarPosition position = FlushbarPosition.BOTTOM,
    double customBottomPadding = 12,
    Duration duration = const Duration(milliseconds: 3000),
  }) async {
    if (globalFlush != null) {
      await globalFlush!.dismiss();
    }

    globalFlush = Flushbar<dynamic>(
      titleText: title != null
          ? Text(
              title,
              textAlign: textAlign ?? TextAlign.center,
              maxLines: 1,
              style: Theme.of(context).textTheme.bodyText2?.copyWith(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontSize: 15,
                  ),
            )
          : null,
      backgroundColor: ThemeColors.blue.shade500,
      borderRadius: BorderRadius.circular(6),
      //blockBackgroundInteraction: false,
      flushbarPosition: position,
      icon: icon,
      messageText: Text(
        message ?? '',
        maxLines: 2,
        textAlign: textAlign ?? TextAlign.center,
        overflow: TextOverflow.ellipsis,
        style: Theme.of(context).textTheme.bodyText2?.copyWith(
              color: Colors.white,
              fontWeight: FontWeight.w400,
              fontSize: 13.33,
            ),
      ),
      duration: duration,
      animationDuration: const Duration(milliseconds: 500),
      boxShadows: hasShadow
          ? [
              const BoxShadow(
                color: Colors.black12,
                spreadRadius: 2,
                blurRadius: 12,
              )
            ]
          : null,
      margin: margin ??
          EdgeInsets.only(
            bottom: customBottomPadding,
            left: 16,
            right: 16,
          ),
      padding: const EdgeInsets.only(
        top: 14,
        bottom: 14,
        left: 12,
        right: 12,
      ),
      onTap: onTap,
    );

    await globalFlush!.show(context);
  }
}
