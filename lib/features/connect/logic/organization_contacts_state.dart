import 'package:core/core.dart';
import 'package:core/graphql/organization/organization_contact_by_id.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'organization_contacts_state.freezed.dart';

extension OrganizationContactsGetters on OrganizationContactsState {
  bool get isLoading => this is _Loading;
}

@freezed
class OrganizationContactsState with _$OrganizationContactsState {
  /// Data is present state
  const factory OrganizationContactsState.data(
      {required List<OrganizationContactModel>? content}) = _Data;

  /// Initial/default state
  const factory OrganizationContactsState.initial() = _Initial;

  /// Data is loading state
  const factory OrganizationContactsState.loading() = _Loading;

  /// Error when loading data state
  const factory OrganizationContactsState.error([String? message]) = _Error;
}
