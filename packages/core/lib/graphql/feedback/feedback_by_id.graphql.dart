// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'feedback_by_id.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class FeedbackById$QueryRoot$FeedbackByPk extends JsonSerializable
    with EquatableMixin {
  FeedbackById$QueryRoot$FeedbackByPk();

  factory FeedbackById$QueryRoot$FeedbackByPk.fromJson(
          Map<String, dynamic> json) =>
      _$FeedbackById$QueryRoot$FeedbackByPkFromJson(json);

  late String body;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  dynamic? data;

  @JsonKey(name: 'feedback_type_id')
  late String feedbackTypeId;

  late String id;

  String? subject;

  @JsonKey(name: 'updated_at')
  late DateTime updatedAt;

  @override
  List<Object?> get props =>
      [body, createdAt, data, feedbackTypeId, id, subject, updatedAt];
  @override
  Map<String, dynamic> toJson() =>
      _$FeedbackById$QueryRoot$FeedbackByPkToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FeedbackById$QueryRoot extends JsonSerializable with EquatableMixin {
  FeedbackById$QueryRoot();

  factory FeedbackById$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$FeedbackById$QueryRootFromJson(json);

  @JsonKey(name: 'feedback_by_pk')
  FeedbackById$QueryRoot$FeedbackByPk? feedbackByPk;

  @override
  List<Object?> get props => [feedbackByPk];
  @override
  Map<String, dynamic> toJson() => _$FeedbackById$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FeedbackByIdArguments extends JsonSerializable with EquatableMixin {
  FeedbackByIdArguments({required this.id});

  @override
  factory FeedbackByIdArguments.fromJson(Map<String, dynamic> json) =>
      _$FeedbackByIdArgumentsFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$FeedbackByIdArgumentsToJson(this);
}

final FEEDBACK_BY_ID_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'FeedbackById'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'feedback_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'id'),
                  value: VariableNode(name: NameNode(value: 'id')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'body'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'feedback_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'subject'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class FeedbackByIdQuery
    extends GraphQLQuery<FeedbackById$QueryRoot, FeedbackByIdArguments> {
  FeedbackByIdQuery({required this.variables});

  @override
  final DocumentNode document = FEEDBACK_BY_ID_QUERY_DOCUMENT;

  @override
  final String operationName = 'FeedbackById';

  @override
  final FeedbackByIdArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  FeedbackById$QueryRoot parse(Map<String, dynamic> json) =>
      FeedbackById$QueryRoot.fromJson(json);
}
