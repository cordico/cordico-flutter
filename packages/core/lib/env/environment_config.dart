import 'package:flutter_dotenv/flutter_dotenv.dart';

class EnvironmentConfig {
  EnvironmentConfig();

  /// URL for API
  static final contentGraphqlUrl = dotenv.env['ENVIRONMENT'] == 'production' ?
  'https://metadata.${dotenv.env['DOMAIN']}/v1/graphql' : 'https://metadata-${dotenv.env['ENVIRONMENT']}.${dotenv.env['DOMAIN']}/v1/graphql';
  static final userGraphqlUrl = dotenv.env['ENVIRONMENT'] == 'production' ?
  'https://cordico-user.${dotenv.env['DOMAIN']}/graphql' : 'https://cordico-user-${dotenv.env['ENVIRONMENT']}.${dotenv.env['DOMAIN']}/graphql';
  String? organizationId = dotenv.env['ORGANIZATION_ID'];
  static String kratosUrl = dotenv.env['ENVIRONMENT'] == 'production' ?
  'https://kratos.${dotenv.env['DOMAIN']}' : 'https://kratos-${dotenv.env['ENVIRONMENT']}.${dotenv.env['DOMAIN']}';
  static String? hasuraSecret = dotenv.env['HASURA_SECRET'];
}
