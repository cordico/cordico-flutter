// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'collection_type_by_id.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CollectionTypeById$QueryRoot$CollectionTypeByPk
    _$CollectionTypeById$QueryRoot$CollectionTypeByPkFromJson(
        Map<String, dynamic> json) {
  return CollectionTypeById$QueryRoot$CollectionTypeByPk()
    ..updaterId = json['updater_id'] as String?
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..schema = json['schema']
    ..name = json['name'] as String
    ..key = json['key'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?
    ..creatorId = json['creator_id'] as String?
    ..createdAt = DateTime.parse(json['created_at'] as String);
}

Map<String, dynamic> _$CollectionTypeById$QueryRoot$CollectionTypeByPkToJson(
        CollectionTypeById$QueryRoot$CollectionTypeByPk instance) =>
    <String, dynamic>{
      'updater_id': instance.updaterId,
      'updated_at': instance.updatedAt?.toIso8601String(),
      'schema': instance.schema,
      'name': instance.name,
      'key': instance.key,
      'id': instance.id,
      'description': instance.description,
      'creator_id': instance.creatorId,
      'created_at': instance.createdAt.toIso8601String(),
    };

CollectionTypeById$QueryRoot _$CollectionTypeById$QueryRootFromJson(
    Map<String, dynamic> json) {
  return CollectionTypeById$QueryRoot()
    ..collectionTypeByPk = json['collection_type_by_pk'] == null
        ? null
        : CollectionTypeById$QueryRoot$CollectionTypeByPk.fromJson(
            json['collection_type_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$CollectionTypeById$QueryRootToJson(
        CollectionTypeById$QueryRoot instance) =>
    <String, dynamic>{
      'collection_type_by_pk': instance.collectionTypeByPk?.toJson(),
    };

CollectionTypeByIdArguments _$CollectionTypeByIdArgumentsFromJson(
    Map<String, dynamic> json) {
  return CollectionTypeByIdArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$CollectionTypeByIdArgumentsToJson(
        CollectionTypeByIdArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
