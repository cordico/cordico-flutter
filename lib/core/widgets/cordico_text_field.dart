import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CordicoTextField extends StatefulWidget {
  const CordicoTextField({
    Key? key,
    this.controller,
    this.focusNode,
    this.hintText,
    this.labelText,
    this.helperText,
    this.errorText,
    this.obscureText = false,
    this.readOnly = false,
    this.prefixIcon,
    this.suffixIcon,
    this.keyboardType,
    this.onSubmitted,
    this.textInputAction,
    this.onChanged,
    this.onTap,
    this.textCapitalization = TextCapitalization.none,
    this.fillColor,
    this.filled = true,
    this.inputFormatters,
    this.isLoading = false,
    this.textAlign = TextAlign.start,
    this.style,
    this.hintStyle,
    this.labelStyle,
    this.borderColor,
    this.borderRadius,
    this.contentPadding,
    this.maxLines,
  }) : super(key: key);

  final TextEditingController? controller;
  final FocusNode? focusNode;
  final String? hintText;
  final String? labelText;
  final String? helperText;
  final String? errorText;
  final bool obscureText;
  final bool readOnly;
  final TextCapitalization? textCapitalization;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final VoidCallback? onTap;
  final TextInputType? keyboardType;
  final Color? fillColor;
  final bool filled;
  final bool isLoading;
  final TextAlign? textAlign;
  final TextStyle? style;
  final TextStyle? labelStyle;
  final TextStyle? hintStyle;
  final List<TextInputFormatter>? inputFormatters;
  final TextInputAction? textInputAction;
  final ValueChanged<String>? onSubmitted;
  final ValueChanged<String>? onChanged;
  final Color? borderColor;
  final double? borderRadius;
  final EdgeInsets? contentPadding;
  final int? maxLines;

  @override
  State createState() => _CordicoTextFieldState();
}

class _CordicoTextFieldState extends State<CordicoTextField> {
  @override
  Widget build(BuildContext context) {
    final children = widget.labelText != null
        ? [_buildLabelWidget(context), _buildInputWidget(context)]
        : [_buildInputWidget(context)];

    if (widget.errorText != null) {
      children.add(_buildErrorText(context, widget.errorText!));
    } else if (widget.helperText != null) {
      children.add(_buildHelperText(context, widget.helperText!));
    }

    return Column(
      children: children,
    );
  }

  Widget _buildLabelWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 2),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(
          widget.labelText!,
          style:
              widget.labelStyle ?? ThemeStyles.floatingLabelTextStyle(context),
        ),
      ),
    );
  }

  Widget _buildInputWidget(BuildContext context) {
    return TextField(
      style: widget.style ?? ThemeStyles.textFieldTextStyle(context),
      readOnly: widget.readOnly,
      onTap: widget.onTap,
      focusNode: widget.focusNode,
      controller: widget.controller,
      obscureText: widget.obscureText,
      onSubmitted: widget.onSubmitted,
      maxLines: widget.maxLines,
      keyboardType: widget.keyboardType,
      onChanged: widget.onChanged,
      textInputAction: widget.textInputAction,
      textCapitalization: widget.textCapitalization ?? TextCapitalization.none,
      textAlign: widget.textAlign ?? TextAlign.start,
      inputFormatters: widget.inputFormatters,
      decoration: InputDecoration(
        fillColor: widget.fillColor,
        filled: widget.filled,
        enabledBorder: ThemeStyles.outlineInputBorder(
            color: widget.borderColor, borderRadius: widget.borderRadius),
        border: widget.errorText != null
            ? ThemeStyles.outlineInputBorder(
                color: ThemeColors.textError, borderRadius: widget.borderRadius)
            : ThemeStyles.outlineInputBorder(
                color: widget.borderColor, borderRadius: widget.borderRadius),
        focusedBorder: ThemeStyles.focusedOutlineInputBorder(
            color: widget.readOnly
                ? (widget.borderColor ?? ThemeColors.gray.shade200)
                : ThemeColors.focusedColor,
            borderRadius: widget.borderRadius),
        disabledBorder: ThemeStyles.outlineInputBorder(
            color: ThemeColors.gray.shade100,
            borderRadius: widget.borderRadius),
        hintStyle: widget.hintStyle ?? ThemeStyles.textFieldTextStyle(context),
        floatingLabelBehavior: FloatingLabelBehavior.never,
        hintText: widget.hintText,
        suffixIcon: widget.isLoading ? _loadingWidget() : widget.suffixIcon,
        prefixIcon: widget.prefixIcon,
        contentPadding: widget.contentPadding ??
            const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
      ),
    );
  }

  Widget _loadingWidget() {
    return const Padding(
        padding: EdgeInsets.all(8),
        child: SizedBox(
          width: 10,
          height: 10,
          child: CircularProgressIndicator(
            strokeWidth: 1,
          ),
        ));
  }

  Widget _buildErrorText(BuildContext context, String text) {
    return Padding(
      padding: const EdgeInsets.only(top: 2),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(
          text,
          style: ThemeStyles.textFieldErrorTextStyle(context),
        ),
      ),
    );
  }

  Widget _buildHelperText(BuildContext context, String text) {
    return Padding(
      padding: const EdgeInsets.only(top: 2),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(
          text,
          style: ThemeStyles.textFieldErrorTextStyle(context)
              ?.copyWith(color: ThemeColors.gray.shade400),
        ),
      ),
    );
  }
}
