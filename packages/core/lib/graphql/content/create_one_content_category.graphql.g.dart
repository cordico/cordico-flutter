// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'create_one_content_category.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateOneContentCategoryMutation$MutationRoot$CreateOneContentCategory
    _$CreateOneContentCategoryMutation$MutationRoot$CreateOneContentCategoryFromJson(
        Map<String, dynamic> json) {
  return CreateOneContentCategoryMutation$MutationRoot$CreateOneContentCategory()
    ..id = json['id'] as String
    ..contentId = json['contentId'] as String
    ..categoryId = json['categoryId'] as String;
}

Map<String, dynamic>
    _$CreateOneContentCategoryMutation$MutationRoot$CreateOneContentCategoryToJson(
            CreateOneContentCategoryMutation$MutationRoot$CreateOneContentCategory
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'contentId': instance.contentId,
          'categoryId': instance.categoryId,
        };

CreateOneContentCategoryMutation$MutationRoot
    _$CreateOneContentCategoryMutation$MutationRootFromJson(
        Map<String, dynamic> json) {
  return CreateOneContentCategoryMutation$MutationRoot()
    ..createOneContentCategory =
        CreateOneContentCategoryMutation$MutationRoot$CreateOneContentCategory
            .fromJson(json['createOneContentCategory'] as Map<String, dynamic>);
}

Map<String, dynamic> _$CreateOneContentCategoryMutation$MutationRootToJson(
        CreateOneContentCategoryMutation$MutationRoot instance) =>
    <String, dynamic>{
      'createOneContentCategory': instance.createOneContentCategory.toJson(),
    };

CreateOneContentCategoryMutationArguments
    _$CreateOneContentCategoryMutationArgumentsFromJson(
        Map<String, dynamic> json) {
  return CreateOneContentCategoryMutationArguments(
    categoryId: json['categoryId'] as String,
    contentId: json['contentId'] as String,
  );
}

Map<String, dynamic> _$CreateOneContentCategoryMutationArgumentsToJson(
        CreateOneContentCategoryMutationArguments instance) =>
    <String, dynamic>{
      'categoryId': instance.categoryId,
      'contentId': instance.contentId,
    };
