import 'package:flutter/material.dart';

Future showSnackBar(BuildContext context, String message) async {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(message)));
}
