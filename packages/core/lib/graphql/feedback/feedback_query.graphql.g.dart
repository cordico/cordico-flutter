// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'feedback_query.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedbackQuery$QueryRoot$Feedback _$FeedbackQuery$QueryRoot$FeedbackFromJson(
    Map<String, dynamic> json) {
  return FeedbackQuery$QueryRoot$Feedback()
    ..body = json['body'] as String
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..data = json['data']
    ..feedbackTypeId = json['feedback_type_id'] as String
    ..id = json['id'] as String
    ..subject = json['subject'] as String?
    ..updatedAt = DateTime.parse(json['updated_at'] as String);
}

Map<String, dynamic> _$FeedbackQuery$QueryRoot$FeedbackToJson(
        FeedbackQuery$QueryRoot$Feedback instance) =>
    <String, dynamic>{
      'body': instance.body,
      'created_at': instance.createdAt.toIso8601String(),
      'data': instance.data,
      'feedback_type_id': instance.feedbackTypeId,
      'id': instance.id,
      'subject': instance.subject,
      'updated_at': instance.updatedAt.toIso8601String(),
    };

FeedbackQuery$QueryRoot _$FeedbackQuery$QueryRootFromJson(
    Map<String, dynamic> json) {
  return FeedbackQuery$QueryRoot()
    ..feedback = (json['feedback'] as List<dynamic>)
        .map((e) => FeedbackQuery$QueryRoot$Feedback.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$FeedbackQuery$QueryRootToJson(
        FeedbackQuery$QueryRoot instance) =>
    <String, dynamic>{
      'feedback': instance.feedback.map((e) => e.toJson()).toList(),
    };

FeedbackQueryArguments _$FeedbackQueryArgumentsFromJson(
    Map<String, dynamic> json) {
  return FeedbackQueryArguments(
    limit: json['limit'] as int?,
    offset: json['offset'] as int?,
    feedbackTypeIds: (json['feedbackTypeIds'] as List<dynamic>?)
        ?.map((e) => e as String)
        .toList(),
  );
}

Map<String, dynamic> _$FeedbackQueryArgumentsToJson(
        FeedbackQueryArguments instance) =>
    <String, dynamic>{
      'limit': instance.limit,
      'offset': instance.offset,
      'feedbackTypeIds': instance.feedbackTypeIds,
    };
