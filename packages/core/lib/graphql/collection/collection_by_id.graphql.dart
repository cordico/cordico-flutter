// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'collection_by_id.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionType
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionType();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionTypeFromJson(json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$OrganizationCollections$Organization
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$OrganizationCollections$Organization();

  factory CollectionById$QueryRoot$CollectionByPk$OrganizationCollections$Organization.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$OrganizationCollections$OrganizationFromJson(
          json);

  late String name;

  late String id;

  @override
  List<Object?> get props => [name, id];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$OrganizationCollections$OrganizationToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$OrganizationCollections
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$OrganizationCollections();

  factory CollectionById$QueryRoot$CollectionByPk$OrganizationCollections.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$OrganizationCollectionsFromJson(
          json);

  late CollectionById$QueryRoot$CollectionByPk$OrganizationCollections$Organization
      organization;

  @override
  List<Object?> get props => [organization];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$OrganizationCollectionsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentType
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentType();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentTypeFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentType
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentType();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentTypeFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ChildContent
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ChildContent();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ChildContentFromJson(
          json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ChildContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ParentContent
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ParentContent();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ParentContentFromJson(
          json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ParentContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategoriesFromJson(
          json);

  late CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category
      category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategoriesToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentState
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentState();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentStateFromJson(
          json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentStateToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$CollectionFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$CollectionsFromJson(
          json);

  late CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection
      collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$CollectionsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPartFromJson(
          json);

  @JsonKey(name: 'content_type')
  late CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentType
      contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<
          CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ChildContent>
      childContent;

  @JsonKey(name: 'parent_content')
  late List<
          CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ParentContent>
      parentContent;

  @JsonKey(name: 'content_categories')
  late List<
          CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentState
      contentState;

  late List<
          CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections>
      collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPartToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContentFromJson(
          json);

  @JsonKey(name: 'content_part')
  late CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart
      contentPart;

  late String id;

  late int index;

  @override
  List<Object?> get props => [contentPart, id, index];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ParentContent
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ParentContent();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ParentContentFromJson(
          json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ParentContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories$Category();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategoriesFromJson(
          json);

  late CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories$Category
      category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategoriesToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentState
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentState();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentStateFromJson(
          json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentStateToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection$CollectionType();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$CollectionFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$CollectionsFromJson(
          json);

  late CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection
      collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$CollectionsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$ContentFromJson(
          json);

  @JsonKey(name: 'content_type')
  late CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentType
      contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<
          CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent>
      childContent;

  @JsonKey(name: 'parent_content')
  late List<
          CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ParentContent>
      parentContent;

  @JsonKey(name: 'content_categories')
  late List<
          CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentState
      contentState;

  late List<
          CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections>
      collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$ContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk$CollectionContents
    extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk$CollectionContents();

  factory CollectionById$QueryRoot$CollectionByPk$CollectionContents.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContentsFromJson(
          json);

  late String id;

  @JsonKey(name: 'content_id')
  late String contentId;

  @JsonKey(name: 'collection_id')
  late String collectionId;

  late CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content
      content;

  @override
  List<Object?> get props => [id, contentId, collectionId, content];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPk$CollectionContentsToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot$CollectionByPk extends JsonSerializable
    with EquatableMixin {
  CollectionById$QueryRoot$CollectionByPk();

  factory CollectionById$QueryRoot$CollectionByPk.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionById$QueryRoot$CollectionByPkFromJson(json);

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String name;

  late String title;

  String? key;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @JsonKey(name: 'collection_type_id')
  String? collectionTypeId;

  @JsonKey(name: 'collection_type')
  CollectionById$QueryRoot$CollectionByPk$CollectionType? collectionType;

  @JsonKey(name: 'organization_collections')
  late List<CollectionById$QueryRoot$CollectionByPk$OrganizationCollections>
      organizationCollections;

  @JsonKey(name: 'collection_contents')
  late List<CollectionById$QueryRoot$CollectionByPk$CollectionContents>
      collectionContents;

  @override
  List<Object?> get props => [
        id,
        isShared,
        name,
        title,
        key,
        createdAt,
        updatedAt,
        collectionTypeId,
        collectionType,
        organizationCollections,
        collectionContents
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionById$QueryRoot$CollectionByPkToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CollectionById$QueryRoot extends JsonSerializable with EquatableMixin {
  CollectionById$QueryRoot();

  factory CollectionById$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$CollectionById$QueryRootFromJson(json);

  @JsonKey(name: 'collection_by_pk')
  CollectionById$QueryRoot$CollectionByPk? collectionByPk;

  @override
  List<Object?> get props => [collectionByPk];
  @override
  Map<String, dynamic> toJson() => _$CollectionById$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CollectionByIdArguments extends JsonSerializable with EquatableMixin {
  CollectionByIdArguments({required this.id, this.contentStateNames});

  @override
  factory CollectionByIdArguments.fromJson(Map<String, dynamic> json) =>
      _$CollectionByIdArgumentsFromJson(json);

  late String id;

  final List<String?>? contentStateNames;

  @override
  List<Object?> get props => [id, contentStateNames];
  @override
  Map<String, dynamic> toJson() => _$CollectionByIdArgumentsToJson(this);
}

final COLLECTION_BY_ID_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'CollectionById'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentStateNames')),
            type: ListTypeNode(
                type: NamedTypeNode(
                    name: NameNode(value: 'String'), isNonNull: false),
                isNonNull: false),
            defaultValue: DefaultValueNode(
                value: ListValueNode(values: [
              StringValueNode(value: 'Published', isBlock: false)
            ])),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'collection_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'id'),
                  value: VariableNode(name: NameNode(value: 'id')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'is_shared'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'title'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'collection_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'collection_type'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'description'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'organization_collections'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'organization'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ]))
                  ])),
              FieldNode(
                  name: NameNode(value: 'collection_contents'),
                  alias: null,
                  arguments: [
                    ArgumentNode(
                        name: NameNode(value: 'where'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'content'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'content_state'),
                                    value: ObjectValueNode(fields: [
                                      ObjectFieldNode(
                                          name: NameNode(value: 'name'),
                                          value: ObjectValueNode(fields: [
                                            ObjectFieldNode(
                                                name: NameNode(value: '_in'),
                                                value: VariableNode(
                                                    name: NameNode(
                                                        value:
                                                            'contentStateNames')))
                                          ]))
                                    ]))
                              ]))
                        ]))
                  ],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'content_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'collection_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'content'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'content_type'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'is_shared'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'description'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'updated_at'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'is_shared'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'data'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'created_at'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'child_content'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'content_part'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'content_type'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'name'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'is_shared'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'description'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null)
                                          ])),
                                      FieldNode(
                                          name: NameNode(value: 'updated_at'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'is_shared'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'data'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'created_at'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name:
                                              NameNode(value: 'child_content'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null)
                                          ])),
                                      FieldNode(
                                          name:
                                              NameNode(value: 'parent_content'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'index'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name:
                                                    NameNode(value: 'part_id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null)
                                          ])),
                                      FieldNode(
                                          name: NameNode(
                                              value: 'content_categories'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name:
                                                    NameNode(value: 'category'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: SelectionSetNode(
                                                    selections: [
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'name'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'id'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value:
                                                                  'description'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null)
                                                    ]))
                                          ])),
                                      FieldNode(
                                          name:
                                              NameNode(value: 'content_state'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'name'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null)
                                          ])),
                                      FieldNode(
                                          name: NameNode(value: 'collections'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'collection'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: SelectionSetNode(
                                                    selections: [
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'name'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'id'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value:
                                                                  'collection_type'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet:
                                                              SelectionSetNode(
                                                                  selections: [
                                                                FieldNode(
                                                                    name: NameNode(
                                                                        value:
                                                                            'name'),
                                                                    alias: null,
                                                                    arguments: [],
                                                                    directives: [],
                                                                    selectionSet:
                                                                        null),
                                                                FieldNode(
                                                                    name: NameNode(
                                                                        value:
                                                                            'id'),
                                                                    alias: null,
                                                                    arguments: [],
                                                                    directives: [],
                                                                    selectionSet:
                                                                        null),
                                                                FieldNode(
                                                                    name: NameNode(
                                                                        value:
                                                                            'description'),
                                                                    alias: null,
                                                                    arguments: [],
                                                                    directives: [],
                                                                    selectionSet:
                                                                        null)
                                                              ]))
                                                    ]))
                                          ]))
                                    ])),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'index'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'parent_content'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'index'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'part_id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'content_categories'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'category'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'description'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ]))
                              ])),
                          FieldNode(
                              name: NameNode(value: 'content_state'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'collections'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'collection'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(
                                              value: 'collection_type'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'name'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'description'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null)
                                          ]))
                                    ]))
                              ]))
                        ]))
                  ]))
            ]))
      ]))
]);

class CollectionByIdQuery
    extends GraphQLQuery<CollectionById$QueryRoot, CollectionByIdArguments> {
  CollectionByIdQuery({required this.variables});

  @override
  final DocumentNode document = COLLECTION_BY_ID_QUERY_DOCUMENT;

  @override
  final String operationName = 'CollectionById';

  @override
  final CollectionByIdArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  CollectionById$QueryRoot parse(Map<String, dynamic> json) =>
      CollectionById$QueryRoot.fromJson(json);
}
