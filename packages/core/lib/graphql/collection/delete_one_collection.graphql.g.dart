// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'delete_one_collection.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeleteOneCollection$MutationRoot$DeleteOneCollection
    _$DeleteOneCollection$MutationRoot$DeleteOneCollectionFromJson(
        Map<String, dynamic> json) {
  return DeleteOneCollection$MutationRoot$DeleteOneCollection()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$DeleteOneCollection$MutationRoot$DeleteOneCollectionToJson(
            DeleteOneCollection$MutationRoot$DeleteOneCollection instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

DeleteOneCollection$MutationRoot _$DeleteOneCollection$MutationRootFromJson(
    Map<String, dynamic> json) {
  return DeleteOneCollection$MutationRoot()
    ..deleteOneCollection = json['deleteOneCollection'] == null
        ? null
        : DeleteOneCollection$MutationRoot$DeleteOneCollection.fromJson(
            json['deleteOneCollection'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DeleteOneCollection$MutationRootToJson(
        DeleteOneCollection$MutationRoot instance) =>
    <String, dynamic>{
      'deleteOneCollection': instance.deleteOneCollection?.toJson(),
    };

DeleteOneCollectionArguments _$DeleteOneCollectionArgumentsFromJson(
    Map<String, dynamic> json) {
  return DeleteOneCollectionArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$DeleteOneCollectionArgumentsToJson(
        DeleteOneCollectionArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
