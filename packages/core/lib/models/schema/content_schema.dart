import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
part 'content_schema.g.dart';

@JsonSerializable(explicitToJson: true)
class ContentSchema extends JsonSerializable {
  ContentSchema();

  factory ContentSchema.fromJson(Map<String, dynamic> json) =>
      _$ContentSchemaFromJson(json);

  String? url;
  String? title;
  String? summary;
  String? body;
  num? lenght;

  String? author;

  String? type;

  @JsonKey(name: 'media_type')
  String? mediaType;

  @JsonKey(name: 'is_urgent')
  bool? isUrgent;

  @JsonKey(name: 'video_url')
  String? videoUrl1;

  @JsonKey(name: 'thumbnail_url')
  String? thumbnailUrl1;

  @JsonKey(
      name: 'body_mime_type',
      disallowNullValue: true,
      unknownEnumValue: MimeType.markdown)
  MimeType? bodyMimeType;

  String? link;
  String? audioUrl;
  String? videoUrl;
  String? coverUrl;

  @JsonKey(name: 'cover_image_url')
  String? coverImageUrl;

  List<AnswerSchema>? answers;

  @JsonKey(name: 'question_text')
  String? questionText;

  @JsonKey(name: 'question_type')
  String? questionType;

  String? imageUrl;
  String? thumbnail;
  String? description;

  @JsonKey(name: 'instructions_body')
  String? instructionsBody;

  String? icon;

  @JsonKey(name: 'contact_phone')
  String? contactPhone;

  @JsonKey(name: 'action_buttons')
  List<ActionButtonSchema>? actionButtons;

  List<OutcomeSchema>? outcomes;

  @override
  Map<String, dynamic> toJson() => _$ContentSchemaToJson(this);

  static ContentSchema fromJSON(Map<String, dynamic> json) {
    return ContentSchema.fromJson(json);
  }
}

@JsonSerializable(explicitToJson: true)
class OutcomeSchema {
  OutcomeSchema();

  factory OutcomeSchema.fromJson(Map<String, dynamic> json) =>
      _$OutcomeSchemaFromJson(json);

  @JsonKey(name: 'score_key')
  String? scoreKey;
  @JsonKey(name: 'score_name')
  String? scoreName;
  @JsonKey(name: 'score_ranges')
  List<ScoreRangeSchema>? scoreRanges;

  Map<String, dynamic> toJson() => _$OutcomeSchemaToJson(this);

  static OutcomeSchema fromJSON(Map<String, dynamic> json) {
    return OutcomeSchema.fromJson(json);
  }
}

@JsonSerializable(explicitToJson: true)
class ScoreRangeSchema {
  ScoreRangeSchema();

  factory ScoreRangeSchema.fromJson(Map<String, dynamic> json) =>
      _$ScoreRangeSchemaFromJson(json);

  @JsonKey(name: 'end_score')
  num? endScore;
  @JsonKey(name: 'start_score')
  num? startScore;
  String? interpretation;

  Map<String, dynamic> toJson() => _$ScoreRangeSchemaToJson(this);

  static ScoreRangeSchema fromJSON(Map<String, dynamic> json) {
    return ScoreRangeSchema.fromJson(json);
  }
}

@JsonSerializable(explicitToJson: true)
class AnswerSchema {
  AnswerSchema();

  factory AnswerSchema.fromJson(Map<String, dynamic> json) =>
      _$AnswerSchemaFromJson(json);

  String? text;
  dynamic index;
  dynamic score;

  @JsonKey(name: 'scale_scores')
  List<ScaleScoreAnswerSchema>? scaleScores;

  int getScore() {
    if (score is num) {
      return (score as num).toInt();
    } else if (score is String) {
      return int.tryParse(score as String) ?? 0;
    }
    return 0;
  }

  int getIndex() {
    if (index is num) {
      return (index as num).toInt();
    } else if (index is String) {
      return int.tryParse(index as String) ?? 0;
    }
    return 0;
  }

  Map<String, dynamic> toJson() => _$AnswerSchemaToJson(this);

  static AnswerSchema fromJSON(Map<String, dynamic> json) {
    return AnswerSchema.fromJson(json);
  }
}

@JsonSerializable(explicitToJson: true)
class ScaleScoreAnswerSchema {
  ScaleScoreAnswerSchema();

  factory ScaleScoreAnswerSchema.fromJson(Map<String, dynamic> json) =>
      _$ScaleScoreAnswerSchemaFromJson(json);

  num? score;
  @JsonKey(name: 'scale_key')
  String? scaleKey;

  Map<String, dynamic> toJson() => _$ScaleScoreAnswerSchemaToJson(this);

  static ScaleScoreAnswerSchema fromJSON(Map<String, dynamic> json) {
    return ScaleScoreAnswerSchema.fromJson(json);
  }
}

@JsonSerializable(explicitToJson: true)
class ActionButtonSchema {
  ActionButtonSchema();

  factory ActionButtonSchema.fromJson(Map<String, dynamic> json) =>
      _$ActionButtonSchemaFromJson(json);
  String? label;

  @JsonKey(name: 'default')
  bool? isDefault;

  @JsonKey(name: 'action_type')
  ActionButtonType? actionType;

  @JsonKey(name: 'display_order')
  num? displayOrder;

  @JsonKey(name: 'material_icon')
  ActionButtonMaterialIcon? materialIcon;

  @JsonKey(name: 'action_context')
  String? actionContext;

  @JsonKey(name: 'action_content')
  String? actionContent;

  @JsonKey(name: 'action_data')
  String? actionData;

  Map<String, dynamic> toJson() => _$ActionButtonSchemaToJson(this);

  static ActionButtonSchema fromJSON(Map<String, dynamic> json) {
    return ActionButtonSchema.fromJson(json);
  }
}

extension ActionButtonIconMapping on ActionButtonSchema {
  IconData? get materialActionIcon {
    if (materialIcon == null) {
      return null;
    }

    switch (materialIcon) {
      case ActionButtonMaterialIcon.call:
        return Icons.phone_locked_outlined;
      case ActionButtonMaterialIcon.textsms:
        return Icons.sms_outlined;
      case ActionButtonMaterialIcon.mailOnline:
        return Icons.mail_outline;
      case ActionButtonMaterialIcon.textSnippet:
        return Icons.text_snippet;
      case ActionButtonMaterialIcon.pictureAsPdf:
        return Icons.picture_as_pdf;
      case ActionButtonMaterialIcon.openInNew:
        return Icons.open_in_new;
      case ActionButtonMaterialIcon.mapOutlined:
        return Icons.map_outlined;
      case ActionButtonMaterialIcon.phoneEnabled:
        return Icons.phone_outlined;
      default:
        return Icons.phone_outlined;
    }
  }
}

enum MimeType {
  @JsonValue('text/html')
  html,
  @JsonValue('text/markdown')
  markdown,
}

extension MimeTypeString on MimeType {
  String asString() {
    switch (this) {
      case MimeType.html:
        return 'text/html';
      case MimeType.markdown:
        return 'text/markdown';
    }
  }
}

enum ActionButtonType {
  call,
  sms,
  email,
  @JsonValue('anonymous_call')
  anonymousCall,
  @JsonValue('load_content')
  loadContent,
  map,
}

extension ActionButtonTypeString on ActionButtonType {
  String asString() {
    switch (this) {
      case ActionButtonType.call:
        return 'Call';
      case ActionButtonType.sms:
        return 'SMS';
      case ActionButtonType.email:
        return 'Email';
      case ActionButtonType.anonymousCall:
        return 'Anonymous Call';
      case ActionButtonType.loadContent:
        return 'Load Content';
      case ActionButtonType.map:
        return 'Map';
    }
  }
}

enum ActionButtonMaterialIcon {
  call,
  textsms,
  @JsonValue('mail_online')
  mailOnline,
  @JsonValue('text_snippet')
  textSnippet,
  @JsonValue('picture_as_pdf')
  pictureAsPdf,
  @JsonValue('open_in_new')
  openInNew,
  @JsonValue('map_outlined')
  mapOutlined,
  @JsonValue('phone_enabled')
  phoneEnabled
}

extension ActionButtonMaterialIconString on ActionButtonMaterialIcon {
  String asString() {
    switch (this) {
      case ActionButtonMaterialIcon.call:
        return 'Call';
      case ActionButtonMaterialIcon.textsms:
        return 'Text SMS';
      case ActionButtonMaterialIcon.mailOnline:
        return 'Mail Online';
      case ActionButtonMaterialIcon.textSnippet:
        return 'Text Snippet';
      case ActionButtonMaterialIcon.pictureAsPdf:
        return 'Picture as PSD';
      case ActionButtonMaterialIcon.openInNew:
        return 'Open in New';
      case ActionButtonMaterialIcon.mapOutlined:
        return 'Map Outlined';
      case ActionButtonMaterialIcon.phoneEnabled:
        return 'Phone Enabled';
    }
  }
}
