// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'content_type_by_id.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContentTypeById$QueryRoot$ContentTypeByPk
    _$ContentTypeById$QueryRoot$ContentTypeByPkFromJson(
        Map<String, dynamic> json) {
  return ContentTypeById$QueryRoot$ContentTypeByPk()
    ..id = json['id'] as String
    ..name = json['name'] as String
    ..description = json['description'] as String?
    ..schema = json['schema']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..creatorId = json['creator_id'] as String
    ..updaterId = json['updater_id'] as String?
    ..parentTypeId = json['parent_type_id'] as String?
    ..isShared = json['is_shared'] as bool
    ..supportsCategory = json['supports_category'] as bool
    ..key = json['key'] as String;
}

Map<String, dynamic> _$ContentTypeById$QueryRoot$ContentTypeByPkToJson(
        ContentTypeById$QueryRoot$ContentTypeByPk instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'schema': instance.schema,
      'created_at': instance.createdAt.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'creator_id': instance.creatorId,
      'updater_id': instance.updaterId,
      'parent_type_id': instance.parentTypeId,
      'is_shared': instance.isShared,
      'supports_category': instance.supportsCategory,
      'key': instance.key,
    };

ContentTypeById$QueryRoot _$ContentTypeById$QueryRootFromJson(
    Map<String, dynamic> json) {
  return ContentTypeById$QueryRoot()
    ..contentTypeByPk = json['content_type_by_pk'] == null
        ? null
        : ContentTypeById$QueryRoot$ContentTypeByPk.fromJson(
            json['content_type_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ContentTypeById$QueryRootToJson(
        ContentTypeById$QueryRoot instance) =>
    <String, dynamic>{
      'content_type_by_pk': instance.contentTypeByPk?.toJson(),
    };

ContentTypeByIdArguments _$ContentTypeByIdArgumentsFromJson(
    Map<String, dynamic> json) {
  return ContentTypeByIdArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$ContentTypeByIdArgumentsToJson(
        ContentTypeByIdArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
