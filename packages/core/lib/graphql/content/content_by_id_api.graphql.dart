// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'content_by_id_api.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$ContentType extends JsonSerializable
    with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$ContentType();

  factory ContentById$QueryRoot$ContentByPk$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$ContentTypeFromJson(json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$ContentTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentType
    extends JsonSerializable with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentType();

  factory ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentTypeFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ChildContent
    extends JsonSerializable with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ChildContent();

  factory ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ChildContentFromJson(
          json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ChildContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ParentContent
    extends JsonSerializable with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ParentContent();

  factory ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ParentContentFromJson(
          json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ParentContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories$Category();

  factory ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories
    extends JsonSerializable with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories();

  factory ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategoriesFromJson(
          json);

  late ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories$Category
      category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategoriesToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentState
    extends JsonSerializable with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentState();

  factory ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentStateFromJson(
          json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentStateToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection$CollectionType();

  factory ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection();

  factory ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$CollectionFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections
    extends JsonSerializable with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections();

  factory ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$CollectionsFromJson(
          json);

  late ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections$Collection
      collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$CollectionsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart
    extends JsonSerializable with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart();

  factory ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPartFromJson(
          json);

  @JsonKey(name: 'content_type')
  late ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentType
      contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<
          ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ChildContent>
      childContent;

  @JsonKey(name: 'parent_content')
  late List<
          ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ParentContent>
      parentContent;

  @JsonKey(name: 'content_categories')
  late List<
          ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$ContentState
      contentState;

  late List<
          ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart$Collections>
      collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$ChildContent$ContentPartToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$ChildContent extends JsonSerializable
    with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$ChildContent();

  factory ContentById$QueryRoot$ContentByPk$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$ChildContentFromJson(json);

  @JsonKey(name: 'content_part')
  late ContentById$QueryRoot$ContentByPk$ChildContent$ContentPart contentPart;

  late String id;

  late int index;

  @override
  List<Object?> get props => [contentPart, id, index];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$ChildContentToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$ParentContent extends JsonSerializable
    with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$ParentContent();

  factory ContentById$QueryRoot$ContentByPk$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$ParentContentFromJson(json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$ParentContentToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$ContentCategories$Category();

  factory ContentById$QueryRoot$ContentByPk$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$ContentCategories
    extends JsonSerializable with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$ContentCategories();

  factory ContentById$QueryRoot$ContentByPk$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$ContentCategoriesFromJson(json);

  late ContentById$QueryRoot$ContentByPk$ContentCategories$Category category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$ContentCategoriesToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$ContentState extends JsonSerializable
    with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$ContentState();

  factory ContentById$QueryRoot$ContentByPk$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$ContentStateFromJson(json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$ContentStateToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$Collections$Collection$CollectionType();

  factory ContentById$QueryRoot$ContentByPk$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$Collections$Collection();

  factory ContentById$QueryRoot$ContentByPk$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$Collections$CollectionFromJson(json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  ContentById$QueryRoot$ContentByPk$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$Collections$CollectionToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk$Collections extends JsonSerializable
    with EquatableMixin {
  ContentById$QueryRoot$ContentByPk$Collections();

  factory ContentById$QueryRoot$ContentByPk$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPk$CollectionsFromJson(json);

  late ContentById$QueryRoot$ContentByPk$Collections$Collection collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPk$CollectionsToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot$ContentByPk extends JsonSerializable
    with EquatableMixin {
  ContentById$QueryRoot$ContentByPk();

  factory ContentById$QueryRoot$ContentByPk.fromJson(
          Map<String, dynamic> json) =>
      _$ContentById$QueryRoot$ContentByPkFromJson(json);

  @JsonKey(name: 'content_type')
  late ContentById$QueryRoot$ContentByPk$ContentType contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<ContentById$QueryRoot$ContentByPk$ChildContent> childContent;

  @JsonKey(name: 'parent_content')
  late List<ContentById$QueryRoot$ContentByPk$ParentContent> parentContent;

  @JsonKey(name: 'content_categories')
  late List<ContentById$QueryRoot$ContentByPk$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late ContentById$QueryRoot$ContentByPk$ContentState contentState;

  late List<ContentById$QueryRoot$ContentByPk$Collections> collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentById$QueryRoot$ContentByPkToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentById$QueryRoot extends JsonSerializable with EquatableMixin {
  ContentById$QueryRoot();

  factory ContentById$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$ContentById$QueryRootFromJson(json);

  @JsonKey(name: 'content_by_pk')
  ContentById$QueryRoot$ContentByPk? contentByPk;

  @override
  List<Object?> get props => [contentByPk];
  @override
  Map<String, dynamic> toJson() => _$ContentById$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentByIdArguments extends JsonSerializable with EquatableMixin {
  ContentByIdArguments({required this.id});

  @override
  factory ContentByIdArguments.fromJson(Map<String, dynamic> json) =>
      _$ContentByIdArgumentsFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$ContentByIdArgumentsToJson(this);
}

final CONTENT_BY_ID_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'ContentById'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'content_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'id'),
                  value: VariableNode(name: NameNode(value: 'id')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'content_type'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'is_shared'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'description'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'is_shared'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'child_content'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'content_part'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'content_type'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'is_shared'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'description'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'updated_at'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'is_shared'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'data'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'created_at'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'child_content'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'parent_content'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'index'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'part_id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'content_categories'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'category'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'description'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ]))
                              ])),
                          FieldNode(
                              name: NameNode(value: 'content_state'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'collections'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'collection'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(
                                              value: 'collection_type'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'name'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'description'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null)
                                          ]))
                                    ]))
                              ]))
                        ])),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'index'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'parent_content'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'index'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'part_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'content_categories'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'category'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'description'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ]))
                  ])),
              FieldNode(
                  name: NameNode(value: 'content_state'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'collections'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'collection'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'collection_type'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'description'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ]))
                        ]))
                  ]))
            ]))
      ]))
]);

class ContentByIdQuery
    extends GraphQLQuery<ContentById$QueryRoot, ContentByIdArguments> {
  ContentByIdQuery({required this.variables});

  @override
  final DocumentNode document = CONTENT_BY_ID_QUERY_DOCUMENT;

  @override
  final String operationName = 'ContentById';

  @override
  final ContentByIdArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  ContentById$QueryRoot parse(Map<String, dynamic> json) =>
      ContentById$QueryRoot.fromJson(json);
}
