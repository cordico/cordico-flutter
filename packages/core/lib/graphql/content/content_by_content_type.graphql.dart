// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'content_by_content_type.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$ContentType
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$ContentType();

  factory ContentByContentType$QueryRoot$Content$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$ContentTypeFromJson(json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$ContentTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentType
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentType();

  factory ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentTypeFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ChildContent
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ChildContent();

  factory ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ChildContentFromJson(
          json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ChildContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ParentContent
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ParentContent();

  factory ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ParentContentFromJson(
          json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ParentContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category();

  factory ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories();

  factory ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategoriesFromJson(
          json);

  late ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category
      category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategoriesToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentState
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentState();

  factory ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentStateFromJson(
          json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentStateToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType();

  factory ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection();

  factory ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$CollectionFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections();

  factory ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$CollectionsFromJson(
          json);

  late ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection
      collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$CollectionsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$ChildContent$ContentPart
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$ChildContent$ContentPart();

  factory ContentByContentType$QueryRoot$Content$ChildContent$ContentPart.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPartFromJson(
          json);

  @JsonKey(name: 'content_type')
  late ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentType
      contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<
          ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ChildContent>
      childContent;

  @JsonKey(name: 'parent_content')
  late List<
          ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ParentContent>
      parentContent;

  @JsonKey(name: 'content_categories')
  late List<
          ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentState
      contentState;

  late List<
          ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections>
      collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPartToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$ChildContent
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$ChildContent();

  factory ContentByContentType$QueryRoot$Content$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$ChildContentFromJson(json);

  @JsonKey(name: 'content_part')
  late ContentByContentType$QueryRoot$Content$ChildContent$ContentPart
      contentPart;

  late String id;

  late int index;

  @override
  List<Object?> get props => [contentPart, id, index];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$ChildContentToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$ParentContent
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$ParentContent();

  factory ContentByContentType$QueryRoot$Content$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$ParentContentFromJson(json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$ParentContentToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$ContentCategories$Category();

  factory ContentByContentType$QueryRoot$Content$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$ContentCategories
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$ContentCategories();

  factory ContentByContentType$QueryRoot$Content$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$ContentCategoriesFromJson(json);

  late ContentByContentType$QueryRoot$Content$ContentCategories$Category
      category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$ContentCategoriesToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$ContentState
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$ContentState();

  factory ContentByContentType$QueryRoot$Content$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$ContentStateFromJson(json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$ContentStateToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$Collections$Collection$CollectionType();

  factory ContentByContentType$QueryRoot$Content$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$Collections$Collection();

  factory ContentByContentType$QueryRoot$Content$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$Collections$CollectionFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  ContentByContentType$QueryRoot$Content$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$Collections$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content$Collections
    extends JsonSerializable with EquatableMixin {
  ContentByContentType$QueryRoot$Content$Collections();

  factory ContentByContentType$QueryRoot$Content$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$Content$CollectionsFromJson(json);

  late ContentByContentType$QueryRoot$Content$Collections$Collection collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$Content$CollectionsToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot$Content extends JsonSerializable
    with EquatableMixin {
  ContentByContentType$QueryRoot$Content();

  factory ContentByContentType$QueryRoot$Content.fromJson(
          Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRoot$ContentFromJson(json);

  @JsonKey(name: 'content_type')
  late ContentByContentType$QueryRoot$Content$ContentType contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<ContentByContentType$QueryRoot$Content$ChildContent> childContent;

  @JsonKey(name: 'parent_content')
  late List<ContentByContentType$QueryRoot$Content$ParentContent> parentContent;

  @JsonKey(name: 'content_categories')
  late List<ContentByContentType$QueryRoot$Content$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late ContentByContentType$QueryRoot$Content$ContentState contentState;

  late List<ContentByContentType$QueryRoot$Content$Collections> collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentByContentType$QueryRoot$ContentToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentType$QueryRoot extends JsonSerializable
    with EquatableMixin {
  ContentByContentType$QueryRoot();

  factory ContentByContentType$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$ContentByContentType$QueryRootFromJson(json);

  late List<ContentByContentType$QueryRoot$Content> content;

  @override
  List<Object?> get props => [content];
  @override
  Map<String, dynamic> toJson() => _$ContentByContentType$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentByContentTypeArguments extends JsonSerializable
    with EquatableMixin {
  ContentByContentTypeArguments(
      {this.contentTypeIDs, this.contentStateNames, this.offset, this.limit});

  @override
  factory ContentByContentTypeArguments.fromJson(Map<String, dynamic> json) =>
      _$ContentByContentTypeArgumentsFromJson(json);

  final List<String>? contentTypeIDs;

  final List<String?>? contentStateNames;

  final int? offset;

  final int? limit;

  @override
  List<Object?> get props => [contentTypeIDs, contentStateNames, offset, limit];
  @override
  Map<String, dynamic> toJson() => _$ContentByContentTypeArgumentsToJson(this);
}

final CONTENT_BY_CONTENT_TYPE_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'ContentByContentType'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentTypeIDs')),
            type: ListTypeNode(
                type: NamedTypeNode(
                    name: NameNode(value: 'uuid'), isNonNull: true),
                isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentStateNames')),
            type: ListTypeNode(
                type: NamedTypeNode(
                    name: NameNode(value: 'String'), isNonNull: false),
                isNonNull: false),
            defaultValue: DefaultValueNode(
                value: ListValueNode(values: [
              StringValueNode(value: 'Published', isBlock: false)
            ])),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'offset')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '0')),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'limit')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '10000')),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'content'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'offset'),
                  value: VariableNode(name: NameNode(value: 'offset'))),
              ArgumentNode(
                  name: NameNode(value: 'limit'),
                  value: VariableNode(name: NameNode(value: 'limit'))),
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'content_state'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'name'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: '_in'),
                                    value: VariableNode(
                                        name: NameNode(
                                            value: 'contentStateNames')))
                              ]))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'content_type_id'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: '_in'),
                              value: VariableNode(
                                  name: NameNode(value: 'contentTypeIDs')))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'content_type'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'is_shared'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'description'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'is_shared'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'child_content'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'content_part'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'content_type'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'is_shared'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'description'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'updated_at'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'is_shared'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'data'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'created_at'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'child_content'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'parent_content'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'index'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'part_id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'content_categories'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'category'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'description'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ]))
                              ])),
                          FieldNode(
                              name: NameNode(value: 'content_state'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'collections'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'collection'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(
                                              value: 'collection_type'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'name'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'description'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null)
                                          ]))
                                    ]))
                              ]))
                        ])),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'index'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'parent_content'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'index'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'part_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'content_categories'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'category'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'description'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ]))
                  ])),
              FieldNode(
                  name: NameNode(value: 'content_state'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'collections'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'collection'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'collection_type'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'description'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ]))
                        ]))
                  ]))
            ]))
      ]))
]);

class ContentByContentTypeQuery extends GraphQLQuery<
    ContentByContentType$QueryRoot, ContentByContentTypeArguments> {
  ContentByContentTypeQuery({required this.variables});

  @override
  final DocumentNode document = CONTENT_BY_CONTENT_TYPE_QUERY_DOCUMENT;

  @override
  final String operationName = 'ContentByContentType';

  @override
  final ContentByContentTypeArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  ContentByContentType$QueryRoot parse(Map<String, dynamic> json) =>
      ContentByContentType$QueryRoot.fromJson(json);
}
