import 'package:cordico_mobile/core/widgets/cordico_ink_well.dart';
import 'package:flutter/material.dart';

class CordicoCard extends StatelessWidget {
  const CordicoCard({
    Key? key,
    this.elevation = 0.0,
    this.cornerRadius = 4.0,
    required this.child,
    this.onTap,
    this.color,
    this.margin,
    this.borderSide,
  }) : super(key: key);

  final double elevation;
  final Widget child;
  final double cornerRadius;
  final VoidCallback? onTap;
  final Color? color;
  final EdgeInsetsGeometry? margin;
  final BorderSide? borderSide;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: elevation,
      margin: margin ?? EdgeInsets.zero,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(cornerRadius)),
        side: borderSide ?? BorderSide.none,
      ),
      color: color ?? Colors.white,
      child: onTap != null
          ? CordicoInkWell(
              onTap: onTap,
              child: child,
            )
          : child,
    );
  }
}
