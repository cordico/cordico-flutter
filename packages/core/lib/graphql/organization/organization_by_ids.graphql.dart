// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'organization_by_ids.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class OrganizationByIDs$QueryRoot$Organization extends JsonSerializable
    with EquatableMixin {
  OrganizationByIDs$QueryRoot$Organization();

  factory OrganizationByIDs$QueryRoot$Organization.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationByIDs$QueryRoot$OrganizationFromJson(json);

  late String name;

  late String id;

  String? description;

  late dynamic data;

  @JsonKey(name: 'organization_type_id')
  late String organizationTypeId;

  @JsonKey(name: 'portal_fqdn')
  String? portalFqdn;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @override
  List<Object?> get props => [
        name,
        id,
        description,
        data,
        organizationTypeId,
        portalFqdn,
        createdAt,
        updatedAt
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationByIDs$QueryRoot$OrganizationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationByIDs$QueryRoot extends JsonSerializable with EquatableMixin {
  OrganizationByIDs$QueryRoot();

  factory OrganizationByIDs$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$OrganizationByIDs$QueryRootFromJson(json);

  late List<OrganizationByIDs$QueryRoot$Organization> organization;

  @override
  List<Object?> get props => [organization];
  @override
  Map<String, dynamic> toJson() => _$OrganizationByIDs$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationByIDsArguments extends JsonSerializable with EquatableMixin {
  OrganizationByIDsArguments({this.ids});

  @override
  factory OrganizationByIDsArguments.fromJson(Map<String, dynamic> json) =>
      _$OrganizationByIDsArgumentsFromJson(json);

  final List<String>? ids;

  @override
  List<Object?> get props => [ids];
  @override
  Map<String, dynamic> toJson() => _$OrganizationByIDsArgumentsToJson(this);
}

final ORGANIZATION_BY_I_DS_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'OrganizationByIDs'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'ids')),
            type: ListTypeNode(
                type: NamedTypeNode(
                    name: NameNode(value: 'uuid'), isNonNull: true),
                isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'organization'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'id'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: '_in'),
                              value: VariableNode(name: NameNode(value: 'ids')))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'description'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'organization_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'portal_fqdn'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class OrganizationByIDsQuery extends GraphQLQuery<OrganizationByIDs$QueryRoot,
    OrganizationByIDsArguments> {
  OrganizationByIDsQuery({required this.variables});

  @override
  final DocumentNode document = ORGANIZATION_BY_I_DS_QUERY_DOCUMENT;

  @override
  final String operationName = 'OrganizationByIDs';

  @override
  final OrganizationByIDsArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  OrganizationByIDs$QueryRoot parse(Map<String, dynamic> json) =>
      OrganizationByIDs$QueryRoot.fromJson(json);
}
