// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'update_one_user_mutation.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateOneUserMutation$MutationRoot$UpdateOneUser
    _$UpdateOneUserMutation$MutationRoot$UpdateOneUserFromJson(
        Map<String, dynamic> json) {
  return UpdateOneUserMutation$MutationRoot$UpdateOneUser()
    ..id = json['id'] as String;
}

Map<String, dynamic> _$UpdateOneUserMutation$MutationRoot$UpdateOneUserToJson(
        UpdateOneUserMutation$MutationRoot$UpdateOneUser instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

UpdateOneUserMutation$MutationRoot _$UpdateOneUserMutation$MutationRootFromJson(
    Map<String, dynamic> json) {
  return UpdateOneUserMutation$MutationRoot()
    ..updateOneUser = json['updateOneUser'] == null
        ? null
        : UpdateOneUserMutation$MutationRoot$UpdateOneUser.fromJson(
            json['updateOneUser'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UpdateOneUserMutation$MutationRootToJson(
        UpdateOneUserMutation$MutationRoot instance) =>
    <String, dynamic>{
      'updateOneUser': instance.updateOneUser?.toJson(),
    };

JsonFieldUpdateOperationsInput _$JsonFieldUpdateOperationsInputFromJson(
    Map<String, dynamic> json) {
  return JsonFieldUpdateOperationsInput(
    kw$set: json['set'],
  );
}

Map<String, dynamic> _$JsonFieldUpdateOperationsInputToJson(
        JsonFieldUpdateOperationsInput instance) =>
    <String, dynamic>{
      'set': instance.kw$set,
    };

UpdateOneUserMutationArguments _$UpdateOneUserMutationArgumentsFromJson(
    Map<String, dynamic> json) {
  return UpdateOneUserMutationArguments(
    id: json['id'] as String?,
    identifier: json['identifier'] as String,
    userTypeId: json['userTypeId'] as String?,
    data: json['data'] == null
        ? null
        : JsonFieldUpdateOperationsInput.fromJson(
            json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$UpdateOneUserMutationArgumentsToJson(
        UpdateOneUserMutationArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
      'identifier': instance.identifier,
      'userTypeId': instance.userTypeId,
      'data': instance.data?.toJson(),
    };
