// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'create_one_asset_mutation.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CreateOneAssetMutation$MutationRoot extends JsonSerializable
    with EquatableMixin {
  CreateOneAssetMutation$MutationRoot();

  factory CreateOneAssetMutation$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneAssetMutation$MutationRootFromJson(json);

  late String createOneAssetWithFile;

  @override
  List<Object?> get props => [createOneAssetWithFile];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneAssetMutation$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateAssetWithFileInput extends JsonSerializable with EquatableMixin {
  CreateAssetWithFileInput(
      {required this.assetTypeId,
      required this.data,
      required this.fileName,
      required this.mimeType});

  factory CreateAssetWithFileInput.fromJson(Map<String, dynamic> json) =>
      _$CreateAssetWithFileInputFromJson(json);

  late String assetTypeId;

  late dynamic data;

  late String fileName;

  late String mimeType;

  @override
  List<Object?> get props => [assetTypeId, data, fileName, mimeType];
  @override
  Map<String, dynamic> toJson() => _$CreateAssetWithFileInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneAssetMutationArguments extends JsonSerializable
    with EquatableMixin {
  CreateOneAssetMutationArguments({required this.data, required this.upload});

  @override
  factory CreateOneAssetMutationArguments.fromJson(Map<String, dynamic> json) =>
      _$CreateOneAssetMutationArgumentsFromJson(json);

  late CreateAssetWithFileInput data;

  late dynamic upload;

  @override
  List<Object?> get props => [data, upload];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneAssetMutationArgumentsToJson(this);
}

final CREATE_ONE_ASSET_MUTATION_MUTATION_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'CreateOneAssetMutation'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'data')),
            type: NamedTypeNode(
                name: NameNode(value: 'CreateAssetWithFileInput'),
                isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'upload')),
            type:
                NamedTypeNode(name: NameNode(value: 'Upload'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'createOneAssetWithFile'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'data'),
                  value: VariableNode(name: NameNode(value: 'data'))),
              ArgumentNode(
                  name: NameNode(value: 'upload'),
                  value: VariableNode(name: NameNode(value: 'upload')))
            ],
            directives: [],
            selectionSet: null)
      ]))
]);

class CreateOneAssetMutationMutation extends GraphQLQuery<
    CreateOneAssetMutation$MutationRoot, CreateOneAssetMutationArguments> {
  CreateOneAssetMutationMutation({required this.variables});

  @override
  final DocumentNode document = CREATE_ONE_ASSET_MUTATION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'CreateOneAssetMutation';

  @override
  final CreateOneAssetMutationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  CreateOneAssetMutation$MutationRoot parse(Map<String, dynamic> json) =>
      CreateOneAssetMutation$MutationRoot.fromJson(json);
}
