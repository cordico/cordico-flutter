import 'package:core/core.dart';
import 'package:core/graphql/contact_type/contact_type.dart';
import 'package:core/graphql/contact_type/contact_type_by_id.dart';
import 'package:core/repositories/graphql_repository.dart';
import 'package:core/utils/failure.dart';
import 'package:core/utils/map_extension.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

abstract class IContactTypeRepository {
  Future<Either<Failure, List<ContactTypeModel>?>> listContactType();
  Future<Either<Failure, ContactTypeModel?>> getContactTypeById(String id,
      {FetchPolicy? fetchPolicy});
}

class ContactTypeRepository extends GraphQLRepository
    implements IContactTypeRepository {
  ContactTypeRepository({required String url, required String userUrl,
    String? userToken,}) :
        super(
          url: url,
          userUrl: userUrl,
        token: userToken,
      );

  @override
  Future<Either<Failure, List<ContactTypeModel>?>> listContactType() async {
    try {
      final listContactType = ContactTypeQueryQuery();

      final response = await graphqlClient.query<ContactTypeModel>(QueryOptions(
          document: listContactType.document,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = ContactTypeQuery$QueryRoot.fromJson(response.data!);
        return Right(root.contactType
            .map((e) => ContactTypeModel.fromJson(e.toJson()))
            .toList());
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, ContactTypeModel?>> getContactTypeById(String id,
      {FetchPolicy? fetchPolicy}) async {
    try {
      final query =
          ContactTypeByIdQuery(variables: ContactTypeByIdArguments(id: id));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<ContactTypeModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = ContactTypeById$QueryRoot.fromJson(response.data!);
        if (root.contactTypeByPk != null) {
          return Right(root.contactTypeByPk);
        }
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }
}
