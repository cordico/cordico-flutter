#!/bin/sh


if [ -z "$ENVIRONMENT" ] || [ "$ENVIRONMENT" = "production" ]
then
  sed -e 's/$ENVIRONMENT/production/g' -e 's/$DOMAIN/'"$DOMAIN"'/g' env.tmpl > ./assets/.env
else
  sed -e 's/$ENVIRONMENT/'"$ENVIRONMENT"'/g' -e 's/$DOMAIN/'"$DOMAIN"'/g' env.tmpl > ./assets/.env
fi

