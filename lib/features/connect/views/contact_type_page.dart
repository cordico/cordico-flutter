// ignore_for_file: lines_longer_than_80_chars

import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/app_bar_provider.dart';
import 'package:cordico_mobile/core/widgets/cordico_search_field.dart';
import 'package:cordico_mobile/features/connect/logic/directory_state.dart';
import 'package:cordico_mobile/features/department/views/content_list_view.dart';
import 'package:core/graphql/organization/organization_contact_by_id.dart';
import 'package:core/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
// ignore: implementation_imports
import 'package:vrouter/src/core/extended_context.dart';

final filtersDirectoryTypeQueryProvider = StateProvider<String>((ref) => '');

final filteredDirectoryTypeContactsProvider =
    Provider<List<OrganizationContactById$QueryRoot$OrganizationContactsByPk>>(
        (ref) {
  final filter = ref.watch(filtersDirectoryTypeQueryProvider);
  final contacts = ref.watch(allContactsProvider);

  return utils.filterContacts(content: contacts, query: filter);
});

class ContactTypePage extends ConsumerStatefulWidget {
  const ContactTypePage({Key? key}) : super(key: key);

  @override
  ConsumerState<ContactTypePage> createState() => _ContactTypePageState();
}

class _ContactTypePageState extends ConsumerState<ContactTypePage> {
  @override
  Widget build(BuildContext context) {
    final stateP = ref.watch(filteredDirectoryTypeContactsProvider);
    final stateNotifier = ref.watch(allContactsProvider.notifier);

    final type = context.vRouter.pathParameters['type'] ?? '';

    var title = '';

    if (type == 'peer') {
      title = 'Peer Support';
    } else if (type == 'therapist') {
      title = 'Therapists';
    } else if (type == 'chaplain') {
      title = 'Chaplain';
    }

    return Scaffold(
      appBar: AppBarProvider.getAppBarWithBackIcon(
        context,
        backgroundColor: ThemeColors.gray.shade900,
        title: Text(
          title,
          style: ThemeStyles.cardContentTitle(context)
              ?.copyWith(color: Colors.white),
        ),
      ),
      body: stateNotifier.isLoading
          ? const Center(child: CircularProgressIndicator())
          : _buildConnectList(context, stateP, type: type),
    );
  }

  Widget _buildConnectList(BuildContext context,
      List<OrganizationContactById$QueryRoot$OrganizationContactsByPk> allData,
      {String type = 'peer'}) {
    final peerSupportList =
        <OrganizationContactById$QueryRoot$OrganizationContactsByPk>[];
    final chaplainList =
        <OrganizationContactById$QueryRoot$OrganizationContactsByPk>[];
    final therapistsContactList =
        <OrganizationContactById$QueryRoot$OrganizationContactsByPk>[];

    for (final element in allData) {
      if (element.contactType.id == kContactTypePeerSupportId) {
        peerSupportList.add(element);
      } else if (element.contactType.id == kContactTypeChaplainId) {
        chaplainList.add(element);
      } else if (element.contactType.id == kContactTypeTherapistId) {
        therapistsContactList.add(element);
      }
    }

    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(child: _buildFilterInputField(context)),
        if (type == 'peer')
          ContentListView<
              OrganizationContactById$QueryRoot$OrganizationContactsByPk>(
            source: peerSupportList,
            isSliver: true,
          ),
        if (type == 'therapist')
          ContentListView<
              OrganizationContactById$QueryRoot$OrganizationContactsByPk>(
            source: therapistsContactList,
            isSliver: true,
          ),
        if (type == 'chaplain')
          ContentListView<
              OrganizationContactById$QueryRoot$OrganizationContactsByPk>(
            source: chaplainList,
            isSliver: true,
          ),
      ],
    );
  }

  Widget _buildFilterInputField(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(12, 8, 12, 19),
      child: CordicoSearchField(
        hintText: 'Search Contacts',
        onChanged: (value) {
          ref.watch(filtersDirectoryTypeQueryProvider.state).state = value;
        },
      ),
    );
  }
}
