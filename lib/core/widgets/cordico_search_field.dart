import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/cordico_text_field.dart';
import 'package:flutter/material.dart';

class CordicoSearchField extends StatefulWidget {
  const CordicoSearchField({
    Key? key,
    this.controller,
    this.isLoading = false,
    required this.onChanged,
    required this.hintText,
    this.focusNode,
    this.onSubmitted,
    this.onClearTap,
  }) : super(key: key);

  final TextEditingController? controller;
  final bool isLoading;
  final ValueChanged<String> onChanged;
  final ValueChanged<String>? onSubmitted;
  final Function()? onClearTap;
  final String hintText;
  final FocusNode? focusNode;

  @override
  State createState() => CordicoSearchFieldState();
}

class CordicoSearchFieldState extends State<CordicoSearchField> {
  late TextEditingController _controller;
  late FocusNode _focusNode;

  @override
  void initState() {
    _controller = widget.controller ?? TextEditingController();
    _focusNode = widget.focusNode ?? FocusNode();
    _focusNode.addListener(_onFocusChanged);
    super.initState();
  }

  void _onFocusChanged() {
    setState(() {});
  }

  void _onClearClicked() {
    _controller.text = '';
    widget.onChanged('');
    if (widget.onClearTap != null) {
      widget.onClearTap!();
    }
    if (_focusNode.hasFocus) {
      _focusNode.unfocus();
    }
  }

  @override
  void dispose() {
    if (widget.controller == null) {
      _controller.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CordicoTextField(
      focusNode: _focusNode,
      borderColor: Colors.black,
      fillColor: ThemeColors.gray.shade900,
      controller: _controller,
      onSubmitted: (val) {
        if (widget.onSubmitted != null) {
          widget.onSubmitted!(val);
        }

        _focusNode.unfocus();
      },
      prefixIcon: Icon(
        Icons.search,
        color: _focusNode.hasFocus
            ? ThemeColors.gray.shade200
            : ThemeColors.gray.shade300,
      ),
      suffixIcon: InkWell(
        onTap: _onClearClicked,
        child: Icon(
          Icons.cancel,
          color: _focusNode.hasFocus
              ? ThemeColors.gray.shade200
              : ThemeColors.gray.shade300,
        ),
      ),
      hintText: widget.hintText,
      hintStyle: ThemeStyles.textFieldTextStyle(context)?.copyWith(
        color: ThemeColors.gray.shade300,
      ),
      style: ThemeStyles.textFieldTextStyle(context)?.copyWith(
        color: ThemeColors.gray.shade100,
      ),
      isLoading: widget.isLoading,
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.search,
      onChanged: widget.onChanged,
    );
  }
}
