# cordico_mobile

Cordico Mobile Application

# README #

This is the Version 2 Cordico Mobile Client Application for Lexipol.

### Inspiration ###
This project structure is heavily influenced by the following repos:

* [https://github.com/bizz84/movie_app_state_management_flutter](https://github.com/bizz84/movie_app_state_management_flutter)
* [https://github.com/NoScopeDevs/flutter_jokes](https://github.com/NoScopeDevs/flutter_jokes)

This provides a beginning for clean architecture without too much verbosity using [Riverpod](https://riverpod.dev/) for state.

This [CodeWithAndrea article](https://codewithandrea.com/videos/flutter-state-management-riverpod/) is a good reference.


### What is this repository for? ###

* This repository is dealing with anything Flutter mobile client related to the version 2 of the Cordico Application
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* OSX is needed since this application targets both iOS and Android
* Install Flutter 2.0  

### Build runner command ###
flutter packages pub run build_runner build --delete-conflicting-outputs


### Contribution guidelines ###

* Writing tests - please ensure they are not brittle
* Code review - run both 'flutter format lib' , 'flutter analyze' commands before pull request

