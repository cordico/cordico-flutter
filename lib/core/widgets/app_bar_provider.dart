import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vrouter/vrouter.dart';

abstract class AppBarProvider {
  static PreferredSizeWidget getAppBarWithTitle(
    BuildContext context, {
    required Widget title,
  }) {
    return getAppBarWithActions(
      context,
      title: title,
    );
  }

  static PreferredSizeWidget getAppBarWithCloseIcon(
    BuildContext context, {
    required Widget title,
    Color? backgroundColor,
    Color? iconTintColor,
    VoidCallback? onIconPressed,
  }) {
    return getAppBarWithActions(
      context,
      title: title,
      backgroundColor: backgroundColor,
      iconTintColor: iconTintColor,
      onIconPressed: onIconPressed,
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.close,
            color: iconTintColor ?? ThemeColors.blue.shade50,
          ),
          onPressed: onIconPressed ?? () => context.vRouter.historyBack(),
        ),
      ],
    );
  }

  static PreferredSizeWidget getAppBarWithBackIcon(
    BuildContext context, {
    required Widget title,
    Color? backgroundColor,
    Color? iconTintColor,
    VoidCallback? onIconPressed,
  }) {
    return getAppBarWithActions(
      context,
      title: title,
      centerTitle: true,
      backgroundColor: backgroundColor,
      iconTintColor: iconTintColor,
      onIconPressed: onIconPressed,
      icon: Icon(
        Icons.arrow_back_ios,
        color: iconTintColor ?? ThemeColors.blue.shade50,
      ),
    );
  }

  static PreferredSizeWidget getAppBarWithActions(BuildContext context,
      {required Widget title,
      Color? backgroundColor,
      Color? iconTintColor,
      bool? centerTitle,
      Widget? icon,
      VoidCallback? onIconPressed,
      List<Widget>? actions}) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.light,
      backgroundColor: backgroundColor ?? Colors.black,
      title: title,
      centerTitle: centerTitle ?? false,
      leading: icon != null
          ? IconButton(
              icon: icon,
              onPressed: onIconPressed ?? () => context.vRouter.historyBack(),
            )
          : null,
      actions: actions,
    );
  }
}
