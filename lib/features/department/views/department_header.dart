import 'dart:math';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/images.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class DepartmentHeader implements SliverPersistentHeaderDelegate {
  DepartmentHeader({
    this.minExtent = 200,
    this.controller,
    required this.maxExtent,
    required this.image,
    required this.departmentImage,
  });
  final String image;
  final String departmentImage;
  final ScrollController? controller;
  @override
  final double minExtent;
  @override
  final double maxExtent;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return GestureDetector(
      onTap: () {
        if (controller != null && controller!.hasClients) {
          controller!.animateTo(0,
              duration: const Duration(milliseconds: 200),
              curve: Curves.linear);
        }
      },
      child: _buildDepartmentCover(context),
    );
  }

  Widget _buildDepartmentCover(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          child: CachedNetworkImage(
            imageUrl: image,
            fit: BoxFit.cover,
            placeholder: (context, url) => Container(
              color: ThemeColors.blue.shade50,
            ),
            imageBuilder: (context, image) {
              return Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: image,
                    fit: BoxFit.cover,
                  ),
                ),
              );
            },
            errorWidget: (context, url, dynamic error) => Container(
              color: ThemeColors.blue.shade50,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
              top: MediaQuery.of(context).viewPadding.top + 6, left: 20),
          child: Image.asset(
            Images.icLogoBig,
            width: 78,
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 29),
            child: Container(
              clipBehavior: Clip.antiAlias,
              decoration: BoxDecoration(
                color: Colors.transparent,
                borderRadius: BorderRadius.circular(53),
              ),
              width: 106,
              height: 106,
              child: CachedNetworkImage(
                imageUrl: departmentImage,
                fit: BoxFit.cover,
                placeholder: (context, url) => Container(
                  color: ThemeColors.blue.shade50,
                ),
                imageBuilder: (context, image) {
                  return Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: image,
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                },
                errorWidget: (context, url, dynamic error) => Container(
                  color: ThemeColors.blue.shade50,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  double titleOpacity(double shrinkOffset) {
    return 1.0 - max(0, shrinkOffset) / maxExtent;
  }

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }

  @override
  FloatingHeaderSnapConfiguration get snapConfiguration =>
      FloatingHeaderSnapConfiguration();

  @override
  OverScrollHeaderStretchConfiguration get stretchConfiguration =>
      OverScrollHeaderStretchConfiguration(
        stretchTriggerOffset: 50,
      );

  @override
  PersistentHeaderShowOnScreenConfiguration get showOnScreenConfiguration =>
      const PersistentHeaderShowOnScreenConfiguration();

  @override
  TickerProvider? get vsync => throw UnimplementedError();
}
