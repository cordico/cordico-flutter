// ignore_for_file: cascade_invocations

import 'package:cordico_mobile/features/discover/views/discover_page.dart';
import 'package:cordico_mobile/features/toolkit/logic/organization_provider.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class ContentFilterState extends StateNotifier<DiscoverContent> {
  ContentFilterState(this.ref) : super(DiscoverContent());

  bool _isLoading = false;
  bool get isLoading => _isLoading;

  bool _contentLoaded = false;
  bool get contentLoaded => _contentLoaded;

  final StateNotifierProviderRef ref;

  Future<void> getContent() async {
    _isLoading = true;
    if (contentLoaded == false) {
      final _content = DiscoverContent();

      final reference = ref.watch(organizatioinContentNotifierProvider);
      final response = reference.cordicoFeatured;
      final response3 = reference.whatsNew;
      final response2 = await ref.watch(discoverContentFuture.future);

      final featuredContent = response?.collectionContents ?? [];

      final fromAllCategoriesContent = response2 ?? [];

      final whatsNewContent = response3?.collectionContents ?? [];

      final featuredList = featuredContent.map((e) {
        return ContentQuery$QueryRoot$Content.fromJson(e.content.toJson());
      }).toList();

      final allContentList = fromAllCategoriesContent.map((e) {
        return ContentQuery$QueryRoot$Content.fromJson(e.toJson());
      }).toList();

      final whatsNewList = whatsNewContent.map((e) {
        return ContentQuery$QueryRoot$Content.fromJson(e.content.toJson());
      }).toList();

      _content.featuredContent = List.from(featuredList);
      _content.fromAllCategoriesContent = List.from(allContentList);
      _content.whatsNewContent = List.from(whatsNewList);

      state = _content;

      _contentLoaded = true;
      _isLoading = false;
    }
  }
}

class DiscoverContent {
  List<ContentQuery$QueryRoot$Content> fromAllCategoriesContent =
      <ContentQuery$QueryRoot$Content>[];

  List<ContentQuery$QueryRoot$Content> featuredContent =
      <ContentQuery$QueryRoot$Content>[];

  List<ContentQuery$QueryRoot$Content> whatsNewContent =
      <ContentQuery$QueryRoot$Content>[];
}

final allContentProvider =
    StateNotifierProvider<ContentFilterState, DiscoverContent>(
        (ref) => ContentFilterState(ref)..getContent());
