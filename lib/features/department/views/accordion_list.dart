// ignore_for_file: lines_longer_than_80_chars

import 'package:cordico_mobile/core/widgets/accordion.dart';
import 'package:cordico_mobile/core/widgets/card/cordico_content_card.dart';
import 'package:cordico_mobile/core/widgets/card/hotline_card.dart';
import 'package:cordico_mobile/features/department/views/bulletin_cell.dart';
import 'package:core/core.dart';
import 'package:core/graphql/collection/collection_by_id.dart';
import 'package:core/graphql/content/content_by_content_type.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:flutter/material.dart';

class AccordionList<T> extends StatefulWidget {
  const AccordionList({
    Key? key,
    required this.title,
    required this.source,
    this.buttonTitle = 'View Archive',
    this.onItemTap,
    this.onViewArchiveTap,
    this.forceExpanded = false,
    this.showArchiveButton = false,
    this.isBulletin = false,
    this.padding = EdgeInsets.zero,
    this.customItemBuilder,
    this.accordionState,
    this.isExpanded = false,
  }) : super(key: key);

  final List<T> source;
  final String title;
  final String buttonTitle;
  final Function(int)? onItemTap;
  final Function()? onViewArchiveTap;
  final bool showArchiveButton;
  final bool forceExpanded;
  final bool isBulletin;
  final bool isExpanded;
  final EdgeInsets padding;
  final Widget Function(BuildContext, int)? customItemBuilder;
  final GlobalKey<AccordionState>? accordionState;

  @override
  State<AccordionList<T>> createState() => _AccordionListState<T>();
}

class _AccordionListState<T> extends State<AccordionList<T>>
    with AutomaticKeepAliveClientMixin {
  late bool isExpanded;

  @override
  void initState() {
    super.initState();
    isExpanded = widget.isExpanded;
  }

  @override
  void didUpdateWidget(covariant AccordionList<T> oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (widget.isExpanded != oldWidget.isExpanded) {
      setState(() => isExpanded = widget.isExpanded);
    }

    if (widget.accordionState?.currentState?.isExpanded !=
        oldWidget.accordionState?.currentState?.isExpanded) {
      setState(() => isExpanded =
          widget.accordionState?.currentState?.isExpanded ?? false);
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Accordion(
      key: widget.accordionState,
      title: widget.title,
      isExpanded: isExpanded,
      buttonTitle: widget.buttonTitle,
      onViewArchiveTap: widget.onViewArchiveTap,
      showArchiveButton: widget.showArchiveButton,
      forceExpanded: widget.forceExpanded,
      onExpanded: () {},
      child: widget.source.isEmpty
          ? const Center(
              child: Padding(
                padding: EdgeInsets.all(24),
                child: Text(
                  'No results.',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            )
          : ListView.builder(
              shrinkWrap: true,
              primary: false,
              padding: widget.padding,
              itemCount: widget.source.length,
              itemBuilder: widget.customItemBuilder ??
                  (context, index) {
                    /*if (widget.source
                    is List<BulletinQueryByOrganization$QueryRoot$Bulletin>) {
                  final item = widget.source[index]
                      as BulletinQueryByOrganization$QueryRoot$Bulletin;
                  return BulletinCell(
                    bulletin: item,
                    onPressed: () {
                      if (widget.onItemTap != null) {
                        widget.onItemTap!(index);
                      }
                    },
                    isLast: index == widget.source.length - 1,
                  );
                } else*/
                    if (widget.source is List<OrganizationContactModel>) {
                      final item =
                          widget.source[index] as OrganizationContactModel;

                      return HotlineCard(
                        contact: item,
                        isLast: index == widget.source.length - 1,
                      );
                    } else if (widget.source is List<
                        CollectionById$QueryRoot$CollectionByPk$CollectionContents>) {
                      final item = widget.source[index]
                          as CollectionById$QueryRoot$CollectionByPk$CollectionContents;

                      final finalItem = ContentQuery$QueryRoot$Content.fromJson(
                          item.content.toJson());

                      return CordicoContentCard(
                        content: finalItem,
                        isLast: index == widget.source.length - 1,
                        onPressed: () {
                          if (widget.onItemTap != null) {
                            widget.onItemTap!(index);
                          }
                        },
                      );
                    } else if (widget.source
                        is List<ContentQuery$QueryRoot$Content>) {
                      final item = widget.source[index]
                          as ContentQuery$QueryRoot$Content;
                      if (widget.isBulletin) {
                        return BulletinCell(
                          bulletin: item,
                          onPressed: () {
                            if (widget.onItemTap != null) {
                              widget.onItemTap!(index);
                            }
                          },
                          isLast: index == widget.source.length - 1,
                        );
                      }

                      return CordicoContentCard(
                        content: item,
                        isLast: index == widget.source.length - 1,
                        onPressed: () {
                          if (widget.onItemTap != null) {
                            widget.onItemTap!(index);
                          }
                        },
                      );
                    } else if (widget.source
                        is List<ContentByContentType$QueryRoot$Content>) {
                      final item = widget.source[index]
                          as ContentByContentType$QueryRoot$Content;

                      final finalItem = ContentQuery$QueryRoot$Content.fromJson(
                          item.toJson());

                      return CordicoContentCard(
                        content: finalItem,
                        isLast: index == widget.source.length - 1,
                        onPressed: () {
                          if (widget.onItemTap != null) {
                            widget.onItemTap!(index);
                          }
                        },
                      );
                    }
                    return Container();
                  },
            ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
