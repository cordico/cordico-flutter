// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'revoked_sessions.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$RevokedSessions extends RevokedSessions {
  @override
  final int? count;

  factory _$RevokedSessions([void Function(RevokedSessionsBuilder)? updates]) =>
      (new RevokedSessionsBuilder()..update(updates)).build();

  _$RevokedSessions._({this.count}) : super._();

  @override
  RevokedSessions rebuild(void Function(RevokedSessionsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RevokedSessionsBuilder toBuilder() =>
      new RevokedSessionsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RevokedSessions && count == other.count;
  }

  @override
  int get hashCode {
    return $jf($jc(0, count.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RevokedSessions')..add('count', count))
        .toString();
  }
}

class RevokedSessionsBuilder
    implements Builder<RevokedSessions, RevokedSessionsBuilder> {
  _$RevokedSessions? _$v;

  int? _count;
  int? get count => _$this._count;
  set count(int? count) => _$this._count = count;

  RevokedSessionsBuilder() {
    RevokedSessions._defaults(this);
  }

  RevokedSessionsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _count = $v.count;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RevokedSessions other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$RevokedSessions;
  }

  @override
  void update(void Function(RevokedSessionsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RevokedSessions build() {
    final _$result = _$v ?? new _$RevokedSessions._(count: count);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
