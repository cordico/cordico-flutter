import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:cordico_mobile/core/managers/offlline_manager.dart';
import 'package:cordico_mobile/core/providers/global_providers.dart';
import 'package:cordico_mobile/features/discover/logic/content_types_holder.dart';
import 'package:core/utils/constants.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ContentTypesNotifier extends StateNotifier<ContentTypesHolder> {
  ContentTypesNotifier(this.ref) : super(ContentTypesHolder());

  bool _isLoading = false;
  bool get isLoading => _isLoading;

  bool _contentLoaded = false;
  bool get contentLoaded => _contentLoaded;

  final ProviderRef ref;

  Future<void> getContentTypes() async {
    if (contentLoaded == false) {
      var contentTypesHolder = ContentTypesHolder();
      final connectityCheck = await Connectivity().checkConnectivity();

      if (connectityCheck == ConnectivityResult.none) {
        final list = OfflineManager.getData(key: ModelsKeys.contentTypes);

        if (list != null) {
          final contents = ContentTypesHolder.fromJson(list);
          contentTypesHolder = contents;
        }
      } else {
        _isLoading = true;

        final result =
            await ref.watch(contentRepositoryProvider).listContentType();

        await result.fold((error) {}, (content) async {
          if (content != null) {
            contentTypesHolder.contentTypes = content
                .where((element) =>
                    element.id == kContentTypeAssessmentId ||
                    element.id == kContentTypeGuideId)
                .toList();

            final types = contentTypesHolder.toJson();

            await OfflineManager.persistData(
              key: ModelsKeys.contentTypes,
              value: types,
            );
          }
        });
        _isLoading = false;
      }
      _contentLoaded = true;
      state = contentTypesHolder;
    }
  }
}
