// ignore_for_file: implementation_imports, cascade_invocations

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/buttons/action_button.dart';
import 'package:cordico_mobile/core/widgets/card/cordico_card.dart';
import 'package:core/graphql/organization/organization_contact_by_id.dart';
import 'package:core/models/schema/content_schema.dart';
import 'package:core/models/schema/organization_contact_schema.dart';
import 'package:core/utils/constants.dart';
import 'package:core/utils/map_extension.dart';
import 'package:core/utils/string_ext.dart';
import 'package:flutter/material.dart';
import 'package:vrouter/src/core/extended_context.dart';

class HotlineCard extends StatefulWidget {
  const HotlineCard({
    Key? key,
    required this.contact,
    this.isLast = false,
    this.reduceDescription = false,
    this.canPresentMap = true,
    this.isFromMap = false,
  }) : super(key: key);

  final OrganizationContactById$QueryRoot$OrganizationContactsByPk contact;

  final bool isLast;
  final bool reduceDescription;
  final bool canPresentMap;
  final bool isFromMap;

  @override
  State<HotlineCard> createState() => _HotlineCardState();
}

class _HotlineCardState extends State<HotlineCard> {
  bool summaryExpanded = false;
  int? maxLines = 2;
  bool exceeded = false;

  late OrganizationContactSchema? content;

  @override
  void initState() {
    super.initState();

    if (widget.contact.data != null) {
      content = OrganizationContactSchema.fromJson(
          widget.contact.data as Map<String, dynamic>);
    }
  }

  @override
  void didUpdateWidget(covariant HotlineCard oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.contact != widget.contact) {
      if (widget.contact.data != null) {
        content = OrganizationContactSchema.fromJson(
            widget.contact.data as Map<String, dynamic>);

        removeNullAndEmptyParams(widget.contact.data as Map<String, dynamic>);
        setState(() {});
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (content == null) {
      return const SizedBox.shrink();
    }

    var summary = '';

    if (content!.summary != null &&
        content!.summary!.body != null &&
        content!.summary!.body!.isNotEmpty) {
      summary = content!.summary!.body ?? '';

      exceeded = summary.isExeeded(
        lines: 2,
        width: MediaQuery.of(context).size.width - (18 * 2),
        style: ThemeStyles.contentDescription(context),
      );
    }

    return GestureDetector(
      onTap: () {
        context.vRouter
            .to('contact_detail/${widget.contact.id}/${widget.isFromMap}');
      },
      child: Padding(
        padding: EdgeInsets.only(bottom: widget.isLast ? 0 : 10),
        child: CordicoCard(
          cornerRadius: 0,
          color: ThemeColors.gray.shade900,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 18),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: _buildCategoryHeader(context),
                ),
                _buildContactDetails(context),
                _buildDescription(context),
                _buildActions(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildCategoryHeader(BuildContext context) {
    return Row(
      children: [
        Flexible(
          child: Text(
            widget.contact.contactType.displayName
                .replaceAll('-', ' ')
                .toUpperCase(),
            style: ThemeStyles.cardCategory(context),
          ),
        ),
        /*if (distance != null)
          Padding(
            padding: const EdgeInsets.only(left: 8),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                const Icon(
                  Icons.map_outlined,
                  size: 11,
                  color: Colors.white,
                ),
                Container(width: 4),
                Text(
                  distance!,
                  style: ThemeStyles.sectionTitle(context)?.copyWith(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                ),
                const SizedBox(width: 2),
                Text(
                  'Miles',
                  style: ThemeStyles.sectionTitle(context)?.copyWith(
                    fontSize: 9,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        if (isNew)
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: NewBadge(),
          ),*/
      ],
    );
  }

  Widget _buildContactDetails(BuildContext context) {
    return Row(
      children: [
        if (content!.profileImageUrl != null &&
            content!.profileImageUrl!.isNotEmpty)
          _buildProfileImage(context),
        if (content!.profileImageUrl != null &&
            content!.profileImageUrl!.isNotEmpty)
          Container(width: 12),
        Expanded(
          child: _buildTitleAndPhoneNumber(context),
        ),
        if (!widget.isFromMap)
          const Icon(
            Icons.chevron_right_rounded,
            color: Colors.white,
          ),
      ],
    );
  }

  Widget _buildProfileImage(BuildContext context) {
    return Container(
      width: 58,
      height: 58,
      clipBehavior: Clip.antiAlias,
      decoration: BoxDecoration(
        color: ThemeColors.gray.shade300,
        borderRadius: BorderRadius.circular(12),
      ),
      child: CachedNetworkImage(
        imageUrl: content?.profileImageUrl ?? '',
        fit: BoxFit.cover,
        placeholder: (context, url) => Container(
          color: ThemeColors.gray.shade300,
        ),
        imageBuilder: (context, image) {
          return Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: image,
                fit: BoxFit.cover,
              ),
            ),
          );
        },
        errorWidget: (context, url, dynamic error) {
          return const Icon(Icons.error);
        },
      ),
    );
  }

  Widget _buildTitleAndPhoneNumber(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          content?.name?.displayName ?? '',
          style: ThemeStyles.bulletinCardTitle(context)
              ?.copyWith(color: Colors.white),
        ),
        Container(height: 8),
        Text(
          content?.title ?? '',
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: ThemeStyles.smallParagraph(context)?.copyWith(
            color: ThemeColors.gray.shade300,
            fontSize: 14,
          ),
        ),
      ],
    );
  }

  Widget _buildDescription(BuildContext context) {
    if (content!.summary != null &&
        content!.summary!.body != null &&
        content!.summary!.body!.isNotEmpty) {
      final size = content!.summary!.body!.getSize(
        context: context,
        maxLines: summaryExpanded ? null : 2,
        maxWidth: MediaQuery.of(context).size.width - (18 * 2),
        style: ThemeStyles.contentDescription(context),
      );
      return Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 8),
                  child: AnimatedContainer(
                    onEnd: () {
                      if (summaryExpanded == false) {
                        setState(() => maxLines = 2);
                      }
                    },
                    duration: const Duration(milliseconds: 300),
                    height: size.height,
                    child: Text(
                      content!.summary?.body ?? '',
                      maxLines: maxLines,
                      overflow: maxLines == null ? null : TextOverflow.ellipsis,
                      style: ThemeStyles.contentDescription(context)?.copyWith(
                        color: ThemeColors.gray.shade200,
                      ),
                    ),
                  ),

                  /*ContentWidget(
                    content: content.summary!.body ?? '',
                    contentType: (content.summary!.bodyMimeType == null ||
                            content.summary!.bodyMimeType!.isEmpty ||
                            content.summary!.bodyMimeType == 'text/markdown')
                        ? MimeType.markdown
                        : MimeType.html,
                  ),*/
                ),
              ),
            ],
          ),
          if (exceeded)
            GestureDetector(
              onTap: () {
                setState(() {
                  summaryExpanded = !summaryExpanded;
                  if (summaryExpanded) {
                    maxLines = null;
                  }
                });
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 8, bottom: 4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      summaryExpanded ? 'Read less...' : 'Read more...',
                      maxLines: 1,
                      style: ThemeStyles.sectionTitle(context)
                          ?.copyWith(fontSize: 13.3),
                    ),
                  ],
                ),
              ),
            )
        ],
      );
    }
    return Container();
  }

  Widget _buildActions(BuildContext context) {
    final allButtons = List<ActionButtonSchema>.from(
        content!.actionButtons ?? <ActionButtonSchema>[]);

    if (widget.contact.contactType.id == kContactTypeTherapistId &&
        content!.address != null &&
        widget.canPresentMap) {
      final mapActionButton = ActionButtonSchema()
        ..label = 'View on Map'
        ..actionType = ActionButtonType.map
        ..materialIcon = ActionButtonMaterialIcon.mapOutlined
        ..isDefault = false;
      allButtons.add(mapActionButton);
    }

    allButtons.sort((a, b) {
      return a.actionType == ActionButtonType.call ||
              a.actionType == ActionButtonType.anonymousCall
          ? -1
          : 1;
    });

    allButtons.sort((a, b) {
      return a.isDefault == true ? -1 : 1;
    });

    final defualtButtons = <ActionButtonSchema>[];
    final normalButtons = <ActionButtonSchema>[];

    var defualtButtonsLenght = 0;
    var normalButtonsLenght = 0;

    // ignore: avoid_function_literals_in_foreach_calls
    allButtons.forEach((element) {
      if (element.isDefault == true) {
        defualtButtonsLenght += 1;
        defualtButtons.add(element);
      } else {
        normalButtonsLenght += 1;
        normalButtons.add(element);
      }
    });

    final width = (MediaQuery.of(context).size.width - (18 * 2) - 16) / 2;
    final fullWidth = MediaQuery.of(context).size.width - (18 * 2);

    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: Wrap(
        alignment: WrapAlignment.center,
        spacing: 16,
        runSpacing: 8,
        children: [
          if (defualtButtons.isNotEmpty)
            ...defualtButtons.asMap().entries.map((e) {
              final firstFullWidth = defualtButtonsLenght % 2 != 0;

              return SizedBox(
                width: firstFullWidth && e.key == 0 ? fullWidth : width,
                child: ActionButton(
                  action: e.value,
                  content: content,
                  onPressed: e.value.actionType == ActionButtonType.map
                      ? () {
                          context.vRouter
                              .to('contacts_map/${widget.contact.id}');
                        }
                      : null,
                  height: 36,
                ),
              );
            }).toList(),
          if (normalButtons.isNotEmpty)
            ...normalButtons.asMap().entries.map((e) {
              final firstFullWidth = normalButtonsLenght % 2 != 0;

              return SizedBox(
                width: firstFullWidth && e.key == 0 ? fullWidth : width,
                child: ActionButton(
                  action: e.value,
                  content: content,
                  onPressed: e.value.actionType == ActionButtonType.map
                      ? () {
                          context.vRouter
                              .to('contacts_map/${widget.contact.id}');
                        }
                      : null,
                  height: 36,
                ),
              );
            }).toList()
        ],
      ),
    );
  }
}
