// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'update_one_content_mutation.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateOneContentMutation$MutationRoot$UpdateOneContent
    _$UpdateOneContentMutation$MutationRoot$UpdateOneContentFromJson(
        Map<String, dynamic> json) {
  return UpdateOneContentMutation$MutationRoot$UpdateOneContent()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$UpdateOneContentMutation$MutationRoot$UpdateOneContentToJson(
            UpdateOneContentMutation$MutationRoot$UpdateOneContent instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

UpdateOneContentMutation$MutationRoot
    _$UpdateOneContentMutation$MutationRootFromJson(Map<String, dynamic> json) {
  return UpdateOneContentMutation$MutationRoot()
    ..updateOneContent = json['updateOneContent'] == null
        ? null
        : UpdateOneContentMutation$MutationRoot$UpdateOneContent.fromJson(
            json['updateOneContent'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UpdateOneContentMutation$MutationRootToJson(
        UpdateOneContentMutation$MutationRoot instance) =>
    <String, dynamic>{
      'updateOneContent': instance.updateOneContent?.toJson(),
    };

UpdateOneContentMutationArguments _$UpdateOneContentMutationArgumentsFromJson(
    Map<String, dynamic> json) {
  return UpdateOneContentMutationArguments(
    id: json['id'] as String?,
    contentStateId: json['contentStateId'] as String?,
    contentTypeId: json['contentTypeId'] as String?,
    name: json['name'] as String,
    data: json['data'],
    isShared: json['isShared'] as bool?,
  );
}

Map<String, dynamic> _$UpdateOneContentMutationArgumentsToJson(
        UpdateOneContentMutationArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
      'contentStateId': instance.contentStateId,
      'contentTypeId': instance.contentTypeId,
      'name': instance.name,
      'data': instance.data,
      'isShared': instance.isShared,
    };
