import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:flutter/material.dart';

class CordicoOutlinedButton extends StatelessWidget {
  const CordicoOutlinedButton({
    Key? key,
    required this.text,
    required this.onPressed,
    this.icon,
    this.child,
    this.enabled = true,
    this.isFullyRounded = true,
    this.height,
    this.textStyle,
    this.borderColor,
  }) : super(key: key);

  final String text;
  final VoidCallback onPressed;
  final Widget? icon;
  final Widget? child;
  final bool enabled;
  final double? height;
  final TextStyle? textStyle;
  final bool isFullyRounded;
  final Color? borderColor;

  @override
  Widget build(BuildContext context) {
    if (icon == null) {
      return SizedBox(
        height: height,
        child: OutlinedButton(
          style: ThemeStyles.outlinedButtonStyle(
            context,
            height: height,
            isFullyRounded: isFullyRounded,
            borderColor: borderColor,
          ),
          onPressed: enabled ? onPressed : null,
          child: child ??
              Text(
                text,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style:
                    textStyle ?? ThemeStyles.outlinedButtonTextStyle(context),
              ),
        ),
      );
    } else {
      return SizedBox(
        height: height,
        child: OutlinedButton.icon(
          style: ThemeStyles.outlinedButtonStyle(
            context,
            height: height,
            isFullyRounded: isFullyRounded,
            borderColor: borderColor,
          ),
          label: child ??
              Text(
                text,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style:
                    textStyle ?? ThemeStyles.outlinedButtonTextStyle(context),
              ),
          icon: icon!,
          onPressed: enabled ? onPressed : null,
        ),
      );
    }
  }
}
