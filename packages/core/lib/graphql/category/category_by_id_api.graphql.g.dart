// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'category_by_id_api.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoryById$QueryRoot$CategoryByPk
    _$CategoryById$QueryRoot$CategoryByPkFromJson(Map<String, dynamic> json) {
  return CategoryById$QueryRoot$CategoryByPk()
    ..id = json['id'] as String
    ..name = json['name'] as String
    ..description = json['description'] as String
    ..key = json['key'] as String;
}

Map<String, dynamic> _$CategoryById$QueryRoot$CategoryByPkToJson(
        CategoryById$QueryRoot$CategoryByPk instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'key': instance.key,
    };

CategoryById$QueryRoot _$CategoryById$QueryRootFromJson(
    Map<String, dynamic> json) {
  return CategoryById$QueryRoot()
    ..categoryByPk = json['category_by_pk'] == null
        ? null
        : CategoryById$QueryRoot$CategoryByPk.fromJson(
            json['category_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$CategoryById$QueryRootToJson(
        CategoryById$QueryRoot instance) =>
    <String, dynamic>{
      'category_by_pk': instance.categoryByPk?.toJson(),
    };

CategoryByIdArguments _$CategoryByIdArgumentsFromJson(
    Map<String, dynamic> json) {
  return CategoryByIdArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$CategoryByIdArgumentsToJson(
        CategoryByIdArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
