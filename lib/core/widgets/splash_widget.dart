import 'package:cordico_mobile/core/theme/images.dart';
import 'package:cordico_mobile/main.dart';
import 'package:flutter/material.dart';
import 'package:vrouter/vrouter.dart';

class SplashWidget extends StatefulWidget {
  const SplashWidget({Key? key}) : super(key: key);

  @override
  State<SplashWidget> createState() => _SplashWidgetState();
}

class _SplashWidgetState extends State<SplashWidget> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async {
      if (await MyApp.isLoggedIn()) {
        context.vRouter.to('/dashboard');
      } else {
        context.vRouter.to('/login');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Image.asset(
          Images.icLogoWhite,
          height: 50,
        ),
      ),
    );
  }
}
