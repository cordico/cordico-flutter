import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CordicoSwitch extends StatelessWidget {
  const CordicoSwitch({
    Key? key,
    required this.text,
    required this.isEnabled,
    this.onSwitch,
  }) : super(key: key);

  final String text;
  final bool? isEnabled;
  final ValueChanged<bool>? onSwitch;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              text,
              style: ThemeStyles.cardContentTitle(context)
                  ?.copyWith(color: Colors.white),
            ),
          ),
          SizedBox(
            height: 32,
            child: CupertinoSwitch(
              value: isEnabled!,
              trackColor: ThemeColors.gray.shade400,
              thumbColor:
                  isEnabled == true ? Colors.white : ThemeColors.gray.shade200,
              activeColor: const Color(0xFF1068EB),
              onChanged: onSwitch,
            ),
          ),
        ],
      ),
    );
  }
}
