// ignore_for_file: lines_longer_than_80_chars, use_build_context_synchronously

import 'dart:async';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/managers/cordico_url.dart';
import 'package:cordico_mobile/core/managers/cordico_utils.dart';
import 'package:cordico_mobile/core/providers/global_providers.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/images.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/buttons/action_button.dart';
import 'package:cordico_mobile/core/widgets/cordico_checkbox.dart';
import 'package:cordico_mobile/core/widgets/cordico_elevated_button.dart';
import 'package:cordico_mobile/core/widgets/cordico_linkable_label.dart';
import 'package:cordico_mobile/core/widgets/cordico_text_field.dart';
import 'package:cordico_mobile/core/widgets/expandable_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_segment/flutter_segment.dart';
//import 'package:flutter_segment/flutter_segment.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:vrouter/vrouter.dart';

class LoginWithSharedCredentialsPage extends ConsumerStatefulWidget {
  const LoginWithSharedCredentialsPage({Key? key}) : super(key: key);

  @override
  ConsumerState<LoginWithSharedCredentialsPage> createState() =>
      _LoginWithSharedCredentialsPageState();
}

class _LoginWithSharedCredentialsPageState
    extends ConsumerState<LoginWithSharedCredentialsPage> {
  final _mediaExpansionController = ExpansionController();

  //'austinofficer', 'feudalism-consulages-sard'
  //'davisofficer', 'brainwaves-overtimed-endozoa'
  //'rosevilleofficer', 'steppe-peptizes-activators'

  String identifier = '';
  String password = '';
  bool isChecked = false;

  bool isLoading = false;
  bool showError = false;

  bool isPasswordVisible = false;

  @override
  Widget build(BuildContext context) {
    //Segment.screen(screenName: 'Login');
    return _buildLoginPage(context);
  }

  Widget _buildLoginPage(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
      ),
      child: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Scaffold(
          backgroundColor: Colors.white,
          body: Stack(
            children: [
              SafeArea(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 21),
                  child: _buildLoginForm(context),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: _buildIndexExpandableSheet(context),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildIndexExpandableSheet(BuildContext context) {
    return ExpandableBottomSheet(
      controller: _mediaExpansionController,
      persistentContentHeight: 0,
      maxHeight: 340,
      persistentHeader: _buildIndexHeader(context),
      onExtendedCallback: () {},
      expandableContent: Container(
        padding:
            const EdgeInsets.only(top: 12, bottom: 12, left: 18, right: 18),
        color: ThemeColors.gray.shade900,
        child: ListView(
          physics: const NeverScrollableScrollPhysics(),
          padding: EdgeInsets.zero,
          children: [
            Text(
              '800 273 8255',
              style: ThemeStyles.cardContentTitle(context)
                  ?.copyWith(color: ThemeColors.green.shade200, fontSize: 22),
            ),
            const SizedBox(height: 8),
            Text(
              '''
The National Suicide Prevention Lifeline is a national network of local crisis centers that provides free and confidential emotional support to people in suicidal crisis or emotional distress 24 hours a day, 7 days a week. We're committed to improving crisis services and advancing suicide prevention by empowering individuals, advancing professional best practices, and building awareness.''',
              maxLines: 4,
              overflow: TextOverflow.ellipsis,
              style: ThemeStyles.contentDescription(context)?.copyWith(
                color: ThemeColors.gray.shade200,
                height: 1.3,
              ),
            ),
            const SizedBox(height: 8),
            ActionButton(
              action: null,
              content: null,
              isDefault: true,
              height: 38,
              onPressed: () {
                CordicoUrl.call(phone: '+18002738255');
              },
              title: 'Call Suicide Prevention Hotline',
              icon: Image.asset(Images.phoneLocked, height: 24, width: 24),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildIndexHeader(BuildContext context) {
    return GestureDetector(
      onTap: _mediaExpansionController.toggle,
      behavior: HitTestBehavior.translucent,
      child: Container(
        height: 60,
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(24), topLeft: Radius.circular(24)),
          color: ThemeColors.buttonsGreyFullyVisibleColor,
        ),
        child: Row(
          children: [
            Image.asset(Images.callIcon, height: 24),
            Container(width: 8),
            Expanded(
              child: Text(
                'National Suicide Prevention Lifeline',
                style: ThemeStyles.bulletinCardTitle(context)
                    ?.copyWith(color: ThemeColors.blue.shade50, height: 1),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildLoginForm(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 53),
            child: Image.asset(
              Images.icLogoBig,
              height: 50,
            ),
          ),
          const SizedBox(height: 45),
          CordicoTextField(
            hintText: 'Department ID',
            labelText: 'Department ID',
            maxLines: 1,
            fillColor: Colors.white,
            onChanged: (v) {
              setState(() => identifier = v);
            },
          ),
          const SizedBox(height: 20),
          CordicoTextField(
            hintText: 'Department Code',
            labelText: 'Department Code',
            fillColor: Colors.white,
            obscureText: !isPasswordVisible,
            maxLines: 1,
            suffixIcon: GestureDetector(
              onTap: () {
                setState(() => isPasswordVisible = !isPasswordVisible);
              },
              child: Icon(
                isPasswordVisible ? Icons.visibility : Icons.visibility_off,
                size: 21,
              ),
            ),
            onChanged: (v) {
              setState(() => password = v);
            },
          ),
          const SizedBox(height: 20),
          CordicoCheckbox(
            value: isChecked,
            onChanged: (value) {
              if (FocusScope.of(context).hasFocus) {
                FocusScope.of(context).unfocus();
              }
              setState(() => isChecked = value ?? false);
            },
            labelWidget: CordicoLinkableLabel(
              text: '''
I have read and understood Cordico Terms and Conditions and Privacy Policy''',
              links: const ['Terms and Conditions', 'Privacy Policy'],
              onTap: (link) {
                if (link == 'Privacy Policy') {
                  CordicoUrl.openUrl(
                      url: 'https://www.cordico.com/app-privacy-policy/');
                } else if (link == 'Terms and Conditions') {
                  CordicoUrl.openUrl(
                      url: 'https://www.cordico.com/terms-and-conditions/');
                }
              },
            ),
          ),
          const SizedBox(height: 37),
          SizedBox(
            height: 40,
            child: Stack(
              fit: StackFit.expand,
              children: [
                CordicoElevatedButton(
                  text: isLoading ? '' : 'Enter',
                  color: showError ? Colors.red.withOpacity(0.7) : null,
                  enabled: identifier.isNotEmpty &&
                      password.isNotEmpty &&
                      isChecked &&
                      !isLoading,
                  onPressed: () async {
                    final connectityCheck =
                        await Connectivity().checkConnectivity();

                    if (connectityCheck == ConnectivityResult.none) {
                      await CordicoUtils.showFlush(
                        context,
                        message: 'No Internet connection',
                        icon: const Icon(
                          Icons.wifi_off_rounded,
                          color: Colors.white,
                        ),
                      );

                      return;
                    }
                    if (isLoading) {
                      return;
                    }
                    setState(() => isLoading = true);
                    try {
                      final result = await authenticationService.login(
                          identifier, password);

                      if (result != null &&
                          result.organization?.data?.id != null &&
                          result.sessionToken != null) {
                        global.user = result;

                        final res = await userRepository
                            .getUserByIdentifier(result.identifier ?? '');
                        res.fold((l) {}, (r) => global.globalUser = r);

                        await Segment.identify(
                            userId: result.identifier,
                            traits: <String, dynamic>{
                              'id': result.id,
                              'organization_name': result.organization?.name,
                              'organization': result.organization?.toJson()
                            });

                        //SAVE USER
                        environmentConfig.organizationId =
                            result.organization?.data?.id;
                        await secureStorage.write(
                            key: 'cordico.sessionToken',
                            value: result.sessionToken);
                        setState(() => isLoading = false);
                        context.vRouter.to('/dashboard');
                      } else {
                        await Segment.track(
                            eventName: 'Login Error',
                            properties: <String, dynamic>{
                              'identifier': identifier,
                              'password': password
                            });
                        //SHOW ERROR
                        setState(() {
                          isLoading = false;
                          showError = true;
                        });
                        Timer(const Duration(seconds: 3), () {
                          setState(() {
                            isLoading = false;
                            showError = false;
                          });
                        });
                      }
                    } catch (e) {
                      //SHOW ERROR
                      setState(() {
                        isLoading = false;
                        showError = true;
                      });
                      Timer(const Duration(seconds: 3), () {
                        setState(() {
                          isLoading = false;
                          showError = false;
                        });
                      });
                    }
                  },
                ),
                if (isLoading)
                  const Align(
                    child: SizedBox(
                      width: 20,
                      height: 20,
                      child: CircularProgressIndicator(
                          color: Colors.white, strokeWidth: 1),
                    ),
                  )
              ],
            ),
          ),
          if (showError) const SizedBox(height: 10),
          if (showError)
            Center(
              child: Text(
                'Error during login',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.red.withOpacity(0.7)),
              ),
            ),
          const SizedBox(height: 15),
          Container(
            decoration: BoxDecoration(
              color: ThemeColors.green.shade100,
              borderRadius: BorderRadius.circular(8),
            ),
            padding: const EdgeInsets.all(16),
            child: Text(
              'Please contact your Agency Administrator or Wellness Coordinator if you need your Department ID and Code.',
              style: TextStyle(color: ThemeColors.green.shade700),
            ),
          ),
          const SizedBox(height: 6),
          /*const Align(
            child: Text(
              'OR',
              style: TextStyle(color: Colors.black),
            ),
          ),
          const SizedBox(height: 6),
          CordicoOutlinedButton(
            text: 'Read Terms & Conditions',
            height: 38,
            isFullyRounded: false,
            borderColor: ThemeColors.blue.shade500,
            enabled: identifier.isNotEmpty &&
                password.isNotEmpty &&
                isChecked &&
                !isLoading,
            onPressed: () async {},
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    'Read Terms & Conditions',
                    textAlign: TextAlign.center,
                    style: ThemeStyles.smallParagraph(context)?.copyWith(
                      color: ThemeColors.blue.shade500,
                      fontSize: 14,
                    ),
                  ),
                )
              ],
            ),
          ),*/
        ],
      ),
    );
  }
}
