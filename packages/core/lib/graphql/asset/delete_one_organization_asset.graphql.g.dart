// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'delete_one_organization_asset.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset$Returning
    _$DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset$ReturningFromJson(
        Map<String, dynamic> json) {
  return DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset$Returning()
    ..assetId = json['asset_id'] as String
    ..id = json['id'] as String
    ..organizationId = json['organization_id'] as String;
}

Map<String, dynamic>
    _$DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset$ReturningToJson(
            DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset$Returning
                instance) =>
        <String, dynamic>{
          'asset_id': instance.assetId,
          'id': instance.id,
          'organization_id': instance.organizationId,
        };

DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset
    _$DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAssetFromJson(
        Map<String, dynamic> json) {
  return DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset()
    ..affectedRows = json['affected_rows'] as int
    ..returning = (json['returning'] as List<dynamic>)
        .map((e) =>
            DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset$Returning
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic>
    _$DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAssetToJson(
            DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset
                instance) =>
        <String, dynamic>{
          'affected_rows': instance.affectedRows,
          'returning': instance.returning.map((e) => e.toJson()).toList(),
        };

DeleteOneOrganizationAsset$MutationRoot
    _$DeleteOneOrganizationAsset$MutationRootFromJson(
        Map<String, dynamic> json) {
  return DeleteOneOrganizationAsset$MutationRoot()
    ..deleteOrganizationAsset = json['delete_organization_asset'] == null
        ? null
        : DeleteOneOrganizationAsset$MutationRoot$DeleteOrganizationAsset
            .fromJson(
                json['delete_organization_asset'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DeleteOneOrganizationAsset$MutationRootToJson(
        DeleteOneOrganizationAsset$MutationRoot instance) =>
    <String, dynamic>{
      'delete_organization_asset': instance.deleteOrganizationAsset?.toJson(),
    };

DeleteOneOrganizationAssetArguments
    _$DeleteOneOrganizationAssetArgumentsFromJson(Map<String, dynamic> json) {
  return DeleteOneOrganizationAssetArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$DeleteOneOrganizationAssetArgumentsToJson(
        DeleteOneOrganizationAssetArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
