import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';

class CordicoVideoPlayer extends StatefulWidget {
  const CordicoVideoPlayer({
    Key? key,
    required this.videoUrl,
  }) : super(key: key);

  final String videoUrl;

  @override
  State<StatefulWidget> createState() => CordicoVideoPlayerState();
}

class CordicoVideoPlayerState extends State<CordicoVideoPlayer> {
  late VideoPlayerController _videoPlayerController;
  ChewieController? _chewieController;

  @override
  void initState() {
    setupVideoPlayerController();
    super.initState();
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    _chewieController?.dispose();
    super.dispose();
  }

  Future<void> setupVideoPlayerController() async {
    //print('widget.videoUrl: ${widget.videoUrl}');
    _videoPlayerController =
        VideoPlayerController.network(Uri.encodeFull(widget.videoUrl));

    try {
      await _videoPlayerController.initialize();

      setState(() {
        _chewieController = ChewieController(
            videoPlayerController: _videoPlayerController,
            autoInitialize: true,
            showOptions: false,
            deviceOrientationsAfterFullScreen: [
              DeviceOrientation.portraitUp,
            ],
            errorBuilder: (context, errorMessage) {
              return Container();
            });
      });
    } catch (e) {
      //print('ERROR: ${e}');
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 16 / 9,
      child: _chewieController != null
          ? Chewie(controller: _chewieController!)
          : Container(color: Colors.black),
    );
  }
}
