
import 'package:cordico_mobile/features/discover/logic/content_types_holder.dart';
import 'package:cordico_mobile/features/discover/logic/content_types_state_notifier.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final contentTypesNotifierProvider = Provider<ContentTypesNotifier>((ref) {
  final notifier = ContentTypesNotifier(ref)..getContentTypes();
  return notifier;
});

// * Logic holder / StateNotifier
final contentTypesStateNotifierProvider =
    StateNotifierProvider<ContentTypesNotifier, ContentTypesHolder>(
  (ref) => ref.watch(contentTypesNotifierProvider),
);
