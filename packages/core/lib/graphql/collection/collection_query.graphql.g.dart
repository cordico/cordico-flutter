// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'collection_query.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CollectionQuery$QueryRoot$Collection$CollectionType
    _$CollectionQuery$QueryRoot$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionTypeToJson(
            CollectionQuery$QueryRoot$Collection$CollectionType instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionQuery$QueryRoot$Collection$OrganizationCollections$Organization
    _$CollectionQuery$QueryRoot$Collection$OrganizationCollections$OrganizationFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$OrganizationCollections$Organization()
    ..name = json['name'] as String
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$OrganizationCollections$OrganizationToJson(
            CollectionQuery$QueryRoot$Collection$OrganizationCollections$Organization
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
        };

CollectionQuery$QueryRoot$Collection$OrganizationCollections
    _$CollectionQuery$QueryRoot$Collection$OrganizationCollectionsFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$OrganizationCollections()
    ..organization =
        CollectionQuery$QueryRoot$Collection$OrganizationCollections$Organization
            .fromJson(json['organization'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$OrganizationCollectionsToJson(
            CollectionQuery$QueryRoot$Collection$OrganizationCollections
                instance) =>
        <String, dynamic>{
          'organization': instance.organization.toJson(),
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentType
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentTypeToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'is_shared': instance.isShared,
          'description': instance.description,
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentTypeToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'is_shared': instance.isShared,
          'description': instance.description,
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContentFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContentToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContentFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContentToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'index': instance.index,
          'part_id': instance.partId,
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$CategoryToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories()
    ..category =
        CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category
            .fromJson(json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategoriesToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories
                instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentStateFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentStateToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$CollectionToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$CollectionsFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections()
    ..collection =
        CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection
            .fromJson(json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$CollectionsToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections
                instance) =>
        <String, dynamic>{
          'collection': instance.collection.toJson(),
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPartFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart()
    ..contentType =
        CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType
            .fromJson(json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) =>
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) =>
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentState =
        CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState
            .fromJson(json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) =>
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPartToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart
                instance) =>
        <String, dynamic>{
          'content_type': instance.contentType.toJson(),
          'updated_at': instance.updatedAt?.toIso8601String(),
          'name': instance.name,
          'is_shared': instance.isShared,
          'id': instance.id,
          'data': instance.data,
          'created_at': instance.createdAt.toIso8601String(),
          'child_content':
              instance.childContent.map((e) => e.toJson()).toList(),
          'parent_content':
              instance.parentContent.map((e) => e.toJson()).toList(),
          'content_categories':
              instance.contentCategories.map((e) => e.toJson()).toList(),
          'content_state': instance.contentState.toJson(),
          'collections': instance.collections.map((e) => e.toJson()).toList(),
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContentFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent()
    ..contentPart =
        CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart
            .fromJson(json['content_part'] as Map<String, dynamic>)
    ..id = json['id'] as String
    ..index = json['index'] as int;
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContentToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent
                instance) =>
        <String, dynamic>{
          'content_part': instance.contentPart.toJson(),
          'id': instance.id,
          'index': instance.index,
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ParentContent
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ParentContentFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ParentContentToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ParentContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'index': instance.index,
          'part_id': instance.partId,
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentCategories$Category
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentCategories$CategoryToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentCategories
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentCategories()
    ..category =
        CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentCategories$Category
            .fromJson(json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentCategoriesToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentCategories
                instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentState
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentStateFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentStateToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentState
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$Collections$Collection$CollectionType
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$Collections$Collection$CollectionTypeToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$Collections$Collection
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : CollectionQuery$QueryRoot$Collection$CollectionContents$Content$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$Collections$CollectionToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$Collections$Collection
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content$Collections
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$CollectionsFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content$Collections()
    ..collection =
        CollectionQuery$QueryRoot$Collection$CollectionContents$Content$Collections$Collection
            .fromJson(json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$Content$CollectionsToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$Collections
                instance) =>
        <String, dynamic>{
          'collection': instance.collection.toJson(),
        };

CollectionQuery$QueryRoot$Collection$CollectionContents$Content
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$ContentFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents$Content()
    ..contentType =
        CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentType
            .fromJson(json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) =>
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ChildContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) =>
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ParentContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentCategories
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentState =
        CollectionQuery$QueryRoot$Collection$CollectionContents$Content$ContentState
            .fromJson(json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) =>
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content$Collections
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContents$ContentToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents$Content
                instance) =>
        <String, dynamic>{
          'content_type': instance.contentType.toJson(),
          'updated_at': instance.updatedAt?.toIso8601String(),
          'name': instance.name,
          'is_shared': instance.isShared,
          'id': instance.id,
          'data': instance.data,
          'created_at': instance.createdAt.toIso8601String(),
          'child_content':
              instance.childContent.map((e) => e.toJson()).toList(),
          'parent_content':
              instance.parentContent.map((e) => e.toJson()).toList(),
          'content_categories':
              instance.contentCategories.map((e) => e.toJson()).toList(),
          'content_state': instance.contentState.toJson(),
          'collections': instance.collections.map((e) => e.toJson()).toList(),
        };

CollectionQuery$QueryRoot$Collection$CollectionContents
    _$CollectionQuery$QueryRoot$Collection$CollectionContentsFromJson(
        Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection$CollectionContents()
    ..id = json['id'] as String
    ..contentId = json['content_id'] as String
    ..collectionId = json['collection_id'] as String
    ..content = CollectionQuery$QueryRoot$Collection$CollectionContents$Content
        .fromJson(json['content'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionQuery$QueryRoot$Collection$CollectionContentsToJson(
            CollectionQuery$QueryRoot$Collection$CollectionContents instance) =>
        <String, dynamic>{
          'id': instance.id,
          'content_id': instance.contentId,
          'collection_id': instance.collectionId,
          'content': instance.content.toJson(),
        };

CollectionQuery$QueryRoot$Collection
    _$CollectionQuery$QueryRoot$CollectionFromJson(Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot$Collection()
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..name = json['name'] as String
    ..title = json['title'] as String
    ..key = json['key'] as String?
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..collectionTypeId = json['collection_type_id'] as String?
    ..collectionType = json['collection_type'] == null
        ? null
        : CollectionQuery$QueryRoot$Collection$CollectionType.fromJson(
            json['collection_type'] as Map<String, dynamic>)
    ..organizationCollections = (json['organization_collections']
            as List<dynamic>)
        .map((e) => CollectionQuery$QueryRoot$Collection$OrganizationCollections
            .fromJson(e as Map<String, dynamic>))
        .toList()
    ..collectionContents = (json['collection_contents'] as List<dynamic>)
        .map((e) =>
            CollectionQuery$QueryRoot$Collection$CollectionContents.fromJson(
                e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$CollectionQuery$QueryRoot$CollectionToJson(
        CollectionQuery$QueryRoot$Collection instance) =>
    <String, dynamic>{
      'id': instance.id,
      'is_shared': instance.isShared,
      'name': instance.name,
      'title': instance.title,
      'key': instance.key,
      'created_at': instance.createdAt.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'collection_type_id': instance.collectionTypeId,
      'collection_type': instance.collectionType?.toJson(),
      'organization_collections':
          instance.organizationCollections.map((e) => e.toJson()).toList(),
      'collection_contents':
          instance.collectionContents.map((e) => e.toJson()).toList(),
    };

CollectionQuery$QueryRoot _$CollectionQuery$QueryRootFromJson(
    Map<String, dynamic> json) {
  return CollectionQuery$QueryRoot()
    ..collection = (json['collection'] as List<dynamic>)
        .map((e) => CollectionQuery$QueryRoot$Collection.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$CollectionQuery$QueryRootToJson(
        CollectionQuery$QueryRoot instance) =>
    <String, dynamic>{
      'collection': instance.collection.map((e) => e.toJson()).toList(),
    };

CollectionQueryArguments _$CollectionQueryArgumentsFromJson(
    Map<String, dynamic> json) {
  return CollectionQueryArguments(
    offset: json['offset'] as int?,
    limit: json['limit'] as int?,
    contentStateNames: (json['contentStateNames'] as List<dynamic>?)
        ?.map((e) => e as String?)
        .toList(),
  );
}

Map<String, dynamic> _$CollectionQueryArgumentsToJson(
        CollectionQueryArguments instance) =>
    <String, dynamic>{
      'offset': instance.offset,
      'limit': instance.limit,
      'contentStateNames': instance.contentStateNames,
    };
