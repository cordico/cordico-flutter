// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'update_organization_by_id.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateOrganizationById$MutationRoot$UpdateOneOrganization
    _$UpdateOrganizationById$MutationRoot$UpdateOneOrganizationFromJson(
        Map<String, dynamic> json) {
  return UpdateOrganizationById$MutationRoot$UpdateOneOrganization()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?
    ..data = json['data']
    ..organizationTypeId = json['organizationTypeId'] as String;
}

Map<String,
    dynamic> _$UpdateOrganizationById$MutationRoot$UpdateOneOrganizationToJson(
        UpdateOrganizationById$MutationRoot$UpdateOneOrganization instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'description': instance.description,
      'data': instance.data,
      'organizationTypeId': instance.organizationTypeId,
    };

UpdateOrganizationById$MutationRoot
    _$UpdateOrganizationById$MutationRootFromJson(Map<String, dynamic> json) {
  return UpdateOrganizationById$MutationRoot()
    ..updateOneOrganization = json['updateOneOrganization'] == null
        ? null
        : UpdateOrganizationById$MutationRoot$UpdateOneOrganization.fromJson(
            json['updateOneOrganization'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UpdateOrganizationById$MutationRootToJson(
        UpdateOrganizationById$MutationRoot instance) =>
    <String, dynamic>{
      'updateOneOrganization': instance.updateOneOrganization?.toJson(),
    };

UpdateOrganizationByIdArguments _$UpdateOrganizationByIdArgumentsFromJson(
    Map<String, dynamic> json) {
  return UpdateOrganizationByIdArguments(
    id: json['id'] as String?,
    data: json['data'],
    description: json['description'] as String?,
    name: json['name'] as String?,
    organization_type_id: json['organization_type_id'] as String?,
  );
}

Map<String, dynamic> _$UpdateOrganizationByIdArgumentsToJson(
        UpdateOrganizationByIdArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
      'data': instance.data,
      'description': instance.description,
      'name': instance.name,
      'organization_type_id': instance.organization_type_id,
    };
