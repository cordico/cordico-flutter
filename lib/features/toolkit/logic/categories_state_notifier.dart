import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:cordico_mobile/core/managers/offline_models.dart/offline_models.dart';
import 'package:cordico_mobile/core/managers/offlline_manager.dart';
import 'package:cordico_mobile/core/providers/global_providers.dart';
import 'package:core/core.dart';
import 'package:core/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class CategoriesNotifier extends ChangeNotifier {
  CategoriesNotifier({required ICategoryRepository categoryRepository})
      : _categoryRepository = categoryRepository;

  bool _isLoading = false;
  bool get isLoading => _isLoading;

  final ICategoryRepository _categoryRepository;

  List<CategoryModel>? _categories;
  List<CategoryModel>? get categories => _categories;

  void reset() {
    _categories = null;
    notifyListeners();
  }

  Future<void> getCategories() async {
    final connectityCheck = await Connectivity().checkConnectivity();

    if (connectityCheck == ConnectivityResult.none) {
      final list = OfflineManager.getData(key: ModelsKeys.categories);

      if (list != null) {
        final contents = CategoriesOffline.fromJson(list).data ?? [];
        _categories = contents;
      } else {
        _categories = [];
      }
    } else {
      _isLoading = true;
      notifyListeners();
      final result = await _categoryRepository.getCategories();
      _isLoading = false;

      await result.fold((error) {}, (categories) async {
        // ORDER THE CATEGORIES BY NAME --- DR GILMARTIN ON TOP OF THEM
        final list = categories
            ?.where((element) => element.id == kCategoryDrGilmartinId)
            .toList();
        final list2 = categories
            ?.where((element) => element.id == kCategoryAdditionalResourcesId)
            .toList();
        categories?.sort((a, b) => a.name.compareTo(b.name));
        if (list?.isNotEmpty == true) {
          final drGilmartin = list?.first;
          categories?.remove(drGilmartin);
          categories?.insert(0, drGilmartin!);
        }

        if (list2?.isNotEmpty == true) {
          final additionalResources = list2?.first;
          categories?.remove(additionalResources);
          categories?.add(additionalResources!);
        }

        final content = CategoriesOffline()..data = categories;

        await OfflineManager.persistData(
          key: ModelsKeys.categories,
          value: content.toJson(),
        );

        _categories = categories ?? <CategoryModel>[];
      });
    }

    notifyListeners();
  }
}

final categoriesNotifierProvider = ChangeNotifierProvider<CategoriesNotifier>(
  (ref) => CategoriesNotifier(
      categoryRepository: ref.watch(categoryRepositoryProvider)),
);
