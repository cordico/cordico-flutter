// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'user_by_user_type.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class UserByUserType$QueryRoot$User extends JsonSerializable
    with EquatableMixin {
  UserByUserType$QueryRoot$User();

  factory UserByUserType$QueryRoot$User.fromJson(Map<String, dynamic> json) =>
      _$UserByUserType$QueryRoot$UserFromJson(json);

  dynamic? data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  late String id;

  late String identifier;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @JsonKey(name: 'user_service_id')
  String? userServiceId;

  @JsonKey(name: 'user_type_id')
  late String userTypeId;

  @override
  List<Object?> get props =>
      [data, createdAt, id, identifier, updatedAt, userServiceId, userTypeId];
  @override
  Map<String, dynamic> toJson() => _$UserByUserType$QueryRoot$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UserByUserType$QueryRoot extends JsonSerializable with EquatableMixin {
  UserByUserType$QueryRoot();

  factory UserByUserType$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$UserByUserType$QueryRootFromJson(json);

  late List<UserByUserType$QueryRoot$User> user;

  @override
  List<Object?> get props => [user];
  @override
  Map<String, dynamic> toJson() => _$UserByUserType$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UserByUserTypeArguments extends JsonSerializable with EquatableMixin {
  UserByUserTypeArguments({this.userTypes});

  @override
  factory UserByUserTypeArguments.fromJson(Map<String, dynamic> json) =>
      _$UserByUserTypeArgumentsFromJson(json);

  final List<String>? userTypes;

  @override
  List<Object?> get props => [userTypes];
  @override
  Map<String, dynamic> toJson() => _$UserByUserTypeArgumentsToJson(this);
}

final USER_BY_USER_TYPE_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'UserByUserType'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'userTypes')),
            type: ListTypeNode(
                type: NamedTypeNode(
                    name: NameNode(value: 'uuid'), isNonNull: true),
                isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'user'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'user_type_id'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: '_in'),
                              value: VariableNode(
                                  name: NameNode(value: 'userTypes')))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'identifier'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'user_service_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'user_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class UserByUserTypeQuery
    extends GraphQLQuery<UserByUserType$QueryRoot, UserByUserTypeArguments> {
  UserByUserTypeQuery({required this.variables});

  @override
  final DocumentNode document = USER_BY_USER_TYPE_QUERY_DOCUMENT;

  @override
  final String operationName = 'UserByUserType';

  @override
  final UserByUserTypeArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  UserByUserType$QueryRoot parse(Map<String, dynamic> json) =>
      UserByUserType$QueryRoot.fromJson(json);
}
