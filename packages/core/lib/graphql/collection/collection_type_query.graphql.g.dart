// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'collection_type_query.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CollectionTypeQuery$QueryRoot$CollectionType
    _$CollectionTypeQuery$QueryRoot$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionTypeQuery$QueryRoot$CollectionType()
    ..updaterId = json['updater_id'] as String?
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..schema = json['schema']
    ..name = json['name'] as String
    ..key = json['key'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?
    ..creatorId = json['creator_id'] as String?
    ..createdAt = DateTime.parse(json['created_at'] as String);
}

Map<String, dynamic> _$CollectionTypeQuery$QueryRoot$CollectionTypeToJson(
        CollectionTypeQuery$QueryRoot$CollectionType instance) =>
    <String, dynamic>{
      'updater_id': instance.updaterId,
      'updated_at': instance.updatedAt?.toIso8601String(),
      'schema': instance.schema,
      'name': instance.name,
      'key': instance.key,
      'id': instance.id,
      'description': instance.description,
      'creator_id': instance.creatorId,
      'created_at': instance.createdAt.toIso8601String(),
    };

CollectionTypeQuery$QueryRoot _$CollectionTypeQuery$QueryRootFromJson(
    Map<String, dynamic> json) {
  return CollectionTypeQuery$QueryRoot()
    ..collectionType = (json['collection_type'] as List<dynamic>)
        .map((e) => CollectionTypeQuery$QueryRoot$CollectionType.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$CollectionTypeQuery$QueryRootToJson(
        CollectionTypeQuery$QueryRoot instance) =>
    <String, dynamic>{
      'collection_type':
          instance.collectionType.map((e) => e.toJson()).toList(),
    };
