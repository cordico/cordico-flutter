// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'user_type_query.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class UserTypeQuery$QueryRoot$UserType extends JsonSerializable
    with EquatableMixin {
  UserTypeQuery$QueryRoot$UserType();

  factory UserTypeQuery$QueryRoot$UserType.fromJson(
          Map<String, dynamic> json) =>
      _$UserTypeQuery$QueryRoot$UserTypeFromJson(json);

  late String id;

  @JsonKey(name: 'identity_schema')
  late String identitySchema;

  late String key;

  late String name;

  late dynamic schema;

  @override
  List<Object?> get props => [id, identitySchema, key, name, schema];
  @override
  Map<String, dynamic> toJson() =>
      _$UserTypeQuery$QueryRoot$UserTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UserTypeQuery$QueryRoot extends JsonSerializable with EquatableMixin {
  UserTypeQuery$QueryRoot();

  factory UserTypeQuery$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$UserTypeQuery$QueryRootFromJson(json);

  @JsonKey(name: 'user_type')
  late List<UserTypeQuery$QueryRoot$UserType> userType;

  @override
  List<Object?> get props => [userType];
  @override
  Map<String, dynamic> toJson() => _$UserTypeQuery$QueryRootToJson(this);
}

final USER_TYPE_QUERY_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'UserTypeQuery'),
      variableDefinitions: [],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'user_type'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'identity_schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class UserTypeQueryQuery
    extends GraphQLQuery<UserTypeQuery$QueryRoot, JsonSerializable> {
  UserTypeQueryQuery();

  @override
  final DocumentNode document = USER_TYPE_QUERY_QUERY_DOCUMENT;

  @override
  final String operationName = 'UserTypeQuery';

  @override
  List<Object?> get props => [document, operationName];
  @override
  UserTypeQuery$QueryRoot parse(Map<String, dynamic> json) =>
      UserTypeQuery$QueryRoot.fromJson(json);
}
