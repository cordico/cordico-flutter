import 'dart:async';

import 'package:cordico_mobile/core/providers/global_providers.dart';
import 'package:core/env/environment_config.dart';
import 'package:core/repositories/content_repository.dart';
import 'package:core/utils/failure.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final contentIdProvider = StateProvider<String>((ref) => '');

final contentRepositoryFamily =
  FutureProvider.autoDispose.family<IContentRepository, String>(
          (ref, token) async {
    final completer = Completer<IContentRepository>();
    final token = await secureStorage.read(key: 'cordico.sessionToken');
    final repository = ContentRepository(
      url: EnvironmentConfig.contentGraphqlUrl,
      userUrl: EnvironmentConfig.userGraphqlUrl,
      userToken: token,
    );
    completer.complete(repository);
    return completer.future;
  });

final contentByIdProvider =
    FutureProvider.autoDispose<Map<String, dynamic>>((ref) async {
  final repository = ref.watch(contentRepositoryProvider);
  final partsRepository = ref.watch(contentPartsRepositoryProvider);

  final contentId = ref.watch(contentIdProvider);

  final result1 = await repository.getContentById(contentId);
  final result2 =
      await partsRepository.getContentPartByContentID(contentID: contentId);

  final finalResult = <String, dynamic>{};

  Failure? failure;

  result1.fold((err) => failure = err, (r) {
    finalResult.putIfAbsent('content', () => r);
  });

  result2.fold((err) {
    failure = err;
  }, (r) {
    finalResult.putIfAbsent('content_parts', () => r);
  });

  if (failure != null) {
    throw Exception(failure!.toString());
  }

  ref.maintainState = true;

  return finalResult;
});
