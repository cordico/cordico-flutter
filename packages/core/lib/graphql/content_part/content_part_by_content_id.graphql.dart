// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'content_part_by_content_id.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentType
    extends JsonSerializable with EquatableMixin {
  ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentType();

  factory ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentTypeFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent$ContentPart
    extends JsonSerializable with EquatableMixin {
  ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent$ContentPart();

  factory ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent$ContentPart.fromJson(
          Map<String, dynamic> json) =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent$ContentPartFromJson(
          json);

  late dynamic data;

  @override
  List<Object?> get props => [data];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent$ContentPartToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent
    extends JsonSerializable with EquatableMixin {
  ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent();

  factory ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContentFromJson(
          json);

  @JsonKey(name: 'content_part')
  late ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent$ContentPart
      contentPart;

  late String id;

  late int index;

  @override
  List<Object?> get props => [contentPart, id, index];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ParentContent
    extends JsonSerializable with EquatableMixin {
  ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ParentContent();

  factory ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ParentContentFromJson(
          json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ParentContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories$Category();

  factory ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories
    extends JsonSerializable with EquatableMixin {
  ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories();

  factory ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategoriesFromJson(
          json);

  late ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories$Category
      category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategoriesToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentState
    extends JsonSerializable with EquatableMixin {
  ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentState();

  factory ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentStateFromJson(
          json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentStateToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection$CollectionType();

  factory ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection();

  factory ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$CollectionFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections
    extends JsonSerializable with EquatableMixin {
  ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections();

  factory ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$CollectionsFromJson(
          json);

  late ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection
      collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$CollectionsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ContentPartByContentID$QueryRoot$ContentPart$ContentPart
    extends JsonSerializable with EquatableMixin {
  ContentPartByContentID$QueryRoot$ContentPart$ContentPart();

  factory ContentPartByContentID$QueryRoot$ContentPart$ContentPart.fromJson(
          Map<String, dynamic> json) =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPartFromJson(json);

  @JsonKey(name: 'content_type')
  late ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentType
      contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<
          ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent>
      childContent;

  @JsonKey(name: 'parent_content')
  late List<
          ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ParentContent>
      parentContent;

  @JsonKey(name: 'content_categories')
  late List<
          ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentState
      contentState;

  late List<
          ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections>
      collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentPartByContentID$QueryRoot$ContentPart$ContentPartToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentPartByContentID$QueryRoot$ContentPart extends JsonSerializable
    with EquatableMixin {
  ContentPartByContentID$QueryRoot$ContentPart();

  factory ContentPartByContentID$QueryRoot$ContentPart.fromJson(
          Map<String, dynamic> json) =>
      _$ContentPartByContentID$QueryRoot$ContentPartFromJson(json);

  late String id;

  late int index;

  @JsonKey(name: 'content_part')
  late ContentPartByContentID$QueryRoot$ContentPart$ContentPart contentPart;

  @override
  List<Object?> get props => [id, index, contentPart];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentPartByContentID$QueryRoot$ContentPartToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentPartByContentID$QueryRoot extends JsonSerializable
    with EquatableMixin {
  ContentPartByContentID$QueryRoot();

  factory ContentPartByContentID$QueryRoot.fromJson(
          Map<String, dynamic> json) =>
      _$ContentPartByContentID$QueryRootFromJson(json);

  @JsonKey(name: 'content_part')
  late List<ContentPartByContentID$QueryRoot$ContentPart> contentPart;

  @override
  List<Object?> get props => [contentPart];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentPartByContentID$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentPartByContentIDArguments extends JsonSerializable
    with EquatableMixin {
  ContentPartByContentIDArguments(
      {required this.contentID,
      this.contentStateNames,
      this.offset,
      this.limit});

  @override
  factory ContentPartByContentIDArguments.fromJson(Map<String, dynamic> json) =>
      _$ContentPartByContentIDArgumentsFromJson(json);

  late String contentID;

  final List<String?>? contentStateNames;

  final int? offset;

  final int? limit;

  @override
  List<Object?> get props => [contentID, contentStateNames, offset, limit];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentPartByContentIDArgumentsToJson(this);
}

final CONTENT_PART_BY_CONTENT_I_D_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'ContentPartByContentID'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentID')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentStateNames')),
            type: ListTypeNode(
                type: NamedTypeNode(
                    name: NameNode(value: 'String'), isNonNull: false),
                isNonNull: false),
            defaultValue: DefaultValueNode(
                value: ListValueNode(values: [
              StringValueNode(value: 'Published', isBlock: false)
            ])),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'offset')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '0')),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'limit')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '10000')),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'content_part'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'offset'),
                  value: VariableNode(name: NameNode(value: 'offset'))),
              ArgumentNode(
                  name: NameNode(value: 'limit'),
                  value: VariableNode(name: NameNode(value: 'limit'))),
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'content_id'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: '_eq'),
                              value: VariableNode(
                                  name: NameNode(value: 'contentID')))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'content'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'content_state'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'name'),
                                    value: ObjectValueNode(fields: [
                                      ObjectFieldNode(
                                          name: NameNode(value: '_in'),
                                          value: VariableNode(
                                              name: NameNode(
                                                  value: 'contentStateNames')))
                                    ]))
                              ]))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'index'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'content_part'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'content_type'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'is_shared'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'description'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ])),
                    FieldNode(
                        name: NameNode(value: 'updated_at'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'is_shared'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'data'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'created_at'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'child_content'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'content_part'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'data'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'index'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ])),
                    FieldNode(
                        name: NameNode(value: 'parent_content'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'index'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'part_id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ])),
                    FieldNode(
                        name: NameNode(value: 'content_categories'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'category'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'description'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ]))
                        ])),
                    FieldNode(
                        name: NameNode(value: 'content_state'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ])),
                    FieldNode(
                        name: NameNode(value: 'collections'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'collection'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'collection_type'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'description'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ]))
                              ]))
                        ]))
                  ]))
            ]))
      ]))
]);

class ContentPartByContentIDQuery extends GraphQLQuery<
    ContentPartByContentID$QueryRoot, ContentPartByContentIDArguments> {
  ContentPartByContentIDQuery({required this.variables});

  @override
  final DocumentNode document = CONTENT_PART_BY_CONTENT_I_D_QUERY_DOCUMENT;

  @override
  final String operationName = 'ContentPartByContentID';

  @override
  final ContentPartByContentIDArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  ContentPartByContentID$QueryRoot parse(Map<String, dynamic> json) =>
      ContentPartByContentID$QueryRoot.fromJson(json);
}
