import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/images.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/card/cordico_card.dart';
import 'package:flutter/material.dart';

class AssessmentAnswer extends StatelessWidget {
  const AssessmentAnswer({
    Key? key,
    required this.onTap,
    required this.isChecked,
    required this.labelText,
  }) : super(key: key);

  final bool isChecked;
  final VoidCallback onTap;
  final String labelText;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: CordicoCard(
        onTap: onTap,
        color: isChecked ? ThemeColors.blue.shade200 : ThemeColors.blue.shade50,
        cornerRadius: 6,
        borderSide: BorderSide(
          color:
              isChecked ? ThemeColors.blue.shade400 : ThemeColors.blue.shade200,
        ),
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Row(
            children: [
              Image.asset(
                isChecked ? Images.icChecked : Images.icUnchecked,
                height: 24,
                width: 24,
              ),
              Container(width: 14),
              Expanded(
                child: Text(
                  labelText,
                  style: ThemeStyles.cardContentTitle(context)?.copyWith(
                      color: isChecked
                          ? ThemeColors.blue.shade600
                          : ThemeColors.blue.shade300),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
