// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'create_one_organization_contact_mutation.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateOneOrganizationContactMutation$MutationRoot$CreateOneOrganizationContact
    _$CreateOneOrganizationContactMutation$MutationRoot$CreateOneOrganizationContactFromJson(
        Map<String, dynamic> json) {
  return CreateOneOrganizationContactMutation$MutationRoot$CreateOneOrganizationContact()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$CreateOneOrganizationContactMutation$MutationRoot$CreateOneOrganizationContactToJson(
            CreateOneOrganizationContactMutation$MutationRoot$CreateOneOrganizationContact
                instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

CreateOneOrganizationContactMutation$MutationRoot
    _$CreateOneOrganizationContactMutation$MutationRootFromJson(
        Map<String, dynamic> json) {
  return CreateOneOrganizationContactMutation$MutationRoot()
    ..createOneOrganizationContact =
        CreateOneOrganizationContactMutation$MutationRoot$CreateOneOrganizationContact
            .fromJson(
                json['createOneOrganizationContact'] as Map<String, dynamic>);
}

Map<String, dynamic> _$CreateOneOrganizationContactMutation$MutationRootToJson(
        CreateOneOrganizationContactMutation$MutationRoot instance) =>
    <String, dynamic>{
      'createOneOrganizationContact':
          instance.createOneOrganizationContact.toJson(),
    };

CreateOneOrganizationContactMutationArguments
    _$CreateOneOrganizationContactMutationArgumentsFromJson(
        Map<String, dynamic> json) {
  return CreateOneOrganizationContactMutationArguments(
    contactTypeId: json['contactTypeId'] as String?,
    organizationId: json['organizationId'] as String?,
    name: json['name'] as String,
    title: json['title'] as String,
    data: json['data'],
    isActive: json['isActive'] as bool?,
  );
}

Map<String, dynamic> _$CreateOneOrganizationContactMutationArgumentsToJson(
        CreateOneOrganizationContactMutationArguments instance) =>
    <String, dynamic>{
      'contactTypeId': instance.contactTypeId,
      'organizationId': instance.organizationId,
      'name': instance.name,
      'title': instance.title,
      'data': instance.data,
      'isActive': instance.isActive,
    };
