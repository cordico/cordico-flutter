// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'organization_contact_by_id.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class OrganizationContactById$QueryRoot$OrganizationContactsByPk$ContactType
    extends JsonSerializable with EquatableMixin {
  OrganizationContactById$QueryRoot$OrganizationContactsByPk$ContactType();

  factory OrganizationContactById$QueryRoot$OrganizationContactsByPk$ContactType.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContactById$QueryRoot$OrganizationContactsByPk$ContactTypeFromJson(
          json);

  @JsonKey(name: 'display_name')
  late String displayName;

  late String id;

  late String name;

  @override
  List<Object?> get props => [displayName, id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContactById$QueryRoot$OrganizationContactsByPk$ContactTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContactById$QueryRoot$OrganizationContactsByPk$Organization
    extends JsonSerializable with EquatableMixin {
  OrganizationContactById$QueryRoot$OrganizationContactsByPk$Organization();

  factory OrganizationContactById$QueryRoot$OrganizationContactsByPk$Organization.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContactById$QueryRoot$OrganizationContactsByPk$OrganizationFromJson(
          json);

  late String name;

  @override
  List<Object?> get props => [name];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContactById$QueryRoot$OrganizationContactsByPk$OrganizationToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContactById$QueryRoot$OrganizationContactsByPk
    extends JsonSerializable with EquatableMixin {
  OrganizationContactById$QueryRoot$OrganizationContactsByPk();

  factory OrganizationContactById$QueryRoot$OrganizationContactsByPk.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContactById$QueryRoot$OrganizationContactsByPkFromJson(
          json);

  dynamic? data;

  late String title;

  @JsonKey(name: 'contact_type_id')
  late String contactTypeId;

  @JsonKey(name: 'contact_type')
  late OrganizationContactById$QueryRoot$OrganizationContactsByPk$ContactType
      contactType;

  @JsonKey(name: 'organization_id')
  late String organizationId;

  late OrganizationContactById$QueryRoot$OrganizationContactsByPk$Organization
      organization;

  late String name;

  @JsonKey(name: 'is_active')
  late bool isActive;

  late String id;

  DateTime? updated;

  late DateTime created;

  @override
  List<Object?> get props => [
        data,
        title,
        contactTypeId,
        contactType,
        organizationId,
        organization,
        name,
        isActive,
        id,
        updated,
        created
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContactById$QueryRoot$OrganizationContactsByPkToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContactById$QueryRoot extends JsonSerializable
    with EquatableMixin {
  OrganizationContactById$QueryRoot();

  factory OrganizationContactById$QueryRoot.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContactById$QueryRootFromJson(json);

  @JsonKey(name: 'organization_contacts_by_pk')
  OrganizationContactById$QueryRoot$OrganizationContactsByPk?
      organizationContactsByPk;

  @override
  List<Object?> get props => [organizationContactsByPk];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContactById$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContactByIdArguments extends JsonSerializable
    with EquatableMixin {
  OrganizationContactByIdArguments({required this.id});

  @override
  factory OrganizationContactByIdArguments.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContactByIdArgumentsFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContactByIdArgumentsToJson(this);
}

final ORGANIZATION_CONTACT_BY_ID_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'OrganizationContactById'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'organization_contacts_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'id'),
                  value: VariableNode(name: NameNode(value: 'id')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'title'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'contact_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'contact_type'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'display_name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'organization_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'organization'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'is_active'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class OrganizationContactByIdQuery extends GraphQLQuery<
    OrganizationContactById$QueryRoot, OrganizationContactByIdArguments> {
  OrganizationContactByIdQuery({required this.variables});

  @override
  final DocumentNode document = ORGANIZATION_CONTACT_BY_ID_QUERY_DOCUMENT;

  @override
  final String operationName = 'OrganizationContactById';

  @override
  final OrganizationContactByIdArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  OrganizationContactById$QueryRoot parse(Map<String, dynamic> json) =>
      OrganizationContactById$QueryRoot.fromJson(json);
}
