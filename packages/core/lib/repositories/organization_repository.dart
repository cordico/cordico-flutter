// ignore_for_file: lines_longer_than_80_chars, cascade_invocations

import 'package:core/core.dart';
import 'package:core/graphql/organization/oc_by_oid_and_ctype.dart';
import 'package:core/graphql/organization/oc_by_oid_ctid.dart';
import 'package:core/graphql/organization/organization_by_id.dart';
import 'package:core/graphql/organization/organization_by_ids.dart';
import 'package:core/graphql/organization/organization_contact_by_id.dart';
import 'package:core/graphql/organization/organization_contact_query.dart';
import 'package:core/graphql/organization/organization_contacts_by_organization.dart';
import 'package:core/graphql/organization/organization_query.dart';
import 'package:core/graphql/organization/organization_type_by_id.dart';
import 'package:core/graphql/organization/organization_type_query.dart';
import 'package:core/graphql/organization/update_organization_by_id.dart';
import 'package:core/repositories/graphql_repository.dart';
import 'package:core/utils/failure.dart';
import 'package:core/utils/map_extension.dart';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:kratos_api/kratos_api.dart';

abstract class IOrganizationRepository {
  //TYPE
  Future<Either<Failure, List<OrganizationTypeModel>?>> listOrganizationType();
  Future<Either<Failure, OrganizationTypeModel?>> getOrganizationTypeById(
      String id,
      {FetchPolicy? fetchPolicy});

  //ORGANIZATION COLLECTION
  Future<Either<Failure, List<OrganizationCollectionModel>?>>
      listOrganizationCollectionByOrganizationIdAndCollectionType({
    int? limit,
    int? offset,
    String? organizationId,
    String? collectionType,
    List<String>? contentStateNames,
  });

  //ORGANIZATION
  Future<Either<Failure, List<OrganizationModel>?>> listOrganization(
      {OrganizationQueryArguments? arguments});
  Future<Either<Failure, List<OrganizationModel>?>> getOrganizationByIDs(
      List<String> ids,
      {FetchPolicy? fetchPolicy});
  Future<Either<Failure, OrganizationModel?>> getOrganizationByID(String id,
      {FetchPolicy? fetchPolicy});

  //ORGANIZATION CONTANT
  Future<Either<Failure, OrganizationContactModel?>> getOrganizationContactByID(
      String id,
      {FetchPolicy? fetchPolicy});
  Future<Either<Failure, List<OrganizationContactModel>?>>
      listOrganizationContactsByOrganizationIdAndContactTypeId({
    required String organizationID,
    required String contactTypeId,
    List<bool>? isActive,
    int? offset,
    int? limit,
  });
  Future<Either<Failure, List<OrganizationContactModel>?>>
      listOrganizationContacts({OrganizationContactQueryArguments? arguments});
  Future<Either<Failure, List<OrganizationContactModel>?>>
      getOrganizationContactsByOrganization(String organizationID,
          {List<bool>? isActive});

  //CRUD ORGANIZATION
  Future<Either<Failure, OrganizationModel?>> updateOrganizationByID(
      {required String id,
      dynamic data,
      String? description,
      String? name,
      String? organizationTypeId,
      String? authorizationToken});

  //CRUD ORGANIZATION CONTACT
  Future<Either<Failure, OrganizationContactModel?>>
      createOneOrganizationContact(
          CreateOneOrganizationContactMutationArguments arguments,
          {String? authorizationToken});
  Future<Either<Failure, OrganizationContactModel?>>
      updateOneOrganizationContact(
          UpdateOneOrganizationContactMutationArguments arguments,
          {String? authorizationToken});
  Future<Either<Failure, OrganizationContactModel?>>
      deleteOneOrganizationContact(
          DeleteOneOrganizationContactMutationArguments arguments,
          {String? authorizationToken});
}

class OrganizationRepository extends GraphQLRepository
    implements IOrganizationRepository {
  OrganizationRepository({required String url, required String userUrl,
    required String kratosUrl,
      String? userToken,})
      : super(url: url, userUrl: userUrl, token: userToken) {
    final dio = Dio(BaseOptions(baseUrl: kratosUrl));
    api = KratosApi(dio: dio, serializers: standardSerializers);
    vAlphaApi = api.getV0alpha2Api();
  }

  late KratosApi api;
  late V0alpha2Api vAlphaApi;

  @override
  Future<Either<Failure, List<OrganizationTypeModel>?>>
      listOrganizationType() async {
    try {
      final listContactType = OrganizationTypeQueryQuery();

      final response = await graphqlClient.query<OrganizationTypeModel>(QueryOptions(
          document: listContactType.document,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = OrganizationTypeQuery$QueryRoot.fromJson(response.data!);
        return Right(root.organizationType
            .map((e) => OrganizationTypeModel.fromJson(e.toJson()))
            .toList());
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, OrganizationTypeModel?>> getOrganizationTypeById(
      String id,
      {FetchPolicy? fetchPolicy}) async {
    try {
      final query = OrganizationTypeByIDQuery(
          variables: OrganizationTypeByIDArguments(id: id));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<OrganizationTypeModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = OrganizationTypeByID$QueryRoot.fromJson(response.data!);
        if (root.organizationTypeByPk != null) {
          return Right(root.organizationTypeByPk);
        }
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<OrganizationCollectionModel>?>>
      listOrganizationCollectionByOrganizationIdAndCollectionType({
    int? limit,
    int? offset,
    String? organizationId,
    String? collectionType,
    List<String>? contentStateNames,
  }) async {
    try {
      final query = OCByOIdAndCTypeQuery(
          variables: OCByOIdAndCTypeArguments(
              collection_type: collectionType,
              limit: limit,
              offset: offset,
              organization_id: organizationId,
              contentStateNames: contentStateNames));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<OrganizationCollectionModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = OCByOIdAndCType$QueryRoot.fromJson(response.data!);
        return Right(root.organizationCollections
            .map((e) =>
                OrganizationCollectionModel.fromJson(e.collection.toJson()))
            .toList());
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<OrganizationModel>?>> listOrganization(
      {OrganizationQueryArguments? arguments}) async {
    try {
      final listContactType = OrganizationQueryQuery(
          variables: arguments ?? OrganizationQueryArguments());

      final response = await graphqlClient.query<OrganizationModel>(QueryOptions(
          document: listContactType.document,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = OrganizationQuery$QueryRoot.fromJson(response.data!);
        return Right(root.organization
            .map((e) => OrganizationModel.fromJson(e.toJson()))
            .toList());
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, OrganizationModel?>> getOrganizationByID(String id,
      {FetchPolicy? fetchPolicy}) async {
    try {
      final query =
          OrganizationByIDQuery(variables: OrganizationByIDArguments(id: id));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<OrganizationModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: fetchPolicy ?? FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = OrganizationByID$QueryRoot.fromJson(response.data!);
        return Right(root.organizationByPk);
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<OrganizationModel>?>> getOrganizationByIDs(
      List<String> ids,
      {FetchPolicy? fetchPolicy}) async {
    try {
      final query = OrganizationByIDsQuery(
          variables: OrganizationByIDsArguments(ids: ids));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<OrganizationModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: fetchPolicy ?? FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = OrganizationByIDs$QueryRoot.fromJson(response.data!);
        return Right(root.organization
            .map((e) => OrganizationModel.fromJson(e.toJson()))
            .toList());
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, OrganizationContactModel?>> getOrganizationContactByID(
      String id,
      {FetchPolicy? fetchPolicy}) async {
    try {
      final query = OrganizationContactByIdQuery(
          variables: OrganizationContactByIdArguments(id: id));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<OrganizationContactModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: fetchPolicy ?? FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = OrganizationContactById$QueryRoot.fromJson(response.data!);
        if (root.organizationContactsByPk != null) {
          return Right(root.organizationContactsByPk);
        }
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<OrganizationContactModel>?>>
      listOrganizationContacts(
          {OrganizationContactQueryArguments? arguments}) async {
    try {
      final listContactType = OrganizationContactQueryQuery(
          variables: arguments ?? OrganizationContactQueryArguments());

      final response = await graphqlClient.query<OrganizationContactModel>(QueryOptions(
          document: listContactType.document,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root =
            OrganizationContactQuery$QueryRoot.fromJson(response.data!);
        return Right(root.organizationContacts
            .map((e) => OrganizationContactModel.fromJson(e.toJson()))
            .toList());
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<OrganizationContactModel>?>>
      listOrganizationContactsByOrganizationIdAndContactTypeId({
    required String organizationID,
    required String contactTypeId,
    List<bool>? isActive,
    int? offset,
    int? limit,
  }) async {
    try {
      final query = OCByOIdCtIdQuery(
          variables: OCByOIdCtIdArguments(
              contact_type_id: contactTypeId,
              organizationID: organizationID,
              isActive: isActive,
              limit: limit,
              offset: offset));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<OrganizationContactModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = OCByOIdCtId$QueryRoot.fromJson(response.data!);
        return Right(root.organizationContacts.map((e) {
          final m = e.toJson();
          removeNullAndEmptyParams(m);
          return OrganizationContactModel.fromJson(m);
        }).toList());
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<OrganizationContactModel>?>>
      getOrganizationContactsByOrganization(String organizationID,
          {List<bool>? isActive}) async {
    try {
      final query = OrganizationContactsByOrganizationQuery(
          variables: OrganizationContactsByOrganizationArguments(
              organizationID: organizationID, isActive: isActive));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<OrganizationContactModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = OrganizationContactsByOrganization$QueryRoot.fromJson(
            response.data!);
        return Right(root.organizationContacts
            .map((e) => OrganizationContactModel.fromJson(e.toJson()))
            .toList());
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, OrganizationModel?>> updateOrganizationByID(
      {required String id,
      dynamic data,
      String? description,
      String? name,
      String? organizationTypeId,
      String? authorizationToken}) async {
    try {
      final query = UpdateOrganizationByIdMutation(
          variables: UpdateOrganizationByIdArguments(
              id: id,
              data: data,
              description: description,
              name: name,
              organization_type_id: organizationTypeId));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      //print(variables);
      //print(jsonEncode(variables).replaceAll('"', r'\"'));

      final response = await userGraphQLClient.query<OrganizationModel>(QueryOptions(
          document: query.document,
          variables: variables,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          fetchPolicy: FetchPolicy.noCache));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root =
            UpdateOrganizationById$MutationRoot.fromJson(response.data!);
        if (root.updateOneOrganization != null) {
          return getOrganizationByID(root.updateOneOrganization!.id,
              fetchPolicy: FetchPolicy.noCache);
        }
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, OrganizationContactModel?>>
      createOneOrganizationContact(
          CreateOneOrganizationContactMutationArguments arguments,
          {String? authorizationToken}) async {
    try {
      final query =
          CreateOneOrganizationContactMutationMutation(variables: arguments);

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await userGraphQLClient.query<OrganizationContactModel>(QueryOptions(
          document: query.document,
          variables: variables,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          fetchPolicy: FetchPolicy.noCache));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = CreateOneOrganizationContactMutation$MutationRoot.fromJson(
            response.data!);
        return getOrganizationContactByID(root.createOneOrganizationContact.id,
            fetchPolicy: FetchPolicy.noCache);
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, OrganizationContactModel?>>
      updateOneOrganizationContact(
          UpdateOneOrganizationContactMutationArguments arguments,
          {String? authorizationToken}) async {
    try {
      final query =
          UpdateOneOrganizationContactMutationMutation(variables: arguments);

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await userGraphQLClient.query<OrganizationContactModel>(QueryOptions(
          document: query.document,
          variables: variables,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          fetchPolicy: FetchPolicy.noCache));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = UpdateOneOrganizationContactMutation$MutationRoot.fromJson(
            response.data!);
        if (root.updateOneOrganizationContact != null) {
          return getOrganizationContactByID(
              root.updateOneOrganizationContact!.id,
              fetchPolicy: FetchPolicy.noCache);
        }
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, OrganizationContactModel?>>
      deleteOneOrganizationContact(
          DeleteOneOrganizationContactMutationArguments arguments,
          {String? authorizationToken}) async {
    try {
      final query =
          DeleteOneOrganizationContactMutationMutation(variables: arguments);

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await userGraphQLClient.query<OrganizationContactModel>(QueryOptions(
          document: query.document,
          variables: variables,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          fetchPolicy: FetchPolicy.noCache));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        return const Right(null);
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }
}
