import 'package:core/core.dart';
import 'package:core/graphql/category/category_api.dart';
import 'package:core/graphql/category/category_by_id_api.dart';
import 'package:core/graphql/category/category_by_name_api.dart';
import 'package:core/repositories/graphql_repository.dart';
import 'package:core/utils/failure.dart';
import 'package:core/utils/map_extension.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

abstract class ICategoryRepository {
  Future<Either<Failure, List<CategoryModel>?>> getCategories();
  Future<Either<Failure, List<CategoryModel>?>> getCategoryByName(String name);
  Future<Either<Failure, CategoryModel?>> getCategoryById(String id);
}

class CategoryRepository extends GraphQLRepository
    implements ICategoryRepository {
  CategoryRepository({required String url, required String userUrl}) :
        super(
          url: url,
        userUrl: userUrl
      );

  @override
  Future<Either<Failure, List<CategoryModel>?>> getCategories() async {
    try {
      final query = CategoryQueryQuery();

      final response = await graphqlClient.query<CategoryModel>(QueryOptions(
          document: query.document,
        fetchPolicy: FetchPolicy.cacheAndNetwork,),);

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = CategoryQuery$QueryRoot.fromJson(response.data!);
        return Right(root.category
            .map((e) => CategoryModel.fromJson(e.toJson()))
            .toList());
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<CategoryModel>?>> getCategoryByName(
      String name) async {
    try {
      final query =
          CategoryByNameQuery(variables: CategoryByNameArguments(name: name));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<CategoryModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = CategoryByName$QueryRoot.fromJson(response.data!);
        return Right(root.category
            .map((e) => CategoryModel.fromJson(e.toJson()))
            .toList());
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, CategoryModel?>> getCategoryById(String id) async {
    try {
      final query = CategoryByIdQuery(variables: CategoryByIdArguments(id: id));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<CategoryModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = CategoryById$QueryRoot.fromJson(response.data!);
        if (root.categoryByPk != null) {
          return Right(root.categoryByPk);
        }
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }
}
