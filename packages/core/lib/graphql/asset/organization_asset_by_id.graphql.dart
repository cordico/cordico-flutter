// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'organization_asset_by_id.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class OrganizationAssetById$QueryRoot$OrganizationAssetByPk$Asset
    extends JsonSerializable with EquatableMixin {
  OrganizationAssetById$QueryRoot$OrganizationAssetByPk$Asset();

  factory OrganizationAssetById$QueryRoot$OrganizationAssetByPk$Asset.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationAssetById$QueryRoot$OrganizationAssetByPk$AssetFromJson(
          json);

  @JsonKey(name: 'asset_type_id')
  late String assetTypeId;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'creator_id')
  late String creatorId;

  late dynamic data;

  @JsonKey(name: 'file_name')
  late String fileName;

  String? hash;

  late String id;

  @JsonKey(name: 'mime_type')
  late String mimeType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @JsonKey(name: 'updater_id')
  String? updaterId;

  String? url;

  @override
  List<Object?> get props => [
        assetTypeId,
        createdAt,
        creatorId,
        data,
        fileName,
        hash,
        id,
        mimeType,
        updatedAt,
        updaterId,
        url
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationAssetById$QueryRoot$OrganizationAssetByPk$AssetToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationAssetById$QueryRoot$OrganizationAssetByPk$Organization
    extends JsonSerializable with EquatableMixin {
  OrganizationAssetById$QueryRoot$OrganizationAssetByPk$Organization();

  factory OrganizationAssetById$QueryRoot$OrganizationAssetByPk$Organization.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationAssetById$QueryRoot$OrganizationAssetByPk$OrganizationFromJson(
          json);

  late String name;

  late String id;

  String? description;

  late dynamic data;

  @JsonKey(name: 'organization_type_id')
  late String organizationTypeId;

  @JsonKey(name: 'portal_fqdn')
  String? portalFqdn;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @override
  List<Object?> get props => [
        name,
        id,
        description,
        data,
        organizationTypeId,
        portalFqdn,
        createdAt,
        updatedAt
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationAssetById$QueryRoot$OrganizationAssetByPk$OrganizationToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationAssetById$QueryRoot$OrganizationAssetByPk
    extends JsonSerializable with EquatableMixin {
  OrganizationAssetById$QueryRoot$OrganizationAssetByPk();

  factory OrganizationAssetById$QueryRoot$OrganizationAssetByPk.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationAssetById$QueryRoot$OrganizationAssetByPkFromJson(json);

  late OrganizationAssetById$QueryRoot$OrganizationAssetByPk$Asset asset;

  late OrganizationAssetById$QueryRoot$OrganizationAssetByPk$Organization
      organization;

  @override
  List<Object?> get props => [asset, organization];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationAssetById$QueryRoot$OrganizationAssetByPkToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationAssetById$QueryRoot extends JsonSerializable
    with EquatableMixin {
  OrganizationAssetById$QueryRoot();

  factory OrganizationAssetById$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$OrganizationAssetById$QueryRootFromJson(json);

  @JsonKey(name: 'organization_asset_by_pk')
  OrganizationAssetById$QueryRoot$OrganizationAssetByPk? organizationAssetByPk;

  @override
  List<Object?> get props => [organizationAssetByPk];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationAssetById$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationAssetByIdArguments extends JsonSerializable
    with EquatableMixin {
  OrganizationAssetByIdArguments({required this.id});

  @override
  factory OrganizationAssetByIdArguments.fromJson(Map<String, dynamic> json) =>
      _$OrganizationAssetByIdArgumentsFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$OrganizationAssetByIdArgumentsToJson(this);
}

final ORGANIZATION_ASSET_BY_ID_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'OrganizationAssetById'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'organization_asset_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'id'),
                  value: VariableNode(name: NameNode(value: 'id')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'asset'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'asset_type_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'created_at'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'creator_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'data'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'file_name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'hash'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'mime_type'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'updated_at'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'updater_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'url'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'organization'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'description'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'data'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'organization_type_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'portal_fqdn'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'created_at'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'updated_at'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ]))
            ]))
      ]))
]);

class OrganizationAssetByIdQuery extends GraphQLQuery<
    OrganizationAssetById$QueryRoot, OrganizationAssetByIdArguments> {
  OrganizationAssetByIdQuery({required this.variables});

  @override
  final DocumentNode document = ORGANIZATION_ASSET_BY_ID_QUERY_DOCUMENT;

  @override
  final String operationName = 'OrganizationAssetById';

  @override
  final OrganizationAssetByIdArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  OrganizationAssetById$QueryRoot parse(Map<String, dynamic> json) =>
      OrganizationAssetById$QueryRoot.fromJson(json);
}
