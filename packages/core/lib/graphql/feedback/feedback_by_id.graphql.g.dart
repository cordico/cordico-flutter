// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'feedback_by_id.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedbackById$QueryRoot$FeedbackByPk
    _$FeedbackById$QueryRoot$FeedbackByPkFromJson(Map<String, dynamic> json) {
  return FeedbackById$QueryRoot$FeedbackByPk()
    ..body = json['body'] as String
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..data = json['data']
    ..feedbackTypeId = json['feedback_type_id'] as String
    ..id = json['id'] as String
    ..subject = json['subject'] as String?
    ..updatedAt = DateTime.parse(json['updated_at'] as String);
}

Map<String, dynamic> _$FeedbackById$QueryRoot$FeedbackByPkToJson(
        FeedbackById$QueryRoot$FeedbackByPk instance) =>
    <String, dynamic>{
      'body': instance.body,
      'created_at': instance.createdAt.toIso8601String(),
      'data': instance.data,
      'feedback_type_id': instance.feedbackTypeId,
      'id': instance.id,
      'subject': instance.subject,
      'updated_at': instance.updatedAt.toIso8601String(),
    };

FeedbackById$QueryRoot _$FeedbackById$QueryRootFromJson(
    Map<String, dynamic> json) {
  return FeedbackById$QueryRoot()
    ..feedbackByPk = json['feedback_by_pk'] == null
        ? null
        : FeedbackById$QueryRoot$FeedbackByPk.fromJson(
            json['feedback_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$FeedbackById$QueryRootToJson(
        FeedbackById$QueryRoot instance) =>
    <String, dynamic>{
      'feedback_by_pk': instance.feedbackByPk?.toJson(),
    };

FeedbackByIdArguments _$FeedbackByIdArgumentsFromJson(
    Map<String, dynamic> json) {
  return FeedbackByIdArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$FeedbackByIdArgumentsToJson(
        FeedbackByIdArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
