// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'delete_one_user_mutation.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class DeleteOneUserMutation$MutationRoot$DeleteOneUser extends JsonSerializable
    with EquatableMixin {
  DeleteOneUserMutation$MutationRoot$DeleteOneUser();

  factory DeleteOneUserMutation$MutationRoot$DeleteOneUser.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneUserMutation$MutationRoot$DeleteOneUserFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneUserMutation$MutationRoot$DeleteOneUserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeleteOneUserMutation$MutationRoot extends JsonSerializable
    with EquatableMixin {
  DeleteOneUserMutation$MutationRoot();

  factory DeleteOneUserMutation$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneUserMutation$MutationRootFromJson(json);

  DeleteOneUserMutation$MutationRoot$DeleteOneUser? deleteOneUser;

  @override
  List<Object?> get props => [deleteOneUser];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneUserMutation$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeleteOneUserMutationArguments extends JsonSerializable
    with EquatableMixin {
  DeleteOneUserMutationArguments({this.id});

  @override
  factory DeleteOneUserMutationArguments.fromJson(Map<String, dynamic> json) =>
      _$DeleteOneUserMutationArgumentsFromJson(json);

  final String? id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$DeleteOneUserMutationArgumentsToJson(this);
}

final DELETE_ONE_USER_MUTATION_MUTATION_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'DeleteOneUserMutation'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'deleteOneUser'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'id'),
                        value: VariableNode(name: NameNode(value: 'id')))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class DeleteOneUserMutationMutation extends GraphQLQuery<
    DeleteOneUserMutation$MutationRoot, DeleteOneUserMutationArguments> {
  DeleteOneUserMutationMutation({required this.variables});

  @override
  final DocumentNode document = DELETE_ONE_USER_MUTATION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'DeleteOneUserMutation';

  @override
  final DeleteOneUserMutationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  DeleteOneUserMutation$MutationRoot parse(Map<String, dynamic> json) =>
      DeleteOneUserMutation$MutationRoot.fromJson(json);
}
