// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'create_one_collection.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateOneCollection$MutationRoot$CreateOneCollection
    _$CreateOneCollection$MutationRoot$CreateOneCollectionFromJson(
        Map<String, dynamic> json) {
  return CreateOneCollection$MutationRoot$CreateOneCollection()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$CreateOneCollection$MutationRoot$CreateOneCollectionToJson(
            CreateOneCollection$MutationRoot$CreateOneCollection instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

CreateOneCollection$MutationRoot _$CreateOneCollection$MutationRootFromJson(
    Map<String, dynamic> json) {
  return CreateOneCollection$MutationRoot()
    ..createOneCollection =
        CreateOneCollection$MutationRoot$CreateOneCollection.fromJson(
            json['createOneCollection'] as Map<String, dynamic>);
}

Map<String, dynamic> _$CreateOneCollection$MutationRootToJson(
        CreateOneCollection$MutationRoot instance) =>
    <String, dynamic>{
      'createOneCollection': instance.createOneCollection.toJson(),
    };

CreateOneCollectionArguments _$CreateOneCollectionArgumentsFromJson(
    Map<String, dynamic> json) {
  return CreateOneCollectionArguments(
    collectionTypeId: json['collectionTypeId'] as String,
    isShared: json['isShared'] as bool,
    key: json['key'] as String,
    name: json['name'] as String,
    title: json['title'] as String,
  );
}

Map<String, dynamic> _$CreateOneCollectionArgumentsToJson(
        CreateOneCollectionArguments instance) =>
    <String, dynamic>{
      'collectionTypeId': instance.collectionTypeId,
      'isShared': instance.isShared,
      'key': instance.key,
      'name': instance.name,
      'title': instance.title,
    };
