// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'content_schema.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContentSchema _$ContentSchemaFromJson(Map<String, dynamic> json) {
  $checkKeys(json, disallowNullValues: const ['body_mime_type']);
  return ContentSchema()
    ..url = json['url'] as String?
    ..title = json['title'] as String?
    ..summary = json['summary'] as String?
    ..body = json['body'] as String?
    ..lenght = json['lenght'] as num?
    ..author = json['author'] as String?
    ..type = json['type'] as String?
    ..mediaType = json['media_type'] as String?
    ..isUrgent = json['is_urgent'] as bool?
    ..videoUrl1 = json['video_url'] as String?
    ..thumbnailUrl1 = json['thumbnail_url'] as String?
    ..bodyMimeType = _$enumDecodeNullable(
        _$MimeTypeEnumMap, json['body_mime_type'],
        unknownValue: MimeType.markdown)
    ..link = json['link'] as String?
    ..audioUrl = json['audioUrl'] as String?
    ..videoUrl = json['videoUrl'] as String?
    ..coverUrl = json['coverUrl'] as String?
    ..coverImageUrl = json['cover_image_url'] as String?
    ..answers = (json['answers'] as List<dynamic>?)
        ?.map((e) => AnswerSchema.fromJson(e as Map<String, dynamic>))
        .toList()
    ..questionText = json['question_text'] as String?
    ..questionType = json['question_type'] as String?
    ..imageUrl = json['imageUrl'] as String?
    ..thumbnail = json['thumbnail'] as String?
    ..description = json['description'] as String?
    ..instructionsBody = json['instructions_body'] as String?
    ..icon = json['icon'] as String?
    ..contactPhone = json['contact_phone'] as String?
    ..actionButtons = (json['action_buttons'] as List<dynamic>?)
        ?.map((e) => ActionButtonSchema.fromJson(e as Map<String, dynamic>))
        .toList()
    ..outcomes = (json['outcomes'] as List<dynamic>?)
        ?.map((e) => OutcomeSchema.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$ContentSchemaToJson(ContentSchema instance) {
  final val = <String, dynamic>{
    'url': instance.url,
    'title': instance.title,
    'summary': instance.summary,
    'body': instance.body,
    'lenght': instance.lenght,
    'author': instance.author,
    'type': instance.type,
    'media_type': instance.mediaType,
    'is_urgent': instance.isUrgent,
    'video_url': instance.videoUrl1,
    'thumbnail_url': instance.thumbnailUrl1,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('body_mime_type', _$MimeTypeEnumMap[instance.bodyMimeType]);
  val['link'] = instance.link;
  val['audioUrl'] = instance.audioUrl;
  val['videoUrl'] = instance.videoUrl;
  val['coverUrl'] = instance.coverUrl;
  val['cover_image_url'] = instance.coverImageUrl;
  val['answers'] = instance.answers?.map((e) => e.toJson()).toList();
  val['question_text'] = instance.questionText;
  val['question_type'] = instance.questionType;
  val['imageUrl'] = instance.imageUrl;
  val['thumbnail'] = instance.thumbnail;
  val['description'] = instance.description;
  val['instructions_body'] = instance.instructionsBody;
  val['icon'] = instance.icon;
  val['contact_phone'] = instance.contactPhone;
  val['action_buttons'] =
      instance.actionButtons?.map((e) => e.toJson()).toList();
  val['outcomes'] = instance.outcomes?.map((e) => e.toJson()).toList();
  return val;
}

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

K? _$enumDecodeNullable<K, V>(
  Map<K, V> enumValues,
  dynamic source, {
  K? unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<K, V>(enumValues, source, unknownValue: unknownValue);
}

const _$MimeTypeEnumMap = {
  MimeType.html: 'text/html',
  MimeType.markdown: 'text/markdown',
};

OutcomeSchema _$OutcomeSchemaFromJson(Map<String, dynamic> json) {
  return OutcomeSchema()
    ..scoreKey = json['score_key'] as String?
    ..scoreName = json['score_name'] as String?
    ..scoreRanges = (json['score_ranges'] as List<dynamic>?)
        ?.map((e) => ScoreRangeSchema.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$OutcomeSchemaToJson(OutcomeSchema instance) =>
    <String, dynamic>{
      'score_key': instance.scoreKey,
      'score_name': instance.scoreName,
      'score_ranges': instance.scoreRanges?.map((e) => e.toJson()).toList(),
    };

ScoreRangeSchema _$ScoreRangeSchemaFromJson(Map<String, dynamic> json) {
  return ScoreRangeSchema()
    ..endScore = json['end_score'] as num?
    ..startScore = json['start_score'] as num?
    ..interpretation = json['interpretation'] as String?;
}

Map<String, dynamic> _$ScoreRangeSchemaToJson(ScoreRangeSchema instance) =>
    <String, dynamic>{
      'end_score': instance.endScore,
      'start_score': instance.startScore,
      'interpretation': instance.interpretation,
    };

AnswerSchema _$AnswerSchemaFromJson(Map<String, dynamic> json) {
  return AnswerSchema()
    ..text = json['text'] as String?
    ..index = json['index']
    ..score = json['score']
    ..scaleScores = (json['scale_scores'] as List<dynamic>?)
        ?.map((e) => ScaleScoreAnswerSchema.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$AnswerSchemaToJson(AnswerSchema instance) =>
    <String, dynamic>{
      'text': instance.text,
      'index': instance.index,
      'score': instance.score,
      'scale_scores': instance.scaleScores?.map((e) => e.toJson()).toList(),
    };

ScaleScoreAnswerSchema _$ScaleScoreAnswerSchemaFromJson(
    Map<String, dynamic> json) {
  return ScaleScoreAnswerSchema()
    ..score = json['score'] as num?
    ..scaleKey = json['scale_key'] as String?;
}

Map<String, dynamic> _$ScaleScoreAnswerSchemaToJson(
        ScaleScoreAnswerSchema instance) =>
    <String, dynamic>{
      'score': instance.score,
      'scale_key': instance.scaleKey,
    };

ActionButtonSchema _$ActionButtonSchemaFromJson(Map<String, dynamic> json) {
  return ActionButtonSchema()
    ..label = json['label'] as String?
    ..isDefault = json['default'] as bool?
    ..actionType =
        _$enumDecodeNullable(_$ActionButtonTypeEnumMap, json['action_type'])
    ..displayOrder = json['display_order'] as num?
    ..materialIcon = _$enumDecodeNullable(
        _$ActionButtonMaterialIconEnumMap, json['material_icon'])
    ..actionContext = json['action_context'] as String?
    ..actionContent = json['action_content'] as String?
    ..actionData = json['action_data'] as String?;
}

Map<String, dynamic> _$ActionButtonSchemaToJson(ActionButtonSchema instance) =>
    <String, dynamic>{
      'label': instance.label,
      'default': instance.isDefault,
      'action_type': _$ActionButtonTypeEnumMap[instance.actionType],
      'display_order': instance.displayOrder,
      'material_icon': _$ActionButtonMaterialIconEnumMap[instance.materialIcon],
      'action_context': instance.actionContext,
      'action_content': instance.actionContent,
      'action_data': instance.actionData,
    };

const _$ActionButtonTypeEnumMap = {
  ActionButtonType.call: 'call',
  ActionButtonType.sms: 'sms',
  ActionButtonType.email: 'email',
  ActionButtonType.anonymousCall: 'anonymous_call',
  ActionButtonType.loadContent: 'load_content',
  ActionButtonType.map: 'map',
};

const _$ActionButtonMaterialIconEnumMap = {
  ActionButtonMaterialIcon.call: 'call',
  ActionButtonMaterialIcon.textsms: 'textsms',
  ActionButtonMaterialIcon.mailOnline: 'mail_online',
  ActionButtonMaterialIcon.textSnippet: 'text_snippet',
  ActionButtonMaterialIcon.pictureAsPdf: 'picture_as_pdf',
  ActionButtonMaterialIcon.openInNew: 'open_in_new',
  ActionButtonMaterialIcon.mapOutlined: 'map_outlined',
  ActionButtonMaterialIcon.phoneEnabled: 'phone_enabled',
};
