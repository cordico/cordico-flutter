// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'configuration_query.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConfigurationQuery$QueryRoot$Configuration$ConfigurationType
    _$ConfigurationQuery$QueryRoot$Configuration$ConfigurationTypeFromJson(
        Map<String, dynamic> json) {
  return ConfigurationQuery$QueryRoot$Configuration$ConfigurationType()
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$ConfigurationQuery$QueryRoot$Configuration$ConfigurationTypeToJson(
            ConfigurationQuery$QueryRoot$Configuration$ConfigurationType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
        };

ConfigurationQuery$QueryRoot$Configuration
    _$ConfigurationQuery$QueryRoot$ConfigurationFromJson(
        Map<String, dynamic> json) {
  return ConfigurationQuery$QueryRoot$Configuration()
    ..name = json['name'] as String
    ..lastUpdateDate = json['last_update_date'] == null
        ? null
        : DateTime.parse(json['last_update_date'] as String)
    ..key = json['key'] as String
    ..id = json['id'] as String
    ..data = json['data']
    ..createDate = DateTime.parse(json['create_date'] as String)
    ..configurationTypeId = json['configuration_type_id'] as String
    ..configurationType =
        ConfigurationQuery$QueryRoot$Configuration$ConfigurationType.fromJson(
            json['configuration_type'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ConfigurationQuery$QueryRoot$ConfigurationToJson(
        ConfigurationQuery$QueryRoot$Configuration instance) =>
    <String, dynamic>{
      'name': instance.name,
      'last_update_date': instance.lastUpdateDate?.toIso8601String(),
      'key': instance.key,
      'id': instance.id,
      'data': instance.data,
      'create_date': instance.createDate.toIso8601String(),
      'configuration_type_id': instance.configurationTypeId,
      'configuration_type': instance.configurationType.toJson(),
    };

ConfigurationQuery$QueryRoot _$ConfigurationQuery$QueryRootFromJson(
    Map<String, dynamic> json) {
  return ConfigurationQuery$QueryRoot()
    ..configuration = (json['configuration'] as List<dynamic>)
        .map((e) => ConfigurationQuery$QueryRoot$Configuration.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$ConfigurationQuery$QueryRootToJson(
        ConfigurationQuery$QueryRoot instance) =>
    <String, dynamic>{
      'configuration': instance.configuration.map((e) => e.toJson()).toList(),
    };
