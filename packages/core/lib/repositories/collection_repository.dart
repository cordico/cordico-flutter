// ignore_for_file: lines_longer_than_80_chars

import 'package:core/core.dart';
import 'package:core/graphql/collection/collection_by_collection_type.dart';
import 'package:core/graphql/collection/collection_by_id.dart';
import 'package:core/graphql/collection/collection_by_organization.dart';
import 'package:core/graphql/collection/collection_query.dart';
import 'package:core/graphql/collection/collection_type_by_id.dart';
import 'package:core/graphql/collection/collection_type_query.dart';
import 'package:core/repositories/graphql_repository.dart';
import 'package:core/utils/failure.dart';
import 'package:core/utils/map_extension.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

abstract class ICollectionRepository {
  Future<Either<Failure, CollectionModel?>> getCollectionById(String id,
      {List<String>? contentStateNames, FetchPolicy? fetchPolicy});
  Future<Either<Failure, List<CollectionModel>?>> listCollections(
      {CollectionQueryArguments? arguments,});

  Future<Either<Failure, List<CollectionModel>?>> getCollectionByOrganization(
      String id,
      {List<String>? contentStateNames});
  Future<Either<Failure, List<CollectionModel>?>>
      getCollectionByCollectionTypeKey(String collectionTypeKey,
          {List<String>? contentStateNames, List<bool>? isShared});

  //TYPES

  Future<Either<Failure, CollectionTypeModel?>> getCollectionTypeById(String id,
      {FetchPolicy? fetchPolicy});
  Future<Either<Failure, List<CollectionTypeModel>?>> listCollectionTypes();

  //COLLECTION CRUD
  Future<Either<Failure, CollectionModel?>> createOneCollection(
      {required CreateOneCollectionArguments arguments,
      String? authorizationToken});
  Future<Either<Failure, CollectionModel?>> updateOneCollection(
      {required UpdateOneCollectionArguments arguments,
      String? authorizationToken});
  Future<Either<Failure, CollectionModel?>> deleteOneCollection(
      {required DeleteOneCollectionArguments arguments,
      String? authorizationToken});
}

class CollectionRepository extends GraphQLRepository
    implements ICollectionRepository {
  CollectionRepository({required String url, required String userUrl,
    String? userToken,})
      : super(url: url, userUrl: userUrl, token: userToken,);

  @override
  Future<Either<Failure, CollectionModel?>> getCollectionById(String id,
      {List<String>? contentStateNames, FetchPolicy? fetchPolicy}) async {
    try {
      final query = CollectionByIdQuery(
          variables: CollectionByIdArguments(
              id: id, contentStateNames: contentStateNames));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<CollectionModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: fetchPolicy ?? FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = CollectionById$QueryRoot.fromJson(response.data!);
        if (root.collectionByPk != null) {
          return Right(root.collectionByPk);
        }
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<CollectionModel>?>> listCollections(
      {CollectionQueryArguments? arguments}) async {
    try {
      final query = CollectionQueryQuery(
          variables: arguments ?? CollectionQueryArguments());

      final response = await graphqlClient.query<CollectionModel>(QueryOptions(
          document: query.document, fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = CollectionQuery$QueryRoot.fromJson(response.data!);
        return Right(root.collection
            .map((e) => CollectionModel.fromJson(e.toJson()))
            .toList());
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<CollectionModel>?>> getCollectionByOrganization(
      String id,
      {List<String>? contentStateNames}) async {
    try {
      final query = CollectionByOrganizationQuery(
          variables: CollectionByOrganizationArguments(
              organizationID: id, contentStateNames: contentStateNames));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<CollectionModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root =
            CollectionByOrganization$QueryRoot.fromJson(response.data!);
        return Right(root.collection
            .map((e) => CollectionModel.fromJson(e.toJson()))
            .toList());
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<CollectionModel>?>>
      getCollectionByCollectionTypeKey(String collectionTypeKey,
          {List<String>? contentStateNames, List<bool>? isShared}) async {
    try {
      final query = CollectionByCollectionTypeKeyQuery(
          variables: CollectionByCollectionTypeKeyArguments(
              collectionTypeKey: collectionTypeKey,
              contentStateNames: contentStateNames,
              isShared: isShared));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<CollectionModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root =
            CollectionByOrganization$QueryRoot.fromJson(response.data!);
        return Right(root.collection
            .map((e) => CollectionModel.fromJson(e.toJson()))
            .toList());
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, CollectionTypeModel?>> getCollectionTypeById(String id,
      {FetchPolicy? fetchPolicy}) async {
    try {
      final query = CollectionTypeByIdQuery(
          variables: CollectionTypeByIdArguments(id: id));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<CollectionTypeModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = CollectionTypeById$QueryRoot.fromJson(response.data!);
        if (root.collectionTypeByPk != null) {
          return Right(root.collectionTypeByPk);
        }
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<CollectionTypeModel>?>>
      listCollectionTypes() async {
    try {
      final listContactType = CollectionTypeQueryQuery();

      final response = await graphqlClient.query<CollectionTypeModel>(QueryOptions(
          document: listContactType.document,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = CollectionTypeQuery$QueryRoot.fromJson(response.data!);
        return Right(root.collectionType
            .map((e) => CollectionTypeModel.fromJson(e.toJson()))
            .toList());
      }

      return Left(EmptyFailure());
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  //COLLECTION CRUD
  @override
  Future<Either<Failure, CollectionModel?>> createOneCollection(
      {required CreateOneCollectionArguments arguments,
      String? authorizationToken}) async {
    try {
      final query = CreateOneCollectionMutation(variables: arguments);

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.mutate<CollectionModel>(MutationOptions(
          document: query.document,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          variables: variables,
          fetchPolicy: FetchPolicy.noCache));
      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }
      if (response.data != null) {
        final root = CreateOneCollection$MutationRoot.fromJson(response.data!);
        final element = await getCollectionById(root.createOneCollection.id,
            fetchPolicy: FetchPolicy.noCache);
        return element;
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, CollectionModel?>> updateOneCollection(
      {required UpdateOneCollectionArguments arguments,
      String? authorizationToken}) async {
    try {
      final query = UpdateOneCollectionMutation(variables: arguments);

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.mutate<CollectionModel>(MutationOptions(
          document: query.document,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          variables: variables,
          fetchPolicy: FetchPolicy.noCache));
      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }
      if (response.data != null) {
        final root = UpdateOneCollection$MutationRoot.fromJson(response.data!);
        if (root.updateOneCollection != null) {
          final element = await getCollectionById(root.updateOneCollection!.id,
              fetchPolicy: FetchPolicy.noCache);
          return element;
        }
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, CollectionModel?>> deleteOneCollection(
      {required DeleteOneCollectionArguments arguments,
      String? authorizationToken}) async {
    try {
      final query = DeleteOneCollectionMutation(variables: arguments);

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.mutate<CollectionModel>(MutationOptions(
          document: query.document,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          variables: variables,
          fetchPolicy: FetchPolicy.noCache));
      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }
      if (response.data != null) {
        //final root = DeleteOneCollection$MutationRoot.fromJson(response.data!);
        return const Right(null);
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }
}
