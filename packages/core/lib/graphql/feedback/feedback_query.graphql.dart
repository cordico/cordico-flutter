// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'feedback_query.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class FeedbackQuery$QueryRoot$Feedback extends JsonSerializable
    with EquatableMixin {
  FeedbackQuery$QueryRoot$Feedback();

  factory FeedbackQuery$QueryRoot$Feedback.fromJson(
          Map<String, dynamic> json) =>
      _$FeedbackQuery$QueryRoot$FeedbackFromJson(json);

  late String body;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  dynamic? data;

  @JsonKey(name: 'feedback_type_id')
  late String feedbackTypeId;

  late String id;

  String? subject;

  @JsonKey(name: 'updated_at')
  late DateTime updatedAt;

  @override
  List<Object?> get props =>
      [body, createdAt, data, feedbackTypeId, id, subject, updatedAt];
  @override
  Map<String, dynamic> toJson() =>
      _$FeedbackQuery$QueryRoot$FeedbackToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FeedbackQuery$QueryRoot extends JsonSerializable with EquatableMixin {
  FeedbackQuery$QueryRoot();

  factory FeedbackQuery$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$FeedbackQuery$QueryRootFromJson(json);

  late List<FeedbackQuery$QueryRoot$Feedback> feedback;

  @override
  List<Object?> get props => [feedback];
  @override
  Map<String, dynamic> toJson() => _$FeedbackQuery$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FeedbackQueryArguments extends JsonSerializable with EquatableMixin {
  FeedbackQueryArguments({this.limit, this.offset, this.feedbackTypeIds});

  @override
  factory FeedbackQueryArguments.fromJson(Map<String, dynamic> json) =>
      _$FeedbackQueryArgumentsFromJson(json);

  final int? limit;

  final int? offset;

  final List<String>? feedbackTypeIds;

  @override
  List<Object?> get props => [limit, offset, feedbackTypeIds];
  @override
  Map<String, dynamic> toJson() => _$FeedbackQueryArgumentsToJson(this);
}

final FEEDBACK_QUERY_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'FeedbackQuery'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'limit')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '10000')),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'offset')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '0')),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'feedbackTypeIds')),
            type: ListTypeNode(
                type: NamedTypeNode(
                    name: NameNode(value: 'uuid'), isNonNull: true),
                isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'feedback'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'limit'),
                  value: VariableNode(name: NameNode(value: 'limit'))),
              ArgumentNode(
                  name: NameNode(value: 'offset'),
                  value: VariableNode(name: NameNode(value: 'offset'))),
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'feedback_type_id'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: '_in'),
                              value: VariableNode(
                                  name: NameNode(value: 'feedbackTypeIds')))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'body'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'feedback_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'subject'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class FeedbackQueryQuery
    extends GraphQLQuery<FeedbackQuery$QueryRoot, FeedbackQueryArguments> {
  FeedbackQueryQuery({required this.variables});

  @override
  final DocumentNode document = FEEDBACK_QUERY_QUERY_DOCUMENT;

  @override
  final String operationName = 'FeedbackQuery';

  @override
  final FeedbackQueryArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  FeedbackQuery$QueryRoot parse(Map<String, dynamic> json) =>
      FeedbackQuery$QueryRoot.fromJson(json);
}
