// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'delete_one_organization_contact_mutation.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class DeleteOneOrganizationContactMutation$MutationRoot$DeleteOneOrganizationContact
    extends JsonSerializable with EquatableMixin {
  DeleteOneOrganizationContactMutation$MutationRoot$DeleteOneOrganizationContact();

  factory DeleteOneOrganizationContactMutation$MutationRoot$DeleteOneOrganizationContact.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneOrganizationContactMutation$MutationRoot$DeleteOneOrganizationContactFromJson(
          json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneOrganizationContactMutation$MutationRoot$DeleteOneOrganizationContactToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class DeleteOneOrganizationContactMutation$MutationRoot extends JsonSerializable
    with EquatableMixin {
  DeleteOneOrganizationContactMutation$MutationRoot();

  factory DeleteOneOrganizationContactMutation$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneOrganizationContactMutation$MutationRootFromJson(json);

  DeleteOneOrganizationContactMutation$MutationRoot$DeleteOneOrganizationContact?
      deleteOneOrganizationContact;

  @override
  List<Object?> get props => [deleteOneOrganizationContact];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneOrganizationContactMutation$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeleteOneOrganizationContactMutationArguments extends JsonSerializable
    with EquatableMixin {
  DeleteOneOrganizationContactMutationArguments({this.id});

  @override
  factory DeleteOneOrganizationContactMutationArguments.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneOrganizationContactMutationArgumentsFromJson(json);

  final String? id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneOrganizationContactMutationArgumentsToJson(this);
}

final DELETE_ONE_ORGANIZATION_CONTACT_MUTATION_MUTATION_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'DeleteOneOrganizationContactMutation'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'deleteOneOrganizationContact'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'id'),
                        value: VariableNode(name: NameNode(value: 'id')))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class DeleteOneOrganizationContactMutationMutation extends GraphQLQuery<
    DeleteOneOrganizationContactMutation$MutationRoot,
    DeleteOneOrganizationContactMutationArguments> {
  DeleteOneOrganizationContactMutationMutation({required this.variables});

  @override
  final DocumentNode document =
      DELETE_ONE_ORGANIZATION_CONTACT_MUTATION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'DeleteOneOrganizationContactMutation';

  @override
  final DeleteOneOrganizationContactMutationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  DeleteOneOrganizationContactMutation$MutationRoot parse(
          Map<String, dynamic> json) =>
      DeleteOneOrganizationContactMutation$MutationRoot.fromJson(json);
}
