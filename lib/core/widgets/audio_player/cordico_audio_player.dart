import 'dart:async';

import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/cordico_linear_progress_bar.dart';
import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';

class CordicoAudioPlayer extends StatefulWidget {
  const CordicoAudioPlayer({
    Key? key,
    required this.url,
  }) : super(key: key);

  final String url;

  @override
  State<StatefulWidget> createState() => CordicoAudioPlayerState();
}

class CordicoAudioPlayerState extends State<CordicoAudioPlayer> {
  late AudioPlayer _player;
  late ValueNotifier<double> _progressNotifier;
  late ValueNotifier<int> _currentPositionNotifier;
  late ValueNotifier<bool> _isPlayingNotifier;
  late ValueNotifier<bool> _isMutedNotifier;
  late ValueNotifier<int> _totalDurationNotifier;
  StreamSubscription? _stateStreamSubscription;
  StreamSubscription? _positionSubscription;
  StreamSubscription? _durationSubscription;

  @override
  void initState() {
    super.initState();
    _progressNotifier = ValueNotifier<double>(0);
    _currentPositionNotifier = ValueNotifier<int>(0);
    _totalDurationNotifier = ValueNotifier<int>(0);
    _isPlayingNotifier = ValueNotifier<bool>(false);
    _isMutedNotifier = ValueNotifier<bool>(false);
    _player = AudioPlayer();
    prepareAudioPlayer();
  }

  Future<void> prepareAudioPlayer() async {
    try {
      await _player.setUrl(widget.url);
    } on PlayerException catch (_) {
      // iOS/macOS: maps to NSError.code
      // Android: maps to ExoPlayerException.type
      // Web: maps to MediaError.code
      //print("Error code: ${e.code}");
      // iOS/macOS: maps to NSError.localizedDescription
      // Android: maps to ExoPlaybackException.getMessage()
      // Web: a generic message
      //print("Error message: ${e.message}");
    } on PlayerInterruptedException catch (_) {
      // This call was interrupted since another audio source was loaded or the
      // player was stopped or disposed before this audio source could complete
      // loading.
      //print("Connection aborted: ${e.message}");
    } catch (e) {
      // Fallback for all errors
      //print(e);
    }

    _stateStreamSubscription = _player.playerStateStream.listen((playerState) {
      _isPlayingNotifier.value = playerState.playing;

      if (playerState.processingState == ProcessingState.completed) {
        _player.pause();
        // ignore: cascade_invocations
        _player.seek(Duration.zero);
      }
    });

    _positionSubscription = _player.positionStream.listen((position) {
      if (_totalDurationNotifier.value == 0) {
        _progressNotifier.value = 0;
      } else {
        _progressNotifier.value =
            position.inSeconds.toDouble() / _totalDurationNotifier.value;
      }

      _currentPositionNotifier.value = position.inSeconds;
    });

    _durationSubscription = _player.durationStream.listen((totalDuration) {
      _totalDurationNotifier.value = (totalDuration ?? Duration.zero).inSeconds;
    });
  }

  @override
  void dispose() {
    _positionSubscription?.cancel();
    _durationSubscription?.cancel();
    _stateStreamSubscription?.cancel();
    _isPlayingNotifier.dispose();
    _isMutedNotifier.dispose();
    _progressNotifier.dispose();
    _player.dispose();
    super.dispose();
  }

  void _togglePlaying() {
    if (_isPlayingNotifier.value) {
      _player.pause();
    } else {
      _player.play();
    }
  }

  void _toggleVolume() {
    if (_isMutedNotifier.value) {
      _player.setVolume(1);
      _isMutedNotifier.value = false;
    } else {
      _player.setVolume(0);
      _isMutedNotifier.value = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ValueListenableBuilder<double>(
          valueListenable: _progressNotifier,
          builder: (_, value, __) {
            return CordicoLinearProgressBar(
              height: 6,
              progress: value,
              progressColor: ThemeColors.blue.shade700,
              secondaryProgressColor: ThemeColors.blue.shade100,
            );
          },
        ),
        Container(
          height: 8,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(vertical: 1, horizontal: 6),
              decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.6),
                borderRadius: BorderRadius.circular(3),
              ),
              child: ValueListenableBuilder<int>(
                valueListenable: _currentPositionNotifier,
                builder: (context, value, __) {
                  return _buildTimeLabel(context, value);
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 1, horizontal: 6),
              decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.6),
                borderRadius: BorderRadius.circular(3),
              ),
              child: ValueListenableBuilder<int>(
                valueListenable: _totalDurationNotifier,
                builder: (context, value, __) {
                  return _buildTimeLabel(context, value);
                },
              ),
            )
          ],
        ),
        Container(
          height: 20,
        ),
        Align(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              //required to center play button (mini fab width + 16)
              Container(
                width: 56,
              ),
              ValueListenableBuilder<bool>(
                valueListenable: _isPlayingNotifier,
                builder: (_, value, __) {
                  return FloatingActionButton(
                    heroTag: 'cordico-audio',
                    backgroundColor: ThemeColors.blue.shade700,
                    foregroundColor: Colors.white,
                    onPressed: _togglePlaying,
                    child: value
                        ? const Icon(
                            Icons.pause,
                            size: 28,
                            color: Colors.white,
                          )
                        : const Icon(
                            Icons.play_arrow,
                            size: 28,
                            color: Colors.white,
                          ),
                  );
                },
              ),
              Container(
                width: 16,
              ),
              ValueListenableBuilder<bool>(
                valueListenable: _isMutedNotifier,
                builder: (_, value, __) {
                  return FloatingActionButton(
                    heroTag: 'cordico-mute',
                    mini: true,
                    backgroundColor: ThemeColors.blue.shade50,
                    foregroundColor: ThemeColors.blue.shade700,
                    onPressed: _toggleVolume,
                    child: value
                        ? Icon(
                            Icons.volume_off,
                            color: ThemeColors.blue.shade700,
                          )
                        : Icon(
                            Icons.volume_up,
                            color: ThemeColors.blue.shade700,
                          ),
                  );
                },
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildTimeLabel(BuildContext context, int timeValueInSeconds) {
    final seconds = timeValueInSeconds % 60;
    final minutes = timeValueInSeconds ~/ 60 % 60;
    final hours = timeValueInSeconds ~/ 3600;
    var totalTime = '';
    if (hours > 0) {
      totalTime = '$hours:';
    }

    if (minutes < 10) {
      totalTime += '0$minutes:';
    } else {
      totalTime += '$minutes:';
    }

    if (seconds < 10) {
      totalTime += '0$seconds';
    } else {
      totalTime += '$seconds';
    }

    return Text(
      totalTime,
      style: ThemeStyles.textFieldErrorTextStyle(context)?.copyWith(
        color: Colors.white,
      ),
    );
  }
}
