import 'package:cordico_mobile/core/managers/cordico_alert.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/images.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/badge/new_badge.dart';
import 'package:cordico_mobile/core/widgets/buttons/cordico_outlined_button.dart';
import 'package:cordico_mobile/core/widgets/card/cordico_card.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/models/schema/content_schema.dart';
import 'package:core/utils/content_utils.dart';
import 'package:core/utils/map_extension.dart';
import 'package:flutter/material.dart';

class ExternalLinkCard extends StatefulWidget {
  const ExternalLinkCard({
    Key? key,
    required this.content,
    this.isLast = false,
  }) : super(key: key);

  final bool isLast;
  final ContentQuery$QueryRoot$Content content;

  @override
  State<ExternalLinkCard> createState() => _ExternalLinkCardState();
}

class _ExternalLinkCardState extends State<ExternalLinkCard> {
  late ContentQuery$QueryRoot$Content content;
  late ContentSchema contentSchema;

  @override
  void initState() {
    super.initState();

    content = widget.content;

    removeNullAndEmptyParams(content.data as Map<String, dynamic>);

    contentSchema =
        ContentSchema.fromJson(content.data as Map<String, dynamic>);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: widget.isLast ? 0 : 10),
      child: CordicoCard(
        cornerRadius: 0,
        color: ThemeColors.gray.shade900,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 18),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              _buildCategoryHeader(context),
              _buildTitle(context),
              if (contentSchema.description != null) _buildDescription(context),
              _buildFooter(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCategoryHeader(BuildContext context) {
    final isNew = ContentUtils.evaluateIsNew(content);
    return Row(
      children: [
        Image.asset(
          Images.icLink,
          height: 12,
        ),
        Container(width: 8),
        Flexible(
          child: Text(
            'External Link'.toUpperCase(),
            style: ThemeStyles.cardCategory(context),
          ),
        ),
        if (isNew)
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: NewBadge(),
          ),
        /*if (isUrgent)
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: UrgentBadge(),
          ),*/
      ],
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Text(
        content.name,
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
        style: ThemeStyles.bulletinCardTitle(context),
      ),
    );
  }

  Widget _buildDescription(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(top: 4),
        child: Text(
          contentSchema.description ?? '',
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          style: ThemeStyles.contentDescription(context)?.copyWith(
            color: ThemeColors.gray.shade200,
          ),
        ));
  }

  Widget _buildFooter(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: CordicoOutlinedButton(
        isFullyRounded: false,
        borderColor: Colors.white,
        text: 'Visit Link',
        onPressed: () {
          CordicoAlert.showExternalLinkAlert(context,
              url: contentSchema.url ?? '');
        },
        textStyle:
            ThemeStyles.fieldLabel(context)?.copyWith(color: Colors.white),
      ),
    );
  }
}
