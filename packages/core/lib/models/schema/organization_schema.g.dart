// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'organization_schema.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrganizationSchema _$OrganizationSchemaFromJson(Map<String, dynamic> json) {
  return OrganizationSchema()
    ..zip = json['zip'] as String?
    ..city = json['city'] as String?
    ..state = json['state'] as String?
    ..address = json['address'] as String?
    ..latitude = json['latitude'] as num?
    ..longitude = json['longitude'] as num?
    ..name = json['name'] as String?
    ..description = json['description'] as String?
    ..phone = json['phone'] as String?
    ..imageUrl = json['image_url'] as String?
    ..longName = json['long_name'] as String?
    ..coverImageUrl = json['cover_image_url'] as String?;
}

Map<String, dynamic> _$OrganizationSchemaToJson(OrganizationSchema instance) =>
    <String, dynamic>{
      'zip': instance.zip,
      'city': instance.city,
      'state': instance.state,
      'address': instance.address,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'name': instance.name,
      'description': instance.description,
      'phone': instance.phone,
      'image_url': instance.imageUrl,
      'long_name': instance.longName,
      'cover_image_url': instance.coverImageUrl,
    };
