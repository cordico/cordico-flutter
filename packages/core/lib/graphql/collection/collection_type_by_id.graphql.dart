// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'collection_type_by_id.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CollectionTypeById$QueryRoot$CollectionTypeByPk extends JsonSerializable
    with EquatableMixin {
  CollectionTypeById$QueryRoot$CollectionTypeByPk();

  factory CollectionTypeById$QueryRoot$CollectionTypeByPk.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionTypeById$QueryRoot$CollectionTypeByPkFromJson(json);

  @JsonKey(name: 'updater_id')
  String? updaterId;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  dynamic? schema;

  late String name;

  late String key;

  late String id;

  String? description;

  @JsonKey(name: 'creator_id')
  String? creatorId;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @override
  List<Object?> get props => [
        updaterId,
        updatedAt,
        schema,
        name,
        key,
        id,
        description,
        creatorId,
        createdAt
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionTypeById$QueryRoot$CollectionTypeByPkToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CollectionTypeById$QueryRoot extends JsonSerializable
    with EquatableMixin {
  CollectionTypeById$QueryRoot();

  factory CollectionTypeById$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$CollectionTypeById$QueryRootFromJson(json);

  @JsonKey(name: 'collection_type_by_pk')
  CollectionTypeById$QueryRoot$CollectionTypeByPk? collectionTypeByPk;

  @override
  List<Object?> get props => [collectionTypeByPk];
  @override
  Map<String, dynamic> toJson() => _$CollectionTypeById$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CollectionTypeByIdArguments extends JsonSerializable with EquatableMixin {
  CollectionTypeByIdArguments({required this.id});

  @override
  factory CollectionTypeByIdArguments.fromJson(Map<String, dynamic> json) =>
      _$CollectionTypeByIdArgumentsFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$CollectionTypeByIdArgumentsToJson(this);
}

final COLLECTION_TYPE_BY_ID_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'CollectionTypeById'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'collection_type_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'id'),
                  value: VariableNode(name: NameNode(value: 'id')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'updater_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'description'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'creator_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class CollectionTypeByIdQuery extends GraphQLQuery<CollectionTypeById$QueryRoot,
    CollectionTypeByIdArguments> {
  CollectionTypeByIdQuery({required this.variables});

  @override
  final DocumentNode document = COLLECTION_TYPE_BY_ID_QUERY_DOCUMENT;

  @override
  final String operationName = 'CollectionTypeById';

  @override
  final CollectionTypeByIdArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  CollectionTypeById$QueryRoot parse(Map<String, dynamic> json) =>
      CollectionTypeById$QueryRoot.fromJson(json);
}
