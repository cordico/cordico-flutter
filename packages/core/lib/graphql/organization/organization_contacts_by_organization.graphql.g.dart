// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'organization_contacts_by_organization.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$ContactType
    _$OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$ContactTypeFromJson(
        Map<String, dynamic> json) {
  return OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$ContactType()
    ..displayName = json['display_name'] as String
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$ContactTypeToJson(
            OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$ContactType
                instance) =>
        <String, dynamic>{
          'display_name': instance.displayName,
          'id': instance.id,
          'name': instance.name,
        };

OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$Organization
    _$OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$OrganizationFromJson(
        Map<String, dynamic> json) {
  return OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$Organization()
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$OrganizationToJson(
            OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$Organization
                instance) =>
        <String, dynamic>{
          'name': instance.name,
        };

OrganizationContactsByOrganization$QueryRoot$OrganizationContacts
    _$OrganizationContactsByOrganization$QueryRoot$OrganizationContactsFromJson(
        Map<String, dynamic> json) {
  return OrganizationContactsByOrganization$QueryRoot$OrganizationContacts()
    ..data = json['data']
    ..title = json['title'] as String
    ..contactTypeId = json['contact_type_id'] as String
    ..contactType =
        OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$ContactType
            .fromJson(json['contact_type'] as Map<String, dynamic>)
    ..organizationId = json['organization_id'] as String
    ..organization =
        OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$Organization
            .fromJson(json['organization'] as Map<String, dynamic>)
    ..name = json['name'] as String
    ..isActive = json['is_active'] as bool
    ..id = json['id'] as String
    ..updated = json['updated'] == null
        ? null
        : DateTime.parse(json['updated'] as String)
    ..created = DateTime.parse(json['created'] as String);
}

Map<String, dynamic>
    _$OrganizationContactsByOrganization$QueryRoot$OrganizationContactsToJson(
            OrganizationContactsByOrganization$QueryRoot$OrganizationContacts
                instance) =>
        <String, dynamic>{
          'data': instance.data,
          'title': instance.title,
          'contact_type_id': instance.contactTypeId,
          'contact_type': instance.contactType.toJson(),
          'organization_id': instance.organizationId,
          'organization': instance.organization.toJson(),
          'name': instance.name,
          'is_active': instance.isActive,
          'id': instance.id,
          'updated': instance.updated?.toIso8601String(),
          'created': instance.created.toIso8601String(),
        };

OrganizationContactsByOrganization$QueryRoot
    _$OrganizationContactsByOrganization$QueryRootFromJson(
        Map<String, dynamic> json) {
  return OrganizationContactsByOrganization$QueryRoot()
    ..organizationContacts = (json['organization_contacts'] as List<dynamic>)
        .map((e) =>
            OrganizationContactsByOrganization$QueryRoot$OrganizationContacts
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$OrganizationContactsByOrganization$QueryRootToJson(
        OrganizationContactsByOrganization$QueryRoot instance) =>
    <String, dynamic>{
      'organization_contacts':
          instance.organizationContacts.map((e) => e.toJson()).toList(),
    };

OrganizationContactsByOrganizationArguments
    _$OrganizationContactsByOrganizationArgumentsFromJson(
        Map<String, dynamic> json) {
  return OrganizationContactsByOrganizationArguments(
    organizationID: json['organizationID'] as String,
    isActive:
        (json['isActive'] as List<dynamic>?)?.map((e) => e as bool).toList(),
  );
}

Map<String, dynamic> _$OrganizationContactsByOrganizationArgumentsToJson(
        OrganizationContactsByOrganizationArguments instance) =>
    <String, dynamic>{
      'organizationID': instance.organizationID,
      'isActive': instance.isActive,
    };
