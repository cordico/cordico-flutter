// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'delete_one_feedback_type_mutation.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeleteOneFeedbackTypeMutation$MutationRoot$DeleteFeedbackTypeByPk
    _$DeleteOneFeedbackTypeMutation$MutationRoot$DeleteFeedbackTypeByPkFromJson(
        Map<String, dynamic> json) {
  return DeleteOneFeedbackTypeMutation$MutationRoot$DeleteFeedbackTypeByPk()
    ..id = json['id'] as String
    ..key = json['key'] as String
    ..name = json['name'] as String
    ..schema = json['schema'];
}

Map<String, dynamic>
    _$DeleteOneFeedbackTypeMutation$MutationRoot$DeleteFeedbackTypeByPkToJson(
            DeleteOneFeedbackTypeMutation$MutationRoot$DeleteFeedbackTypeByPk
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'key': instance.key,
          'name': instance.name,
          'schema': instance.schema,
        };

DeleteOneFeedbackTypeMutation$MutationRoot
    _$DeleteOneFeedbackTypeMutation$MutationRootFromJson(
        Map<String, dynamic> json) {
  return DeleteOneFeedbackTypeMutation$MutationRoot()
    ..deleteFeedbackTypeByPk = json['delete_feedback_type_by_pk'] == null
        ? null
        : DeleteOneFeedbackTypeMutation$MutationRoot$DeleteFeedbackTypeByPk
            .fromJson(
                json['delete_feedback_type_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DeleteOneFeedbackTypeMutation$MutationRootToJson(
        DeleteOneFeedbackTypeMutation$MutationRoot instance) =>
    <String, dynamic>{
      'delete_feedback_type_by_pk': instance.deleteFeedbackTypeByPk?.toJson(),
    };

DeleteOneFeedbackTypeMutationArguments
    _$DeleteOneFeedbackTypeMutationArgumentsFromJson(
        Map<String, dynamic> json) {
  return DeleteOneFeedbackTypeMutationArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$DeleteOneFeedbackTypeMutationArgumentsToJson(
        DeleteOneFeedbackTypeMutationArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
