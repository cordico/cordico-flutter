// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_schema.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserSchema _$UserSchemaFromJson(Map<String, dynamic> json) {
  return UserSchema()
    ..email = json['email'] as String?
    ..id = json['id'] as String?
    ..identifier = json['identifier'] as String?
    ..sessionToken = json['sessionToken'] as String?
    ..userTypeId = json['userTypeId'] as String?
    ..roles = (json['roles'] as List<dynamic>?)
        ?.map((e) => _$enumDecode(_$UserSchemaRoleEnumMap, e))
        .toList()
    ..lastName = json['last_name'] as String?
    ..firstName = json['first_name'] as String?
    ..organizationId = json['organization_id'] as String?
    ..organizationIds = (json['organization_ids'] as List<dynamic>?)
        ?.map((e) => e as String)
        .toList()
    ..connectedOrganizations =
        (json['connected_organizations'] as List<dynamic>?)
            ?.map((e) => UserSchemaConnectedOrganizations.fromJson(
                e as Map<String, dynamic>))
            .toList()
    ..lidIdentifier = json['lid_identifier'] as String?
    ..credentials = json['credentials'] == null
        ? null
        : UserSchemaCredentials.fromJson(
            json['credentials'] as Map<String, dynamic>)
    ..organization = json['organization'] == null
        ? null
        : UserSchemaOrganization.fromJson(
            json['organization'] as Map<String, dynamic>)
    ..name = json['name'];
}

Map<String, dynamic> _$UserSchemaToJson(UserSchema instance) =>
    <String, dynamic>{
      'email': instance.email,
      'id': instance.id,
      'identifier': instance.identifier,
      'sessionToken': instance.sessionToken,
      'userTypeId': instance.userTypeId,
      'roles': instance.roles?.map((e) => _$UserSchemaRoleEnumMap[e]).toList(),
      'last_name': instance.lastName,
      'first_name': instance.firstName,
      'organization_id': instance.organizationId,
      'organization_ids': instance.organizationIds,
      'connected_organizations':
          instance.connectedOrganizations?.map((e) => e.toJson()).toList(),
      'lid_identifier': instance.lidIdentifier,
      'credentials': instance.credentials?.toJson(),
      'organization': instance.organization?.toJson(),
      'name': instance.name,
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

const _$UserSchemaRoleEnumMap = {
  UserSchemaRole.admin: 'admin',
  UserSchemaRole.editor: 'editor',
  UserSchemaRole.user: 'user',
};

UserSchemaNameSchema _$UserSchemaNameSchemaFromJson(Map<String, dynamic> json) {
  return UserSchemaNameSchema()
    ..first = json['first'] as String?
    ..last = json['last'] as String?;
}

Map<String, dynamic> _$UserSchemaNameSchemaToJson(
        UserSchemaNameSchema instance) =>
    <String, dynamic>{
      'first': instance.first,
      'last': instance.last,
    };

UserSchemaCredentials _$UserSchemaCredentialsFromJson(
    Map<String, dynamic> json) {
  return UserSchemaCredentials()
    ..identifier = json['identifier'] as String?
    ..password = json['password'] as String?;
}

Map<String, dynamic> _$UserSchemaCredentialsToJson(
        UserSchemaCredentials instance) =>
    <String, dynamic>{
      'identifier': instance.identifier,
      'password': instance.password,
    };

UserSchemaOrganization _$UserSchemaOrganizationFromJson(
    Map<String, dynamic> json) {
  return UserSchemaOrganization()
    ..data = json['data'] == null
        ? null
        : UserSchemaOrganizationData.fromJson(
            json['data'] as Map<String, dynamic>)
    ..name = json['name'] as String?
    ..organizationType = json['organization_type'] as String?;
}

Map<String, dynamic> _$UserSchemaOrganizationToJson(
        UserSchemaOrganization instance) =>
    <String, dynamic>{
      'data': instance.data?.toJson(),
      'name': instance.name,
      'organization_type': instance.organizationType,
    };

UserSchemaOrganizationData _$UserSchemaOrganizationDataFromJson(
    Map<String, dynamic> json) {
  return UserSchemaOrganizationData()..id = json['id'] as String?;
}

Map<String, dynamic> _$UserSchemaOrganizationDataToJson(
        UserSchemaOrganizationData instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

UserSchemaConnectedOrganizations _$UserSchemaConnectedOrganizationsFromJson(
    Map<String, dynamic> json) {
  return UserSchemaConnectedOrganizations()
    ..roles =
        (json['roles'] as List<dynamic>?)?.map((e) => e as String).toList()
    ..organizationId = json['organization_id'] as String?;
}

Map<String, dynamic> _$UserSchemaConnectedOrganizationsToJson(
        UserSchemaConnectedOrganizations instance) =>
    <String, dynamic>{
      'roles': instance.roles,
      'organization_id': instance.organizationId,
    };
