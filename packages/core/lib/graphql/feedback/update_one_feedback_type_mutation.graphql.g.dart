// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'update_one_feedback_type_mutation.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateOneFeedbackTypeMutation$MutationRoot$UpdateFeedbackTypeByPk
    _$UpdateOneFeedbackTypeMutation$MutationRoot$UpdateFeedbackTypeByPkFromJson(
        Map<String, dynamic> json) {
  return UpdateOneFeedbackTypeMutation$MutationRoot$UpdateFeedbackTypeByPk()
    ..id = json['id'] as String
    ..key = json['key'] as String
    ..name = json['name'] as String
    ..schema = json['schema'];
}

Map<String, dynamic>
    _$UpdateOneFeedbackTypeMutation$MutationRoot$UpdateFeedbackTypeByPkToJson(
            UpdateOneFeedbackTypeMutation$MutationRoot$UpdateFeedbackTypeByPk
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'key': instance.key,
          'name': instance.name,
          'schema': instance.schema,
        };

UpdateOneFeedbackTypeMutation$MutationRoot
    _$UpdateOneFeedbackTypeMutation$MutationRootFromJson(
        Map<String, dynamic> json) {
  return UpdateOneFeedbackTypeMutation$MutationRoot()
    ..updateFeedbackTypeByPk = json['update_feedback_type_by_pk'] == null
        ? null
        : UpdateOneFeedbackTypeMutation$MutationRoot$UpdateFeedbackTypeByPk
            .fromJson(
                json['update_feedback_type_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UpdateOneFeedbackTypeMutation$MutationRootToJson(
        UpdateOneFeedbackTypeMutation$MutationRoot instance) =>
    <String, dynamic>{
      'update_feedback_type_by_pk': instance.updateFeedbackTypeByPk?.toJson(),
    };

UpdateOneFeedbackTypeMutationArguments
    _$UpdateOneFeedbackTypeMutationArgumentsFromJson(
        Map<String, dynamic> json) {
  return UpdateOneFeedbackTypeMutationArguments(
    id: json['id'] as String,
    key: json['key'] as String,
    name: json['name'] as String,
    schema: json['schema'],
  );
}

Map<String, dynamic> _$UpdateOneFeedbackTypeMutationArgumentsToJson(
        UpdateOneFeedbackTypeMutationArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
      'key': instance.key,
      'name': instance.name,
      'schema': instance.schema,
    };
