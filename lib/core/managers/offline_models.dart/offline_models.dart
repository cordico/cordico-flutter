import 'package:core/graphql/category/category_by_id_api.dart';
import 'package:core/graphql/collection/collection_by_id.dart';
import 'package:core/graphql/content/content_by_id_api.graphql.dart';
import 'package:core/graphql/organization/organization_contact_by_id.dart';
import 'package:json_annotation/json_annotation.dart';

part 'offline_models.g.dart';

@JsonSerializable(explicitToJson: true)
class OrganizationContactOffline extends JsonSerializable {
  OrganizationContactOffline();
  factory OrganizationContactOffline.fromJson(Map<String, dynamic> json) =>
      _$OrganizationContactOfflineFromJson(json);

  List<OrganizationContactById$QueryRoot$OrganizationContactsByPk>? data;

  @override
  Map<String, dynamic> toJson() => _$OrganizationContactOfflineToJson(this);

  static OrganizationContactOffline fromJSON(Map<String, dynamic> json) {
    return OrganizationContactOffline.fromJson(json);
  }
}

@JsonSerializable(explicitToJson: true)
class ContentOffline extends JsonSerializable {
  ContentOffline();
  factory ContentOffline.fromJson(Map<String, dynamic> json) =>
      _$ContentOfflineFromJson(json);

  List<ContentById$QueryRoot$ContentByPk>? data;

  @override
  Map<String, dynamic> toJson() => _$ContentOfflineToJson(this);

  static ContentOffline fromJSON(Map<String, dynamic> json) {
    return ContentOffline.fromJson(json);
  }
}

@JsonSerializable(explicitToJson: true)
class CategoriesOffline extends JsonSerializable {
  CategoriesOffline();
  factory CategoriesOffline.fromJson(Map<String, dynamic> json) =>
      _$CategoriesOfflineFromJson(json);

  List<CategoryById$QueryRoot$CategoryByPk>? data;

  @override
  Map<String, dynamic> toJson() => _$CategoriesOfflineToJson(this);

  static CategoriesOffline fromJSON(Map<String, dynamic> json) {
    return CategoriesOffline.fromJson(json);
  }
}

@JsonSerializable(explicitToJson: true)
class CollectionsOffline extends JsonSerializable {
  CollectionsOffline();
  factory CollectionsOffline.fromJson(Map<String, dynamic> json) =>
      _$CollectionsOfflineFromJson(json);

  List<CollectionById$QueryRoot$CollectionByPk>? data;

  @override
  Map<String, dynamic> toJson() => _$CollectionsOfflineToJson(this);

  static CollectionsOffline fromJSON(Map<String, dynamic> json) {
    return CollectionsOffline.fromJson(json);
  }
}
