// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'collection_by_collection_type.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionType
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionTypeToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections$Organization
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections$OrganizationFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections$Organization()
    ..name = json['name'] as String
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections$OrganizationToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections$Organization
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollectionsFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections()
    ..organization =
        CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections$Organization
            .fromJson(json['organization'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollectionsToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections
                instance) =>
        <String, dynamic>{
          'organization': instance.organization.toJson(),
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentType
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentTypeToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'is_shared': instance.isShared,
          'description': instance.description,
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentTypeToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'is_shared': instance.isShared,
          'description': instance.description,
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContentFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContentToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContentFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContentToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'index': instance.index,
          'part_id': instance.partId,
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$CategoryToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories()
    ..category =
        CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category
            .fromJson(json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategoriesToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories
                instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentStateFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentStateToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$CollectionToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$CollectionsFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections()
    ..collection =
        CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection
            .fromJson(json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$CollectionsToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections
                instance) =>
        <String, dynamic>{
          'collection': instance.collection.toJson(),
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPartFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart()
    ..contentType =
        CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType
            .fromJson(json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) =>
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) =>
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentState =
        CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState
            .fromJson(json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) =>
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPartToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart
                instance) =>
        <String, dynamic>{
          'content_type': instance.contentType.toJson(),
          'updated_at': instance.updatedAt?.toIso8601String(),
          'name': instance.name,
          'is_shared': instance.isShared,
          'id': instance.id,
          'data': instance.data,
          'created_at': instance.createdAt.toIso8601String(),
          'child_content':
              instance.childContent.map((e) => e.toJson()).toList(),
          'parent_content':
              instance.parentContent.map((e) => e.toJson()).toList(),
          'content_categories':
              instance.contentCategories.map((e) => e.toJson()).toList(),
          'content_state': instance.contentState.toJson(),
          'collections': instance.collections.map((e) => e.toJson()).toList(),
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContentFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent()
    ..contentPart =
        CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent$ContentPart
            .fromJson(json['content_part'] as Map<String, dynamic>)
    ..id = json['id'] as String
    ..index = json['index'] as int;
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContentToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent
                instance) =>
        <String, dynamic>{
          'content_part': instance.contentPart.toJson(),
          'id': instance.id,
          'index': instance.index,
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ParentContent
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ParentContentFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ParentContentToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ParentContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'index': instance.index,
          'part_id': instance.partId,
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories$Category
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories$CategoryToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories()
    ..category =
        CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories$Category
            .fromJson(json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategoriesToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories
                instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentState
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentStateFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentStateToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentState
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection$CollectionType
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection$CollectionTypeToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$CollectionToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$CollectionsFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections()
    ..collection =
        CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections$Collection
            .fromJson(json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$CollectionsToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections
                instance) =>
        <String, dynamic>{
          'collection': instance.collection.toJson(),
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$ContentFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content()
    ..contentType =
        CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentType
            .fromJson(json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) =>
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ChildContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) =>
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ParentContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentCategories
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentState =
        CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$ContentState
            .fromJson(json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) =>
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content$Collections
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$ContentToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content
                instance) =>
        <String, dynamic>{
          'content_type': instance.contentType.toJson(),
          'updated_at': instance.updatedAt?.toIso8601String(),
          'name': instance.name,
          'is_shared': instance.isShared,
          'id': instance.id,
          'data': instance.data,
          'created_at': instance.createdAt.toIso8601String(),
          'child_content':
              instance.childContent.map((e) => e.toJson()).toList(),
          'parent_content':
              instance.parentContent.map((e) => e.toJson()).toList(),
          'content_categories':
              instance.contentCategories.map((e) => e.toJson()).toList(),
          'content_state': instance.contentState.toJson(),
          'collections': instance.collections.map((e) => e.toJson()).toList(),
        };

CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContentsFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents()
    ..id = json['id'] as String
    ..contentId = json['content_id'] as String
    ..collectionId = json['collection_id'] as String
    ..content =
        CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents$Content
            .fromJson(json['content'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContentsToJson(
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'content_id': instance.contentId,
          'collection_id': instance.collectionId,
          'content': instance.content.toJson(),
        };

CollectionByCollectionTypeKey$QueryRoot$Collection
    _$CollectionByCollectionTypeKey$QueryRoot$CollectionFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot$Collection()
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..name = json['name'] as String
    ..title = json['title'] as String
    ..key = json['key'] as String?
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..collectionTypeId = json['collection_type_id'] as String?
    ..collectionType = json['collection_type'] == null
        ? null
        : CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>)
    ..organizationCollections = (json['organization_collections']
            as List<dynamic>)
        .map((e) =>
            CollectionByCollectionTypeKey$QueryRoot$Collection$OrganizationCollections
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..collectionContents = (json['collection_contents'] as List<dynamic>)
        .map((e) =>
            CollectionByCollectionTypeKey$QueryRoot$Collection$CollectionContents
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$CollectionByCollectionTypeKey$QueryRoot$CollectionToJson(
        CollectionByCollectionTypeKey$QueryRoot$Collection instance) =>
    <String, dynamic>{
      'id': instance.id,
      'is_shared': instance.isShared,
      'name': instance.name,
      'title': instance.title,
      'key': instance.key,
      'created_at': instance.createdAt.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'collection_type_id': instance.collectionTypeId,
      'collection_type': instance.collectionType?.toJson(),
      'organization_collections':
          instance.organizationCollections.map((e) => e.toJson()).toList(),
      'collection_contents':
          instance.collectionContents.map((e) => e.toJson()).toList(),
    };

CollectionByCollectionTypeKey$QueryRoot
    _$CollectionByCollectionTypeKey$QueryRootFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKey$QueryRoot()
    ..collection = (json['collection'] as List<dynamic>)
        .map((e) => CollectionByCollectionTypeKey$QueryRoot$Collection.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$CollectionByCollectionTypeKey$QueryRootToJson(
        CollectionByCollectionTypeKey$QueryRoot instance) =>
    <String, dynamic>{
      'collection': instance.collection.map((e) => e.toJson()).toList(),
    };

CollectionByCollectionTypeKeyArguments
    _$CollectionByCollectionTypeKeyArgumentsFromJson(
        Map<String, dynamic> json) {
  return CollectionByCollectionTypeKeyArguments(
    collectionTypeKey: json['collectionTypeKey'] as String,
    contentStateNames: (json['contentStateNames'] as List<dynamic>?)
        ?.map((e) => e as String?)
        .toList(),
    isShared:
        (json['isShared'] as List<dynamic>?)?.map((e) => e as bool).toList(),
  );
}

Map<String, dynamic> _$CollectionByCollectionTypeKeyArgumentsToJson(
        CollectionByCollectionTypeKeyArguments instance) =>
    <String, dynamic>{
      'collectionTypeKey': instance.collectionTypeKey,
      'contentStateNames': instance.contentStateNames,
      'isShared': instance.isShared,
    };
