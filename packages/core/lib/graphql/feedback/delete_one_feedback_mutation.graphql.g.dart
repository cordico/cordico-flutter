// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'delete_one_feedback_mutation.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeleteOneFeedbackMutation$MutationRoot$DeleteFeedbackByPk
    _$DeleteOneFeedbackMutation$MutationRoot$DeleteFeedbackByPkFromJson(
        Map<String, dynamic> json) {
  return DeleteOneFeedbackMutation$MutationRoot$DeleteFeedbackByPk()
    ..body = json['body'] as String
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..data = json['data']
    ..feedbackTypeId = json['feedback_type_id'] as String
    ..id = json['id'] as String
    ..subject = json['subject'] as String?
    ..updatedAt = DateTime.parse(json['updated_at'] as String);
}

Map<String,
    dynamic> _$DeleteOneFeedbackMutation$MutationRoot$DeleteFeedbackByPkToJson(
        DeleteOneFeedbackMutation$MutationRoot$DeleteFeedbackByPk instance) =>
    <String, dynamic>{
      'body': instance.body,
      'created_at': instance.createdAt.toIso8601String(),
      'data': instance.data,
      'feedback_type_id': instance.feedbackTypeId,
      'id': instance.id,
      'subject': instance.subject,
      'updated_at': instance.updatedAt.toIso8601String(),
    };

DeleteOneFeedbackMutation$MutationRoot
    _$DeleteOneFeedbackMutation$MutationRootFromJson(
        Map<String, dynamic> json) {
  return DeleteOneFeedbackMutation$MutationRoot()
    ..deleteFeedbackByPk = json['delete_feedback_by_pk'] == null
        ? null
        : DeleteOneFeedbackMutation$MutationRoot$DeleteFeedbackByPk.fromJson(
            json['delete_feedback_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DeleteOneFeedbackMutation$MutationRootToJson(
        DeleteOneFeedbackMutation$MutationRoot instance) =>
    <String, dynamic>{
      'delete_feedback_by_pk': instance.deleteFeedbackByPk?.toJson(),
    };

DeleteOneFeedbackMutationArguments _$DeleteOneFeedbackMutationArgumentsFromJson(
    Map<String, dynamic> json) {
  return DeleteOneFeedbackMutationArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$DeleteOneFeedbackMutationArgumentsToJson(
        DeleteOneFeedbackMutationArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
