// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'organization_query.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class OrganizationQuery$QueryRoot$Organization extends JsonSerializable
    with EquatableMixin {
  OrganizationQuery$QueryRoot$Organization();

  factory OrganizationQuery$QueryRoot$Organization.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationQuery$QueryRoot$OrganizationFromJson(json);

  late String name;

  late String id;

  String? description;

  late dynamic data;

  @JsonKey(name: 'organization_type_id')
  late String organizationTypeId;

  @JsonKey(name: 'portal_fqdn')
  String? portalFqdn;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @override
  List<Object?> get props => [
        name,
        id,
        description,
        data,
        organizationTypeId,
        portalFqdn,
        createdAt,
        updatedAt
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationQuery$QueryRoot$OrganizationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationQuery$QueryRoot extends JsonSerializable with EquatableMixin {
  OrganizationQuery$QueryRoot();

  factory OrganizationQuery$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$OrganizationQuery$QueryRootFromJson(json);

  late List<OrganizationQuery$QueryRoot$Organization> organization;

  @override
  List<Object?> get props => [organization];
  @override
  Map<String, dynamic> toJson() => _$OrganizationQuery$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationQueryArguments extends JsonSerializable with EquatableMixin {
  OrganizationQueryArguments({this.offset, this.limit});

  @override
  factory OrganizationQueryArguments.fromJson(Map<String, dynamic> json) =>
      _$OrganizationQueryArgumentsFromJson(json);

  final int? offset;

  final int? limit;

  @override
  List<Object?> get props => [offset, limit];
  @override
  Map<String, dynamic> toJson() => _$OrganizationQueryArgumentsToJson(this);
}

final ORGANIZATION_QUERY_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'OrganizationQuery'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'offset')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '0')),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'limit')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '10000')),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'organization'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'offset'),
                  value: VariableNode(name: NameNode(value: 'offset'))),
              ArgumentNode(
                  name: NameNode(value: 'limit'),
                  value: VariableNode(name: NameNode(value: 'limit')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'description'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'organization_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'portal_fqdn'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class OrganizationQueryQuery extends GraphQLQuery<OrganizationQuery$QueryRoot,
    OrganizationQueryArguments> {
  OrganizationQueryQuery({required this.variables});

  @override
  final DocumentNode document = ORGANIZATION_QUERY_QUERY_DOCUMENT;

  @override
  final String operationName = 'OrganizationQuery';

  @override
  final OrganizationQueryArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  OrganizationQuery$QueryRoot parse(Map<String, dynamic> json) =>
      OrganizationQuery$QueryRoot.fromJson(json);
}
