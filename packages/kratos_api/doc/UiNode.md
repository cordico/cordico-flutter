# kratos_api.model.UiNode

## Load the model package
```dart
import 'package:kratos_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attributes** | [**UiNodeAttributes**](UiNodeAttributes.md) |  | [optional] 
**group** | **String** |  | [optional] 
**messages** | [**BuiltList&lt;UiText&gt;**](UiText.md) |  | [optional] 
**meta** | [**UiNodeMeta**](UiNodeMeta.md) |  | [optional] 
**type** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


