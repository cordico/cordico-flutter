import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:flutter/material.dart';

abstract class ThemeStyles {
  static TextStyle? appBarTitleStyle(BuildContext context) {
    return Theme.of(context).textTheme.headline5?.copyWith(
          color: ThemeColors.blue.shade50,
          fontWeight: FontWeight.w400,
          fontSize: 27,
        );
  }

  static TextStyle? cardContentTitle(BuildContext context) {
    return Theme.of(context).textTheme.subtitle1?.copyWith(
          color: ThemeColors.blue.shade700,
          fontWeight: FontWeight.w700,
          fontSize: 16,
        );
  }

  static TextStyle? extraSmallParagraph(BuildContext context) {
    return Theme.of(context).textTheme.overline?.copyWith(
          color: ThemeColors.blue.shade700,
          fontWeight: FontWeight.w400,
          fontSize: 11,
        );
  }

  static TextStyle? sectionTitle(BuildContext context) {
    return Theme.of(context).textTheme.headline6?.copyWith(
          color: ThemeColors.blue.shade50,
          fontWeight: FontWeight.w700,
          fontSize: 19,
        );
  }

  static TextStyle? fieldLabel(BuildContext context) {
    return Theme.of(context).textTheme.bodyText2?.copyWith(
          color: ThemeColors.blue.shade50,
          fontWeight: FontWeight.w400,
        );
  }

  static TextStyle? elevatedButtonTextStyle(BuildContext context) {
    return Theme.of(context).textTheme.button?.copyWith(
          color: Colors.white,
          fontWeight: FontWeight.w600,
          fontSize: 16,
        );
  }

  static TextStyle? outlinedButtonTextStyle(BuildContext context) {
    return Theme.of(context).textTheme.button?.copyWith(
          color: ThemeColors.blue,
          fontWeight: FontWeight.w700,
          fontSize: 16,
        );
  }

  static ButtonStyle? elevatedButtonStyle(
    BuildContext context, {
    bool isFullyRounded = false,
    double? width,
    double? height,
    Color? color,
    BorderSide? border,
  }) {
    final primaryColor = color ?? ThemeColors.blue.shade500;

    return ElevatedButton.styleFrom(
      primary: primaryColor,
      onSurface: primaryColor,
      elevation: 0,
      side: border,
      minimumSize: Size(width ?? double.infinity, height ?? 32.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(isFullyRounded ? 54 : 6),
      ),
    );
  }

  static ButtonStyle? outlinedButtonStyle(
    BuildContext context, {
    double? height,
    double? width,
    bool isFullyRounded = true,
    Color? borderColor,
    Color? color,
  }) {
    return OutlinedButton.styleFrom(
      elevation: 0,
      backgroundColor: color,
      padding: const EdgeInsets.only(left: 12, right: 12),
      side: BorderSide(color: borderColor ?? Colors.white),
      // minimumSize: height == null ? null : Size(width ?? 0, height),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(isFullyRounded ? 54 : 6),
      ),
      tapTargetSize: MaterialTapTargetSize.shrinkWrap,
    );
  }

  static TextStyle? floatingLabelTextStyle(BuildContext context) {
    return Theme.of(context).textTheme.bodyText2?.copyWith(
          color: ThemeColors.gray.shade600,
          fontWeight: FontWeight.w400,
          fontSize: 14,
        );
  }

  static OutlineInputBorder outlineInputBorder(
      {Color? color, double? borderWidth, double? borderRadius}) {
    return OutlineInputBorder(
        borderSide: BorderSide(
            color: color ?? ThemeColors.gray.shade200,
            width: borderWidth ?? 1.0),
        borderRadius: BorderRadius.circular(borderRadius ?? 6.0));
  }

  static OutlineInputBorder focusedOutlineInputBorder({
    required Color color,
    double? borderRadius,
  }) {
    return outlineInputBorder(color: color, borderRadius: borderRadius);
  }

  static TextStyle? textFieldTextStyle(BuildContext context) {
    return Theme.of(context).textTheme.bodyText2?.copyWith(
          color: ThemeColors.gray.shade400,
          fontSize: 14,
        );
  }

  static TextStyle? textFieldErrorTextStyle(BuildContext context) {
    return Theme.of(context).textTheme.caption?.copyWith(
          color: ThemeColors.textError,
          fontSize: 12,
        );
  }

  static TextStyle? contentDescription(BuildContext context) {
    return Theme.of(context)
        .textTheme
        .bodyText2
        ?.copyWith(color: ThemeColors.gray.shade100, fontSize: 13.3);
  }

  static TextStyle? cardCategory(BuildContext context) {
    return Theme.of(context).textTheme.bodyText2?.copyWith(
          color: ThemeColors.gray.shade100,
          fontWeight: FontWeight.w400,
          fontSize: 11,
          letterSpacing: 2,
        );
  }

  static TextStyle? cardTitle(BuildContext context) {
    return Theme.of(context).textTheme.headline6?.copyWith(
          color: Colors.white,
          fontWeight: FontWeight.w700,
          fontSize: 16,
        );
  }

  static TextStyle? bulletinCardTitle(BuildContext context) {
    return Theme.of(context).textTheme.headline6?.copyWith(
        color: Colors.white,
        fontWeight: FontWeight.w700,
        fontSize: 16,
        fontFamily: 'PTSerif');
  }

  static TextStyle? smallParagraph(BuildContext context) {
    return Theme.of(context).textTheme.bodyText2?.copyWith(
          color: ThemeColors.gray.shade200,
          fontWeight: FontWeight.w400,
          fontSize: 13.33,
        );
  }
}
