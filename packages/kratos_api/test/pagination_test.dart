import 'package:test/test.dart';
import 'package:kratos_api/kratos_api.dart';

// tests for Pagination
void main() {
  final instance = PaginationBuilder();
  // TODO add properties to the builder and call build()

  group(Pagination, () {
    // Pagination Page
    // int page (default value: 0)
    test('to test the property `page`', () async {
      // TODO
    });

    // Items per Page  This is the number of items per page.
    // int perPage (default value: 250)
    test('to test the property `perPage`', () async {
      // TODO
    });

  });
}
