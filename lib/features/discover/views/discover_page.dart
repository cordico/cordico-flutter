// ignore_for_file: cascade_invocations

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/managers/cordico_alert.dart';
import 'package:cordico_mobile/core/managers/offline_models.dart/offline_models.dart';
import 'package:cordico_mobile/core/managers/offlline_manager.dart';
import 'package:cordico_mobile/core/providers/global_providers.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/cordico_elevated_button.dart';
import 'package:cordico_mobile/core/widgets/cordico_search_field.dart';
import 'package:cordico_mobile/features/content/logic/content_provider.dart';
import 'package:cordico_mobile/features/department/views/content_list_view.dart';
import 'package:cordico_mobile/features/discover/logic/content_filter_state.dart';
import 'package:core/core.dart';
import 'package:core/graphql/content/content_by_id_api.graphql.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_segment/flutter_segment.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:vrouter/vrouter.dart';

final filtersQueryProvider = StateProvider<String>((ref) => '');
final filtersTypesProvider = StateProvider<List<String>>((ref) => []);
final filtersCategoriesProvider = StateProvider<List<String>>((ref) => []);

final filteredContentProvider = Provider<DiscoverContent>((ref) {
  final discoverContent = DiscoverContent();
  final query = ref.watch(filtersQueryProvider);
  final types = ref.watch(filtersTypesProvider);
  final categories = ref.watch(filtersCategoriesProvider);
  final content = ref.watch(allContentProvider);

  discoverContent.featuredContent = utils.filterContent(
    content: content.featuredContent,
    query: query,
    types: types,
    categories: categories,
  );
  discoverContent.whatsNewContent = utils.filterContent(
    content: content.whatsNewContent,
    query: query,
    types: types,
    categories: categories,
  );
  discoverContent.fromAllCategoriesContent = utils.filterContent(
    content: content.fromAllCategoriesContent,
    query: query,
    types: types,
    categories: categories,
  );

  return discoverContent;
});

final discoverContentFuture = FutureProvider<List<ContentModel>?>((ref) async {
  final repository = ref.watch(contentRepositoryProvider);

  List<ContentById$QueryRoot$ContentByPk>? resultContent;

  final connectityCheck = await Connectivity().checkConnectivity();

  if (connectityCheck == ConnectivityResult.none) {
    final list = OfflineManager.getData(key: ModelsKeys.discoverContent);

    if (list != null) {
      final contents = ContentOffline.fromJson(list).data ?? [];
      resultContent = contents.map((e) {
        return ContentModel.fromJson(e.toJson());
      }).toList();
    } else {
      resultContent = [];
    }
  } else {
    final result = await repository.getContentByContentType([
      kContentTypeGuideId,
      kContentTypeAssessmentId,
    ]);

    await result.fold((error) {}, (state) async {
      resultContent = state;

      final content = ContentOffline();
      content.data = resultContent;

      await OfflineManager.persistData(
        key: ModelsKeys.discoverContent,
        value: content.toJson(),
      );
    });
  }
  return resultContent;
});

class DiscoverPage extends ConsumerStatefulWidget {
  const DiscoverPage({Key? key}) : super(key: key);

  @override
  ConsumerState<DiscoverPage> createState() => _DiscoverPageState();
}

class _DiscoverPageState extends ConsumerState<DiscoverPage> {
  late Map<String, String> paramteres;

  bool showAllCollections = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Segment.screen(
        screenName: 'Discover',
        properties: <String, dynamic>{'type': 'screen', 'name': 'Discover'});

    return _buildDiscoverPage(context, ref);
  }

  Widget _buildDiscoverPage(BuildContext context, WidgetRef ref) {
    return Column(
      children: [
        _buildFilterInputField(context, ref),
        //  _buildCategoriesFilter(context),
        _buildResourceTypeFilter(context, ref),
        buildContentList(ref),
      ],
    );
  }

  Widget buildContentList(WidgetRef ref) {
    paramteres = context.vRouter.pathParameters;

    final state = ref.watch(filteredContentProvider);
    final notifier = ref.watch(allContentProvider.notifier);

    // if (notifier.contentLoaded && global.discoverFilter != null) {
    //   WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
    //     global.discoverFilter!();
    //     global.discoverFilter = null;
    //   });
    // }

    return notifier.isLoading
        ? const Center(
            child: Padding(
              padding: EdgeInsets.all(24),
              child: CircularProgressIndicator(),
            ),
          )
        : Expanded(
            child: CustomScrollView(
              slivers: [
                if (state.featuredContent.isNotEmpty && showAllCollections)
                  ContentListView<ContentQuery$QueryRoot$Content>(
                    header: 'Featured Content',
                    isSliver: true,
                    source: state.featuredContent,
                    onItemTap: (index) {
                      final co = state.featuredContent[index];
                      //SETTING THE ID TO PERFORM THE CALL
                      ref.watch(contentIdProvider.state).state = co.id;
                      global.goTo(context, content: co, index: 0);
                    },
                  ),
                if (state.whatsNewContent.isNotEmpty && showAllCollections)
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Divider(
                        height: 1,
                        thickness: 1,
                        color: Colors.grey.shade800,
                      ),
                    ),
                  ),
                if (state.whatsNewContent.isNotEmpty && showAllCollections)
                  ContentListView<ContentQuery$QueryRoot$Content>(
                    header: "What's New",
                    isSliver: true,
                    source: state.whatsNewContent,
                    onItemTap: (index) {
                      final co = state.whatsNewContent[index];
                      //SETTING THE ID TO PERFORM THE CALL
                      ref.watch(contentIdProvider.state).state = co.id;
                      global.goTo(context, content: co, index: 0);
                    },
                  ),
                if (state.fromAllCategoriesContent.isNotEmpty &&
                    showAllCollections)
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Divider(
                        height: 1,
                        thickness: 1,
                        color: Colors.grey.shade800,
                      ),
                    ),
                  ),
                if (state.fromAllCategoriesContent.isNotEmpty)
                  ContentListView<ContentQuery$QueryRoot$Content>(
                    header: 'From All Categories',
                    source: state.fromAllCategoriesContent,
                    isSliver: true,
                    onItemTap: (index) {
                      final co = state.fromAllCategoriesContent[index];
                      //SETTING THE ID TO PERFORM THE CALL
                      ref.watch(contentIdProvider.state).state = co.id;
                      global.goTo(context, content: co, index: 0);
                    },
                  )
              ],
            ),
          );
  }

  Widget _buildFilterInputField(BuildContext context, WidgetRef ref) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(12, 8, 12, 12),
      child: CordicoSearchField(
        hintText: 'Search Cordico',
        controller: global.discoverTextController,
        onChanged: (value) {
          ref.watch(filtersQueryProvider.state).state = value;
          checkShowAllCollection(ref);
        },
      ),
    );
  }

  void checkShowAllCollection(WidgetRef ref) {
    final string = ref.watch(filtersQueryProvider);
    final types = ref.watch(filtersTypesProvider);
    final categories = ref.watch(filtersCategoriesProvider);

    if (string.isNotEmpty || types.isNotEmpty || categories.isNotEmpty) {
      setState(() => showAllCollections = false);
    } else {
      setState(() => showAllCollections = true);
    }
  }

  /*Widget _buildCategoriesFilter(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(18, 16, 0, 7),
          child: Text(
            'Resource Category',
            style: ThemeStyles.fieldLabel(context),
          ),
        ),
        _buildCategories(context),
      ],
    );
  }

  Widget _buildCategories(BuildContext context) {
    return Consumer(
        builder: (BuildContext context, WidgetRef ref, Widget? child) {
      final state = ref.watch(categoriesNotifierProvider);

      return state.when(
        initial: () => Container(height: 33),
        data: (categories) {
          return SizedBox(
            height: 33,
            child: ListView.builder(
                padding: const EdgeInsets.only(left: 12, right: 12),
                itemCount: categories.list.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  final category = categories.list[index];
                  final item = CategoryFilterChip(
                    categoryName: category.name,
                    leading: CircleAvatar(
                      radius: 5,
                      backgroundColor: global.colorFromCategoryModel(category),
                    ),
                    onTap: () {
                      context.vRouter.to(
                          'archive/${category.name}/${category.id}/category');
                    },
                  );

                  if (index < categories.list.length - 1) {
                    return Padding(
                      padding: const EdgeInsets.only(right: 8),
                      child: item,
                    );
                  }

                  return item;
                }),
          );
        },
        loading: () => Container(height: 33),
        error: (e) => Container(height: 33),
      );
    });
  }*/

  Widget _buildResourceTypeFilter(BuildContext context, WidgetRef ref) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        //Container(height: 16),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: CordicoElevatedButton(
                color: ThemeColors.gray.shade500,
                hasBorder: true,
                height: 37,
                child: Row(
                  children: [
                    Icon(
                      Icons.filter_list,
                      color: ThemeColors.gray.shade100,
                      size: 19,
                    ),
                    Container(width: 7),
                    Text(
                      'Filters',
                      style: ThemeStyles.contentDescription(context),
                    ),
                    if (global.discoverFilters.filtersApplied != 0)
                      Container(
                        margin: const EdgeInsets.only(left: 8),
                        height: 21,
                        width: 21,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(11),
                        ),
                        child: Center(
                          child: Text(
                            '${global.discoverFilters.filtersApplied}',
                            style: ThemeStyles.contentDescription(context)!
                                .copyWith(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                  ],
                ),
                onPressed: () async {
                  final res = await CordicoAlert.showContentFilters(context,
                      filters: global.discoverFilters, filterByCategory: true);

                  if (res != null) {
                    setState(() => global.discoverFilters = res);

                    ref.watch(filtersTypesProvider.state).state = res.contentTypesIds;

                    ref.watch(filtersCategoriesProvider.state).state =
                        res.categoriesTypesIds;

                    checkShowAllCollection(ref);
                  }
                },
              ),
            ),
          ],
        ),

        /*CordicoMultiselectDropDown(
                title: 'Resource Type ',
                filtersApplied: ref.watch(filtersTypesProvider).state,
                contentTypes: data.filterContentTypes ?? [],
                onItemSelected: (ids) {
                  ref.watch(filtersTypesProvider).state = ids;
                  checkShowAllCollection(ref);
                },
                //['Guide', 'Assessment', 'Article', 'Audio', 'Vid0eo'],
              ),*/

        Container(height: 19),
        Container(
          width: double.infinity,
          height: 1,
          color: ThemeColors.gray.shade700,
        )
      ],
    );
  }
}
