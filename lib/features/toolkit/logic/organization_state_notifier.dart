import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:cordico_mobile/core/managers/offlline_manager.dart';
import 'package:cordico_mobile/core/providers/global_providers.dart';
import 'package:cordico_mobile/features/toolkit/logic/organization_state.dart';
import 'package:core/core.dart';
import 'package:core/graphql/organization/organization_by_id.graphql.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class OrganizationStateNotifier extends StateNotifier<OrganizationState> {
  OrganizationStateNotifier({
    required IOrganizationRepository organizationRepository,
    OrganizationState? initialState,
  })  : _organizationRepository = organizationRepository,
        super(initialState ?? const OrganizationState.initial());

  final IOrganizationRepository _organizationRepository;

  void reset() => state = const OrganizationState.initial();

  Future<void> getContent() async {
    state = const OrganizationState.loading();
    final connectityCheck = await Connectivity().checkConnectivity();

    if (connectityCheck == ConnectivityResult.none) {
      final map = OfflineManager.getData(key: ModelsKeys.organization);
      if (map != null) {
        final content =
            OrganizationByID$QueryRoot$OrganizationByPk.fromJson(map);
        state = OrganizationState.data(content: content);
      }
    } else {
      final result2 = await _organizationRepository
          .getOrganizationByID(environmentConfig.organizationId!);
      result2.fold(
        (error) {
          state = OrganizationState.error(error.toString());
        },
        (content) {
          OfflineManager.persistData(
              key: ModelsKeys.organization, value: content?.toJson());

          state = OrganizationState.data(content: content);
        },
      );
    }
  }
}
