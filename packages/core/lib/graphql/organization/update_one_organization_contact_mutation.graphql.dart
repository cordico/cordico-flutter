// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'update_one_organization_contact_mutation.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class UpdateOneOrganizationContactMutation$MutationRoot$UpdateOneOrganizationContact
    extends JsonSerializable with EquatableMixin {
  UpdateOneOrganizationContactMutation$MutationRoot$UpdateOneOrganizationContact();

  factory UpdateOneOrganizationContactMutation$MutationRoot$UpdateOneOrganizationContact.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateOneOrganizationContactMutation$MutationRoot$UpdateOneOrganizationContactFromJson(
          json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$UpdateOneOrganizationContactMutation$MutationRoot$UpdateOneOrganizationContactToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class UpdateOneOrganizationContactMutation$MutationRoot extends JsonSerializable
    with EquatableMixin {
  UpdateOneOrganizationContactMutation$MutationRoot();

  factory UpdateOneOrganizationContactMutation$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateOneOrganizationContactMutation$MutationRootFromJson(json);

  UpdateOneOrganizationContactMutation$MutationRoot$UpdateOneOrganizationContact?
      updateOneOrganizationContact;

  @override
  List<Object?> get props => [updateOneOrganizationContact];
  @override
  Map<String, dynamic> toJson() =>
      _$UpdateOneOrganizationContactMutation$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateOneOrganizationContactMutationArguments extends JsonSerializable
    with EquatableMixin {
  UpdateOneOrganizationContactMutationArguments(
      {this.id,
      this.contactTypeId,
      this.organizationId,
      required this.name,
      required this.title,
      this.data,
      this.isActive});

  @override
  factory UpdateOneOrganizationContactMutationArguments.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateOneOrganizationContactMutationArgumentsFromJson(json);

  final String? id;

  final String? contactTypeId;

  final String? organizationId;

  late String name;

  late String title;

  final dynamic? data;

  final bool? isActive;

  @override
  List<Object?> get props =>
      [id, contactTypeId, organizationId, name, title, data, isActive];
  @override
  Map<String, dynamic> toJson() =>
      _$UpdateOneOrganizationContactMutationArgumentsToJson(this);
}

final UPDATE_ONE_ORGANIZATION_CONTACT_MUTATION_MUTATION_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'UpdateOneOrganizationContactMutation'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contactTypeId')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'organizationId')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'name')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'title')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'data')),
            type:
                NamedTypeNode(name: NameNode(value: 'Json'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'isActive')),
            type: NamedTypeNode(
                name: NameNode(value: 'Boolean'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'updateOneOrganizationContact'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'id'),
                        value: VariableNode(name: NameNode(value: 'id')))
                  ])),
              ArgumentNode(
                  name: NameNode(value: 'data'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'contactType'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'connect'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'id'),
                                    value: VariableNode(
                                        name: NameNode(value: 'contactTypeId')))
                              ]))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'name'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'set'),
                              value:
                                  VariableNode(name: NameNode(value: 'name')))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'organization'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'connect'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'id'),
                                    value: VariableNode(
                                        name:
                                            NameNode(value: 'organizationId')))
                              ]))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'title'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'set'),
                              value:
                                  VariableNode(name: NameNode(value: 'title')))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'data'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'set'),
                              value:
                                  VariableNode(name: NameNode(value: 'data')))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'isActive'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'set'),
                              value: VariableNode(
                                  name: NameNode(value: 'isActive')))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class UpdateOneOrganizationContactMutationMutation extends GraphQLQuery<
    UpdateOneOrganizationContactMutation$MutationRoot,
    UpdateOneOrganizationContactMutationArguments> {
  UpdateOneOrganizationContactMutationMutation({required this.variables});

  @override
  final DocumentNode document =
      UPDATE_ONE_ORGANIZATION_CONTACT_MUTATION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'UpdateOneOrganizationContactMutation';

  @override
  final UpdateOneOrganizationContactMutationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  UpdateOneOrganizationContactMutation$MutationRoot parse(
          Map<String, dynamic> json) =>
      UpdateOneOrganizationContactMutation$MutationRoot.fromJson(json);
}
