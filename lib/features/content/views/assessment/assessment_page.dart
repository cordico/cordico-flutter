import 'package:collection/collection.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/app_bar_provider.dart';
import 'package:cordico_mobile/core/widgets/content_widget.dart';
import 'package:cordico_mobile/core/widgets/cordico_elevated_button.dart';
import 'package:cordico_mobile/features/content/logic/content_provider.dart';
import 'package:cordico_mobile/features/discover/logic/content_filter_state.dart';
import 'package:core/core.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/utils/map_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_segment/flutter_segment.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
// ignore: implementation_imports
import 'package:vrouter/src/core/extended_context.dart';
import 'package:vrouter/vrouter.dart';

class AssessmentPage extends StatefulWidget {
  const AssessmentPage({Key? key}) : super(key: key);

  @override
  State<AssessmentPage> createState() => _AssessmentPageState();
}

class _AssessmentPageState extends State<AssessmentPage> {
  late ContentQuery$QueryRoot$Content mainContent;
  late ContentSchema contentSchema;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (BuildContext context, WidgetRef ref, Widget? child) {
        //final contentValue = ref.watch(contentByIdProvider);

        final state = ref.watch(allContentProvider);
        final contentID = ref.watch(contentIdProvider);

        final content = state.fromAllCategoriesContent
            .firstWhereOrNull((element) => element.id == contentID);

        Segment.screen(
            screenName: 'View Content',
            properties: <String, dynamic>{
              'type': 'screen',
              'name': 'Content Summary'
            });

        if (content == null) {
          return const SizedBox.shrink();
        }

        final contentParts = content.childContent.map((e) {
          final map = e.toJson();
          return ContentPartModel.fromJson(map);
        }).toList();

        mainContent = ContentQuery$QueryRoot$Content.fromJson(content.toJson());
        removeNullAndEmptyParams(mainContent.data as Map<String, dynamic>);

        contentSchema =
            ContentSchema.fromJson(mainContent.data as Map<String, dynamic>);

        Segment.screen(screenName: 'Assessment', properties: <String, dynamic>{
          'type': mainContent.contentType.name,
          'name': mainContent.name
        });

        return Scaffold(
          appBar: AppBarProvider.getAppBarWithBackIcon(
            context,
            title: Text(
              'Self Assessment',
              style: ThemeStyles.cardContentTitle(context)
                  ?.copyWith(color: Colors.white),
            ),
          ),
          body: SafeArea(
            child: _buildContent(
              context,
              contentParts: contentParts,
            ),
          ),
        );
      },
    );
  }

  Widget _buildContent(
    BuildContext context, {
    List<ContentPartModel>? contentParts,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Expanded(
            child: SingleChildScrollView(
          child: _buildAssessmentDetails(context, content: mainContent),
        )),
        Align(
          child: Padding(
            padding: const EdgeInsets.only(bottom: 19),
            child: CordicoElevatedButton(
              text: 'Start Assessment',
              height: 38,
              width: 140,
              color: ThemeColors.blue.shade400,
              onPressed: contentParts != null && contentParts.isNotEmpty
                  ? _onStartAssessmentClicked
                  : null,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildAssessmentDetails(
    BuildContext context, {
    required ContentQuery$QueryRoot$Content? content,
  }) {
    return Padding(
      padding: const EdgeInsets.all(14),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 15),
          Text(
            contentSchema.title ?? '',
            style: ThemeStyles.appBarTitleStyle(context),
          ),
          const SizedBox(height: 23),
          ContentWidget(
            content: contentSchema.instructionsBody ?? '',
            contentType: (contentSchema.bodyMimeType == null ||
                    contentSchema.bodyMimeType == MimeType.markdown)
                ? MimeType.markdown
                : MimeType.html,
          ),
        ],
      ),
    );
  }

  void _onStartAssessmentClicked() {
    context.vRouter.to('assessment_questionnaire');
  }
}
