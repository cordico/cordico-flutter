// ignore_for_file: lines_longer_than_80_chars, implementation_imports

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/managers/cordico_url.dart';
import 'package:cordico_mobile/core/providers/global_providers.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/images.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/app_bar_provider.dart';
import 'package:cordico_mobile/core/widgets/cordico_elevated_button.dart';
import 'package:cordico_mobile/core/widgets/cordico_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:vrouter/src/core/extended_context.dart';

class SettingsPage extends ConsumerStatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  ConsumerState createState() => _SettingsPageState();
}

class _SettingsPageState extends ConsumerState<SettingsPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBarProvider.getAppBarWithBackIcon(
        context,
        backgroundColor: ThemeColors.gray.shade900,
        title: Text(
          'Cordico',
          style: ThemeStyles.cardContentTitle(context)
              ?.copyWith(color: Colors.white),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding:
              const EdgeInsets.only(left: 24, right: 24, top: 45, bottom: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Image.asset(
                Images.icLogoWhite,
                height: 38,
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.2),
              CordicoElevatedButton(
                color: Colors.black,
                hasBorder: true,
                height: 38,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      Images.notificationIcon,
                      height: 24,
                      width: 24,
                    ),
                    const SizedBox(width: 10),
                    Text(
                      'App Notification Settings',
                      textAlign: TextAlign.center,
                      style: ThemeStyles.smallParagraph(context)
                          ?.copyWith(fontSize: 14),
                    ),
                  ],
                ),
                onPressed: () {
                  context.vRouter.to('notification_settings');
                },
              ),
              const SizedBox(height: 16),
              CordicoElevatedButton(
                color: Colors.black,
                hasBorder: true,
                height: 38,
                child: Text(
                  'Log out from the current Department',
                  textAlign: TextAlign.center,
                  style: ThemeStyles.smallParagraph(context)
                      ?.copyWith(fontSize: 14),
                ),
                onPressed: () async {
                  await secureStorage.delete(key: 'cordico.sessionToken');
                  global.resetAll(ref);
                  context.vRouter.to('/login');

                  Future.delayed(const Duration(milliseconds: 300), () {
                    ref.watch(currentIndexProvider.state).state = 0;
                  });
                },
              ),
              const SizedBox(height: 36),
              CordicoElevatedButton(
                color: Colors.black,
                hasBorder: true,
                height: 38,
                child: Text(
                  'Give feedback to Cordico',
                  textAlign: TextAlign.center,
                  style: ThemeStyles.smallParagraph(context)
                      ?.copyWith(fontSize: 14),
                ),
                onPressed: () {
                  context.vRouter.to('feedback');
                },
              ),
              const SizedBox(height: 16),
              CordicoElevatedButton(
                color: Colors.black,
                hasBorder: true,
                height: 38,
                child: Text(
                  'Read Terms & Conditions',
                  textAlign: TextAlign.center,
                  style: ThemeStyles.smallParagraph(context)
                      ?.copyWith(fontSize: 14),
                ),
                onPressed: () {
                  CordicoUrl.openUrl(
                      url: 'https://www.cordico.com/terms-and-conditions/');
                },
              ),
              const SizedBox(height: 16),
              CordicoElevatedButton(
                color: Colors.black,
                hasBorder: true,
                height: 38,
                child: Text(
                  'Visit Cordico Website',
                  textAlign: TextAlign.center,
                  style: ThemeStyles.smallParagraph(context)
                      ?.copyWith(fontSize: 14),
                ),
                onPressed: () {
                  CordicoUrl.openUrl(url: 'https://www.cordico.com/');
                },
              ),
              const Spacer(),
              ref.watch(packageInfoProvider).when(
                    data: (info) => Row(
                      children: [
                        Expanded(
                          child: AutoSizeText(
                              'App Version ${info.version}    Copyright 2021 Cordico Inc',
                              textAlign: TextAlign.center,
                              minFontSize: 11,
                              maxLines: 1,
                              style: ThemeStyles.smallParagraph(context)),
                        ),
                      ],
                    ),
                    loading: () => const SizedBox.shrink(),
                    error: (error, stackTrace) => const SizedBox.shrink(),
                  ),
            ],
          ),
        ),
      ),
    );
  }
}
