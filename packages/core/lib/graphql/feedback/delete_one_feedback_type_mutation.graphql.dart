// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'delete_one_feedback_type_mutation.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class DeleteOneFeedbackTypeMutation$MutationRoot$DeleteFeedbackTypeByPk
    extends JsonSerializable with EquatableMixin {
  DeleteOneFeedbackTypeMutation$MutationRoot$DeleteFeedbackTypeByPk();

  factory DeleteOneFeedbackTypeMutation$MutationRoot$DeleteFeedbackTypeByPk.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneFeedbackTypeMutation$MutationRoot$DeleteFeedbackTypeByPkFromJson(
          json);

  late String id;

  late String key;

  late String name;

  dynamic? schema;

  @override
  List<Object?> get props => [id, key, name, schema];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneFeedbackTypeMutation$MutationRoot$DeleteFeedbackTypeByPkToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class DeleteOneFeedbackTypeMutation$MutationRoot extends JsonSerializable
    with EquatableMixin {
  DeleteOneFeedbackTypeMutation$MutationRoot();

  factory DeleteOneFeedbackTypeMutation$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneFeedbackTypeMutation$MutationRootFromJson(json);

  @JsonKey(name: 'delete_feedback_type_by_pk')
  DeleteOneFeedbackTypeMutation$MutationRoot$DeleteFeedbackTypeByPk?
      deleteFeedbackTypeByPk;

  @override
  List<Object?> get props => [deleteFeedbackTypeByPk];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneFeedbackTypeMutation$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeleteOneFeedbackTypeMutationArguments extends JsonSerializable
    with EquatableMixin {
  DeleteOneFeedbackTypeMutationArguments({required this.id});

  @override
  factory DeleteOneFeedbackTypeMutationArguments.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneFeedbackTypeMutationArgumentsFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneFeedbackTypeMutationArgumentsToJson(this);
}

final DELETE_ONE_FEEDBACK_TYPE_MUTATION_MUTATION_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'DeleteOneFeedbackTypeMutation'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'delete_feedback_type_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'id'),
                  value: VariableNode(name: NameNode(value: 'id')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class DeleteOneFeedbackTypeMutationMutation extends GraphQLQuery<
    DeleteOneFeedbackTypeMutation$MutationRoot,
    DeleteOneFeedbackTypeMutationArguments> {
  DeleteOneFeedbackTypeMutationMutation({required this.variables});

  @override
  final DocumentNode document =
      DELETE_ONE_FEEDBACK_TYPE_MUTATION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'DeleteOneFeedbackTypeMutation';

  @override
  final DeleteOneFeedbackTypeMutationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  DeleteOneFeedbackTypeMutation$MutationRoot parse(Map<String, dynamic> json) =>
      DeleteOneFeedbackTypeMutation$MutationRoot.fromJson(json);
}
