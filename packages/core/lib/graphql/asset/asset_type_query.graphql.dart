// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'asset_type_query.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class AssetTypeQuery$QueryRoot$AssetType extends JsonSerializable
    with EquatableMixin {
  AssetTypeQuery$QueryRoot$AssetType();

  factory AssetTypeQuery$QueryRoot$AssetType.fromJson(
          Map<String, dynamic> json) =>
      _$AssetTypeQuery$QueryRoot$AssetTypeFromJson(json);

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'creator_id')
  late String creatorId;

  String? description;

  late String id;

  late String key;

  @JsonKey(name: 'mime_types')
  late String mimeTypes;

  late String name;

  late dynamic schema;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @JsonKey(name: 'updater_id')
  String? updaterId;

  @override
  List<Object?> get props => [
        createdAt,
        creatorId,
        description,
        id,
        key,
        mimeTypes,
        name,
        schema,
        updatedAt,
        updaterId
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$AssetTypeQuery$QueryRoot$AssetTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AssetTypeQuery$QueryRoot extends JsonSerializable with EquatableMixin {
  AssetTypeQuery$QueryRoot();

  factory AssetTypeQuery$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$AssetTypeQuery$QueryRootFromJson(json);

  @JsonKey(name: 'asset_type')
  late List<AssetTypeQuery$QueryRoot$AssetType> assetType;

  @override
  List<Object?> get props => [assetType];
  @override
  Map<String, dynamic> toJson() => _$AssetTypeQuery$QueryRootToJson(this);
}

final ASSET_TYPE_QUERY_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'AssetTypeQuery'),
      variableDefinitions: [],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'asset_type'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'creator_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'description'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'mime_types'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updater_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class AssetTypeQueryQuery
    extends GraphQLQuery<AssetTypeQuery$QueryRoot, JsonSerializable> {
  AssetTypeQueryQuery();

  @override
  final DocumentNode document = ASSET_TYPE_QUERY_QUERY_DOCUMENT;

  @override
  final String operationName = 'AssetTypeQuery';

  @override
  List<Object?> get props => [document, operationName];
  @override
  AssetTypeQuery$QueryRoot parse(Map<String, dynamic> json) =>
      AssetTypeQuery$QueryRoot.fromJson(json);
}
