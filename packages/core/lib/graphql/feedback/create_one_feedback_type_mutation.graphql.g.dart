// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'create_one_feedback_type_mutation.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateOneFeedbackTypeMutation$MutationRoot$InsertFeedbackTypeOne
    _$CreateOneFeedbackTypeMutation$MutationRoot$InsertFeedbackTypeOneFromJson(
        Map<String, dynamic> json) {
  return CreateOneFeedbackTypeMutation$MutationRoot$InsertFeedbackTypeOne()
    ..id = json['id'] as String
    ..key = json['key'] as String
    ..name = json['name'] as String
    ..schema = json['schema'];
}

Map<String, dynamic>
    _$CreateOneFeedbackTypeMutation$MutationRoot$InsertFeedbackTypeOneToJson(
            CreateOneFeedbackTypeMutation$MutationRoot$InsertFeedbackTypeOne
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'key': instance.key,
          'name': instance.name,
          'schema': instance.schema,
        };

CreateOneFeedbackTypeMutation$MutationRoot
    _$CreateOneFeedbackTypeMutation$MutationRootFromJson(
        Map<String, dynamic> json) {
  return CreateOneFeedbackTypeMutation$MutationRoot()
    ..insertFeedbackTypeOne = json['insert_feedback_type_one'] == null
        ? null
        : CreateOneFeedbackTypeMutation$MutationRoot$InsertFeedbackTypeOne
            .fromJson(json['insert_feedback_type_one'] as Map<String, dynamic>);
}

Map<String, dynamic> _$CreateOneFeedbackTypeMutation$MutationRootToJson(
        CreateOneFeedbackTypeMutation$MutationRoot instance) =>
    <String, dynamic>{
      'insert_feedback_type_one': instance.insertFeedbackTypeOne?.toJson(),
    };

CreateOneFeedbackTypeMutationArguments
    _$CreateOneFeedbackTypeMutationArgumentsFromJson(
        Map<String, dynamic> json) {
  return CreateOneFeedbackTypeMutationArguments(
    key: json['key'] as String,
    name: json['name'] as String,
    schema: json['schema'],
  );
}

Map<String, dynamic> _$CreateOneFeedbackTypeMutationArgumentsToJson(
        CreateOneFeedbackTypeMutationArguments instance) =>
    <String, dynamic>{
      'key': instance.key,
      'name': instance.name,
      'schema': instance.schema,
    };
