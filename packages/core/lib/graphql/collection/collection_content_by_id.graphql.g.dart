// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'collection_content_by_id.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CollectionContentById$QueryRoot$CollectionContentByPk$Collection$CollectionType
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Collection$CollectionTypeToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Collection
    _$CollectionContentById$QueryRoot$CollectionContentByPk$CollectionFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Collection()
    ..id = json['id'] as String
    ..name = json['name'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : CollectionContentById$QueryRoot$CollectionContentByPk$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$CollectionToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Collection
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
          'collection_type': instance.collectionType?.toJson(),
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentType
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentTypeToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'is_shared': instance.isShared,
          'description': instance.description,
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentType
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentTypeToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'is_shared': instance.isShared,
          'description': instance.description,
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ChildContent
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ChildContentFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ChildContent()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ChildContentToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ChildContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ParentContent
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ParentContentFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ParentContentToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ParentContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'index': instance.index,
          'part_id': instance.partId,
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories$Category
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories$CategoryToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories()
    ..category =
        CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories$Category
            .fromJson(json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategoriesToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories
                instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentState
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentStateFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentStateToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentState
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$CollectionToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$CollectionsFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections()
    ..collection =
        CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection
            .fromJson(json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$CollectionsToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections
                instance) =>
        <String, dynamic>{
          'collection': instance.collection.toJson(),
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPartFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart()
    ..contentType =
        CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentType
            .fromJson(json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) =>
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ChildContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) =>
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ParentContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentState =
        CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentState
            .fromJson(json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) =>
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPartToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart
                instance) =>
        <String, dynamic>{
          'content_type': instance.contentType.toJson(),
          'updated_at': instance.updatedAt?.toIso8601String(),
          'name': instance.name,
          'is_shared': instance.isShared,
          'id': instance.id,
          'data': instance.data,
          'created_at': instance.createdAt.toIso8601String(),
          'child_content':
              instance.childContent.map((e) => e.toJson()).toList(),
          'parent_content':
              instance.parentContent.map((e) => e.toJson()).toList(),
          'content_categories':
              instance.contentCategories.map((e) => e.toJson()).toList(),
          'content_state': instance.contentState.toJson(),
          'collections': instance.collections.map((e) => e.toJson()).toList(),
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContentFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent()
    ..contentPart =
        CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart
            .fromJson(json['content_part'] as Map<String, dynamic>)
    ..id = json['id'] as String
    ..index = json['index'] as int;
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContentToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent
                instance) =>
        <String, dynamic>{
          'content_part': instance.contentPart.toJson(),
          'id': instance.id,
          'index': instance.index,
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$ParentContent
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ParentContentFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ParentContentToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ParentContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'index': instance.index,
          'part_id': instance.partId,
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories$Category
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories$CategoryToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories()
    ..category =
        CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories$Category
            .fromJson(json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategoriesToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories
                instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentState
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentStateFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentStateToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentState
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection$CollectionType
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection$CollectionTypeToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$CollectionToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$CollectionsFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections()
    ..collection =
        CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection
            .fromJson(json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$CollectionsToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections
                instance) =>
        <String, dynamic>{
          'collection': instance.collection.toJson(),
        };

CollectionContentById$QueryRoot$CollectionContentByPk$Content
    _$CollectionContentById$QueryRoot$CollectionContentByPk$ContentFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk$Content()
    ..contentType =
        CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentType
            .fromJson(json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) =>
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) =>
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ParentContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentState =
        CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentState
            .fromJson(json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) =>
            CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPk$ContentToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk$Content
                instance) =>
        <String, dynamic>{
          'content_type': instance.contentType.toJson(),
          'updated_at': instance.updatedAt?.toIso8601String(),
          'name': instance.name,
          'is_shared': instance.isShared,
          'id': instance.id,
          'data': instance.data,
          'created_at': instance.createdAt.toIso8601String(),
          'child_content':
              instance.childContent.map((e) => e.toJson()).toList(),
          'parent_content':
              instance.parentContent.map((e) => e.toJson()).toList(),
          'content_categories':
              instance.contentCategories.map((e) => e.toJson()).toList(),
          'content_state': instance.contentState.toJson(),
          'collections': instance.collections.map((e) => e.toJson()).toList(),
        };

CollectionContentById$QueryRoot$CollectionContentByPk
    _$CollectionContentById$QueryRoot$CollectionContentByPkFromJson(
        Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot$CollectionContentByPk()
    ..collection =
        CollectionContentById$QueryRoot$CollectionContentByPk$Collection
            .fromJson(json['collection'] as Map<String, dynamic>)
    ..id = json['id'] as String
    ..content =
        CollectionContentById$QueryRoot$CollectionContentByPk$Content.fromJson(
            json['content'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionContentById$QueryRoot$CollectionContentByPkToJson(
            CollectionContentById$QueryRoot$CollectionContentByPk instance) =>
        <String, dynamic>{
          'collection': instance.collection.toJson(),
          'id': instance.id,
          'content': instance.content.toJson(),
        };

CollectionContentById$QueryRoot _$CollectionContentById$QueryRootFromJson(
    Map<String, dynamic> json) {
  return CollectionContentById$QueryRoot()
    ..collectionContentByPk = json['collection_content_by_pk'] == null
        ? null
        : CollectionContentById$QueryRoot$CollectionContentByPk.fromJson(
            json['collection_content_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$CollectionContentById$QueryRootToJson(
        CollectionContentById$QueryRoot instance) =>
    <String, dynamic>{
      'collection_content_by_pk': instance.collectionContentByPk?.toJson(),
    };

CollectionContentByIdArguments _$CollectionContentByIdArgumentsFromJson(
    Map<String, dynamic> json) {
  return CollectionContentByIdArguments(
    id: json['id'] as String,
    contentStateNames: (json['contentStateNames'] as List<dynamic>?)
        ?.map((e) => e as String?)
        .toList(),
    contentOffset: json['contentOffset'] as int?,
    contentLimit: json['contentLimit'] as int?,
  );
}

Map<String, dynamic> _$CollectionContentByIdArgumentsToJson(
        CollectionContentByIdArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
      'contentStateNames': instance.contentStateNames,
      'contentOffset': instance.contentOffset,
      'contentLimit': instance.contentLimit,
    };
