// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'feedback_type_by_id.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class FeedbackTypeById$QueryRoot$FeedbackTypeByPk extends JsonSerializable
    with EquatableMixin {
  FeedbackTypeById$QueryRoot$FeedbackTypeByPk();

  factory FeedbackTypeById$QueryRoot$FeedbackTypeByPk.fromJson(
          Map<String, dynamic> json) =>
      _$FeedbackTypeById$QueryRoot$FeedbackTypeByPkFromJson(json);

  late String id;

  late String key;

  late String name;

  dynamic? schema;

  @override
  List<Object?> get props => [id, key, name, schema];
  @override
  Map<String, dynamic> toJson() =>
      _$FeedbackTypeById$QueryRoot$FeedbackTypeByPkToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FeedbackTypeById$QueryRoot extends JsonSerializable with EquatableMixin {
  FeedbackTypeById$QueryRoot();

  factory FeedbackTypeById$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$FeedbackTypeById$QueryRootFromJson(json);

  @JsonKey(name: 'feedback_type_by_pk')
  FeedbackTypeById$QueryRoot$FeedbackTypeByPk? feedbackTypeByPk;

  @override
  List<Object?> get props => [feedbackTypeByPk];
  @override
  Map<String, dynamic> toJson() => _$FeedbackTypeById$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FeedbackTypeByIdArguments extends JsonSerializable with EquatableMixin {
  FeedbackTypeByIdArguments({required this.id});

  @override
  factory FeedbackTypeByIdArguments.fromJson(Map<String, dynamic> json) =>
      _$FeedbackTypeByIdArgumentsFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$FeedbackTypeByIdArgumentsToJson(this);
}

final FEEDBACK_TYPE_BY_ID_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'FeedbackTypeById'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'feedback_type_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'id'),
                  value: VariableNode(name: NameNode(value: 'id')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class FeedbackTypeByIdQuery extends GraphQLQuery<FeedbackTypeById$QueryRoot,
    FeedbackTypeByIdArguments> {
  FeedbackTypeByIdQuery({required this.variables});

  @override
  final DocumentNode document = FEEDBACK_TYPE_BY_ID_QUERY_DOCUMENT;

  @override
  final String operationName = 'FeedbackTypeById';

  @override
  final FeedbackTypeByIdArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  FeedbackTypeById$QueryRoot parse(Map<String, dynamic> json) =>
      FeedbackTypeById$QueryRoot.fromJson(json);
}
