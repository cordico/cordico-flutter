// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'content_part_by_content_id.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentType
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentTypeToJson(
            ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'is_shared': instance.isShared,
          'description': instance.description,
        };

ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent$ContentPart
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent$ContentPartFromJson(
        Map<String, dynamic> json) {
  return ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent$ContentPart()
    ..data = json['data'];
}

Map<String, dynamic>
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent$ContentPartToJson(
            ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent$ContentPart
                instance) =>
        <String, dynamic>{
          'data': instance.data,
        };

ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContentFromJson(
        Map<String, dynamic> json) {
  return ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent()
    ..contentPart =
        ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent$ContentPart
            .fromJson(json['content_part'] as Map<String, dynamic>)
    ..id = json['id'] as String
    ..index = json['index'] as int;
}

Map<String, dynamic>
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContentToJson(
            ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent
                instance) =>
        <String, dynamic>{
          'content_part': instance.contentPart.toJson(),
          'id': instance.id,
          'index': instance.index,
        };

ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ParentContent
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ParentContentFromJson(
        Map<String, dynamic> json) {
  return ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic>
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ParentContentToJson(
            ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ParentContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'index': instance.index,
          'part_id': instance.partId,
        };

ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories$Category
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories$CategoryToJson(
            ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories()
    ..category =
        ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories$Category
            .fromJson(json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategoriesToJson(
            ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories
                instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentState
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentStateFromJson(
        Map<String, dynamic> json) {
  return ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentStateToJson(
            ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentState
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
        };

ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection$CollectionType
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection$CollectionTypeToJson(
            ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$CollectionToJson(
            ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$CollectionsFromJson(
        Map<String, dynamic> json) {
  return ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections()
    ..collection =
        ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections$Collection
            .fromJson(json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPart$CollectionsToJson(
            ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections
                instance) =>
        <String, dynamic>{
          'collection': instance.collection.toJson(),
        };

ContentPartByContentID$QueryRoot$ContentPart$ContentPart
    _$ContentPartByContentID$QueryRoot$ContentPart$ContentPartFromJson(
        Map<String, dynamic> json) {
  return ContentPartByContentID$QueryRoot$ContentPart$ContentPart()
    ..contentType =
        ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentType
            .fromJson(json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) =>
            ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ChildContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) =>
            ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ParentContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentCategories
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentState =
        ContentPartByContentID$QueryRoot$ContentPart$ContentPart$ContentState
            .fromJson(json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) =>
            ContentPartByContentID$QueryRoot$ContentPart$ContentPart$Collections
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String,
    dynamic> _$ContentPartByContentID$QueryRoot$ContentPart$ContentPartToJson(
        ContentPartByContentID$QueryRoot$ContentPart$ContentPart instance) =>
    <String, dynamic>{
      'content_type': instance.contentType.toJson(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'name': instance.name,
      'is_shared': instance.isShared,
      'id': instance.id,
      'data': instance.data,
      'created_at': instance.createdAt.toIso8601String(),
      'child_content': instance.childContent.map((e) => e.toJson()).toList(),
      'parent_content': instance.parentContent.map((e) => e.toJson()).toList(),
      'content_categories':
          instance.contentCategories.map((e) => e.toJson()).toList(),
      'content_state': instance.contentState.toJson(),
      'collections': instance.collections.map((e) => e.toJson()).toList(),
    };

ContentPartByContentID$QueryRoot$ContentPart
    _$ContentPartByContentID$QueryRoot$ContentPartFromJson(
        Map<String, dynamic> json) {
  return ContentPartByContentID$QueryRoot$ContentPart()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..contentPart =
        ContentPartByContentID$QueryRoot$ContentPart$ContentPart.fromJson(
            json['content_part'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ContentPartByContentID$QueryRoot$ContentPartToJson(
        ContentPartByContentID$QueryRoot$ContentPart instance) =>
    <String, dynamic>{
      'id': instance.id,
      'index': instance.index,
      'content_part': instance.contentPart.toJson(),
    };

ContentPartByContentID$QueryRoot _$ContentPartByContentID$QueryRootFromJson(
    Map<String, dynamic> json) {
  return ContentPartByContentID$QueryRoot()
    ..contentPart = (json['content_part'] as List<dynamic>)
        .map((e) => ContentPartByContentID$QueryRoot$ContentPart.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$ContentPartByContentID$QueryRootToJson(
        ContentPartByContentID$QueryRoot instance) =>
    <String, dynamic>{
      'content_part': instance.contentPart.map((e) => e.toJson()).toList(),
    };

ContentPartByContentIDArguments _$ContentPartByContentIDArgumentsFromJson(
    Map<String, dynamic> json) {
  return ContentPartByContentIDArguments(
    contentID: json['contentID'] as String,
    contentStateNames: (json['contentStateNames'] as List<dynamic>?)
        ?.map((e) => e as String?)
        .toList(),
    offset: json['offset'] as int?,
    limit: json['limit'] as int?,
  );
}

Map<String, dynamic> _$ContentPartByContentIDArgumentsToJson(
        ContentPartByContentIDArguments instance) =>
    <String, dynamic>{
      'contentID': instance.contentID,
      'contentStateNames': instance.contentStateNames,
      'offset': instance.offset,
      'limit': instance.limit,
    };
