import 'package:equatable/equatable.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

abstract class Failure extends Equatable {
  @override
  List<Object> get props => [];
}

class EmptyFailure extends Failure {}

class ServerFailure extends Failure {
  ServerFailure({this.exception});
  final OperationException? exception;

  @override
  String toString() {
    if (exception != null) {
      return exception?.graphqlErrors.map((e) => e.message).join('\n') ??
          super.toString();
    }
    return super.toString();
  }
}

class CacheFailure extends Failure {}
