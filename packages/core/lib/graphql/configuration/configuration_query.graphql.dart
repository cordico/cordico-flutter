// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'configuration_query.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class ConfigurationQuery$QueryRoot$Configuration$ConfigurationType
    extends JsonSerializable with EquatableMixin {
  ConfigurationQuery$QueryRoot$Configuration$ConfigurationType();

  factory ConfigurationQuery$QueryRoot$Configuration$ConfigurationType.fromJson(
          Map<String, dynamic> json) =>
      _$ConfigurationQuery$QueryRoot$Configuration$ConfigurationTypeFromJson(
          json);

  late String name;

  @override
  List<Object?> get props => [name];
  @override
  Map<String, dynamic> toJson() =>
      _$ConfigurationQuery$QueryRoot$Configuration$ConfigurationTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class ConfigurationQuery$QueryRoot$Configuration extends JsonSerializable
    with EquatableMixin {
  ConfigurationQuery$QueryRoot$Configuration();

  factory ConfigurationQuery$QueryRoot$Configuration.fromJson(
          Map<String, dynamic> json) =>
      _$ConfigurationQuery$QueryRoot$ConfigurationFromJson(json);

  late String name;

  @JsonKey(name: 'last_update_date')
  DateTime? lastUpdateDate;

  late String key;

  late String id;

  late dynamic data;

  @JsonKey(name: 'create_date')
  late DateTime createDate;

  @JsonKey(name: 'configuration_type_id')
  late String configurationTypeId;

  @JsonKey(name: 'configuration_type')
  late ConfigurationQuery$QueryRoot$Configuration$ConfigurationType
      configurationType;

  @override
  List<Object?> get props => [
        name,
        lastUpdateDate,
        key,
        id,
        data,
        createDate,
        configurationTypeId,
        configurationType
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$ConfigurationQuery$QueryRoot$ConfigurationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ConfigurationQuery$QueryRoot extends JsonSerializable
    with EquatableMixin {
  ConfigurationQuery$QueryRoot();

  factory ConfigurationQuery$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$ConfigurationQuery$QueryRootFromJson(json);

  late List<ConfigurationQuery$QueryRoot$Configuration> configuration;

  @override
  List<Object?> get props => [configuration];
  @override
  Map<String, dynamic> toJson() => _$ConfigurationQuery$QueryRootToJson(this);
}

final CONFIGURATION_QUERY_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'ConfigurationQuery'),
      variableDefinitions: [],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'configuration'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'last_update_date'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'create_date'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'configuration_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'configuration_type'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ]))
            ]))
      ]))
]);

class ConfigurationQueryQuery
    extends GraphQLQuery<ConfigurationQuery$QueryRoot, JsonSerializable> {
  ConfigurationQueryQuery();

  @override
  final DocumentNode document = CONFIGURATION_QUERY_QUERY_DOCUMENT;

  @override
  final String operationName = 'ConfigurationQuery';

  @override
  List<Object?> get props => [document, operationName];
  @override
  ConfigurationQuery$QueryRoot parse(Map<String, dynamic> json) =>
      ConfigurationQuery$QueryRoot.fromJson(json);
}
