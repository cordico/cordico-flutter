// ignore_for_file: lines_longer_than_80_chars, implementation_imports, avoid_bool_literals_in_conditional_expressions, cascade_invocations

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cordico_mobile/core/managers/cordico_alert.dart';
import 'package:cordico_mobile/core/managers/cordico_url.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/app_bar_provider.dart';
import 'package:cordico_mobile/core/widgets/buttons/action_button.dart';
import 'package:cordico_mobile/core/widgets/buttons/cordico_outlined_button.dart';
import 'package:cordico_mobile/core/widgets/card/hotline_card.dart';
import 'package:cordico_mobile/core/widgets/cordico_elevated_button.dart';
import 'package:cordico_mobile/features/connect/logic/directory_state.dart';
import 'package:cordico_mobile/features/content/views/plan_guide/plan_guide_details.dart';
import 'package:core/graphql/organization/organization_contact_by_id.dart';
import 'package:core/models/schema/content_schema.dart';
import 'package:core/models/schema/organization_contact_schema.dart';
import 'package:core/utils/constants.dart';
import 'package:core/utils/map_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_segment/flutter_segment.dart';
import 'package:vrouter/src/core/extended_context.dart';

class ContactDetailsPage extends ConsumerStatefulWidget {
  const ContactDetailsPage({Key? key}) : super(key: key);

  @override
  ConsumerState<ContactDetailsPage> createState() => _ContactDetailsPageState();
}

class _ContactDetailsPageState extends ConsumerState<ContactDetailsPage> {
  late OrganizationContactById$QueryRoot$OrganizationContactsByPk contact;

  OrganizationContactSchema get content {
    final content = OrganizationContactSchema.fromJson(
        contact.data as Map<String, dynamic>);
    return content;
  }

  ContentSchema get contactSchema {
    removeNullAndEmptyParams(contact.data as Map<String, dynamic>);
    final content =
        ContentSchema.fromJson(contact.data as Map<String, dynamic>);
    return content;
  }

  late bool isFromMap;

  @override
  Widget build(BuildContext context) {
    final id = context.vRouter.pathParameters['id'];

    isFromMap =
        context.vRouter.pathParameters['isFromMap'] == 'true' ? true : false;

    final contacts = ref
        .watch(allContactsProvider)
        .where((element) => element.id == id)
        .toList();

    if (contacts.isNotEmpty) {
      contact = contacts.first;

      Segment.track(eventName: 'View Contact', properties: <String, dynamic>{
        'id': contact.id,
        'type': contact.contactType.name,
        'name': contact.name
      });
    }
    return Scaffold(
      appBar: AppBarProvider.getAppBarWithBackIcon(
        context,
        backgroundColor: ThemeColors.gray.shade900,
        title: Text(
          contact.name,
          style: ThemeStyles.cardContentTitle(context)
              ?.copyWith(color: Colors.white),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 18),
        child: Column(
          children: [
            Expanded(
              child: ListView(
                shrinkWrap: true,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: _buildCategoryHeader(context),
                  ),
                  _buildContactDetails(context),
                  _buildDescription(context),
                  buildLocationSection(),
                ],
              ),
            ),
            _buildActions(context),
          ],
        ),
      ),
    );
  }

  Widget _buildCategoryHeader(BuildContext context) {
    return Row(
      children: [
        Flexible(
          child: Text(
            contact.contactType.displayName.replaceAll('-', ' ').toUpperCase(),
            style: ThemeStyles.cardCategory(context),
          ),
        ),
        /*if (distance != null)
          Padding(
            padding: const EdgeInsets.only(left: 8),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                const Icon(
                  Icons.map_outlined,
                  size: 11,
                  color: Colors.white,
                ),
                Container(width: 4),
                Text(
                  distance!,
                  style: ThemeStyles.sectionTitle(context)?.copyWith(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                ),
                const SizedBox(width: 2),
                Text(
                  'Miles',
                  style: ThemeStyles.sectionTitle(context)?.copyWith(
                    fontSize: 9,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        if (isNew)
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: NewBadge(),
          ),*/
      ],
    );
  }

  Widget _buildContactDetails(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (content.profileImageUrl != null &&
            content.profileImageUrl!.isNotEmpty)
          _buildProfileImage(context),
        if (content.profileImageUrl != null &&
            content.profileImageUrl!.isNotEmpty)
          Container(width: 12),
        Expanded(
          child: _buildTitleAndPhoneNumber(context),
        ),
      ],
    );
  }

  Widget _buildProfileImage(BuildContext context) {
    return Container(
      width: 58,
      height: 58,
      clipBehavior: Clip.antiAlias,
      decoration: BoxDecoration(
        color: ThemeColors.gray.shade300,
        borderRadius: BorderRadius.circular(12),
      ),
      child: CachedNetworkImage(
        imageUrl: content.profileImageUrl!,
        fit: BoxFit.cover,
        placeholder: (context, url) => Container(
          color: ThemeColors.gray.shade300,
        ),
        imageBuilder: (context, image) {
          return Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: image,
                fit: BoxFit.cover,
              ),
            ),
          );
        },
        errorWidget: (context, url, dynamic error) {
          return const Icon(Icons.error);
        },
      ),
    );
  }

  Widget _buildTitleAndPhoneNumber(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          content.name?.displayName ?? '',
          style: ThemeStyles.bulletinCardTitle(context)
              ?.copyWith(color: Colors.white),
        ),
        const SizedBox(height: 8),
        Text(
          content.title ?? '',
          style: ThemeStyles.smallParagraph(context)?.copyWith(
            color: ThemeColors.gray.shade300,
            fontSize: 14,
          ),
        ),
      ],
    );
  }

  Widget buildLocationSection() {
    if (content.address == null) {
      return Container();
    }
    return Padding(
      padding: const EdgeInsets.only(top: 40),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Location',
            style:
                ThemeStyles.cardTitle(context)?.copyWith(color: Colors.white),
          ),
          const SizedBox(height: 11),
          Text(
            content.address?.street ?? '',
            style: ThemeStyles.smallParagraph(context)
                ?.copyWith(color: Colors.white),
          ),
          const SizedBox(height: 2),
          Text(
            '${content.address?.city ?? ''}, ${content.address?.state ?? ''} ${content.address?.zip ?? ''}',
            style: ThemeStyles.smallParagraph(context)
                ?.copyWith(color: Colors.white),
          ),
          if (contact.contactType.id == kContactTypeTherapistId &&
              content.address != null &&
              isFromMap == false)
            Padding(
              padding: const EdgeInsets.only(top: 16),
              child: ActionButton(
                action: null,
                content: content,
                height: 37,
                isDefault: true,
                title: 'View on Map',
                onPressed: () {
                  context.vRouter.to('contacts_map/${contact.id}');
                },
                icon: const Icon(Icons.map_rounded,
                    color: ThemeColors.buttonsGreyFullyVisibleColor),
              ),
            ),
        ],
      ),
    );
  }

  Widget _buildDescription(BuildContext context) {
    if (content.summary != null &&
        content.summary!.body != null &&
        content.summary!.body!.isNotEmpty) {
      return Padding(
          padding: const EdgeInsets.only(top: 9),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              if (contact.contactType.id != kContactTypeChaplainId &&
                  contact.contactType.id != kContactTypePeerSupportId)
                Text(
                  content.contactPhone ?? '',
                  style: ThemeStyles.cardContentTitle(context)
                      ?.copyWith(color: ThemeColors.green.shade200),
                ),
              const SizedBox(height: 40),
              Text(
                content.summary?.body ?? '',
                style: ThemeStyles.contentDescription(context)?.copyWith(
                  color: ThemeColors.gray.shade200,
                ),
              ),
            ],
          )

          /*ContentWidget(
          content: content.summary!.body ?? '',
          contentType: (content.summary!.bodyMimeType == null ||
                  content.summary!.bodyMimeType!.isEmpty ||
                  content.summary!.bodyMimeType == 'text/markdown')
              ? MimeType.markdown
              : MimeType.html,
        ),*/
          );
    }
    return Container();
  }

  Widget _buildActions(BuildContext context) {
    final allButtons = List<ActionButtonSchema>.from(
        content.actionButtons ?? <ActionButtonSchema>[]);

    allButtons.sort((a, b) {
      return a.actionType == ActionButtonType.call ||
              a.actionType == ActionButtonType.anonymousCall
          ? -1
          : 1;
    });

    allButtons.sort((a, b) {
      return a.isDefault == true ? -1 : 1;
    });

    final defualtButtons = <ActionButtonSchema>[];
    final normalButtons = <ActionButtonSchema>[];

    var defualtButtonsLenght = 0;
    var normalButtonsLenght = 0;

    // ignore: avoid_function_literals_in_foreach_calls
    allButtons.forEach((element) {
      if (element.isDefault == true) {
        defualtButtonsLenght += 1;
        defualtButtons.add(element);
      } else {
        normalButtonsLenght += 1;
        normalButtons.add(element);
      }
    });

    final width = (MediaQuery.of(context).size.width - (18 * 2) - 16) / 2;
    final fullWidth = MediaQuery.of(context).size.width - (18 * 2);

    return Padding(
      padding: EdgeInsets.only(
        top: 8,
        bottom: MediaQuery.of(context).viewPadding.bottom + 12,
      ),
      child: Wrap(
        alignment: WrapAlignment.center,
        spacing: 16,
        runSpacing: 8,
        children: [
          if (defualtButtons.isNotEmpty)
            ...defualtButtons.asMap().entries.map((e) {
              final firstFullWidth =
                  (normalButtonsLenght + defualtButtonsLenght) % 2 != 0;

              return SizedBox(
                width: firstFullWidth && e.key == 0 ? fullWidth : width,
                child: ActionButton(
                  action: e.value,
                  content: null,
                  height: 36,
                ),
              );
            }).toList(),
          if (normalButtons.isNotEmpty)
            ...normalButtons.asMap().entries.map((e) {
              final firstFullWidth =
                  (normalButtonsLenght + defualtButtonsLenght) % 2 != 0;

              return SizedBox(
                width: firstFullWidth && e.key == 0 ? fullWidth : width,
                child: ActionButton(
                  action: e.value,
                  content: null,
                  height: 36,
                ),
              );
            }).toList()
        ],
      ),
    );
  }

  Widget buildActionButton(BuildContext context, ActionButtonSchema action) {
    final Widget? icon = action.materialIcon != null
        ? Icon(action.materialActionIcon,
            color: action.isDefault ?? false
                ? ThemeColors.buttonsGreyFullyVisibleColor
                : Colors.white)
        : null;

    return (action.isDefault ?? false)
        ? CordicoElevatedButton(
            color: Colors.white,
            onPressed: () =>
                handleAction(context, action: action, content: content),
            icon: icon,
            child: Text(
              action.label ?? '',
              style: ThemeStyles.textFieldTextStyle(context)?.copyWith(
                color: ThemeColors.buttonsGreyFullyVisibleColor,
              ),
            ),
          )
        : CordicoOutlinedButton(
            isFullyRounded: false,
            borderColor: Colors.white,
            textStyle: ThemeStyles.textFieldTextStyle(context)?.copyWith(
              color: Colors.white,
            ),
            text: action.label ?? '',
            onPressed: () =>
                handleAction(context, action: action, content: content),
            icon: icon,
          );
  }

  void handleAction(
    BuildContext context, {
    required ActionButtonSchema action,
    required OrganizationContactSchema? content,
  }) {
    if (action.actionType == ActionButtonType.call) {
      final phoneNumber =
          CordicoUrl.cleanPhone(phone: action.actionContext ?? '');
      CordicoUrl.call(phone: phoneNumber);
    } else if (action.actionType == ActionButtonType.anonymousCall) {
      if (action.actionContext != null && action.actionContext!.isNotEmpty) {
        CordicoAlert.showCallAlert(
          context,
          phone: action.actionContext ?? '',
          action: action,
        );
      }
    } else if (action.actionType == ActionButtonType.loadContent) {
      Navigator.push<dynamic>(
        context,
        MaterialPageRoute<dynamic>(
          builder: (context) => PlanGuideDetails(
            content: action.actionContent ?? '',
          ),
          fullscreenDialog: true,
        ),
      );
    } else if (action.actionType == ActionButtonType.email) {
      CordicoUrl.sendEmail(mail: action.actionContext ?? '');
    } else if (action.actionType == ActionButtonType.sms) {
      CordicoUrl.sendSMS(phone: action.actionContext ?? '');
    }
  }

  Future<void> showMapContactModal(
    BuildContext context,
    OrganizationContactById$QueryRoot$OrganizationContactsByPk contact,
  ) {
    return showModalBottomSheet(
      context: context,
      useRootNavigator: true,
      builder: (ctx) {
        return StatefulBuilder(
          builder: (context, stateSet) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  height: 5,
                  width: 61,
                  margin: const EdgeInsets.only(top: 10),
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.5),
                    borderRadius: BorderRadius.circular(3),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: 8,
                    bottom: MediaQuery.of(ctx).viewPadding.bottom + 12,
                  ),
                  child: HotlineCard(contact: contact),
                ),
              ],
            );
          },
        );
      },
      elevation: 10,
      barrierColor: Colors.white10,
      backgroundColor: const Color(0xFF17171A),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(8),
          topRight: Radius.circular(8),
        ),
      ),
    );
  }
}
