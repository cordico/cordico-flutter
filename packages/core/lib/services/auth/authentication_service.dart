import 'package:core/models/schema/user_schema.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:kratos_api/kratos_api.dart';

abstract class IAuthenticationService {
  Future<UserSchema?> login(final String identifier, final String password);

  Future<UserSchema?> session(final String sessionToken);
}

class AuthenticationService implements IAuthenticationService {
  AuthenticationService(final String kratosUrl) {
    final dio = Dio(BaseOptions(baseUrl: kratosUrl));
    api = KratosApi(dio: dio, serializers: standardSerializers);
    vAlphaApi = api.getV0alpha2Api();
  }

  late KratosApi api;
  late V0alpha2Api vAlphaApi;

  @override
  Future<UserSchema?> session(final String sessionToken) async {
    try {
      final serviceResponse =
          await vAlphaApi.toSession(xSessionToken: sessionToken);
      if (serviceResponse.data != null) {
        final map = serviceResponse.data?.identity.traits?.asMap;
        if (map != null) {
          final mapString = <String, dynamic>{};
          map.forEach((dynamic key, dynamic value) {
            mapString['$key'] = value;
          });
          mapString['sessionToken'] = sessionToken;
          return UserSchema.fromJson(mapString);
        }
      }
      return null;
    } catch (e) {
      debugPrint('$e');
      return null;
    }
  }

  @override
  Future<UserSchema?> login(
      final String identifier, final String password) async {
    try {
      final response =
          await vAlphaApi.initializeSelfServiceLoginFlowWithoutBrowser();
      if (response.statusCode == 200) {
        final flow = response.data;

        if (flow != null) {
          final body = SubmitSelfServiceLoginFlowBody((builder) {
            builder
              ..method = 'password'
              ..passwordIdentifier = identifier
              ..password = password;
          });

          final serviceResponse = await vAlphaApi.submitSelfServiceLoginFlow(
              flow: flow.id, submitSelfServiceLoginFlowBody: body);

          if (serviceResponse.data != null) {
            final map = serviceResponse.data?.session.identity.traits?.asMap;
            if (map != null) {
              final mapString = <String, dynamic>{};
              map.forEach((dynamic key, dynamic value) {
                mapString['$key'] = value;
              });
              mapString['sessionToken'] = serviceResponse.data?.sessionToken;
              return UserSchema.fromJson(mapString);
            }
          }
        }
      }

      return null;
    } catch (e) {
      debugPrint('$e');
      return null;
    }
  }
}
