import 'package:core/graphql/content/content_type_by_id.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'content_types_holder.g.dart';

@JsonSerializable(explicitToJson: true)
class ContentTypesHolder extends JsonSerializable {
  ContentTypesHolder({this.contentTypes});
  
  factory ContentTypesHolder.fromJson(Map<String, dynamic> json) =>
      _$ContentTypesHolderFromJson(json);

  List<ContentTypeById$QueryRoot$ContentTypeByPk>? contentTypes;
  List<ContentTypeById$QueryRoot$ContentTypeByPk>? get filterContentTypes {
    final mainContentTypos = contentTypes?.where((element) {
      return element.parentTypeId == null;
    }).toList();
    return mainContentTypos;
  }

  List<String>? get filterContentTypesIds {
    final mainContentTypos = contentTypes?.where((element) {
      return element.parentTypeId == null;
    }).toList();
    return mainContentTypos?.map((e) => e.id).toList();
  }

  @override
  Map<String, dynamic> toJson() => _$ContentTypesHolderToJson(this);
}
