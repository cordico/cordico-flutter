// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'collection_content.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Collection$CollectionType();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Collection
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Collection();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$CollectionFromJson(
          json);

  late String id;

  late String name;

  @JsonKey(name: 'collection_type')
  CollectionContentQuery$QueryRoot$CollectionContent$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [id, name, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentType
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentType();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentTypeFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentType
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentType();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentTypeFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ChildContent
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ChildContent();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ChildContentFromJson(
          json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ChildContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ParentContent
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ParentContent();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ParentContentFromJson(
          json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ParentContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories$Category();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategoriesFromJson(
          json);

  late CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories$Category
      category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategoriesToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentState
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentState();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentStateFromJson(
          json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentStateToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection$CollectionType();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$CollectionFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$CollectionsFromJson(
          json);

  late CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections$Collection
      collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$CollectionsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPartFromJson(
          json);

  @JsonKey(name: 'content_type')
  late CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentType
      contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<
          CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ChildContent>
      childContent;

  @JsonKey(name: 'parent_content')
  late List<
          CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ParentContent>
      parentContent;

  @JsonKey(name: 'content_categories')
  late List<
          CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$ContentState
      contentState;

  late List<
          CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart$Collections>
      collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPartToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContentFromJson(
          json);

  @JsonKey(name: 'content_part')
  late CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent$ContentPart
      contentPart;

  late String id;

  late int index;

  @override
  List<Object?> get props => [contentPart, id, index];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$ParentContent
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$ParentContent();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ParentContentFromJson(
          json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ParentContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories$Category();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategoriesFromJson(
          json);

  late CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories$Category
      category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategoriesToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentState
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentState();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentStateFromJson(
          json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentStateToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection$CollectionType();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$CollectionFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$CollectionsFromJson(
          json);

  late CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections$Collection
      collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$Content$CollectionsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent$Content
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent$Content();

  factory CollectionContentQuery$QueryRoot$CollectionContent$Content.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$ContentFromJson(
          json);

  @JsonKey(name: 'content_type')
  late CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentType
      contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<
          CollectionContentQuery$QueryRoot$CollectionContent$Content$ChildContent>
      childContent;

  @JsonKey(name: 'parent_content')
  late List<
          CollectionContentQuery$QueryRoot$CollectionContent$Content$ParentContent>
      parentContent;

  @JsonKey(name: 'content_categories')
  late List<
          CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late CollectionContentQuery$QueryRoot$CollectionContent$Content$ContentState
      contentState;

  late List<
          CollectionContentQuery$QueryRoot$CollectionContent$Content$Collections>
      collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContent$ContentToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot$CollectionContent
    extends JsonSerializable with EquatableMixin {
  CollectionContentQuery$QueryRoot$CollectionContent();

  factory CollectionContentQuery$QueryRoot$CollectionContent.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRoot$CollectionContentFromJson(json);

  late CollectionContentQuery$QueryRoot$CollectionContent$Collection collection;

  late String id;

  late CollectionContentQuery$QueryRoot$CollectionContent$Content content;

  @override
  List<Object?> get props => [collection, id, content];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRoot$CollectionContentToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQuery$QueryRoot extends JsonSerializable
    with EquatableMixin {
  CollectionContentQuery$QueryRoot();

  factory CollectionContentQuery$QueryRoot.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentQuery$QueryRootFromJson(json);

  @JsonKey(name: 'collection_content')
  late List<CollectionContentQuery$QueryRoot$CollectionContent>
      collectionContent;

  @override
  List<Object?> get props => [collectionContent];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQuery$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentQueryArguments extends JsonSerializable
    with EquatableMixin {
  CollectionContentQueryArguments(
      {this.contentStateNames, this.contentOffset, this.contentLimit});

  @override
  factory CollectionContentQueryArguments.fromJson(Map<String, dynamic> json) =>
      _$CollectionContentQueryArgumentsFromJson(json);

  final List<String?>? contentStateNames;

  final int? contentOffset;

  final int? contentLimit;

  @override
  List<Object?> get props => [contentStateNames, contentOffset, contentLimit];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentQueryArgumentsToJson(this);
}

final COLLECTION_CONTENT_QUERY_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'CollectionContentQuery'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentStateNames')),
            type: ListTypeNode(
                type: NamedTypeNode(
                    name: NameNode(value: 'String'), isNonNull: false),
                isNonNull: false),
            defaultValue: DefaultValueNode(
                value: ListValueNode(values: [
              StringValueNode(value: 'Published', isBlock: false)
            ])),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentOffset')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '0')),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentLimit')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '10000')),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'collection_content'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'collection'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'collection_type'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'description'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ]))
                  ])),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'content'),
                  alias: null,
                  arguments: [
                    ArgumentNode(
                        name: NameNode(value: 'offset'),
                        value: VariableNode(
                            name: NameNode(value: 'contentOffset'))),
                    ArgumentNode(
                        name: NameNode(value: 'limit'),
                        value: VariableNode(
                            name: NameNode(value: 'contentLimit'))),
                    ArgumentNode(
                        name: NameNode(value: 'where'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'content_state'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'name'),
                                    value: ObjectValueNode(fields: [
                                      ObjectFieldNode(
                                          name: NameNode(value: '_in'),
                                          value: VariableNode(
                                              name: NameNode(
                                                  value: 'contentStateNames')))
                                    ]))
                              ]))
                        ]))
                  ],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'content_type'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'is_shared'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'description'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ])),
                    FieldNode(
                        name: NameNode(value: 'updated_at'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'is_shared'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'data'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'created_at'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'child_content'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'content_part'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'content_type'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'is_shared'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'description'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ])),
                                FieldNode(
                                    name: NameNode(value: 'updated_at'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'is_shared'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'data'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'created_at'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'child_content'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ])),
                                FieldNode(
                                    name: NameNode(value: 'parent_content'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'index'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'part_id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ])),
                                FieldNode(
                                    name: NameNode(value: 'content_categories'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'category'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'name'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'description'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null)
                                          ]))
                                    ])),
                                FieldNode(
                                    name: NameNode(value: 'content_state'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ])),
                                FieldNode(
                                    name: NameNode(value: 'collections'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'collection'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'name'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'collection_type'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: SelectionSetNode(
                                                    selections: [
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'name'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'id'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value:
                                                                  'description'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null)
                                                    ]))
                                          ]))
                                    ]))
                              ])),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'index'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ])),
                    FieldNode(
                        name: NameNode(value: 'parent_content'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'index'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'part_id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ])),
                    FieldNode(
                        name: NameNode(value: 'content_categories'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'category'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'description'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ]))
                        ])),
                    FieldNode(
                        name: NameNode(value: 'content_state'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ])),
                    FieldNode(
                        name: NameNode(value: 'collections'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'collection'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'collection_type'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'description'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ]))
                              ]))
                        ]))
                  ]))
            ]))
      ]))
]);

class CollectionContentQueryQuery extends GraphQLQuery<
    CollectionContentQuery$QueryRoot, CollectionContentQueryArguments> {
  CollectionContentQueryQuery({required this.variables});

  @override
  final DocumentNode document = COLLECTION_CONTENT_QUERY_QUERY_DOCUMENT;

  @override
  final String operationName = 'CollectionContentQuery';

  @override
  final CollectionContentQueryArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  CollectionContentQuery$QueryRoot parse(Map<String, dynamic> json) =>
      CollectionContentQuery$QueryRoot.fromJson(json);
}
