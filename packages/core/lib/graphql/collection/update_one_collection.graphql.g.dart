// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'update_one_collection.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateOneCollection$MutationRoot$UpdateOneCollection
    _$UpdateOneCollection$MutationRoot$UpdateOneCollectionFromJson(
        Map<String, dynamic> json) {
  return UpdateOneCollection$MutationRoot$UpdateOneCollection()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$UpdateOneCollection$MutationRoot$UpdateOneCollectionToJson(
            UpdateOneCollection$MutationRoot$UpdateOneCollection instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

UpdateOneCollection$MutationRoot _$UpdateOneCollection$MutationRootFromJson(
    Map<String, dynamic> json) {
  return UpdateOneCollection$MutationRoot()
    ..updateOneCollection = json['updateOneCollection'] == null
        ? null
        : UpdateOneCollection$MutationRoot$UpdateOneCollection.fromJson(
            json['updateOneCollection'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UpdateOneCollection$MutationRootToJson(
        UpdateOneCollection$MutationRoot instance) =>
    <String, dynamic>{
      'updateOneCollection': instance.updateOneCollection?.toJson(),
    };

UpdateOneCollectionArguments _$UpdateOneCollectionArgumentsFromJson(
    Map<String, dynamic> json) {
  return UpdateOneCollectionArguments(
    id: json['id'] as String,
    isShared: json['isShared'] as bool,
    key: json['key'] as String,
    name: json['name'] as String,
    title: json['title'] as String,
    collectionTypeId: json['collectionTypeId'] as String,
  );
}

Map<String, dynamic> _$UpdateOneCollectionArgumentsToJson(
        UpdateOneCollectionArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
      'isShared': instance.isShared,
      'key': instance.key,
      'name': instance.name,
      'title': instance.title,
      'collectionTypeId': instance.collectionTypeId,
    };
