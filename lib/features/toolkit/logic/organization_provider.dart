import 'package:cordico_mobile/core/providers/global_providers.dart';
import 'package:cordico_mobile/features/connect/logic/organization_contacts_state.dart';
import 'package:cordico_mobile/features/connect/logic/organization_state_notifier.dart';
import 'package:cordico_mobile/features/toolkit/logic/organization_content_state.dart';
import 'package:cordico_mobile/features/toolkit/logic/organization_state.dart';
import 'package:cordico_mobile/features/toolkit/logic/organization_state_notifier.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final organizationContactsNotifierProvider =
    Provider<OrganizationContactsStateNotifier>((ref) {
  final notifier = OrganizationContactsStateNotifier(
      repository: ref.watch(organizationRepositoryProvider));
  return notifier;
});

// * Logic holder / StateNotifier
final organizationContactsStateNotifierProvider = StateNotifierProvider<
    OrganizationContactsStateNotifier, OrganizationContactsState>(
  (ref) => ref.watch(organizationContactsNotifierProvider),
);

final organizationNotifierProvider = Provider<OrganizationStateNotifier>((ref) {
  final notifier = OrganizationStateNotifier(
    organizationRepository: ref.watch(organizationRepositoryProvider),
  );
  return notifier;
});

// * Logic holder / StateNotifier
final organizationStateNotifierProvider =
    StateNotifierProvider<OrganizationStateNotifier, OrganizationState>(
  (ref) => ref.watch(organizationNotifierProvider),
);

// * Logic holder / StateNotifier
final organizatioinContentNotifierProvider =
    ChangeNotifierProvider<OrganizationContentState>(
  (ref) => OrganizationContentState(
      collectionRepository: ref.watch(collectionRepositoryProvider)),
);
