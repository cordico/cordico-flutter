import 'package:collection/collection.dart';
import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/images.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/article_item_list_item.dart';
import 'package:cordico_mobile/core/widgets/expandable_bottom_sheet.dart';
import 'package:cordico_mobile/features/content/logic/content_provider.dart';
import 'package:cordico_mobile/features/content/views/article/content_part_widget.dart';
import 'package:cordico_mobile/features/department/logic/department_provider.dart';
import 'package:cordico_mobile/features/discover/logic/content_filter_state.dart';
import 'package:core/core.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/graphql/content_part/content_part_by_content_id.dart';
import 'package:core/utils/map_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_segment/flutter_segment.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:vrouter/vrouter.dart';

class ArticlePage extends ConsumerStatefulWidget {
  const ArticlePage({Key? key}) : super(key: key);

  @override
  ConsumerState<ArticlePage> createState() => _ArticlePageState();
}

class _ArticlePageState extends ConsumerState<ArticlePage> {
  final _mediaExpansionController = ExpansionController();

  late PageController pageController;

  late ContentSchema contentSchema;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var index = 0;
    if (context.vRouter.pathParameters.containsKey('index')) {
      index = int.parse(context.vRouter.pathParameters['index']!);
    }

    //print('index: $index');
    //print('contentId: $contentId');

    Segment.screen(screenName: 'Article', properties: <String, dynamic>{
      'type': 'screen',
      'name': 'Article',
      'index': index
    });

    pageController = PageController(initialPage: index);

    return Consumer(
        builder: (BuildContext context, WidgetRef ref, Widget? child) {
      final contentID = ref.watch(contentIdProvider);
      //final contentValue = ref.watch(contentByIdProvider);

      ContentQuery$QueryRoot$Content? content;

      final source = context.vRouter.pathParameters['source'];

      if (source == 'eap') {
        final state = ref.watch(departmentPageStateNotifierProvider);
        final id = context.vRouter.pathParameters['id'];
        content =
            state.eapResources?.firstWhereOrNull((element) => element.id == id);
      } else {
        final state = ref.watch(allContentProvider);
        content = state.fromAllCategoriesContent
            .firstWhereOrNull((element) => element.id == contentID);
      }

      if (content == null) {
        return const SizedBox.shrink();
      }
      final contentParts = content.childContent.map((e) {
        final map = e.toJson();
        return ContentPartModel.fromJson(map);
      }).toList();

      removeNullAndEmptyParams(content.data as Map<String, dynamic>);
      contentSchema =
          ContentSchema.fromJson(content.data as Map<String, dynamic>);

      final mainContent =
          ContentQuery$QueryRoot$Content.fromJson(content.toJson());

      Segment.track(eventName: 'View Content', properties: <String, dynamic>{
        'id': mainContent.id,
        'type': mainContent.contentType.name,
        'name': mainContent.name
      });

      return AnnotatedRegion<SystemUiOverlayStyle>(
          value: const SystemUiOverlayStyle(
            statusBarBrightness: Brightness.light,
            statusBarIconBrightness: Brightness.light,
          ),
          child: Scaffold(
            backgroundColor: Colors.white,
            body: Stack(children: [
              SafeArea(
                child: _buildContent(context,
                    content: mainContent,
                    contentParts: contentParts,
                    pageController: pageController,
                    index: index),
              ),
              if (contentParts.isNotEmpty)
                Align(
                  alignment: Alignment.bottomCenter,
                  child: _buildIndexExpandableSheet(
                      context, contentParts, mainContent),
                )
            ]),
          ));
    });
  }

  PageView _buildPageView(
    BuildContext context, {
    required PageController pageController,
    ContentQuery$QueryRoot$Content? mainContent,
    List<ContentPartModel>? contentParts,
  }) {
    final pages = <Widget>[];
    var index = 0;

    final parts = contentParts ?? <ContentPartModel>[];

    // ignore: cascade_invocations
    parts.sort((a, b) => a.index.compareTo(b.index));

    for (final part in parts) {
      ContentPartModel? nextPart;
      if (index < parts.length - 1) {
        nextPart = parts[index + 1];
      }

      final content =
          ContentQuery$QueryRoot$Content.fromJson(part.contentPart.toJson());
      final nextContent = nextPart != null
          ? ContentQuery$QueryRoot$Content.fromJson(
              nextPart.contentPart.toJson())
          : null;
      pages.add(
        ContentPartWidget(
            content: content,
            mainContent: mainContent,
            index: index,
            pageController: pageController,
            nextContent: nextContent,
            updatePage: () {
              WidgetsBinding.instance?.addPostFrameCallback((_) {
                if (mounted) {
                  setState(() {});
                }
              });
            }),
      );
      ++index;
    }

    return PageView(
      controller: pageController,
      children: pages,
    );
  }

  Widget _buildContent(
    BuildContext context, {
    required PageController pageController,
    ContentQuery$QueryRoot$Content? content,
    List<ContentPartModel>? contentParts,
    required int index,
  }) {
    Widget contentWidget;
    if (contentParts != null && contentParts.isNotEmpty) {
      contentWidget = _buildPageView(
        context,
        pageController: pageController,
        mainContent: content,
        contentParts: contentParts,
      );
    } else {
      contentWidget = ContentPartWidget(
        content: content,
        mainContent: content,
        index: index,
        pageController: pageController,
      );
    }

    return Padding(
      padding: EdgeInsets.only(bottom: contentParts?.isEmpty == true ? 0 : 60),
      child: contentWidget,
    );
  }

  Widget _buildIndexExpandableSheet(
    BuildContext context,
    List<ContentPartModel>? contentParts,
    ContentQuery$QueryRoot$Content? content,
  ) {
    final media = MediaQuery.of(context);
    return ExpandableBottomSheet(
      controller: _mediaExpansionController,
      persistentContentHeight: 60,
      maxHeight: media.size.height - media.padding.top - media.padding.bottom,
      persistentHeader: _buildIndexHeader(context),
      onExtendedCallback: () {},
      expandableContent: Container(
        padding: EdgeInsets.zero,
        color: ThemeColors.gray.shade900,
        child: _buildIndexContent(
          context,
          contentParts: contentParts,
          content: content,
        ),
      ),
    );
  }

  Widget _buildIndexHeader(BuildContext context) {
    return GestureDetector(
      onTap: _mediaExpansionController.toggle,
      behavior: HitTestBehavior.translucent,
      child: Container(
        height: 60,
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(24), topLeft: Radius.circular(24)),
          color: ThemeColors.buttonsGreyFullyVisibleColor,
        ),
        child: Row(
          children: [
            Image.asset(Images.icIndex, height: 24),
            Container(width: 8),
            Expanded(
              child: Text(
                'Index',
                style: ThemeStyles.smallParagraph(context)?.copyWith(
                  color: ThemeColors.blue.shade50,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildIndexContent(
    BuildContext context, {
    List<ContentPartModel>? contentParts,
    ContentQuery$QueryRoot$Content? content,
  }) {
    final readContentParts =
        global.prefs.getStringList(content?.id ?? '') ?? [];

    if (contentParts != null && contentParts.isNotEmpty) {
      contentParts.sort((ContentPartByContentID$QueryRoot$ContentPart a,
          ContentPartByContentID$QueryRoot$ContentPart b) {
        return a.index < b.index ? 0 : 1;
      });

      return ListView.builder(
        padding: EdgeInsets.zero,
        itemBuilder: (BuildContext context, int index) {
          if (index == 0) {
            return _buildIndexTitle(
              context,
              title: contentSchema.title ?? '',
            );
          }

          return _buildArticleItem(
            context,
            ContentQuery$QueryRoot$Content.fromJson(
              contentParts[index - 1].contentPart.toJson(),
            ),
            isRead: readContentParts
                .contains(contentParts[index - 1].contentPart.id),
            index: index - 1,
          );
        },
        itemCount: contentParts.length + 1,
      );
    }
    return Container();
  }

  Widget _buildIndexTitle(
    BuildContext context, {
    required String title,
  }) {
    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            title,
            style: ThemeStyles.bulletinCardTitle(context)
                ?.copyWith(fontSize: 27, color: Colors.white),
          ),
        ],
      ),
    );
  }

  Widget _buildArticleItem(
      BuildContext context, ContentQuery$QueryRoot$Content? part,
      {required int index, bool isRead = false}) {
    final partContentSchema =
        ContentSchema.fromJson(part?.data as Map<String, dynamic>);

    var type = 'Text';
    if ((partContentSchema.videoUrl != null &&
            partContentSchema.videoUrl!.isNotEmpty) ||
        (partContentSchema.videoUrl1 != null &&
            partContentSchema.videoUrl1!.isNotEmpty)) {
      type = 'Video';
    } else if (partContentSchema.audioUrl != null &&
        partContentSchema.audioUrl!.isNotEmpty) {
      type = 'Audio';
    }

    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        _mediaExpansionController.toggle();
        Future.delayed(const Duration(milliseconds: 400), () {
          pageController.jumpToPage(index);
        });
      },
      child: ArticleItemListItem(
        content: part,
        type: type,
        isRead: isRead,
        contentSchema: partContentSchema,
      ),
    );
  }
}
