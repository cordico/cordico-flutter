// ignore_for_file: cascade_invocations

import 'package:collection/collection.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:cordico_mobile/core/managers/offlline_manager.dart';
import 'package:core/core.dart';
import 'package:core/utils/constants.dart';
import 'package:flutter/cupertino.dart';

class OrganizationContentState extends ChangeNotifier {
  OrganizationContentState(
      {required ICollectionRepository collectionRepository})
      : _collectionRepository = collectionRepository;

  bool _isLoading = false;
  bool get isLoading => _isLoading;

  bool _contentLoaded = false;
  bool get contentLoaded => _contentLoaded;

  final ICollectionRepository _collectionRepository;

  CollectionModel? cordicoFeatured;
  CollectionModel? whatsNew;
  CollectionModel? newsFeed;

  void reset() {
    _contentLoaded = false;
    notifyListeners();
  }

  Future<void> getContent() async {
    _isLoading = true;

    final connectityCheck = await Connectivity().checkConnectivity();

    if (connectityCheck == ConnectivityResult.none) {
      final c1 = OfflineManager.getData(key: ModelsKeys.cordicoFeatured);
      if (c1 != null) {
        cordicoFeatured = CollectionModel.fromJson(c1);
      }

      final c2 = OfflineManager.getData(key: ModelsKeys.whatsNew);
      if (c2 != null) {
        whatsNew = CollectionModel.fromJson(c2);
      }

      final c3 = OfflineManager.getData(key: ModelsKeys.newsFeed);
      if (c3 != null) {
        newsFeed = CollectionModel.fromJson(c3);
      }
    } else {
      if (contentLoaded == false) {
        await Future.wait([
          _collectionRepository.getCollectionById(kCollectionCordicoFeaturedId),
          _collectionRepository.getCollectionById(kCollectionWhatsNewId),
          _collectionRepository
              .getCollectionByCollectionTypeKey(kCollectionBulletinTypeKey),
        ]).then((value) {
          value[0].fold((_) {}, (l) {
            cordicoFeatured = l as CollectionModel?;
            OfflineManager.persistData(
              key: ModelsKeys.cordicoFeatured,
              value: cordicoFeatured?.toJson(),
            );
          });
          value[1].fold((_) {}, (l) {
            whatsNew = l as CollectionModel?;
            OfflineManager.persistData(
              key: ModelsKeys.whatsNew,
              value: whatsNew?.toJson(),
            );
          });
          value[2].fold((_) {}, (l) {
            final cList = l as List<CollectionModel>?;
            if (cList != null && cList.isNotEmpty) {
              newsFeed = cList.firstOrNull;

              cList.mapIndexed((index, element) {
                if (index > 0) {
                  newsFeed?.collectionContents
                      .addAll(element.collectionContents);
                }
              });

              OfflineManager.persistData(
                key: ModelsKeys.newsFeed,
                value: newsFeed?.toJson(),
              );
            }
          });
        });

        _contentLoaded = true;
      }
    }
    notifyListeners();
  }
}
