// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'asset_by_id.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class AssetById$QueryRoot$AssetByPk extends JsonSerializable
    with EquatableMixin {
  AssetById$QueryRoot$AssetByPk();

  factory AssetById$QueryRoot$AssetByPk.fromJson(Map<String, dynamic> json) =>
      _$AssetById$QueryRoot$AssetByPkFromJson(json);

  @JsonKey(name: 'asset_type_id')
  late String assetTypeId;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'creator_id')
  late String creatorId;

  late dynamic data;

  @JsonKey(name: 'file_name')
  late String fileName;

  String? hash;

  late String id;

  @JsonKey(name: 'mime_type')
  late String mimeType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @JsonKey(name: 'updater_id')
  String? updaterId;

  String? url;

  @override
  List<Object?> get props => [
        assetTypeId,
        createdAt,
        creatorId,
        data,
        fileName,
        hash,
        id,
        mimeType,
        updatedAt,
        updaterId,
        url
      ];
  @override
  Map<String, dynamic> toJson() => _$AssetById$QueryRoot$AssetByPkToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AssetById$QueryRoot extends JsonSerializable with EquatableMixin {
  AssetById$QueryRoot();

  factory AssetById$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$AssetById$QueryRootFromJson(json);

  @JsonKey(name: 'asset_by_pk')
  AssetById$QueryRoot$AssetByPk? assetByPk;

  @override
  List<Object?> get props => [assetByPk];
  @override
  Map<String, dynamic> toJson() => _$AssetById$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AssetByIdArguments extends JsonSerializable with EquatableMixin {
  AssetByIdArguments({required this.id});

  @override
  factory AssetByIdArguments.fromJson(Map<String, dynamic> json) =>
      _$AssetByIdArgumentsFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$AssetByIdArgumentsToJson(this);
}

final ASSET_BY_ID_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'AssetById'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'asset_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'id'),
                  value: VariableNode(name: NameNode(value: 'id')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'asset_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'creator_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'file_name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'hash'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'mime_type'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updater_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'url'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class AssetByIdQuery
    extends GraphQLQuery<AssetById$QueryRoot, AssetByIdArguments> {
  AssetByIdQuery({required this.variables});

  @override
  final DocumentNode document = ASSET_BY_ID_QUERY_DOCUMENT;

  @override
  final String operationName = 'AssetById';

  @override
  final AssetByIdArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  AssetById$QueryRoot parse(Map<String, dynamic> json) =>
      AssetById$QueryRoot.fromJson(json);
}
