// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'category_api.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CategoryQuery$QueryRoot$Category extends JsonSerializable
    with EquatableMixin {
  CategoryQuery$QueryRoot$Category();

  factory CategoryQuery$QueryRoot$Category.fromJson(
          Map<String, dynamic> json) =>
      _$CategoryQuery$QueryRoot$CategoryFromJson(json);

  late String id;

  late String name;

  late String description;

  late String key;

  @override
  List<Object?> get props => [id, name, description, key];
  @override
  Map<String, dynamic> toJson() =>
      _$CategoryQuery$QueryRoot$CategoryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CategoryQuery$QueryRoot extends JsonSerializable with EquatableMixin {
  CategoryQuery$QueryRoot();

  factory CategoryQuery$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$CategoryQuery$QueryRootFromJson(json);

  late List<CategoryQuery$QueryRoot$Category> category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() => _$CategoryQuery$QueryRootToJson(this);
}

final CATEGORY_QUERY_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'CategoryQuery'),
      variableDefinitions: [],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'category'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'description'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class CategoryQueryQuery
    extends GraphQLQuery<CategoryQuery$QueryRoot, JsonSerializable> {
  CategoryQueryQuery();

  @override
  final DocumentNode document = CATEGORY_QUERY_QUERY_DOCUMENT;

  @override
  final String operationName = 'CategoryQuery';

  @override
  List<Object?> get props => [document, operationName];
  @override
  CategoryQuery$QueryRoot parse(Map<String, dynamic> json) =>
      CategoryQuery$QueryRoot.fromJson(json);
}
