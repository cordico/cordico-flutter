// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'update_organization_by_id.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class UpdateOrganizationById$MutationRoot$UpdateOneOrganization
    extends JsonSerializable with EquatableMixin {
  UpdateOrganizationById$MutationRoot$UpdateOneOrganization();

  factory UpdateOrganizationById$MutationRoot$UpdateOneOrganization.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateOrganizationById$MutationRoot$UpdateOneOrganizationFromJson(json);

  late String name;

  late String id;

  String? description;

  late dynamic data;

  late String organizationTypeId;

  @override
  List<Object?> get props => [name, id, description, data, organizationTypeId];
  @override
  Map<String, dynamic> toJson() =>
      _$UpdateOrganizationById$MutationRoot$UpdateOneOrganizationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateOrganizationById$MutationRoot extends JsonSerializable
    with EquatableMixin {
  UpdateOrganizationById$MutationRoot();

  factory UpdateOrganizationById$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateOrganizationById$MutationRootFromJson(json);

  UpdateOrganizationById$MutationRoot$UpdateOneOrganization?
      updateOneOrganization;

  @override
  List<Object?> get props => [updateOneOrganization];
  @override
  Map<String, dynamic> toJson() =>
      _$UpdateOrganizationById$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateOrganizationByIdArguments extends JsonSerializable
    with EquatableMixin {
  UpdateOrganizationByIdArguments(
      {this.id,
      this.data,
      this.description,
      this.name,
      this.organization_type_id});

  @override
  factory UpdateOrganizationByIdArguments.fromJson(Map<String, dynamic> json) =>
      _$UpdateOrganizationByIdArgumentsFromJson(json);

  final String? id;

  final dynamic? data;

  final String? description;

  final String? name;

  final String? organization_type_id;

  @override
  List<Object?> get props =>
      [id, data, description, name, organization_type_id];
  @override
  Map<String, dynamic> toJson() =>
      _$UpdateOrganizationByIdArgumentsToJson(this);
}

final UPDATE_ORGANIZATION_BY_ID_MUTATION_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'UpdateOrganizationById'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'data')),
            type:
                NamedTypeNode(name: NameNode(value: 'Json'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'description')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'name')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable:
                VariableNode(name: NameNode(value: 'organization_type_id')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'updateOneOrganization'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'id'),
                        value: VariableNode(name: NameNode(value: 'id')))
                  ])),
              ArgumentNode(
                  name: NameNode(value: 'data'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'data'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'set'),
                              value:
                                  VariableNode(name: NameNode(value: 'data')))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'description'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'set'),
                              value: VariableNode(
                                  name: NameNode(value: 'description')))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'name'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'set'),
                              value:
                                  VariableNode(name: NameNode(value: 'name')))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'organizationType'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'connect'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'id'),
                                    value: VariableNode(
                                        name: NameNode(
                                            value: 'organization_type_id')))
                              ]))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'description'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'organizationTypeId'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class UpdateOrganizationByIdMutation extends GraphQLQuery<
    UpdateOrganizationById$MutationRoot, UpdateOrganizationByIdArguments> {
  UpdateOrganizationByIdMutation({required this.variables});

  @override
  final DocumentNode document = UPDATE_ORGANIZATION_BY_ID_MUTATION_DOCUMENT;

  @override
  final String operationName = 'UpdateOrganizationById';

  @override
  final UpdateOrganizationByIdArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  UpdateOrganizationById$MutationRoot parse(Map<String, dynamic> json) =>
      UpdateOrganizationById$MutationRoot.fromJson(json);
}
