// ignore_for_file: implementation_imports, unused_import

import 'package:cordico_mobile/core/managers/cordico_alert.dart';
import 'package:cordico_mobile/core/managers/cordico_url.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/buttons/cordico_outlined_button.dart';
import 'package:cordico_mobile/core/widgets/cordico_elevated_button.dart';
import 'package:cordico_mobile/features/content/views/plan_guide/plan_guide_details.dart';
import 'package:core/models/schema/content_schema.dart';
import 'package:core/models/schema/organization_contact_schema.dart';
import 'package:flutter/material.dart';
import 'package:vrouter/src/core/extended_context.dart';

class ActionButton extends StatelessWidget {
  const ActionButton({
    Key? key,
    required this.action,
    required this.content,
    this.isDefault,
    this.onPressed,
    this.title,
    this.icon,
    this.height,
  }) : super(key: key);

  final ActionButtonSchema? action;
  final OrganizationContactSchema? content;
  final Function()? onPressed;
  final String? title;
  final bool? isDefault;
  final Widget? icon;
  final double? height;

  @override
  Widget build(BuildContext context) {
    return buildActionButton(context);
  }

  Widget buildActionButton(BuildContext context) {
    final Widget? actionIcon =
        action?.materialActionIcon != null && action?.materialIcon != null
            ? Icon(action?.materialActionIcon,
                color: (isDefault ?? action?.isDefault ?? false)
                    ? ThemeColors.buttonsGreyFullyVisibleColor
                    : Colors.white)
            : null;

    return (isDefault ?? action?.isDefault ?? false)
        ? SizedBox(
            height: height,
            child: CordicoElevatedButton(
              color: Colors.white,
              onPressed: onPressed ??
                  () =>
                      handleAction(context, action: action!, content: content),
              icon: icon ?? actionIcon,
              child: Text(
                title ?? action?.label ?? '',
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: ThemeStyles.textFieldTextStyle(context)?.copyWith(
                  color: ThemeColors.buttonsGreyFullyVisibleColor,
                ),
              ),
            ),
          )
        : SizedBox(
            height: height,
            child: CordicoOutlinedButton(
              isFullyRounded: false,
              borderColor: Colors.white,
              textStyle: ThemeStyles.textFieldTextStyle(context)?.copyWith(
                color: Colors.white,
              ),
              text: title ?? action?.label ?? '',
              onPressed: onPressed ??
                  () =>
                      handleAction(context, action: action!, content: content),
              icon: icon ?? actionIcon,
            ),
          );
  }

  void handleAction(
    BuildContext context, {
    required ActionButtonSchema action,
    required OrganizationContactSchema? content,
  }) {
    if (action.actionType == ActionButtonType.call) {
      final phoneNumber =
          CordicoUrl.cleanPhone(phone: action.actionContext ?? '');

      CordicoUrl.call(phone: phoneNumber);
    } else if (action.actionType == ActionButtonType.anonymousCall) {
      if (action.actionContext != null && action.actionContext!.isNotEmpty) {
        CordicoAlert.showCallAlert(
          context,
          phone: action.actionContext ?? '',
          action: action,
        );
      }
    } else if (action.actionType == ActionButtonType.loadContent) {
      Navigator.push<dynamic>(
        context,
        MaterialPageRoute<dynamic>(
          builder: (context) => PlanGuideDetails(
            content: action.actionContent ?? '',
          ),
          fullscreenDialog: true,
        ),
      );
    } else if (action.actionType == ActionButtonType.email) {
      CordicoUrl.sendEmail(mail: action.actionContext ?? '');
    } else if (action.actionType == ActionButtonType.sms) {
      CordicoUrl.sendSMS(phone: action.actionContext ?? '');
    }
  }
}
