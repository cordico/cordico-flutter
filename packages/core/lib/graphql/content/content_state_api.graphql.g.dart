// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'content_state_api.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContentState$QueryRoot$ContentState
    _$ContentState$QueryRoot$ContentStateFromJson(Map<String, dynamic> json) {
  return ContentState$QueryRoot$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic> _$ContentState$QueryRoot$ContentStateToJson(
        ContentState$QueryRoot$ContentState instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };

ContentState$QueryRoot _$ContentState$QueryRootFromJson(
    Map<String, dynamic> json) {
  return ContentState$QueryRoot()
    ..contentState = (json['content_state'] as List<dynamic>)
        .map((e) => ContentState$QueryRoot$ContentState.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$ContentState$QueryRootToJson(
        ContentState$QueryRoot instance) =>
    <String, dynamic>{
      'content_state': instance.contentState.map((e) => e.toJson()).toList(),
    };
