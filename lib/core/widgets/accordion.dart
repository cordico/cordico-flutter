import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/images.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/badge/new_badge.dart';
import 'package:cordico_mobile/core/widgets/buttons/cordico_outlined_button.dart';
import 'package:flutter/material.dart';

class Accordion extends StatefulWidget {
  const Accordion({
    Key? key,
    required this.title,
    required this.onExpanded,
    this.child,
    this.numberOfNewItems = 0,
    this.isExpanded = false,
    this.showArchiveButton = false,
    this.onViewArchiveTap,
    this.buttonTitle = 'View Archive',
    this.forceExpanded = false,
  }) : super(key: key);

  final String title;
  final String buttonTitle;
  final int numberOfNewItems;
  final VoidCallback onExpanded;
  final Widget? child;
  final bool isExpanded;
  final bool forceExpanded;
  final bool showArchiveButton;
  final Function()? onViewArchiveTap;

  @override
  State createState() => AccordionState();
}

class AccordionState extends State<Accordion>
    with SingleTickerProviderStateMixin {
  bool isExpanded = false;

  late AnimationController expandController;
  late Animation<double> animation;

  @override
  void initState() {
    isExpanded = widget.isExpanded;
    prepareAnimations();

    expandController.value = isExpanded ? 1 : 0;
    //runExpandCheck();
    if (widget.forceExpanded) {
      expandController.value = 1;
    }
    super.initState();
  }

  void prepareAnimations() {
    expandController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));

    animation = CurvedAnimation(
      parent: expandController,
      curve: Curves.linear,
    );
  }

  void runExpandCheck() {
    if (isExpanded) {
      expandController.forward();
    } else {
      expandController.reverse();
    }
  }

  void expand() {
    if (isExpanded == false) {
      expandController.forward();
      isExpanded = true;
    }
  }

  void collapse() {
    if (isExpanded == true) {
      expandController.reverse();
      isExpanded = false;
    }
  }

  @override
  void didUpdateWidget(Accordion oldWidget) {
    super.didUpdateWidget(oldWidget);
    // if (widget.forceExpanded == false) {
    //   runExpandCheck();
    // }
  }

  @override
  void dispose() {
    expandController.dispose();
    super.dispose();
  }

  void _onExpandButtonPressed() {
    if (widget.forceExpanded == false) {
      isExpanded = !isExpanded;
      runExpandCheck();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(bottom: 4),
      child: Column(
        children: [
          InkWell(
            onTap: _onExpandButtonPressed,
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
              color: ThemeColors.gray.shade600,
              child: Row(
                children: [
                  Expanded(
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        FittedBox(
                          fit: BoxFit.fitWidth,
                          child: Text(
                            widget.title,
                            style: ThemeStyles.sectionTitle(context),
                          ),
                        ),
                        if (widget.numberOfNewItems != 0)
                          Padding(
                            padding: const EdgeInsets.only(left: 12),
                            child: NewBadge(
                              numberOfNewItems: widget.numberOfNewItems,
                              compact: false,
                            ),
                          ),
                      ],
                    ),
                  ),
                  Container(width: 12),
                  if (widget.forceExpanded == false)
                    Container(
                      padding: const EdgeInsets.all(8),
                      child: RotationTransition(
                        turns: Tween(begin: 0.0.toDouble(), end: 0.5)
                            .animate(expandController),
                        child: Image.asset(
                          Images.icChevronDown,
                          fit: BoxFit.contain,
                          width: 20,
                          color: ThemeColors.blue.shade50,
                        ),
                      ),
                    ),
                ],
              ),
            ),
          ),
          if (widget.child != null)
            SizeTransition(
              axisAlignment: 1,
              sizeFactor: animation,
              child: Column(
                children: [
                  if (widget.child != null) widget.child!,
                  if (widget.showArchiveButton) _buildViewArchiveButton(context)
                ],
              ),
            ),
        ],
      ),
    );
  }

  Widget _buildViewArchiveButton(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(right: 12, top: 22, bottom: 12),
      color: ThemeColors.gray.shade600,
      child: Align(
        alignment: Alignment.bottomRight,
        child: SizedBox(
          width: 125,
          child: CordicoOutlinedButton(
            borderColor: Colors.white,
            height: 28,
            text: widget.buttonTitle,
            textStyle: ThemeStyles.fieldLabel(context)?.copyWith(
              color: Colors.white,
            ),
            isFullyRounded: false,
            onPressed: () {
              if (widget.onViewArchiveTap != null) {
                widget.onViewArchiveTap!();
              }
            },
          ),
        ),
      ),
    );
  }
}
