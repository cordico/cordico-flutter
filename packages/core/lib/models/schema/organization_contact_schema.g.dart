// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'organization_contact_schema.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrganizationContactSchema _$OrganizationContactSchemaFromJson(
    Map<String, dynamic> json) {
  return OrganizationContactSchema()
    ..email = json['email'] as String?
    ..title = json['title'] as String?
    ..name = json['name'] == null
        ? null
        : NameSchemaContainer.fromJson(json['name'] as Map<String, dynamic>)
    ..address = json['address'] == null
        ? null
        : AddressSchema.fromJson(json['address'] as Map<String, dynamic>)
    ..summary = json['summary'] == null
        ? null
        : SummarySchemaContainer.fromJson(
            json['summary'] as Map<String, dynamic>)
    ..contactPhone = json['contact_phone'] as String?
    ..profileImageUrl = json['profile_image_url'] as String?
    ..actionButtons = (json['action_buttons'] as List<dynamic>?)
        ?.map((e) => ActionButtonSchema.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$OrganizationContactSchemaToJson(
        OrganizationContactSchema instance) =>
    <String, dynamic>{
      'email': instance.email,
      'title': instance.title,
      'name': instance.name?.toJson(),
      'address': instance.address?.toJson(),
      'summary': instance.summary?.toJson(),
      'contact_phone': instance.contactPhone,
      'profile_image_url': instance.profileImageUrl,
      'action_buttons': instance.actionButtons?.map((e) => e.toJson()).toList(),
    };

AddressSchema _$AddressSchemaFromJson(Map<String, dynamic> json) {
  return AddressSchema()
    ..zip = json['zip'] as String?
    ..city = json['city'] as String?
    ..state = json['state'] as String?
    ..street = json['street'] as String?
    ..latitude = json['latitude'] as num?
    ..longitude = json['longitude'] as num?;
}

Map<String, dynamic> _$AddressSchemaToJson(AddressSchema instance) =>
    <String, dynamic>{
      'zip': instance.zip,
      'city': instance.city,
      'state': instance.state,
      'street': instance.street,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
    };

NameSchemaContainer _$NameSchemaContainerFromJson(Map<String, dynamic> json) {
  return NameSchemaContainer()
    ..lastName = json['last_name'] as String?
    ..firstName = json['first_name'] as String?
    ..displayName = json['display_name'] as String?;
}

Map<String, dynamic> _$NameSchemaContainerToJson(
        NameSchemaContainer instance) =>
    <String, dynamic>{
      'last_name': instance.lastName,
      'first_name': instance.firstName,
      'display_name': instance.displayName,
    };

SummarySchemaContainer _$SummarySchemaContainerFromJson(
    Map<String, dynamic> json) {
  $checkKeys(json, disallowNullValues: const ['body_mime_type']);
  return SummarySchemaContainer()
    ..body = json['body'] as String?
    ..bodyMimeType = _$enumDecodeNullable(
        _$MimeTypeEnumMap, json['body_mime_type'],
        unknownValue: MimeType.markdown);
}

Map<String, dynamic> _$SummarySchemaContainerToJson(
    SummarySchemaContainer instance) {
  final val = <String, dynamic>{
    'body': instance.body,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('body_mime_type', _$MimeTypeEnumMap[instance.bodyMimeType]);
  return val;
}

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

K? _$enumDecodeNullable<K, V>(
  Map<K, V> enumValues,
  dynamic source, {
  K? unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<K, V>(enumValues, source, unknownValue: unknownValue);
}

const _$MimeTypeEnumMap = {
  MimeType.html: 'text/html',
  MimeType.markdown: 'text/markdown',
};
