// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'oc_by_oid_ctid.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class OCByOIdCtId$QueryRoot$OrganizationContacts$ContactType
    extends JsonSerializable with EquatableMixin {
  OCByOIdCtId$QueryRoot$OrganizationContacts$ContactType();

  factory OCByOIdCtId$QueryRoot$OrganizationContacts$ContactType.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdCtId$QueryRoot$OrganizationContacts$ContactTypeFromJson(json);

  @JsonKey(name: 'display_name')
  late String displayName;

  late String id;

  late String name;

  @override
  List<Object?> get props => [displayName, id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdCtId$QueryRoot$OrganizationContacts$ContactTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdCtId$QueryRoot$OrganizationContacts$Organization
    extends JsonSerializable with EquatableMixin {
  OCByOIdCtId$QueryRoot$OrganizationContacts$Organization();

  factory OCByOIdCtId$QueryRoot$OrganizationContacts$Organization.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdCtId$QueryRoot$OrganizationContacts$OrganizationFromJson(json);

  late String name;

  @override
  List<Object?> get props => [name];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdCtId$QueryRoot$OrganizationContacts$OrganizationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdCtId$QueryRoot$OrganizationContacts extends JsonSerializable
    with EquatableMixin {
  OCByOIdCtId$QueryRoot$OrganizationContacts();

  factory OCByOIdCtId$QueryRoot$OrganizationContacts.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdCtId$QueryRoot$OrganizationContactsFromJson(json);

  dynamic? data;

  late String title;

  @JsonKey(name: 'contact_type_id')
  late String contactTypeId;

  @JsonKey(name: 'contact_type')
  late OCByOIdCtId$QueryRoot$OrganizationContacts$ContactType contactType;

  @JsonKey(name: 'organization_id')
  late String organizationId;

  late OCByOIdCtId$QueryRoot$OrganizationContacts$Organization organization;

  late String name;

  @JsonKey(name: 'is_active')
  late bool isActive;

  late String id;

  DateTime? updated;

  late DateTime created;

  @override
  List<Object?> get props => [
        data,
        title,
        contactTypeId,
        contactType,
        organizationId,
        organization,
        name,
        isActive,
        id,
        updated,
        created
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdCtId$QueryRoot$OrganizationContactsToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdCtId$QueryRoot extends JsonSerializable with EquatableMixin {
  OCByOIdCtId$QueryRoot();

  factory OCByOIdCtId$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$OCByOIdCtId$QueryRootFromJson(json);

  @JsonKey(name: 'organization_contacts')
  late List<OCByOIdCtId$QueryRoot$OrganizationContacts> organizationContacts;

  @override
  List<Object?> get props => [organizationContacts];
  @override
  Map<String, dynamic> toJson() => _$OCByOIdCtId$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdCtIdArguments extends JsonSerializable with EquatableMixin {
  OCByOIdCtIdArguments(
      {required this.organizationID,
      required this.contact_type_id,
      this.isActive,
      this.offset,
      this.limit});

  @override
  factory OCByOIdCtIdArguments.fromJson(Map<String, dynamic> json) =>
      _$OCByOIdCtIdArgumentsFromJson(json);

  late String organizationID;

  late String contact_type_id;

  final List<bool>? isActive;

  final int? offset;

  final int? limit;

  @override
  List<Object?> get props =>
      [organizationID, contact_type_id, isActive, offset, limit];
  @override
  Map<String, dynamic> toJson() => _$OCByOIdCtIdArgumentsToJson(this);
}

final O_C_BY_O_ID_CT_ID_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'OCByOIdCtId'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'organizationID')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contact_type_id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'isActive')),
            type: ListTypeNode(
                type: NamedTypeNode(
                    name: NameNode(value: 'Boolean'), isNonNull: true),
                isNonNull: false),
            defaultValue: DefaultValueNode(
                value: ListValueNode(values: [BooleanValueNode(value: true)])),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'offset')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '0')),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'limit')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '10000')),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'organization_contacts'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'offset'),
                  value: VariableNode(name: NameNode(value: 'offset'))),
              ArgumentNode(
                  name: NameNode(value: 'limit'),
                  value: VariableNode(name: NameNode(value: 'limit'))),
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'organization_id'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: '_eq'),
                              value: VariableNode(
                                  name: NameNode(value: 'organizationID')))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'contact_type_id'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: '_eq'),
                              value: VariableNode(
                                  name: NameNode(value: 'contact_type_id')))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'is_active'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: '_in'),
                              value: VariableNode(
                                  name: NameNode(value: 'isActive')))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'title'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'contact_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'contact_type'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'display_name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'organization_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'organization'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'is_active'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class OCByOIdCtIdQuery
    extends GraphQLQuery<OCByOIdCtId$QueryRoot, OCByOIdCtIdArguments> {
  OCByOIdCtIdQuery({required this.variables});

  @override
  final DocumentNode document = O_C_BY_O_ID_CT_ID_QUERY_DOCUMENT;

  @override
  final String operationName = 'OCByOIdCtId';

  @override
  final OCByOIdCtIdArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  OCByOIdCtId$QueryRoot parse(Map<String, dynamic> json) =>
      OCByOIdCtId$QueryRoot.fromJson(json);
}
