// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'oc_by_oid_and_ctype.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentType
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentType();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentTypeFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentTypeFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContentFromJson(
          json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContentFromJson(
          json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategoriesFromJson(
          json);

  late OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category
      category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategoriesToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentStateFromJson(
          json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentStateToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$CollectionFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$CollectionsFromJson(
          json);

  late OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection
      collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$CollectionsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPartFromJson(
          json);

  @JsonKey(name: 'content_type')
  late OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentType
      contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<
          OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ChildContent>
      childContent;

  @JsonKey(name: 'parent_content')
  late List<
          OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ParentContent>
      parentContent;

  @JsonKey(name: 'content_categories')
  late List<
          OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$ContentState
      contentState;

  late List<
          OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart$Collections>
      collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPartToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContentFromJson(
          json);

  @JsonKey(name: 'content_part')
  late OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent$ContentPart
      contentPart;

  late String id;

  late int index;

  @override
  List<Object?> get props => [contentPart, id, index];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ParentContent
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ParentContent();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ParentContentFromJson(
          json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ParentContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories$Category();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategoriesFromJson(
          json);

  late OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories$Category
      category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategoriesToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentState
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentState();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentStateFromJson(
          json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentStateToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection$CollectionType();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$CollectionFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$CollectionsFromJson(
          json);

  late OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections$Collection
      collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$CollectionsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$ContentFromJson(
          json);

  @JsonKey(name: 'content_type')
  late OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentType
      contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<
          OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ChildContent>
      childContent;

  @JsonKey(name: 'parent_content')
  late List<
          OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ParentContent>
      parentContent;

  @JsonKey(name: 'content_categories')
  late List<
          OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$ContentState
      contentState;

  late List<
          OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content$Collections>
      collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$ContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContentsFromJson(
          json);

  late String id;

  late OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents$Content
      content;

  @override
  List<Object?> get props => [id, content];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContentsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection
    extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$CollectionFromJson(
          json);

  late String id;

  @JsonKey(name: 'collection_contents')
  late List<
          OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection$CollectionContents>
      collectionContents;

  @override
  List<Object?> get props => [id, collectionContents];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollections$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot$OrganizationCollections extends JsonSerializable
    with EquatableMixin {
  OCByOIdAndCType$QueryRoot$OrganizationCollections();

  factory OCByOIdAndCType$QueryRoot$OrganizationCollections.fromJson(
          Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollectionsFromJson(json);

  late OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$OCByOIdAndCType$QueryRoot$OrganizationCollectionsToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCType$QueryRoot extends JsonSerializable with EquatableMixin {
  OCByOIdAndCType$QueryRoot();

  factory OCByOIdAndCType$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$OCByOIdAndCType$QueryRootFromJson(json);

  @JsonKey(name: 'organization_collections')
  late List<OCByOIdAndCType$QueryRoot$OrganizationCollections>
      organizationCollections;

  @override
  List<Object?> get props => [organizationCollections];
  @override
  Map<String, dynamic> toJson() => _$OCByOIdAndCType$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OCByOIdAndCTypeArguments extends JsonSerializable with EquatableMixin {
  OCByOIdAndCTypeArguments(
      {this.limit,
      this.offset,
      this.organization_id,
      this.collection_type,
      this.contentStateNames});

  @override
  factory OCByOIdAndCTypeArguments.fromJson(Map<String, dynamic> json) =>
      _$OCByOIdAndCTypeArgumentsFromJson(json);

  final int? limit;

  final int? offset;

  final String? organization_id;

  final String? collection_type;

  final List<String?>? contentStateNames;

  @override
  List<Object?> get props =>
      [limit, offset, organization_id, collection_type, contentStateNames];
  @override
  Map<String, dynamic> toJson() => _$OCByOIdAndCTypeArgumentsToJson(this);
}

final O_C_BY_O_ID_AND_C_TYPE_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'OCByOIdAndCType'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'limit')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue:
                DefaultValueNode(value: IntValueNode(value: '100000')),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'offset')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '0')),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'organization_id')),
            type:
                NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'collection_type')),
            type:
                NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentStateNames')),
            type: ListTypeNode(
                type: NamedTypeNode(
                    name: NameNode(value: 'String'), isNonNull: false),
                isNonNull: false),
            defaultValue: DefaultValueNode(
                value: ListValueNode(values: [
              StringValueNode(value: 'Published', isBlock: false)
            ])),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'organization_collections'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'organization_id'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: '_eq'),
                              value: VariableNode(
                                  name: NameNode(value: 'organization_id')))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'collection'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'collection_type'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'id'),
                                    value: ObjectValueNode(fields: [
                                      ObjectFieldNode(
                                          name: NameNode(value: '_eq'),
                                          value: VariableNode(
                                              name: NameNode(
                                                  value: 'collection_type')))
                                    ]))
                              ]))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'collection'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'collection_contents'),
                        alias: null,
                        arguments: [
                          ArgumentNode(
                              name: NameNode(value: 'limit'),
                              value:
                                  VariableNode(name: NameNode(value: 'limit'))),
                          ArgumentNode(
                              name: NameNode(value: 'offset'),
                              value: VariableNode(
                                  name: NameNode(value: 'offset'))),
                          ArgumentNode(
                              name: NameNode(value: 'where'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'content'),
                                    value: ObjectValueNode(fields: [
                                      ObjectFieldNode(
                                          name:
                                              NameNode(value: 'content_state'),
                                          value: ObjectValueNode(fields: [
                                            ObjectFieldNode(
                                                name: NameNode(value: 'name'),
                                                value: ObjectValueNode(fields: [
                                                  ObjectFieldNode(
                                                      name: NameNode(
                                                          value: '_in'),
                                                      value: VariableNode(
                                                          name: NameNode(
                                                              value:
                                                                  'contentStateNames')))
                                                ]))
                                          ]))
                                    ]))
                              ]))
                        ],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'content'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'content_type'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'is_shared'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'description'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ])),
                                FieldNode(
                                    name: NameNode(value: 'updated_at'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'is_shared'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'data'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'created_at'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'child_content'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'content_part'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'content_type'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: SelectionSetNode(
                                                    selections: [
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'name'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'id'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value:
                                                                  'is_shared'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value:
                                                                  'description'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null)
                                                    ])),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'updated_at'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'name'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'is_shared'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'data'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'created_at'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'child_content'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: SelectionSetNode(
                                                    selections: [
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'id'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null)
                                                    ])),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'parent_content'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: SelectionSetNode(
                                                    selections: [
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'id'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'index'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'part_id'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null)
                                                    ])),
                                            FieldNode(
                                                name: NameNode(
                                                    value:
                                                        'content_categories'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: SelectionSetNode(
                                                    selections: [
                                                      FieldNode(
                                                          name: NameNode(
                                                              value:
                                                                  'category'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet:
                                                              SelectionSetNode(
                                                                  selections: [
                                                                FieldNode(
                                                                    name: NameNode(
                                                                        value:
                                                                            'name'),
                                                                    alias: null,
                                                                    arguments: [],
                                                                    directives: [],
                                                                    selectionSet:
                                                                        null),
                                                                FieldNode(
                                                                    name: NameNode(
                                                                        value:
                                                                            'id'),
                                                                    alias: null,
                                                                    arguments: [],
                                                                    directives: [],
                                                                    selectionSet:
                                                                        null),
                                                                FieldNode(
                                                                    name: NameNode(
                                                                        value:
                                                                            'description'),
                                                                    alias: null,
                                                                    arguments: [],
                                                                    directives: [],
                                                                    selectionSet:
                                                                        null)
                                                              ]))
                                                    ])),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'content_state'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: SelectionSetNode(
                                                    selections: [
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'id'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'name'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null)
                                                    ])),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'collections'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: SelectionSetNode(
                                                    selections: [
                                                      FieldNode(
                                                          name: NameNode(
                                                              value:
                                                                  'collection'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet:
                                                              SelectionSetNode(
                                                                  selections: [
                                                                FieldNode(
                                                                    name: NameNode(
                                                                        value:
                                                                            'name'),
                                                                    alias: null,
                                                                    arguments: [],
                                                                    directives: [],
                                                                    selectionSet:
                                                                        null),
                                                                FieldNode(
                                                                    name: NameNode(
                                                                        value:
                                                                            'id'),
                                                                    alias: null,
                                                                    arguments: [],
                                                                    directives: [],
                                                                    selectionSet:
                                                                        null),
                                                                FieldNode(
                                                                    name: NameNode(
                                                                        value:
                                                                            'collection_type'),
                                                                    alias: null,
                                                                    arguments: [],
                                                                    directives: [],
                                                                    selectionSet:
                                                                        SelectionSetNode(
                                                                            selections: [
                                                                          FieldNode(
                                                                              name: NameNode(value: 'name'),
                                                                              alias: null,
                                                                              arguments: [],
                                                                              directives: [],
                                                                              selectionSet: null),
                                                                          FieldNode(
                                                                              name: NameNode(value: 'id'),
                                                                              alias: null,
                                                                              arguments: [],
                                                                              directives: [],
                                                                              selectionSet: null),
                                                                          FieldNode(
                                                                              name: NameNode(value: 'description'),
                                                                              alias: null,
                                                                              arguments: [],
                                                                              directives: [],
                                                                              selectionSet: null)
                                                                        ]))
                                                              ]))
                                                    ]))
                                          ])),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'index'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ])),
                                FieldNode(
                                    name: NameNode(value: 'parent_content'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'index'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'part_id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ])),
                                FieldNode(
                                    name: NameNode(value: 'content_categories'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'category'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'name'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'description'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null)
                                          ]))
                                    ])),
                                FieldNode(
                                    name: NameNode(value: 'content_state'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ])),
                                FieldNode(
                                    name: NameNode(value: 'collections'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'collection'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'name'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'collection_type'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: SelectionSetNode(
                                                    selections: [
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'name'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'id'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value:
                                                                  'description'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null)
                                                    ]))
                                          ]))
                                    ]))
                              ]))
                        ]))
                  ]))
            ]))
      ]))
]);

class OCByOIdAndCTypeQuery
    extends GraphQLQuery<OCByOIdAndCType$QueryRoot, OCByOIdAndCTypeArguments> {
  OCByOIdAndCTypeQuery({required this.variables});

  @override
  final DocumentNode document = O_C_BY_O_ID_AND_C_TYPE_QUERY_DOCUMENT;

  @override
  final String operationName = 'OCByOIdAndCType';

  @override
  final OCByOIdAndCTypeArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  OCByOIdAndCType$QueryRoot parse(Map<String, dynamic> json) =>
      OCByOIdAndCType$QueryRoot.fromJson(json);
}
