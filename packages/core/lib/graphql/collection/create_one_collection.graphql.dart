// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'create_one_collection.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CreateOneCollection$MutationRoot$CreateOneCollection
    extends JsonSerializable with EquatableMixin {
  CreateOneCollection$MutationRoot$CreateOneCollection();

  factory CreateOneCollection$MutationRoot$CreateOneCollection.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneCollection$MutationRoot$CreateOneCollectionFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneCollection$MutationRoot$CreateOneCollectionToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneCollection$MutationRoot extends JsonSerializable
    with EquatableMixin {
  CreateOneCollection$MutationRoot();

  factory CreateOneCollection$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneCollection$MutationRootFromJson(json);

  late CreateOneCollection$MutationRoot$CreateOneCollection createOneCollection;

  @override
  List<Object?> get props => [createOneCollection];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneCollection$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneCollectionArguments extends JsonSerializable
    with EquatableMixin {
  CreateOneCollectionArguments(
      {required this.collectionTypeId,
      required this.isShared,
      required this.key,
      required this.name,
      required this.title});

  @override
  factory CreateOneCollectionArguments.fromJson(Map<String, dynamic> json) =>
      _$CreateOneCollectionArgumentsFromJson(json);

  late String collectionTypeId;

  late bool isShared;

  late String key;

  late String name;

  late String title;

  @override
  List<Object?> get props => [collectionTypeId, isShared, key, name, title];
  @override
  Map<String, dynamic> toJson() => _$CreateOneCollectionArgumentsToJson(this);
}

final CREATE_ONE_COLLECTION_MUTATION_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'CreateOneCollection'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'collectionTypeId')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'isShared')),
            type: NamedTypeNode(
                name: NameNode(value: 'Boolean'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'key')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'name')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'title')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'createOneCollection'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'data'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'name'),
                        value: VariableNode(name: NameNode(value: 'name'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'title'),
                        value: VariableNode(name: NameNode(value: 'title'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'isShared'),
                        value: VariableNode(name: NameNode(value: 'isShared'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'key'),
                        value: VariableNode(name: NameNode(value: 'key'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'collectionType'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'connect'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'id'),
                                    value: VariableNode(
                                        name: NameNode(
                                            value: 'collectionTypeId')))
                              ]))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class CreateOneCollectionMutation extends GraphQLQuery<
    CreateOneCollection$MutationRoot, CreateOneCollectionArguments> {
  CreateOneCollectionMutation({required this.variables});

  @override
  final DocumentNode document = CREATE_ONE_COLLECTION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'CreateOneCollection';

  @override
  final CreateOneCollectionArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  CreateOneCollection$MutationRoot parse(Map<String, dynamic> json) =>
      CreateOneCollection$MutationRoot.fromJson(json);
}
