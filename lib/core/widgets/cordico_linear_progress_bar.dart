import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:flutter/material.dart';

class CordicoLinearProgressBar extends StatelessWidget {
  const CordicoLinearProgressBar({
    Key? key,
    this.height = 10,
    this.progressColor = ThemeColors.progressBarColor,
    this.secondaryProgressColor = Colors.white,
    required this.progress,
  }) : super(key: key);

  final int height;
  final Color progressColor;
  final Color secondaryProgressColor;
  final double progress;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: height.toDouble(),
      child: ClipRRect(
        borderRadius: const BorderRadius.all(Radius.circular(20)),
        child: LinearProgressIndicator(
          backgroundColor: secondaryProgressColor,
          valueColor: AlwaysStoppedAnimation(progressColor),
          value: progress,
        ),
      ),
    );
  }
}
