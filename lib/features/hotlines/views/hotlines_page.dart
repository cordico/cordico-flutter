// ignore_for_file: lines_longer_than_80_chars
import 'package:cordico_mobile/core/widgets/card/crisis_hotline_card.dart';
import 'package:cordico_mobile/features/department/views/content_list_view.dart';
import 'package:cordico_mobile/features/hotlines/logic/get_help_state.dart';
import 'package:core/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_segment/flutter_segment.dart';

class HotlinesPage extends ConsumerStatefulWidget {
  const HotlinesPage({Key? key}) : super(key: key);

  @override
  ConsumerState<HotlinesPage> createState() => _HotlinesPageState();
}

class _HotlinesPageState extends ConsumerState<HotlinesPage> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      ref.watch(getHelpContentProvider.notifier).getContent();
    });
  }

  @override
  Widget build(BuildContext context) {
    Segment.screen(
        screenName: 'Hotlines',
        properties: <String, dynamic>{'type': 'screen', 'name': 'Hotlines'});

    final state = ref.watch(getHelpContentProvider);
    final notifier = ref.watch(getHelpContentProvider.notifier);
    return notifier.isLoading
        ? const Center(
            child: Padding(
              padding: EdgeInsets.all(24),
              child: CircularProgressIndicator(),
            ),
          )
        : ContentListView<ContentModel>(
            source: state,
            padding: const EdgeInsets.only(top: 20),
            customItemBuilder: (context, index) {
              final item = state[index];
              return CrisisHotlineCard(contact: item);
            },
            onItemTap: (index) {},
          );
  }
}
