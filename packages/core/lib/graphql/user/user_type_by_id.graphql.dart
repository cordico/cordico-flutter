// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'user_type_by_id.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class UserTypeById$QueryRoot$UserTypeByPk extends JsonSerializable
    with EquatableMixin {
  UserTypeById$QueryRoot$UserTypeByPk();

  factory UserTypeById$QueryRoot$UserTypeByPk.fromJson(
          Map<String, dynamic> json) =>
      _$UserTypeById$QueryRoot$UserTypeByPkFromJson(json);

  late String id;

  @JsonKey(name: 'identity_schema')
  late String identitySchema;

  late String key;

  late String name;

  late dynamic schema;

  @override
  List<Object?> get props => [id, identitySchema, key, name, schema];
  @override
  Map<String, dynamic> toJson() =>
      _$UserTypeById$QueryRoot$UserTypeByPkToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UserTypeById$QueryRoot extends JsonSerializable with EquatableMixin {
  UserTypeById$QueryRoot();

  factory UserTypeById$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$UserTypeById$QueryRootFromJson(json);

  @JsonKey(name: 'user_type_by_pk')
  UserTypeById$QueryRoot$UserTypeByPk? userTypeByPk;

  @override
  List<Object?> get props => [userTypeByPk];
  @override
  Map<String, dynamic> toJson() => _$UserTypeById$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UserTypeByIdArguments extends JsonSerializable with EquatableMixin {
  UserTypeByIdArguments({required this.id});

  @override
  factory UserTypeByIdArguments.fromJson(Map<String, dynamic> json) =>
      _$UserTypeByIdArgumentsFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$UserTypeByIdArgumentsToJson(this);
}

final USER_TYPE_BY_ID_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'UserTypeById'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'user_type_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'id'),
                  value: VariableNode(name: NameNode(value: 'id')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'identity_schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class UserTypeByIdQuery
    extends GraphQLQuery<UserTypeById$QueryRoot, UserTypeByIdArguments> {
  UserTypeByIdQuery({required this.variables});

  @override
  final DocumentNode document = USER_TYPE_BY_ID_QUERY_DOCUMENT;

  @override
  final String operationName = 'UserTypeById';

  @override
  final UserTypeByIdArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  UserTypeById$QueryRoot parse(Map<String, dynamic> json) =>
      UserTypeById$QueryRoot.fromJson(json);
}
