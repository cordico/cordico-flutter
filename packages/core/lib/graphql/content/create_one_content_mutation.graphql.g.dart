// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'create_one_content_mutation.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateOneContentMutation$MutationRoot$CreateOneContent
    _$CreateOneContentMutation$MutationRoot$CreateOneContentFromJson(
        Map<String, dynamic> json) {
  return CreateOneContentMutation$MutationRoot$CreateOneContent()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$CreateOneContentMutation$MutationRoot$CreateOneContentToJson(
            CreateOneContentMutation$MutationRoot$CreateOneContent instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

CreateOneContentMutation$MutationRoot
    _$CreateOneContentMutation$MutationRootFromJson(Map<String, dynamic> json) {
  return CreateOneContentMutation$MutationRoot()
    ..createOneContent =
        CreateOneContentMutation$MutationRoot$CreateOneContent.fromJson(
            json['createOneContent'] as Map<String, dynamic>);
}

Map<String, dynamic> _$CreateOneContentMutation$MutationRootToJson(
        CreateOneContentMutation$MutationRoot instance) =>
    <String, dynamic>{
      'createOneContent': instance.createOneContent.toJson(),
    };

CreateOneContentMutationArguments _$CreateOneContentMutationArgumentsFromJson(
    Map<String, dynamic> json) {
  return CreateOneContentMutationArguments(
    contentStateId: json['contentStateId'] as String?,
    contentTypeId: json['contentTypeId'] as String?,
    name: json['name'] as String,
    data: json['data'],
    isShared: json['isShared'] as bool?,
  );
}

Map<String, dynamic> _$CreateOneContentMutationArgumentsToJson(
        CreateOneContentMutationArguments instance) =>
    <String, dynamic>{
      'contentStateId': instance.contentStateId,
      'contentTypeId': instance.contentTypeId,
      'name': instance.name,
      'data': instance.data,
      'isShared': instance.isShared,
    };
