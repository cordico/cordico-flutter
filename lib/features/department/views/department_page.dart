import 'dart:ui';
import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/providers/global_providers.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/accordion.dart';
import 'package:cordico_mobile/core/widgets/card/external_link_card.dart';
import 'package:cordico_mobile/core/widgets/cordico_elevated_button.dart';
import 'package:cordico_mobile/core/widgets/cordico_scaffold.dart';
import 'package:cordico_mobile/features/department/logic/department_page_holder.dart';
import 'package:cordico_mobile/features/department/logic/department_provider.dart';
import 'package:cordico_mobile/features/department/views/accordion_list.dart';
import 'package:cordico_mobile/features/department/views/department_header.dart';
import 'package:cordico_mobile/features/toolkit/logic/organization_provider.dart';
import 'package:core/core.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_segment/flutter_segment.dart';
import 'package:vrouter/vrouter.dart';

class DepartmentPage extends ConsumerStatefulWidget {
  const DepartmentPage({Key? key}) : super(key: key);

  @override
  ConsumerState<DepartmentPage> createState() => _DepartmentPageState();
}

class _DepartmentPageState extends ConsumerState<DepartmentPage> {
  late OrganizationSchema? organizationSchema;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      ref.watch(departmentPageStateNotifierProvider.notifier).getContent();
    });
  }

  @override
  Widget build(BuildContext context) {
    Segment.screen(
        screenName: 'Department',
        properties: <String, dynamic>{'type': 'screen', 'name': 'Department'});

    final state = ref.watch(departmentPageStateNotifierProvider);
    final organizationState = ref.watch(organizationStateNotifierProvider);

    return organizationState.when(
      initial: () => Container(),
      loading: () => const Center(child: CircularProgressIndicator()),
      data: (data) {
        organizationSchema =
            OrganizationSchema.fromJson(data?.data as Map<String, dynamic>);
        return Builder(
          builder: (context) => _buildDepartmentList(context, state),
        );
      },
      error: (errorDetail) => Container(
        color: Colors.white,
        child: Text(
          'Error occurred with $errorDetail',
        ),
      ),
    );
  }

  Widget _buildDepartmentList(BuildContext context, DepartmentPageHolder data) {
    if (organizationSchema == null) {
      return Text(
        'Department page is currently not available',
        style: ThemeStyles.cardContentTitle(context)
            ?.copyWith(color: Colors.white)
            .copyWith(fontSize: 22),
      );
    }
    return CustomScrollView(
      slivers: [
        SliverPersistentHeader(
          delegate: DepartmentHeader(
            maxExtent: 200 + MediaQuery.of(context).viewPadding.top,
            minExtent: 150 + MediaQuery.of(context).viewPadding.top,
            image: organizationSchema?.coverImageUrl ?? '',
            departmentImage: organizationSchema?.imageUrl ?? '',
          ),
        ),
        SliverToBoxAdapter(
          child: _buildDepartmentHeader(context),
        ),
        if (data.bulletin != null && data.bulletin!.isNotEmpty)
          SliverToBoxAdapter(
            child: AccordionList<ContentQuery$QueryRoot$Content>(
              title: 'Bulletin',
              source: data.bulletin ?? [],
              showArchiveButton: true,
              isBulletin: true,
              onItemTap: (index) {
                final bulletin = data.bulletin?[index];
                if (bulletin != null) {
                  global.goTo(context, content: bulletin, index: 0);
                }
              },
              onViewArchiveTap: () {
                context.vRouter.to('archive/Bulletin Archive/_/bulletin');
              },
            ),
          ),
        if (data.departmentResources != null &&
            data.departmentResources!.isNotEmpty)
          SliverToBoxAdapter(
            child: _buildDepartmentResourcesAccordion(
              context,
              title: 'Department Resources',
              resources: data.departmentResources ?? [],
            ),
          ),
        if (data.eapResources != null && data.eapResources!.isNotEmpty)
          SliverToBoxAdapter(
            child: AccordionList<ContentQuery$QueryRoot$Content>(
              title: 'EAP',
              source: data.eapResources ?? [],
              showArchiveButton: true,
              onItemTap: (index) {
                final eap = data.eapResources?[index];
                if (eap != null) {
                  global.goTo(context,
                      content: eap, index: 0, contentSource: 'eap');
                }
              },
              onViewArchiveTap: () {
                context.vRouter.to('archive/EAP Archive/_/eap');
              },
            ),
          ),
        SliverToBoxAdapter(
            child: Padding(
          padding:
              const EdgeInsets.only(left: 28, right: 28, top: 50, bottom: 80),
          child: CordicoElevatedButton(
            color: Colors.black,
            hasBorder: true,
            height: 48,
            child: Text(
              'Logout from Department',
              textAlign: TextAlign.center,
              style:
                  ThemeStyles.smallParagraph(context)?.copyWith(fontSize: 14),
            ),
            onPressed: () async {
              await secureStorage.delete(key: 'cordico.sessionToken');
              global.resetAll(ref);
              context.vRouter.to('/login');

              Future.delayed(const Duration(milliseconds: 300), () {
                ref.watch(currentIndexProvider.state).state = 0;
              });
            },
          ),
        )),
      ],
    );
  }

  Widget _buildDepartmentHeader(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        _buildDepartmentContact(context),
        Container(height: 14),
      ],
    );
  }

  Widget _buildDepartmentContact(BuildContext context) {
    return ClipRect(
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
        child: Container(
          color: Colors.white.withOpacity(0.3),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 27),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  organizationSchema?.longName ?? '',
                  textAlign: TextAlign.center,
                  style: ThemeStyles.sectionTitle(context)?.copyWith(
                    color: Colors.white,
                  ),
                ),
                /*Container(height: 10),
                Row(
                  children: [
                    Expanded(
                      child: CordicoElevatedButton(
                        height: 38,
                        color: Colors.white,
                        text: 'Directions',
                        icon: Icon(Icons.map_outlined,
                            color: ThemeColors.gray.shade900),
                        onPressed: () => CordicoUrl.openPlatformMap(
                          latitude: organizationSchema!.latitude?.toDouble(),
                          longitude: organizationSchema!.longitude?.toDouble(),
                        ),
                        textStyle: ThemeStyles.fieldLabel(context)?.copyWith(
                            color: ThemeColors.buttonsGreyFullyVisibleColor),
                      ),
                    ),
                    Container(width: 18),
                    if (organizationSchema?.phone != null &&
                        organizationSchema!.phone!.isNotEmpty)
                      Expanded(
                        child: CordicoElevatedButton(
                          height: 38,
                          color: Colors.white,
                          text: 'Call',
                          icon: Icon(Icons.phone_outlined,
                              color: ThemeColors.gray.shade900),
                          onPressed: () {
                            launchCaller(
                                context, organizationSchema?.phone ?? '');
                          },
                          textStyle: ThemeStyles.fieldLabel(context)?.copyWith(
                              color: ThemeColors.buttonsGreyFullyVisibleColor),
                        ),
                      ),
                  ],
                ),*/
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildDepartmentResourcesAccordion(BuildContext context,
      {required String title,
      required List<ContentQuery$QueryRoot$Content>? resources}) {
    return Accordion(
      title: title,
      onExpanded: _onBulletinExpanded,
      child: ListView.builder(
        shrinkWrap: true,
        primary: false,
        padding: EdgeInsets.zero,
        itemCount: resources?.length ?? 0,
        itemBuilder: (context, index) {
          final resource = resources?[index];

          if (resource != null) {
            return ExternalLinkCard(
              content: resource,
              isLast: index == (resources?.length ?? 0) - 1,
            );
          }
          return Container();
        },
      ),
    );
  }

  void _onBulletinExpanded() {}
}
