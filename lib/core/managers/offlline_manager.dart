import 'dart:convert';

import 'package:cordico_mobile/core/providers/global_providers.dart';
import 'package:flutter/foundation.dart';
import 'package:hive_flutter/adapters.dart';

class OfflineManager {
  static const String _appBox = 'app_cache_box2';
  static const String dataKey = 'data';
  static late Box<String> _box;

  static Future<void> openAppCacheBox() async {
    _box = await Hive.openBox<String>(_appBox);
  }

  static Future<void> closeAppCacheBox() async {}

  static String _getKey(ModelsKeys key) {
    if (environmentConfig.organizationId != null) {
      final stringKey =
          '${describeEnum(key)}-${environmentConfig.organizationId}';
      return stringKey;
    }
    return '';
  }

  static Future<bool> persistData({
    required ModelsKeys key,
    required Map<String, dynamic>? value,
  }) async {
    if (value != null) {
      final stringJson = jsonEncode(value);
      await _box.put(_getKey(key), stringJson);
    }
    return false;
  }

  static Map<String, dynamic>? getData({required ModelsKeys key}) {
    final val = _box.get(_getKey(key));

    if (val == null || val.isEmpty) {
      return null;
    }
    try {
      final map = jsonDecode(val) as Map<String, dynamic>;
      //print('VALUE $key: ${val}');
      return map;
    } catch (e) {
      return null;
    }
  }

  static void updateData() {}

  static Future<void> deleteData({required ModelsKeys key}) async {
    await _box.delete(_getKey(key));
  }

  //THIS SHOULD BE CALLED IN CASE OF LOGOUT TO RESET ALL SAVED CALLS
  static void clearAllData() {
    Hive.box<Map<String, dynamic>>(_appBox).clear();
  }
}

enum ModelsKeys {
  organization,
  cordicoFeatured,
  whatsNew,
  newsFeed,
  discoverContent,
  getHelp,
  contacts,
  departmentHolder,
  categories,
  contentTypes,
}
