// ignore_for_file: unawaited_futures, use_build_context_synchronously, lines_longer_than_80_chars
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/cordico_elevated_button.dart';
import 'package:flutter/material.dart';

class ContentAlert extends StatefulWidget {
  const ContentAlert({
    Key? key,
    this.dismissable = true,
    required this.title,
    required this.image,
    required this.actionTitle,
    required this.message,
    required this.action,
  }) : super(key: key);

  final bool dismissable;
  final String actionTitle;
  final String title;
  final String image;
  final String message;
  final Function()? action;

  @override
  _ContentAlertState createState() => _ContentAlertState();
}

class _ContentAlertState extends State<ContentAlert> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => widget.dismissable,
      child: AlertDialog(
        backgroundColor: ThemeColors.gray.shade900,
        content: Container(
          clipBehavior: Clip.hardEdge,
          width: MediaQuery.of(context).size.width,
          decoration: const BoxDecoration(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              Align(
                child: Container(
                  clipBehavior: Clip.hardEdge,
                  height: 112,
                  width: 112,
                  decoration: BoxDecoration(
                      color: ThemeColors.green.shade500,
                      borderRadius: BorderRadius.circular(64)),
                  child: Center(
                    child: Image.asset(
                      widget.image,
                      width: 45,
                      height: 70,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 25),
              Text(
                widget.title,
                style: ThemeStyles.sectionTitle(context),
              ),
              const SizedBox(height: 4),
              Text(
                widget.message,
                style: ThemeStyles.smallParagraph(context)
                    ?.copyWith(fontSize: 14, height: 1.5),
              ),
              const SizedBox(height: 16),
              CordicoElevatedButton(
                color: ThemeColors.gray.shade900,
                hasBorder: true,
                height: 44,
                onPressed: widget.action,
                child: Text(
                  widget.actionTitle,
                  textAlign: TextAlign.center,
                  style: ThemeStyles.smallParagraph(context)
                      ?.copyWith(fontSize: 14),
                ),
              ),
            ],
          ),
        ),
        contentPadding:
            const EdgeInsets.only(left: 16, right: 16, bottom: 27, top: 34),
        insetPadding: const EdgeInsets.only(left: 25, right: 25),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16),
        ),
      ),
    );
  }
}
