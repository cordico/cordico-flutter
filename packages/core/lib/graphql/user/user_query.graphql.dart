// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'user_query.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class UserQuery$QueryRoot$User extends JsonSerializable with EquatableMixin {
  UserQuery$QueryRoot$User();

  factory UserQuery$QueryRoot$User.fromJson(Map<String, dynamic> json) =>
      _$UserQuery$QueryRoot$UserFromJson(json);

  dynamic? data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  late String id;

  late String identifier;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @JsonKey(name: 'user_service_id')
  String? userServiceId;

  @JsonKey(name: 'user_type_id')
  late String userTypeId;

  @override
  List<Object?> get props =>
      [data, createdAt, id, identifier, updatedAt, userServiceId, userTypeId];
  @override
  Map<String, dynamic> toJson() => _$UserQuery$QueryRoot$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UserQuery$QueryRoot extends JsonSerializable with EquatableMixin {
  UserQuery$QueryRoot();

  factory UserQuery$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$UserQuery$QueryRootFromJson(json);

  late List<UserQuery$QueryRoot$User> user;

  @override
  List<Object?> get props => [user];
  @override
  Map<String, dynamic> toJson() => _$UserQuery$QueryRootToJson(this);
}

final USER_QUERY_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'UserQuery'),
      variableDefinitions: [],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'user'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'identifier'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'user_service_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'user_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class UserQueryQuery
    extends GraphQLQuery<UserQuery$QueryRoot, JsonSerializable> {
  UserQueryQuery();

  @override
  final DocumentNode document = USER_QUERY_QUERY_DOCUMENT;

  @override
  final String operationName = 'UserQuery';

  @override
  List<Object?> get props => [document, operationName];
  @override
  UserQuery$QueryRoot parse(Map<String, dynamic> json) =>
      UserQuery$QueryRoot.fromJson(json);
}
