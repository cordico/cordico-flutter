// ignore_for_file: lines_longer_than_80_chars, implementation_imports, avoid_bool_literals_in_conditional_expressions

import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/app_bar_provider.dart';
import 'package:cordico_mobile/core/widgets/card/hotline_card.dart';
import 'package:cordico_mobile/core/widgets/cordico_search_field.dart';
import 'package:cordico_mobile/core/widgets/map/cordico_map.dart';
import 'package:cordico_mobile/features/connect/logic/directory_state.dart';
import 'package:core/core.dart';
import 'package:core/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:vrouter/src/core/extended_context.dart';

final filtersDirectoryMapQueryProvider = StateProvider<String>((ref) => '');

final filteredDirectoryMapContactsProvider =
    Provider<List<OrganizationContactModel>>((ref) {
  final filter = ref.watch(filtersDirectoryMapQueryProvider);
  final contacts = ref.watch(allContactsProvider);

  return utils.filterContacts(content: contacts, query: filter);
});

class MapViewPage extends ConsumerStatefulWidget {
  const MapViewPage({Key? key}) : super(key: key);

  @override
  ConsumerState<MapViewPage> createState() => _MapViewPageState();
}

class _MapViewPageState extends ConsumerState<MapViewPage> {
  bool showSingleOnMap = false;

  @override
  Widget build(BuildContext context) {
    final stateP = ref.watch(filteredDirectoryMapContactsProvider);
    final stateNotifier = ref.watch(allContactsProvider.notifier);

    final id = context.vRouter.pathParameters['id'];

    return Scaffold(
      appBar: AppBarProvider.getAppBarWithBackIcon(
        context,
        title: Text(
          'Find Contacts Near You',
          style: ThemeStyles.cardContentTitle(context)
              ?.copyWith(color: Colors.white),
        ),
      ),
      body: stateNotifier.isLoading
          ? const Center(child: CircularProgressIndicator())
          : Builder(
              builder: (context) {
                final therapistsContactList = <OrganizationContactModel>[];

                if (id != null && id != '_') {
                  for (final element in stateP) {
                    if (element.contactType.id == kContactTypeTherapistId &&
                        element.id == id) {
                      therapistsContactList.add(element);
                    }
                  }
                  if (therapistsContactList.isNotEmpty) {
                    showSingleOnMap = true;
                  }
                } else {
                  for (final element in stateP) {
                    if (element.contactType.id == kContactTypeTherapistId) {
                      therapistsContactList.add(element);
                    }
                  }
                }

                return Stack(
                  children: [
                    CordicoMap(
                      therapistsContactList: therapistsContactList,
                      showSingleOnMap: showSingleOnMap,
                      onContactTap: (contact) {
                        showMapContactModal(context, contact);
                      },
                    ),
                    _buildFilterInputField(context),
                  ],
                );
              },
            ),
    );
  }

  Widget _buildFilterInputField(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(12, 8, 12, 19),
      child: CordicoSearchField(
        hintText: 'Find Contacts Near You',
        onChanged: (val) {
          //if (val.isEmpty) {
          ref.watch(filtersDirectoryMapQueryProvider.state).state = val;
          //}
        },
        onSubmitted: (value) {
          //ref.watch(filtersDirectoryMapQueryProvider).state = value;
        },
      ),
    );
  }

  Future<void> showMapContactModal(
    BuildContext context,
    OrganizationContactModel contact,
  ) {
    return showModalBottomSheet(
      context: context,
      useRootNavigator: true,
      builder: (ctx) {
        return StatefulBuilder(
          builder: (context, stateSet) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  height: 5,
                  width: 61,
                  margin: const EdgeInsets.only(top: 10),
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.5),
                    borderRadius: BorderRadius.circular(3),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: 8,
                    bottom: MediaQuery.of(ctx).viewPadding.bottom + 12,
                  ),
                  child: HotlineCard(
                    isFromMap: true,
                    contact: contact,
                    canPresentMap: false,
                  ),
                ),
              ],
            );
          },
        );
      },
      elevation: 10,
      barrierColor: Colors.white10,
      backgroundColor: const Color(0xFF17171A),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(8),
          topRight: Radius.circular(8),
        ),
      ),
    );
  }
}
