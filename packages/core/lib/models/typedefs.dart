// ignore_for_file: lines_longer_than_80_chars

import 'package:core/graphql/asset/asset_by_id.dart';
import 'package:core/graphql/asset/asset_type_query.dart';
import 'package:core/graphql/asset/organization_asset_by_id.dart';
import 'package:core/graphql/category/category_by_id_api.dart';
import 'package:core/graphql/collection/collection_by_id.dart';
import 'package:core/graphql/collection/collection_type_by_id.dart';
import 'package:core/graphql/configuration/configuration_query.dart';
import 'package:core/graphql/configuration/configuration_type_query.dart';
import 'package:core/graphql/contact_type/contact_type_by_id.dart';
import 'package:core/graphql/content/content_by_id_api.dart';
import 'package:core/graphql/content/content_state_api.dart';
import 'package:core/graphql/content/content_type_by_id.dart';
import 'package:core/graphql/content_part/content_part_by_content_id.dart';
import 'package:core/graphql/feedback/feedback_by_id.dart';
import 'package:core/graphql/feedback/feedback_type_by_id.dart';
import 'package:core/graphql/login/login_mutation.dart';
import 'package:core/graphql/organization/oc_by_oid_and_ctype.dart';
import 'package:core/graphql/organization/organization_by_id.dart';
import 'package:core/graphql/organization/organization_contact_by_id.dart';
import 'package:core/graphql/organization/organization_type_by_id.dart';
import 'package:core/graphql/user/user_by_id.dart';
import 'package:core/graphql/user/user_type_by_id.dart';

typedef ContactTypeModel = ContactTypeById$QueryRoot$ContactTypeByPk;

typedef AssetTypeModel = AssetTypeQuery$QueryRoot$AssetType;
typedef AssetModel = AssetById$QueryRoot$AssetByPk;

typedef FeedbackTypeModel = FeedbackTypeById$QueryRoot$FeedbackTypeByPk;
typedef FeedbackModel = FeedbackById$QueryRoot$FeedbackByPk;

typedef ConfigurationModel = ConfigurationQuery$QueryRoot$Configuration;
typedef ConfigurationTypeModel
    = ConfigurationTypeQuery$QueryRoot$ConfigurationType;

typedef ContentModel = ContentById$QueryRoot$ContentByPk;
typedef ContentPartModel = ContentPartByContentID$QueryRoot$ContentPart;
typedef ContentTypeModel = ContentTypeById$QueryRoot$ContentTypeByPk;
typedef ContentStateModel = ContentState$QueryRoot$ContentState;

typedef LoginMutationModel = LoginMutation$MutationRoot$Login;

typedef CollectionModel = CollectionById$QueryRoot$CollectionByPk;
typedef CollectionTypeModel = CollectionTypeById$QueryRoot$CollectionTypeByPk;

typedef CategoryModel = CategoryById$QueryRoot$CategoryByPk;

typedef OrganizationAsset
    = OrganizationAssetById$QueryRoot$OrganizationAssetByPk$Asset;
typedef OrganizationTypeModel
    = OrganizationTypeByID$QueryRoot$OrganizationTypeByPk;
typedef OrganizationModel = OrganizationByID$QueryRoot$OrganizationByPk;
typedef OrganizationCollectionModel
    = OCByOIdAndCType$QueryRoot$OrganizationCollections$Collection;
typedef OrganizationContactModel
    = OrganizationContactById$QueryRoot$OrganizationContactsByPk;

typedef UserModel = UserById$QueryRoot$UserByPk;
typedef UserTypeModel = UserTypeById$QueryRoot$UserTypeByPk;
