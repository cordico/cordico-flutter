# kratos_api.model.UiNodeScriptAttributes

## Load the model package
```dart
import 'package:kratos_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**async_** | **bool** | The script async type | [optional] 
**crossorigin** | **String** | The script cross origin policy | [optional] 
**id** | **String** | A unique identifier | [optional] 
**integrity** | **String** | The script's integrity hash | [optional] 
**nodeType** | **String** |  | [optional] 
**nonce** | **String** | Nonce for CSP  A nonce you may want to use to improve your Content Security Policy. You do not have to use this value but if you want to improve your CSP policies you may use it. You can also choose to use your own nonce value! | [optional] 
**referrerpolicy** | **String** | The script referrer policy | [optional] 
**src** | **String** | The script source | [optional] 
**type** | **String** | The script MIME type | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


