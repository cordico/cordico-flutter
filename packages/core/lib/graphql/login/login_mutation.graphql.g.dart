// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'login_mutation.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginMutation$MutationRoot$Login$User
    _$LoginMutation$MutationRoot$Login$UserFromJson(Map<String, dynamic> json) {
  return LoginMutation$MutationRoot$Login$User()
    ..userTypeId = json['userTypeId'] as String
    ..userServiceId = json['userServiceId'] as String?
    ..identifier = json['identifier'] as String
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['createdAt'] as String);
}

Map<String, dynamic> _$LoginMutation$MutationRoot$Login$UserToJson(
        LoginMutation$MutationRoot$Login$User instance) =>
    <String, dynamic>{
      'userTypeId': instance.userTypeId,
      'userServiceId': instance.userServiceId,
      'identifier': instance.identifier,
      'id': instance.id,
      'data': instance.data,
      'createdAt': instance.createdAt.toIso8601String(),
    };

LoginMutation$MutationRoot$Login _$LoginMutation$MutationRoot$LoginFromJson(
    Map<String, dynamic> json) {
  return LoginMutation$MutationRoot$Login()
    ..token = json['token'] as String
    ..user = LoginMutation$MutationRoot$Login$User.fromJson(
        json['user'] as Map<String, dynamic>);
}

Map<String, dynamic> _$LoginMutation$MutationRoot$LoginToJson(
        LoginMutation$MutationRoot$Login instance) =>
    <String, dynamic>{
      'token': instance.token,
      'user': instance.user.toJson(),
    };

LoginMutation$MutationRoot _$LoginMutation$MutationRootFromJson(
    Map<String, dynamic> json) {
  return LoginMutation$MutationRoot()
    ..login = json['login'] == null
        ? null
        : LoginMutation$MutationRoot$Login.fromJson(
            json['login'] as Map<String, dynamic>);
}

Map<String, dynamic> _$LoginMutation$MutationRootToJson(
        LoginMutation$MutationRoot instance) =>
    <String, dynamic>{
      'login': instance.login?.toJson(),
    };

AuthInput _$AuthInputFromJson(Map<String, dynamic> json) {
  return AuthInput(
    identifier: json['identifier'] as String,
    password: json['password'] as String,
  );
}

Map<String, dynamic> _$AuthInputToJson(AuthInput instance) => <String, dynamic>{
      'identifier': instance.identifier,
      'password': instance.password,
    };

LoginMutationArguments _$LoginMutationArgumentsFromJson(
    Map<String, dynamic> json) {
  return LoginMutationArguments(
    input: json['input'] == null
        ? null
        : AuthInput.fromJson(json['input'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$LoginMutationArgumentsToJson(
        LoginMutationArguments instance) =>
    <String, dynamic>{
      'input': instance.input?.toJson(),
    };
