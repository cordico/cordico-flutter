import 'package:core/graphql/content/content_query.dart';
import 'package:core/graphql/organization/organization_by_id.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'department_page_holder.g.dart';

@JsonSerializable(explicitToJson: true)
class DepartmentPageHolder extends JsonSerializable {
  DepartmentPageHolder();
  factory DepartmentPageHolder.fromJson(Map<String, dynamic> json) =>
      _$DepartmentPageHolderFromJson(json);

  List<ContentQuery$QueryRoot$Content>? bulletin;
  List<ContentQuery$QueryRoot$Content>? gallery;
  List<ContentQuery$QueryRoot$Content>? departmentResources;
  List<ContentQuery$QueryRoot$Content>? eapResources;
  OrganizationByID$QueryRoot$OrganizationByPk? organization;

  @override
  Map<String, dynamic> toJson() => _$DepartmentPageHolderToJson(this);
}
