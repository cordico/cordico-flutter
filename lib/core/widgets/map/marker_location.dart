import 'package:google_maps_flutter/google_maps_flutter.dart';

class MarkerLocation {
  MarkerLocation({
    required this.id,
    required this.position,
  });

  final String id;
  final LatLng position;
}
