// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'delete_many_content_category.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class DeleteManyContentCategoryMutation$MutationRoot$DeleteManyContentCategory
    extends JsonSerializable with EquatableMixin {
  DeleteManyContentCategoryMutation$MutationRoot$DeleteManyContentCategory();

  factory DeleteManyContentCategoryMutation$MutationRoot$DeleteManyContentCategory.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteManyContentCategoryMutation$MutationRoot$DeleteManyContentCategoryFromJson(
          json);

  late int count;

  @override
  List<Object?> get props => [count];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteManyContentCategoryMutation$MutationRoot$DeleteManyContentCategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class DeleteManyContentCategoryMutation$MutationRoot extends JsonSerializable
    with EquatableMixin {
  DeleteManyContentCategoryMutation$MutationRoot();

  factory DeleteManyContentCategoryMutation$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteManyContentCategoryMutation$MutationRootFromJson(json);

  late DeleteManyContentCategoryMutation$MutationRoot$DeleteManyContentCategory
      deleteManyContentCategory;

  @override
  List<Object?> get props => [deleteManyContentCategory];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteManyContentCategoryMutation$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeleteManyContentCategoryMutationArguments extends JsonSerializable
    with EquatableMixin {
  DeleteManyContentCategoryMutationArguments({this.contentIds});

  @override
  factory DeleteManyContentCategoryMutationArguments.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteManyContentCategoryMutationArgumentsFromJson(json);

  final List<String?>? contentIds;

  @override
  List<Object?> get props => [contentIds];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteManyContentCategoryMutationArgumentsToJson(this);
}

final DELETE_MANY_CONTENT_CATEGORY_MUTATION_MUTATION_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'DeleteManyContentCategoryMutation'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentIds')),
            type: ListTypeNode(
                type: NamedTypeNode(
                    name: NameNode(value: 'String'), isNonNull: false),
                isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'deleteManyContentCategory'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'contentId'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'in'),
                              value: VariableNode(
                                  name: NameNode(value: 'contentIds')))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'count'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class DeleteManyContentCategoryMutationMutation extends GraphQLQuery<
    DeleteManyContentCategoryMutation$MutationRoot,
    DeleteManyContentCategoryMutationArguments> {
  DeleteManyContentCategoryMutationMutation({required this.variables});

  @override
  final DocumentNode document =
      DELETE_MANY_CONTENT_CATEGORY_MUTATION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'DeleteManyContentCategoryMutation';

  @override
  final DeleteManyContentCategoryMutationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  DeleteManyContentCategoryMutation$MutationRoot parse(
          Map<String, dynamic> json) =>
      DeleteManyContentCategoryMutation$MutationRoot.fromJson(json);
}
