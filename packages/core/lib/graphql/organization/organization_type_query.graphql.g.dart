// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'organization_type_query.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrganizationTypeQuery$QueryRoot$OrganizationType
    _$OrganizationTypeQuery$QueryRoot$OrganizationTypeFromJson(
        Map<String, dynamic> json) {
  return OrganizationTypeQuery$QueryRoot$OrganizationType()
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..creatorId = json['creator_id'] as String
    ..description = json['description'] as String?
    ..id = json['id'] as String
    ..key = json['key'] as String
    ..name = json['name'] as String
    ..schema = json['schema']
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..updaterId = json['updater_id'] as String?;
}

Map<String, dynamic> _$OrganizationTypeQuery$QueryRoot$OrganizationTypeToJson(
        OrganizationTypeQuery$QueryRoot$OrganizationType instance) =>
    <String, dynamic>{
      'created_at': instance.createdAt.toIso8601String(),
      'creator_id': instance.creatorId,
      'description': instance.description,
      'id': instance.id,
      'key': instance.key,
      'name': instance.name,
      'schema': instance.schema,
      'updated_at': instance.updatedAt?.toIso8601String(),
      'updater_id': instance.updaterId,
    };

OrganizationTypeQuery$QueryRoot _$OrganizationTypeQuery$QueryRootFromJson(
    Map<String, dynamic> json) {
  return OrganizationTypeQuery$QueryRoot()
    ..organizationType = (json['organization_type'] as List<dynamic>)
        .map((e) => OrganizationTypeQuery$QueryRoot$OrganizationType.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$OrganizationTypeQuery$QueryRootToJson(
        OrganizationTypeQuery$QueryRoot instance) =>
    <String, dynamic>{
      'organization_type':
          instance.organizationType.map((e) => e.toJson()).toList(),
    };
