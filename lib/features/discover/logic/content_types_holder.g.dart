// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'content_types_holder.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContentTypesHolder _$ContentTypesHolderFromJson(Map<String, dynamic> json) {
  return ContentTypesHolder(
    contentTypes: (json['contentTypes'] as List<dynamic>?)
        ?.map((e) => ContentTypeById$QueryRoot$ContentTypeByPk.fromJson(
            e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$ContentTypesHolderToJson(ContentTypesHolder instance) =>
    <String, dynamic>{
      'contentTypes': instance.contentTypes?.map((e) => e.toJson()).toList(),
    };
