import 'dart:convert';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/managers/offlline_manager.dart';
import 'package:cordico_mobile/core/providers/global_providers.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/widgets/cordico_scaffold.dart';
import 'package:cordico_mobile/core/widgets/splash_widget.dart';
import 'package:cordico_mobile/features/archive/views/archive_page.dart';
import 'package:cordico_mobile/features/authentication/login/views/login_with_shared_credentials.dart';
import 'package:cordico_mobile/features/connect/views/contact_detail_page.dart';
import 'package:cordico_mobile/features/connect/views/contact_type_page.dart';
import 'package:cordico_mobile/features/connect/views/map_view_page.dart';
import 'package:cordico_mobile/features/content/views/article/article_page.dart';
import 'package:cordico_mobile/features/content/views/article/bulletin_detail_page.dart';
import 'package:cordico_mobile/features/content/views/article/content_summary_page.dart';
import 'package:cordico_mobile/features/content/views/assessment/assessment_page.dart';
import 'package:cordico_mobile/features/content/views/assessment/assessment_questionnaire_page.dart';
import 'package:cordico_mobile/features/content/views/assessment/assessment_result_page.dart';
import 'package:cordico_mobile/features/feedback/feedback_page.dart';
import 'package:cordico_mobile/features/settings/app_notification_settings_page.dart';
import 'package:cordico_mobile/features/settings/settings_page.dart';
import 'package:core/core.dart';
import 'package:core/graphql/user/user_by_id.graphql.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_segment/flutter_segment.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:shake_flutter/shake_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vrouter/vrouter.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  var writeKey = 'YZyMED8qUgrsm357aMrCC9GCEICr3GQJ';
  if (!kIsWeb) {
    if (Platform.isAndroid) {
      writeKey = 'l6aT32VcwcrqHduIhawr6PpuqQ3fwsyN';
    }
  } else {
    writeKey = 'hXnJZAFaTBU1pDbZy6nh0vgLi2zBymmK';
  }

  await Segment.config(
    options: SegmentConfig(
      writeKey: writeKey,
      trackApplicationLifecycleEvents: true,
      debug: true,
    ),
  );

  await Segment.track(eventName: 'Application Opened');

  if (!kIsWeb && kDebugMode) {
    Shake.setShowFloatingReportButton(false);
    Shake.setInvokeShakeOnScreenshot(true);
    Shake.setInvokeShakeOnShakeDeviceEvent(true);
    Shake.start('Fbozd6mZH6fPw2X15pzgQzUUFa3bRd4SLa75K7LK',
        'bGa921O4lRbItNaezdiQq7cVKlqegoTipGx77trnMWUImoXCHLP3WNr');
  }

  HttpOverrides.global = MyHttpOverrides();

  await dotenv.load(fileName: 'assets/.env');
  await initHiveForFlutter();

  await Hive.initFlutter();

  await OfflineManager.openAppCacheBox();

  global.prefs = await SharedPreferences.getInstance();

  runApp(ProviderScope(overrides: [
    // override the previous value with the new object
    sharedPreferencesProvider.overrideWithValue(global.prefs),
  ], child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static Future<bool> isLoggedIn() async {
    debugPrint('isLoggedIn');

    UserSchema? authUser;
    UserById$QueryRoot$UserByPk? globalUser;
    if (await secureStorage.containsKey(key: 'cordico.sessionToken')) {
      final sessionToken =
          await secureStorage.read(key: 'cordico.sessionToken');
      if (sessionToken != null && sessionToken.isNotEmpty) {
        final connectityCheck = await Connectivity().checkConnectivity();

        UserSchema? authUser;

        if (connectityCheck == ConnectivityResult.none) {
          final stringUser = await secureStorage.read(key: 'cordico.user');
          final globalStringUser =
              await secureStorage.read(key: 'cordico.globaluser');
          if (stringUser != null && stringUser.isNotEmpty) {
            final map = jsonDecode(stringUser) as Map<String, dynamic>;
            authUser = UserSchema.fromJson(map);

            if (globalStringUser != null && globalStringUser.isNotEmpty) {
              final map = jsonDecode(globalStringUser) as Map<String, dynamic>;
              globalUser = UserById$QueryRoot$UserByPk.fromJson(map);
            }

            environmentConfig.organizationId = authUser.organization?.data?.id;
          }
        } else {
          authUser = await authenticationService.session(sessionToken);
          if (authUser != null && authUser.organization?.data?.id != null) {
            final res = await userRepository
                .getUserByIdentifier(authUser.identifier ?? '');
            res.fold((l) {}, (r) => globalUser = r);

            if (globalUser != null) {
              final globalUserString = jsonEncode(globalUser?.toJson());
              await secureStorage.write(
                  key: 'cordico.globaluser', value: globalUserString);
            }

            final userString = jsonEncode(authUser.toJson());
            await secureStorage.write(key: 'cordico.user', value: userString);

            environmentConfig.organizationId = authUser.organization?.data?.id;
          }
        }

        if (authUser != null) {
          /*final context = <String, dynamic>{
            'user': <String, dynamic>{
              'username': authUser.identifier,
              'organization': authUser.organization?.name,
              'organizationType': authUser.organization?.organizationType
            }
          };*/
          //await Segment.setContext(context);
        }

        global.globalUser = globalUser;
        global.user = authUser;
        return authUser != null && authUser.organization?.data?.id != null;
      }
    }

    //CLEARING THE SHARED PREFERENCES IF SESSION IS NOT FOUND

    global.user = authUser;
    global.globalUser = globalUser;

    await global.prefs.clear();
    environmentConfig.organizationId = null;
    return false;
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(<DeviceOrientation>[
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown
    ]);

    //MARK:- ALL THE PARAMETERS ARE REQUIRED

    final contentRoute = VWidget(
      fullscreenDialog: true,
      path: 'content/:id/:index/:source',
      widget: const ArticlePage(),
    );

    final contentSummaryRoute = VWidget(
      path: 'content_summary/:id/:source',
      widget: const ContentSummaryPage(),
      stackedRoutes: [contentRoute],
    );
    return VRouter(
      routes: [
        VWidget(
          fullscreenDialog: true,
          path: '/login',
          widget: const LoginWithSharedCredentialsPage(),
        ),
        VWidget(
          path: '/splash',
          widget: const SplashWidget(),
        ),
        VGuard(
          beforeEnter: (vRedirector) async {
            return; //await isLoggedIn() ? null : vRedirector.to('/login');
          },
          stackedRoutes: [
            VWidget(
              path: '/dashboard',
              widget: const CordicoScaffold(),
              fullscreenDialog: true,
              stackedRoutes: [
                //--------------------------------------------------
                contentSummaryRoute,
                //--------------------------------------------------
                contentRoute,
                //--------------------------------------------------
                VWidget(
                  fullscreenDialog: true,
                  path: 'bulletin/:id/:source',
                  widget: const BulletinDetailPage(),
                ),
                VWidget(
                  fullscreenDialog: true,
                  path: 'plan_guide/:url',
                  widget: const BulletinDetailPage(),
                ),
                VWidget(
                  path: 'contact_detail/:id/:isFromMap',
                  widget: const ContactDetailsPage(),
                  stackedRoutes: [
                    VWidget(
                      path: 'contacts_map/:id',
                      widget: const MapViewPage(),
                    ),
                  ],
                ),
                VWidget(
                  path: 'feedback',
                  widget: const FeedbackPage(),
                ),

                VWidget(
                  path: 'settings',
                  widget: const SettingsPage(),
                  stackedRoutes: [
                    VWidget(
                      path: 'feedback',
                      widget: const FeedbackPage(),
                    ),
                    VWidget(
                      path: 'notification_settings',
                      widget: const AppNotficationsSettingsPage(),
                    ),
                  ],
                ),
                VWidget(
                  path: 'archive/:title/:id/:type',
                  widget: const ArchivePage(),
                  stackedRoutes: [
                    //--------------------------------------------------
                    contentSummaryRoute,
                    //--------------------------------------------------
                    VWidget(
                      path: 'assessment/:id',
                      widget: const AssessmentPage(),
                      stackedRoutes: [
                        VWidget(
                          fullscreenDialog: true,
                          path: 'assessment_questionnaire',
                          widget: const AssessmentQuestionnairePage(),
                        ),
                      ],
                    ),
                    VWidget(
                      fullscreenDialog: true,
                      path: 'bulletin/:id/:source',
                      widget: const BulletinDetailPage(),
                    ),
                    contentRoute,
                  ],
                ),
                VWidget(
                  path: 'contacts_map/:id',
                  widget: const MapViewPage(),
                  stackedRoutes: [
                    VWidget(
                      path: 'contacts_map/:id',
                      widget: const MapViewPage(),
                    ),
                    VWidget(
                      path: 'contact_detail/:id/:isFromMap',
                      widget: const ContactDetailsPage(),
                    ),
                  ],
                ),
                VWidget(
                  path: 'contact_type/:type',
                  widget: const ContactTypePage(),
                  stackedRoutes: [
                    VWidget(
                      path: 'contacts_map/:id',
                      widget: const MapViewPage(),
                    ),
                    VWidget(
                      path: 'contact_detail/:id/:isFromMap',
                      widget: const ContactDetailsPage(),
                    ),
                  ],
                ),
                VWidget(
                  path: 'assessment/:id',
                  widget: const AssessmentPage(),
                  stackedRoutes: [
                    VGuard(
                      stackedRoutes: [
                        VWidget(
                          path: 'assessment_questionnaire',
                          widget: const AssessmentQuestionnairePage(),
                          stackedRoutes: [
                            VWidget(
                              fullscreenDialog: true,
                              path: 'assessment_result/:id',
                              widget: const AssessmentResultPage(),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
        VRouteRedirector(
          redirectTo: '/splash',
          path: '*',
        )
      ],
      title: 'Cordico',
      mode: VRouterMode.history,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: ThemeColors.green,
        fontFamily: 'ProximaNova',
        scaffoldBackgroundColor: Colors.black,
        splashColor: ThemeColors.blue.withOpacity(0.3),
        disabledColor: ThemeColors.blue.withOpacity(0.5),
        highlightColor: Colors.transparent,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        colorScheme:
            ColorScheme.fromSwatch().copyWith(secondary: ThemeColors.blue),
      ),
    );
  }
}
