// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'content_by_content_type.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContentByContentType$QueryRoot$Content$ContentType
    _$ContentByContentType$QueryRoot$Content$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic> _$ContentByContentType$QueryRoot$Content$ContentTypeToJson(
        ContentByContentType$QueryRoot$Content$ContentType instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'is_shared': instance.isShared,
      'description': instance.description,
    };

ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentType
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentTypeToJson(
            ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'is_shared': instance.isShared,
          'description': instance.description,
        };

ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ChildContent
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ChildContentFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ChildContent()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ChildContentToJson(
            ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ChildContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ParentContent
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ParentContentFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic>
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ParentContentToJson(
            ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ParentContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'index': instance.index,
          'part_id': instance.partId,
        };

ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$CategoryToJson(
            ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories()
    ..category =
        ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category
            .fromJson(json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategoriesToJson(
            ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories
                instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentState
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentStateFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentStateToJson(
            ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentState
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
        };

ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
            ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$CollectionToJson(
            ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$CollectionsFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections()
    ..collection =
        ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection
            .fromJson(json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$CollectionsToJson(
            ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections
                instance) =>
        <String, dynamic>{
          'collection': instance.collection.toJson(),
        };

ContentByContentType$QueryRoot$Content$ChildContent$ContentPart
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPartFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$ChildContent$ContentPart()
    ..contentType =
        ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentType
            .fromJson(json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) =>
            ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ChildContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) =>
            ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ParentContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentCategories
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentState =
        ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$ContentState
            .fromJson(json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) =>
            ContentByContentType$QueryRoot$Content$ChildContent$ContentPart$Collections
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic>
    _$ContentByContentType$QueryRoot$Content$ChildContent$ContentPartToJson(
            ContentByContentType$QueryRoot$Content$ChildContent$ContentPart
                instance) =>
        <String, dynamic>{
          'content_type': instance.contentType.toJson(),
          'updated_at': instance.updatedAt?.toIso8601String(),
          'name': instance.name,
          'is_shared': instance.isShared,
          'id': instance.id,
          'data': instance.data,
          'created_at': instance.createdAt.toIso8601String(),
          'child_content':
              instance.childContent.map((e) => e.toJson()).toList(),
          'parent_content':
              instance.parentContent.map((e) => e.toJson()).toList(),
          'content_categories':
              instance.contentCategories.map((e) => e.toJson()).toList(),
          'content_state': instance.contentState.toJson(),
          'collections': instance.collections.map((e) => e.toJson()).toList(),
        };

ContentByContentType$QueryRoot$Content$ChildContent
    _$ContentByContentType$QueryRoot$Content$ChildContentFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$ChildContent()
    ..contentPart =
        ContentByContentType$QueryRoot$Content$ChildContent$ContentPart
            .fromJson(json['content_part'] as Map<String, dynamic>)
    ..id = json['id'] as String
    ..index = json['index'] as int;
}

Map<String, dynamic>
    _$ContentByContentType$QueryRoot$Content$ChildContentToJson(
            ContentByContentType$QueryRoot$Content$ChildContent instance) =>
        <String, dynamic>{
          'content_part': instance.contentPart.toJson(),
          'id': instance.id,
          'index': instance.index,
        };

ContentByContentType$QueryRoot$Content$ParentContent
    _$ContentByContentType$QueryRoot$Content$ParentContentFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic>
    _$ContentByContentType$QueryRoot$Content$ParentContentToJson(
            ContentByContentType$QueryRoot$Content$ParentContent instance) =>
        <String, dynamic>{
          'id': instance.id,
          'index': instance.index,
          'part_id': instance.partId,
        };

ContentByContentType$QueryRoot$Content$ContentCategories$Category
    _$ContentByContentType$QueryRoot$Content$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$ContentByContentType$QueryRoot$Content$ContentCategories$CategoryToJson(
            ContentByContentType$QueryRoot$Content$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

ContentByContentType$QueryRoot$Content$ContentCategories
    _$ContentByContentType$QueryRoot$Content$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$ContentCategories()
    ..category =
        ContentByContentType$QueryRoot$Content$ContentCategories$Category
            .fromJson(json['category'] as Map<String, dynamic>);
}

Map<String,
    dynamic> _$ContentByContentType$QueryRoot$Content$ContentCategoriesToJson(
        ContentByContentType$QueryRoot$Content$ContentCategories instance) =>
    <String, dynamic>{
      'category': instance.category.toJson(),
    };

ContentByContentType$QueryRoot$Content$ContentState
    _$ContentByContentType$QueryRoot$Content$ContentStateFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$ContentByContentType$QueryRoot$Content$ContentStateToJson(
            ContentByContentType$QueryRoot$Content$ContentState instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
        };

ContentByContentType$QueryRoot$Content$Collections$Collection$CollectionType
    _$ContentByContentType$QueryRoot$Content$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$ContentByContentType$QueryRoot$Content$Collections$Collection$CollectionTypeToJson(
            ContentByContentType$QueryRoot$Content$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

ContentByContentType$QueryRoot$Content$Collections$Collection
    _$ContentByContentType$QueryRoot$Content$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : ContentByContentType$QueryRoot$Content$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentByContentType$QueryRoot$Content$Collections$CollectionToJson(
            ContentByContentType$QueryRoot$Content$Collections$Collection
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

ContentByContentType$QueryRoot$Content$Collections
    _$ContentByContentType$QueryRoot$Content$CollectionsFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content$Collections()
    ..collection =
        ContentByContentType$QueryRoot$Content$Collections$Collection.fromJson(
            json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ContentByContentType$QueryRoot$Content$CollectionsToJson(
        ContentByContentType$QueryRoot$Content$Collections instance) =>
    <String, dynamic>{
      'collection': instance.collection.toJson(),
    };

ContentByContentType$QueryRoot$Content
    _$ContentByContentType$QueryRoot$ContentFromJson(
        Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot$Content()
    ..contentType = ContentByContentType$QueryRoot$Content$ContentType.fromJson(
        json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) =>
            ContentByContentType$QueryRoot$Content$ChildContent.fromJson(
                e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) =>
            ContentByContentType$QueryRoot$Content$ParentContent.fromJson(
                e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            ContentByContentType$QueryRoot$Content$ContentCategories.fromJson(
                e as Map<String, dynamic>))
        .toList()
    ..contentState =
        ContentByContentType$QueryRoot$Content$ContentState.fromJson(
            json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) => ContentByContentType$QueryRoot$Content$Collections.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$ContentByContentType$QueryRoot$ContentToJson(
        ContentByContentType$QueryRoot$Content instance) =>
    <String, dynamic>{
      'content_type': instance.contentType.toJson(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'name': instance.name,
      'is_shared': instance.isShared,
      'id': instance.id,
      'data': instance.data,
      'created_at': instance.createdAt.toIso8601String(),
      'child_content': instance.childContent.map((e) => e.toJson()).toList(),
      'parent_content': instance.parentContent.map((e) => e.toJson()).toList(),
      'content_categories':
          instance.contentCategories.map((e) => e.toJson()).toList(),
      'content_state': instance.contentState.toJson(),
      'collections': instance.collections.map((e) => e.toJson()).toList(),
    };

ContentByContentType$QueryRoot _$ContentByContentType$QueryRootFromJson(
    Map<String, dynamic> json) {
  return ContentByContentType$QueryRoot()
    ..content = (json['content'] as List<dynamic>)
        .map((e) => ContentByContentType$QueryRoot$Content.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$ContentByContentType$QueryRootToJson(
        ContentByContentType$QueryRoot instance) =>
    <String, dynamic>{
      'content': instance.content.map((e) => e.toJson()).toList(),
    };

ContentByContentTypeArguments _$ContentByContentTypeArgumentsFromJson(
    Map<String, dynamic> json) {
  return ContentByContentTypeArguments(
    contentTypeIDs: (json['contentTypeIDs'] as List<dynamic>?)
        ?.map((e) => e as String)
        .toList(),
    contentStateNames: (json['contentStateNames'] as List<dynamic>?)
        ?.map((e) => e as String?)
        .toList(),
    offset: json['offset'] as int?,
    limit: json['limit'] as int?,
  );
}

Map<String, dynamic> _$ContentByContentTypeArgumentsToJson(
        ContentByContentTypeArguments instance) =>
    <String, dynamic>{
      'contentTypeIDs': instance.contentTypeIDs,
      'contentStateNames': instance.contentStateNames,
      'offset': instance.offset,
      'limit': instance.limit,
    };
