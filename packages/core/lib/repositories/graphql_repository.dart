// ignore_for_file: avoid_unused_constructor_parameters

import 'package:core/core.dart';

abstract class GraphQLRepository {
  GraphQLRepository(
      {required String url, required String userUrl, String? uploadUrl,
        String? token,}) {
    final socketLink = WebSocketLink(userUrl);
    final headers = <String, String>{};
    if (token != null) {
      headers['Authorization'] = 'Bearer $token';
    }
    Link link = HttpLink(
        url,
      defaultHeaders: headers,
    );
    if (EnvironmentConfig.hasuraSecret != null &&
        EnvironmentConfig.hasuraSecret!.isNotEmpty) {
      headers['x-hasura-admin-secret'] = EnvironmentConfig.hasuraSecret!;
    }
    link = Link.split((request) => request.isSubscription, socketLink, link);
    graphqlClient =
        GraphQLClient(link: link, cache: GraphQLCache(store: HiveStore()));

    final Link userLink = HttpLink(userUrl);
    userGraphQLClient =
        GraphQLClient(link: userLink, cache: GraphQLCache(store: HiveStore()));

    if (uploadUrl != null) {
      final Link uploadLink = HttpLink(uploadUrl);
      uploadGraphQLClient = GraphQLClient(
          link: uploadLink, cache: GraphQLCache(store: HiveStore()),);
    }
  }

  late final GraphQLClient graphqlClient;
  late final GraphQLClient userGraphQLClient;
  late final GraphQLClient uploadGraphQLClient;
}
