// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'organization_content_by_collection_type_key.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentType
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentType();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentTypeFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentType
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentType();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentTypeFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ChildContent
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ChildContent();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ChildContentFromJson(
          json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ChildContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ParentContent
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ParentContent();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ParentContentFromJson(
          json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ParentContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentCategories
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentCategories();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentCategoriesFromJson(
          json);

  late OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category
      category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentCategoriesToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentState
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentState();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentStateFromJson(
          json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentStateToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$Collections$CollectionFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$Collections$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$Collections
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$Collections();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$CollectionsFromJson(
          json);

  late OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection
      collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$CollectionsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPartFromJson(
          json);

  @JsonKey(name: 'content_type')
  late OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentType
      contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<
          OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ChildContent>
      childContent;

  @JsonKey(name: 'parent_content')
  late List<
          OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ParentContent>
      parentContent;

  @JsonKey(name: 'content_categories')
  late List<
          OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$ContentState
      contentState;

  late List<
          OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart$Collections>
      collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPartToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContentFromJson(
          json);

  @JsonKey(name: 'content_part')
  late OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent$ContentPart
      contentPart;

  late String id;

  late int index;

  @override
  List<Object?> get props => [contentPart, id, index];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$ParentContent
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$ParentContent();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ParentContentFromJson(
          json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ParentContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentCategories$Category();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentCategories
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentCategories();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentCategoriesFromJson(
          json);

  late OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentCategories$Category
      category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentCategoriesToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentState
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentState();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentStateFromJson(
          json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentStateToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$Collections$Collection$CollectionType();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$Collections$Collection();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$Collections$CollectionFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$Collections$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content$Collections
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content$Collections();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$CollectionsFromJson(
          json);

  late OrganizationContentByCollectionTypeKey$QueryRoot$Content$Collections$Collection
      collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$Content$CollectionsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot$Content
    extends JsonSerializable with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot$Content();

  factory OrganizationContentByCollectionTypeKey$QueryRoot$Content.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$ContentFromJson(json);

  @JsonKey(name: 'content_type')
  late OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentType
      contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<
          OrganizationContentByCollectionTypeKey$QueryRoot$Content$ChildContent>
      childContent;

  @JsonKey(name: 'parent_content')
  late List<
          OrganizationContentByCollectionTypeKey$QueryRoot$Content$ParentContent>
      parentContent;

  @JsonKey(name: 'content_categories')
  late List<
          OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late OrganizationContentByCollectionTypeKey$QueryRoot$Content$ContentState
      contentState;

  late List<
          OrganizationContentByCollectionTypeKey$QueryRoot$Content$Collections>
      collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRoot$ContentToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKey$QueryRoot extends JsonSerializable
    with EquatableMixin {
  OrganizationContentByCollectionTypeKey$QueryRoot();

  factory OrganizationContentByCollectionTypeKey$QueryRoot.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKey$QueryRootFromJson(json);

  late List<OrganizationContentByCollectionTypeKey$QueryRoot$Content> content;

  @override
  List<Object?> get props => [content];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKey$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContentByCollectionTypeKeyArguments extends JsonSerializable
    with EquatableMixin {
  OrganizationContentByCollectionTypeKeyArguments(
      {required this.organizationId,
      required this.collectionTypeKey,
      this.contentStateNames,
      this.offset,
      this.limit});

  @override
  factory OrganizationContentByCollectionTypeKeyArguments.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContentByCollectionTypeKeyArgumentsFromJson(json);

  late String organizationId;

  late String collectionTypeKey;

  final List<String>? contentStateNames;

  final int? offset;

  final int? limit;

  @override
  List<Object?> get props =>
      [organizationId, collectionTypeKey, contentStateNames, offset, limit];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContentByCollectionTypeKeyArgumentsToJson(this);
}

final ORGANIZATION_CONTENT_BY_COLLECTION_TYPE_KEY_QUERY_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'OrganizationContentByCollectionTypeKey'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'organizationId')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'collectionTypeKey')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentStateNames')),
            type: ListTypeNode(
                type: NamedTypeNode(
                    name: NameNode(value: 'String'), isNonNull: true),
                isNonNull: false),
            defaultValue: DefaultValueNode(
                value: ListValueNode(values: [
              StringValueNode(value: 'Published', isBlock: false)
            ])),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'offset')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '0')),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'limit')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '10000')),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'content'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'offset'),
                  value: VariableNode(name: NameNode(value: 'offset'))),
              ArgumentNode(
                  name: NameNode(value: 'limit'),
                  value: VariableNode(name: NameNode(value: 'limit'))),
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'content_state'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'name'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: '_in'),
                                    value: VariableNode(
                                        name: NameNode(
                                            value: 'contentStateNames')))
                              ]))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'collections'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'collection'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'collection_type'),
                                    value: ObjectValueNode(fields: [
                                      ObjectFieldNode(
                                          name: NameNode(value: 'key'),
                                          value: ObjectValueNode(fields: [
                                            ObjectFieldNode(
                                                name: NameNode(value: '_eq'),
                                                value: VariableNode(
                                                    name: NameNode(
                                                        value:
                                                            'collectionTypeKey')))
                                          ]))
                                    ])),
                                ObjectFieldNode(
                                    name: NameNode(
                                        value: 'organization_collections'),
                                    value: ObjectValueNode(fields: [
                                      ObjectFieldNode(
                                          name: NameNode(value: 'organization'),
                                          value: ObjectValueNode(fields: [
                                            ObjectFieldNode(
                                                name: NameNode(value: 'id'),
                                                value: ObjectValueNode(fields: [
                                                  ObjectFieldNode(
                                                      name: NameNode(
                                                          value: '_eq'),
                                                      value: VariableNode(
                                                          name: NameNode(
                                                              value:
                                                                  'organizationId')))
                                                ]))
                                          ]))
                                    ]))
                              ]))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'content_type'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'is_shared'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'description'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'is_shared'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'child_content'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'content_part'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'content_type'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'is_shared'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'description'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'updated_at'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'is_shared'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'data'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'created_at'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'child_content'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'parent_content'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'index'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'part_id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'content_categories'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'category'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'description'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ]))
                              ])),
                          FieldNode(
                              name: NameNode(value: 'content_state'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ])),
                          FieldNode(
                              name: NameNode(value: 'collections'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'collection'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(
                                              value: 'collection_type'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'name'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'description'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null)
                                          ]))
                                    ]))
                              ]))
                        ])),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'index'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'parent_content'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'index'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'part_id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'content_categories'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'category'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'description'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ]))
                  ])),
              FieldNode(
                  name: NameNode(value: 'content_state'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'collections'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'collection'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'collection_type'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'description'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ]))
                        ]))
                  ]))
            ]))
      ]))
]);

class OrganizationContentByCollectionTypeKeyQuery extends GraphQLQuery<
    OrganizationContentByCollectionTypeKey$QueryRoot,
    OrganizationContentByCollectionTypeKeyArguments> {
  OrganizationContentByCollectionTypeKeyQuery({required this.variables});

  @override
  final DocumentNode document =
      ORGANIZATION_CONTENT_BY_COLLECTION_TYPE_KEY_QUERY_DOCUMENT;

  @override
  final String operationName = 'OrganizationContentByCollectionTypeKey';

  @override
  final OrganizationContentByCollectionTypeKeyArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  OrganizationContentByCollectionTypeKey$QueryRoot parse(
          Map<String, dynamic> json) =>
      OrganizationContentByCollectionTypeKey$QueryRoot.fromJson(json);
}
