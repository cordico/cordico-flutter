// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'create_one_user_mutation.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CreateOneUserMutation$MutationRoot$CreateOneUser extends JsonSerializable
    with EquatableMixin {
  CreateOneUserMutation$MutationRoot$CreateOneUser();

  factory CreateOneUserMutation$MutationRoot$CreateOneUser.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneUserMutation$MutationRoot$CreateOneUserFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneUserMutation$MutationRoot$CreateOneUserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneUserMutation$MutationRoot extends JsonSerializable
    with EquatableMixin {
  CreateOneUserMutation$MutationRoot();

  factory CreateOneUserMutation$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneUserMutation$MutationRootFromJson(json);

  late CreateOneUserMutation$MutationRoot$CreateOneUser createOneUser;

  @override
  List<Object?> get props => [createOneUser];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneUserMutation$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneUserMutationArguments extends JsonSerializable
    with EquatableMixin {
  CreateOneUserMutationArguments(
      {this.userTypeId,
      required this.identifier,
      this.data,
      this.userServiceId});

  @override
  factory CreateOneUserMutationArguments.fromJson(Map<String, dynamic> json) =>
      _$CreateOneUserMutationArgumentsFromJson(json);

  final String? userTypeId;

  late String identifier;

  final dynamic? data;

  final String? userServiceId;

  @override
  List<Object?> get props => [userTypeId, identifier, data, userServiceId];
  @override
  Map<String, dynamic> toJson() => _$CreateOneUserMutationArgumentsToJson(this);
}

final CREATE_ONE_USER_MUTATION_MUTATION_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'CreateOneUserMutation'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'userTypeId')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'identifier')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'data')),
            type:
                NamedTypeNode(name: NameNode(value: 'Json'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'userServiceId')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'createOneUser'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'data'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'identifier'),
                        value:
                            VariableNode(name: NameNode(value: 'identifier'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'data'),
                        value: VariableNode(name: NameNode(value: 'data'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'userServiceId'),
                        value: VariableNode(
                            name: NameNode(value: 'userServiceId'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'userType'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'connect'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'id'),
                                    value: VariableNode(
                                        name: NameNode(value: 'userTypeId')))
                              ]))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class CreateOneUserMutationMutation extends GraphQLQuery<
    CreateOneUserMutation$MutationRoot, CreateOneUserMutationArguments> {
  CreateOneUserMutationMutation({required this.variables});

  @override
  final DocumentNode document = CREATE_ONE_USER_MUTATION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'CreateOneUserMutation';

  @override
  final CreateOneUserMutationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  CreateOneUserMutation$MutationRoot parse(Map<String, dynamic> json) =>
      CreateOneUserMutation$MutationRoot.fromJson(json);
}
