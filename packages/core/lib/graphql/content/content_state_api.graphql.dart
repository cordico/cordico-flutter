// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'content_state_api.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class ContentState$QueryRoot$ContentState extends JsonSerializable
    with EquatableMixin {
  ContentState$QueryRoot$ContentState();

  factory ContentState$QueryRoot$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$ContentState$QueryRoot$ContentStateFromJson(json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentState$QueryRoot$ContentStateToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentState$QueryRoot extends JsonSerializable with EquatableMixin {
  ContentState$QueryRoot();

  factory ContentState$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$ContentState$QueryRootFromJson(json);

  @JsonKey(name: 'content_state')
  late List<ContentState$QueryRoot$ContentState> contentState;

  @override
  List<Object?> get props => [contentState];
  @override
  Map<String, dynamic> toJson() => _$ContentState$QueryRootToJson(this);
}

final CONTENT_STATE_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'ContentState'),
      variableDefinitions: [],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'content_state'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class ContentStateQuery
    extends GraphQLQuery<ContentState$QueryRoot, JsonSerializable> {
  ContentStateQuery();

  @override
  final DocumentNode document = CONTENT_STATE_QUERY_DOCUMENT;

  @override
  final String operationName = 'ContentState';

  @override
  List<Object?> get props => [document, operationName];
  @override
  ContentState$QueryRoot parse(Map<String, dynamic> json) =>
      ContentState$QueryRoot.fromJson(json);
}
