// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'update_one_collection.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class UpdateOneCollection$MutationRoot$UpdateOneCollection
    extends JsonSerializable with EquatableMixin {
  UpdateOneCollection$MutationRoot$UpdateOneCollection();

  factory UpdateOneCollection$MutationRoot$UpdateOneCollection.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateOneCollection$MutationRoot$UpdateOneCollectionFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$UpdateOneCollection$MutationRoot$UpdateOneCollectionToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateOneCollection$MutationRoot extends JsonSerializable
    with EquatableMixin {
  UpdateOneCollection$MutationRoot();

  factory UpdateOneCollection$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateOneCollection$MutationRootFromJson(json);

  UpdateOneCollection$MutationRoot$UpdateOneCollection? updateOneCollection;

  @override
  List<Object?> get props => [updateOneCollection];
  @override
  Map<String, dynamic> toJson() =>
      _$UpdateOneCollection$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateOneCollectionArguments extends JsonSerializable
    with EquatableMixin {
  UpdateOneCollectionArguments(
      {required this.id,
      required this.isShared,
      required this.key,
      required this.name,
      required this.title,
      required this.collectionTypeId});

  @override
  factory UpdateOneCollectionArguments.fromJson(Map<String, dynamic> json) =>
      _$UpdateOneCollectionArgumentsFromJson(json);

  late String id;

  late bool isShared;

  late String key;

  late String name;

  late String title;

  late String collectionTypeId;

  @override
  List<Object?> get props => [id, isShared, key, name, title, collectionTypeId];
  @override
  Map<String, dynamic> toJson() => _$UpdateOneCollectionArgumentsToJson(this);
}

final UPDATE_ONE_COLLECTION_MUTATION_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'UpdateOneCollection'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'isShared')),
            type: NamedTypeNode(
                name: NameNode(value: 'Boolean'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'key')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'name')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'title')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'collectionTypeId')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'updateOneCollection'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'id'),
                        value: VariableNode(name: NameNode(value: 'id')))
                  ])),
              ArgumentNode(
                  name: NameNode(value: 'data'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'isShared'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'set'),
                              value: VariableNode(
                                  name: NameNode(value: 'isShared')))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'key'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'set'),
                              value: VariableNode(name: NameNode(value: 'key')))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'name'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'set'),
                              value:
                                  VariableNode(name: NameNode(value: 'name')))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'title'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'set'),
                              value:
                                  VariableNode(name: NameNode(value: 'title')))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'collectionType'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'connect'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'id'),
                                    value: VariableNode(
                                        name: NameNode(
                                            value: 'collectionTypeId')))
                              ]))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class UpdateOneCollectionMutation extends GraphQLQuery<
    UpdateOneCollection$MutationRoot, UpdateOneCollectionArguments> {
  UpdateOneCollectionMutation({required this.variables});

  @override
  final DocumentNode document = UPDATE_ONE_COLLECTION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'UpdateOneCollection';

  @override
  final UpdateOneCollectionArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  UpdateOneCollection$MutationRoot parse(Map<String, dynamic> json) =>
      UpdateOneCollection$MutationRoot.fromJson(json);
}
