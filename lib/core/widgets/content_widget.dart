// ignore_for_file: lines_longer_than_80_chars

import 'dart:convert';
import 'package:cordico_mobile/core/managers/cordico_url.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:core/models/schema/content_schema.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ContentWidget extends StatefulWidget {
  const ContentWidget({
    Key? key,
    required this.content,
    required this.contentType,
    this.isDarkMode = true,
  }) : super(key: key);

  final String content;
  final MimeType contentType;
  final bool isDarkMode;

  @override
  State createState() => ContentState();
}

class ContentState extends State<ContentWidget> {
  double _webViewHeight = 1;
  WebViewController? _controller;

  @override
  void initState() {
    _webViewHeight = 1;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    switch (widget.contentType) {
      case MimeType.markdown:
        return _buildMarkdownWidget(context);

      default:
        return _buildHtmlWidget(context);
    }
  }

  Widget _buildMarkdownWidget(BuildContext context) {
    return Markdown(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      onTapLink: (link, val1, val2) async {
        if (val1 != null && val1.startsWith('http')) {
          await CordicoUrl.openUrl(url: val1);
        } else if (val1 != null && val1.startsWith('tel')) {
          final phoneNumber = val1.replaceAll('tell:', '');
          await CordicoUrl.call(
              phone: CordicoUrl.cleanPhone(phone: phoneNumber));
        }
      },
      data: widget.content,
      padding: EdgeInsets.zero,
      softLineBreak: true,
      styleSheet: MarkdownStyleSheet.fromTheme(Theme.of(context)).copyWith(
        p: ThemeStyles.contentDescription(context)?.copyWith(
          color: widget.isDarkMode
              ? ThemeColors.blue.shade50
              : ThemeColors.gray.shade900,
          fontSize: 16,
          height: 1.5,
        ),
        h2: ThemeStyles.cardTitle(context)?.copyWith(
          color: widget.isDarkMode
              ? ThemeColors.blue.shade50
              : ThemeColors.gray.shade900,
          fontSize: 23,
          fontWeight: FontWeight.w500,
        ),

        // h5: ThemeStyles.cardTitle(context)?.copyWith(
        //   color: widget.isDarkMode
        //       ? ThemeColors.blue.shade50
        //       : ThemeColors.gray.shade900,
        //   fontSize: 16,
        // ),
      ),
    );
  }

  Widget _buildHtmlWidget(BuildContext context) {
    return SizedBox(
      height: _webViewHeight,
      child: WebView(
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController webViewController) async {
          _controller = webViewController;
          await _loadWebViewContent(webViewController, false);
        },
        onPageFinished: (url) {
          __updateWebViewHeight();
        },
      ),
    );
  }

  Future _loadWebViewContent(
      WebViewController webViewController, bool isDarkMode) async {
    final css = isDarkMode ? await _loadDarkCSS() : await _loadCSS();

    final contentBuffer = StringBuffer()
      ..writeln(
          "<html><head><meta name='viewport' content='width=device-width, initial-scale=1.0' />")
      ..writeln('<style>$css</style></head><body>')
      ..writeln(widget.content)
      ..writeln('</body></html>');

    await webViewController.loadUrl(Uri.dataFromString(contentBuffer.toString(),
            mimeType: 'text/html', encoding: Encoding.getByName('utf-8'))
        .toString());
  }

  Future<String> _loadCSS() async {
    return rootBundle.loadString('assets/index.css');
  }

  Future<String> _loadDarkCSS() async {
    return rootBundle.loadString('assets/index_dark.css');
  }

  Future<void> __updateWebViewHeight() async {
    final htmlHeight = _controller != null
        ? await _controller
            ?.runJavascriptReturningResult('document.documentElement.scrollHeight;')
        : '0';

    var height = _webViewHeight;
    if (htmlHeight != null && htmlHeight != 'null') {
      height = double.parse(htmlHeight);
    }

    if (_webViewHeight != height) {
      if (mounted) {
        setState(() => _webViewHeight = height);
      }
    }
  }
}
