import 'package:core/core.dart';
import 'package:core/graphql/configuration/configuration_query.dart';
import 'package:core/graphql/configuration/configuration_type_query.dart';
import 'package:core/repositories/graphql_repository.dart';
import 'package:core/utils/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

abstract class IConfigurationRepository {
  Future<Either<Failure, List<ConfigurationModel>?>> listConfiguration();
  Future<Either<Failure, List<ConfigurationTypeModel>?>>
      listConfigurationType();
}

class ConfigurationRepository extends GraphQLRepository
    implements IConfigurationRepository {
  ConfigurationRepository({required String url, required String userUrl,
    String? userToken,}) :
        super(
          url: url,
          userUrl: userUrl,
        token: userToken,
      );

  @override
  Future<Either<Failure, List<ConfigurationModel>?>> listConfiguration() async {
    try {
      final query = ConfigurationQueryQuery();

      final response = await graphqlClient.query<ConfigurationModel>(QueryOptions(
          document: query.document, fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = ConfigurationQuery$QueryRoot.fromJson(response.data!);
        return Right(root.configuration);
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<ConfigurationTypeModel>?>>
      listConfigurationType() async {
    try {
      final query = ConfigurationTypeQueryQuery();

      final response = await graphqlClient.query<ConfigurationTypeModel>(QueryOptions(
          document: query.document, fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = ConfigurationTypeQuery$QueryRoot.fromJson(response.data!);
        return Right(root.configurationType);
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }
}
