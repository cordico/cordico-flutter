import 'package:cached_network_image/cached_network_image.dart';
import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/badge/new_badge.dart';
import 'package:cordico_mobile/core/widgets/badge/urgent_badge.dart';
import 'package:cordico_mobile/core/widgets/brightcove_player.dart';
import 'package:cordico_mobile/core/widgets/card/cordico_card.dart';
import 'package:cordico_mobile/core/widgets/cordico_video_player.dart';
import 'package:cordico_mobile/core/widgets/vimeo_player.dart';
import 'package:core/core.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/utils/constants.dart';
import 'package:core/utils/content_utils.dart';
import 'package:core/utils/map_extension.dart';
import 'package:flutter/material.dart';

class CordicoContentCard extends StatefulWidget {
  const CordicoContentCard({
    Key? key,
    this.isUrgent = false,
    this.isLargePicture = false,
    this.chapters,
    this.readTime,
    this.isLast = false,
    this.content,
    required this.onPressed,
  }) : super(key: key);

  final bool isUrgent;
  final bool isLargePicture;
  final String? chapters;
  final String? readTime;
  final VoidCallback onPressed;
  final bool isLast;

  final ContentQuery$QueryRoot$Content? content;

  @override
  State<CordicoContentCard> createState() => _CordicoContentCardState();
}

class _CordicoContentCardState extends State<CordicoContentCard> {
  late ContentQuery$QueryRoot$Content? content;
  late ContentSchema contentSchema;

  bool summaryExpanded = false;
  int? maxLines = 2;
  bool exceeded = false;

  @override
  void initState() {
    super.initState();

    content = widget.content;

    if (widget.content != null) {
      removeNullAndEmptyParams(content!.data as Map<String, dynamic>);
      contentSchema =
          ContentSchema.fromJson(content!.data as Map<String, dynamic>);
    }
  }

  @override
  void didUpdateWidget(covariant CordicoContentCard oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.content != widget.content) {
      content = widget.content;

      if (widget.content != null) {
        removeNullAndEmptyParams(content!.data as Map<String, dynamic>);
        contentSchema =
            ContentSchema.fromJson(content!.data as Map<String, dynamic>);
      }
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    final summary = contentSchema.summary ?? contentSchema.description ?? '';

    exceeded = summary.isExeeded(
      lines: 2,
      width: MediaQuery.of(context).size.width - (18 * 2),
      style: ThemeStyles.contentDescription(context),
    );

    return Padding(
      padding: EdgeInsets.only(bottom: widget.isLast ? 0 : 10),
      child: CordicoCard(
        cornerRadius: 0,
        onTap: widget.onPressed,
        color: ThemeColors.gray.shade900,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 18),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              _buildCategoryHeader(context),
              if (widget.isLargePicture && contentSchema.thumbnailUrl1 != null)
                _buildBigPicture(context),
              if (contentSchema.videoUrl1 != null) _buildVideoPlayer(context),
              _buildTitle(context),
              if ((contentSchema.description != null &&
                      contentSchema.description!.isNotEmpty) ||
                  contentSchema.summary != null &&
                      contentSchema.summary!.isNotEmpty)
                _buildDescription(context),
              _buildFooter(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCategoryHeader(BuildContext context) {
    final isNew = ContentUtils.evaluateIsNew(content!);
    if ((content?.contentCategories ?? []).isNotEmpty) {
      return Wrap(
        spacing: 12,
        children: [
          ...(content?.contentCategories ?? []).map((e) {
            return Padding(
              padding: const EdgeInsets.only(bottom: 4),
              child: Container(
                constraints: const BoxConstraints(minHeight: 16),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    CircleAvatar(
                      radius: 5,
                      backgroundColor: global.colorFromCategory(e.category),
                    ),
                    const SizedBox(width: 8),
                    Text(
                      e.category.name.toUpperCase(),
                      style: ThemeStyles.cardCategory(context),
                    ),
                  ],
                ),
              ),
            );
          }).toList(),
          if (isNew)
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 8),
              child: NewBadge(),
            ),
          if (widget.isUrgent)
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 8),
              child: UrgentBadge(),
            ),
        ],
      );
    }

    return Container();
  }

  Widget _buildBigPicture(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: AspectRatio(
        aspectRatio: 16 / 9,
        child: CachedNetworkImage(
          imageUrl:
              contentSchema.thumbnailUrl1 ?? contentSchema.thumbnail ?? '',
          fit: BoxFit.cover,
          // ignore: lines_longer_than_80_chars
          placeholder: (context, url) => Container(
            color: ThemeColors.blue.shade50,
          ),
          errorWidget: (context, url, dynamic error) => Container(
            color: ThemeColors.blue.shade50,
          ),
        ),
      ),
    );
  }

  Widget getVideoPlayerWidget(String url) {
    if (url.contains('players.brightcove.net')) {
      return BrightcovePlayer(url: url);
    } else if (url.contains('player.vimeo.com') ||
        url.contains('youtube.com')) {
      return VimeoPlayer(url: url);
    }
    return CordicoVideoPlayer(videoUrl: url);
  }

  Widget _buildVideoPlayer(BuildContext context) {
    final url = contentSchema.videoUrl1!;
    final playerChild = getVideoPlayerWidget(url);

    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: playerChild,
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if ((contentSchema.thumbnailUrl1 != null ||
                  contentSchema.thumbnail != null) &&
              !widget.isLargePicture)
            Padding(
              padding: const EdgeInsets.only(right: 8),
              child: SizedBox(
                height: 43,
                child: AspectRatio(
                  aspectRatio: 16 / 9,
                  child: CachedNetworkImage(
                    imageUrl: contentSchema.thumbnailUrl1 ??
                        contentSchema.thumbnail ??
                        '',
                    fit: BoxFit.cover,
                    placeholder: (context, url) => Container(
                      color: ThemeColors.blue.shade50,
                    ),
                    imageBuilder: (context, image) {
                      return Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: image,
                            fit: BoxFit.cover,
                          ),
                        ),
                      );
                    },
                    errorWidget: (context, url, dynamic error) => Container(
                      color: ThemeColors.blue.shade50,
                    ),
                  ),
                ),
              ),
            ),
          Expanded(
            child: Text(
              contentSchema.title ?? '',
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: ThemeStyles.bulletinCardTitle(context),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildDescription(BuildContext context) {
    final summary = contentSchema.summary ?? contentSchema.description ?? '';
    final size = summary.getSize(
      context: context,
      maxLines: summaryExpanded ? null : 2,
      maxWidth: MediaQuery.of(context).size.width - (18 * 2),
      style: ThemeStyles.contentDescription(context),
    );
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: AnimatedContainer(
                onEnd: () {
                  if (summaryExpanded == false) {
                    setState(() => maxLines = 2);
                  }
                },
                duration: const Duration(milliseconds: 300),
                height: size.height,
                child: Text(
                  summary,
                  maxLines: maxLines,
                  overflow: maxLines == null ? null : TextOverflow.ellipsis,
                  style: ThemeStyles.contentDescription(context),
                ),
              ),
            ),
          ],
        ),
        if (exceeded)
          GestureDetector(
            onTap: () {
              setState(() {
                summaryExpanded = !summaryExpanded;
                if (summaryExpanded) {
                  maxLines = null;
                }
              });
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    summaryExpanded ? 'Read less...' : 'Read more...',
                    maxLines: 1,
                    style: ThemeStyles.sectionTitle(context)
                        ?.copyWith(fontSize: 13.3),
                  ),
                ],
              ),
            ),
          )
      ],
    );
  }

  Widget _buildFooter(BuildContext context) {
    var footer = '';
    final formattedDate = DateExtensionCore.formatDate(
        date: content?.createdAt, format: 'MMM dd, yyyy \u00B7 hh:mm');

    if (content?.contentType != null) {
      footer += footer.isEmpty
          ? '${content?.contentType.name} '
          : '\u00B7 ${content?.contentType.name} ';
    }

    footer += footer.isEmpty
        ? '$formattedDate'
        : ' \u00B7  ${ContentUtils.returnPartsCount(content: content!)}';

    if (content?.contentType != null &&
        content?.contentType.id != kContentTypeAssessmentId) {
      //ADDING READING TIME ONLY IF IS NOT AN ASSESSMENT
      footer += ' \u00B7  ${ContentUtils.readingTimeFromContent(content!)}';
    }

    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: Row(
        children: [
          Expanded(
            child: Text(
              footer,
              style: ThemeStyles.smallParagraph(context),
            ),
          )
        ],
      ),
    );
  }
}
