// ignore_for_file: cascade_invocations

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/buttons/action_button.dart';
import 'package:cordico_mobile/core/widgets/card/cordico_card.dart';
import 'package:core/core.dart';
import 'package:core/utils/map_extension.dart';
import 'package:flutter/material.dart';

class CrisisHotlineCard extends StatelessWidget {
  const CrisisHotlineCard({
    Key? key,
    required this.contact,
    this.isLast = false,
    this.reduceDescription = false,
  }) : super(key: key);

  final ContentModel? contact;

  final bool isLast;
  final bool reduceDescription;

  ContentSchema get content {
    removeNullAndEmptyParams(contact!.data as Map<String, dynamic>);
    final content =
        ContentSchema.fromJson(contact?.data as Map<String, dynamic>);
    return content;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: isLast ? 0 : 10),
      child: CordicoCard(
        cornerRadius: 0,
        color: ThemeColors.gray.shade900,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 18),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: _buildCategoryHeader(context),
              ),
              _buildContactDetails(context),
              _buildDescription(context),
              _buildActions(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCategoryHeader(BuildContext context) {
    return Row(
      children: [
        Flexible(
          child: Text(
            (contact?.contentType.name ?? '')
                .replaceAll('-', ' ')
                .toUpperCase(),
            style: ThemeStyles.cardCategory(context),
          ),
        ),
        /*if (distance != null)
          Padding(
            padding: const EdgeInsets.only(left: 8),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                const Icon(
                  Icons.map_outlined,
                  size: 11,
                  color: Colors.white,
                ),
                Container(width: 4),
                Text(
                  distance!,
                  style: ThemeStyles.sectionTitle(context)?.copyWith(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                ),
                const SizedBox(width: 2),
                Text(
                  'Miles',
                  style: ThemeStyles.sectionTitle(context)?.copyWith(
                    fontSize: 9,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        if (isNew)
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: NewBadge(),
          ),*/
      ],
    );
  }

  Widget _buildContactDetails(BuildContext context) {
    return Row(
      children: [
        if (content.icon != null && content.icon!.isNotEmpty)
          _buildProfileImage(context),
        if (content.icon != null && content.icon!.isNotEmpty)
          Container(width: 12),
        Expanded(
          child: _buildTitleAndPhoneNumber(context),
        ),
      ],
    );
  }

  Widget _buildProfileImage(BuildContext context) {
    return Container(
      width: 58,
      height: 58,
      clipBehavior: Clip.antiAlias,
      decoration: BoxDecoration(
        color: ThemeColors.gray.shade300,
        borderRadius: BorderRadius.circular(12),
      ),
      child: CachedNetworkImage(
        imageUrl: content.icon!,
        fit: BoxFit.cover,
        placeholder: (context, url) => Container(
          color: ThemeColors.gray.shade300,
        ),
        imageBuilder: (context, image) {
          return Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: image,
                fit: BoxFit.cover,
              ),
            ),
          );
        },
        errorWidget: (context, url, dynamic error) {
          return const Icon(Icons.error);
        },
      ),
    );
  }

  Widget _buildTitleAndPhoneNumber(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          content.title ?? '',
          style: ThemeStyles.bulletinCardTitle(context)
              ?.copyWith(color: Colors.white),
        ),
        Container(height: 8),
        Text(
          content.contactPhone ?? '',
          style: ThemeStyles.cardContentTitle(context)
              ?.copyWith(color: ThemeColors.green.shade200),
        ),
      ],
    );
  }

  Widget _buildDescription(BuildContext context) {
    if (content.body != null && content.body!.isNotEmpty) {
      return Padding(
          padding: const EdgeInsets.only(top: 8),
          child: Text(
            content.body ?? '',
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: ThemeStyles.contentDescription(context)?.copyWith(
              color: ThemeColors.gray.shade200,
            ),
          )

          //     ContentWidget(
          //   content: content.body ?? '',
          //   contentType: (content.bodyMimeType!.isEmpty ||
          //           content.bodyMimeType == 'text/markdown')
          //       ? MimeType.markdown
          //       : MimeType.html,
          // ),
          );
    }
    return Container();
  }

  Widget _buildActions(BuildContext context) {
    final allButtons = List<ActionButtonSchema>.from(
        content.actionButtons ?? <ActionButtonSchema>[]);

    allButtons.sort((a, b) {
      return a.actionType == ActionButtonType.call ||
              a.actionType == ActionButtonType.anonymousCall
          ? -1
          : 1;
    });

    allButtons.sort((a, b) {
      return a.isDefault == true ? -1 : 1;
    });

    final defualtButtons = <ActionButtonSchema>[];
    final normalButtons = <ActionButtonSchema>[];

    var defualtButtonsLenght = 0;
    var normalButtonsLenght = 0;

    // ignore: avoid_function_literals_in_foreach_calls
    allButtons.forEach((element) {
      if (element.isDefault == true) {
        defualtButtonsLenght += 1;
        defualtButtons.add(element);
      } else {
        normalButtonsLenght += 1;
        normalButtons.add(element);
      }
    });

    final width = (MediaQuery.of(context).size.width - (18 * 2) - 16) / 2;
    final fullWidth = MediaQuery.of(context).size.width - (18 * 2);

    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: Wrap(
        alignment: WrapAlignment.center,
        spacing: 16,
        runSpacing: 8,
        children: [
          if (defualtButtons.isNotEmpty)
            ...defualtButtons.asMap().entries.map((e) {
              final firstFullWidth =
                  (normalButtonsLenght + defualtButtonsLenght) % 2 != 0;

              return SizedBox(
                width: firstFullWidth && e.key == 0 ? fullWidth : width,
                child: ActionButton(
                  action: e.value,
                  content: null,
                  height: 36,
                ),
              );
            }).toList(),
          if (normalButtons.isNotEmpty)
            ...normalButtons.asMap().entries.map((e) {
              final firstFullWidth =
                  (normalButtonsLenght + defualtButtonsLenght) % 2 != 0;

              return SizedBox(
                width: firstFullWidth && e.key == 0 ? fullWidth : width,
                child: ActionButton(
                  action: e.value,
                  content: null,
                  height: 36,
                ),
              );
            }).toList()
        ],
      ),
    );
  }
}
