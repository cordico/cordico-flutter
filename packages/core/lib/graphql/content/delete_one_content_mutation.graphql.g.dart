// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'delete_one_content_mutation.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeleteOneContentMutation$MutationRoot$DeleteOneContent
    _$DeleteOneContentMutation$MutationRoot$DeleteOneContentFromJson(
        Map<String, dynamic> json) {
  return DeleteOneContentMutation$MutationRoot$DeleteOneContent()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$DeleteOneContentMutation$MutationRoot$DeleteOneContentToJson(
            DeleteOneContentMutation$MutationRoot$DeleteOneContent instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

DeleteOneContentMutation$MutationRoot
    _$DeleteOneContentMutation$MutationRootFromJson(Map<String, dynamic> json) {
  return DeleteOneContentMutation$MutationRoot()
    ..deleteOneContent = json['deleteOneContent'] == null
        ? null
        : DeleteOneContentMutation$MutationRoot$DeleteOneContent.fromJson(
            json['deleteOneContent'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DeleteOneContentMutation$MutationRootToJson(
        DeleteOneContentMutation$MutationRoot instance) =>
    <String, dynamic>{
      'deleteOneContent': instance.deleteOneContent?.toJson(),
    };

DeleteOneContentMutationArguments _$DeleteOneContentMutationArgumentsFromJson(
    Map<String, dynamic> json) {
  return DeleteOneContentMutationArguments(
    id: json['id'] as String?,
  );
}

Map<String, dynamic> _$DeleteOneContentMutationArgumentsToJson(
        DeleteOneContentMutationArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
