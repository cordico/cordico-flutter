// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'content_by_category.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContentByCategory$QueryRoot$Content$ContentType
    _$ContentByCategory$QueryRoot$Content$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic> _$ContentByCategory$QueryRoot$Content$ContentTypeToJson(
        ContentByCategory$QueryRoot$Content$ContentType instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'is_shared': instance.isShared,
      'description': instance.description,
    };

ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentType
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentTypeToJson(
            ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'is_shared': instance.isShared,
          'description': instance.description,
        };

ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ChildContent
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ChildContentFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ChildContent()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ChildContentToJson(
            ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ChildContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ParentContent
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ParentContentFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic>
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ParentContentToJson(
            ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ParentContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'index': instance.index,
          'part_id': instance.partId,
        };

ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$CategoryToJson(
            ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories()
    ..category =
        ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories$Category
            .fromJson(json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategoriesToJson(
            ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories
                instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentState
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentStateFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentStateToJson(
            ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentState
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
        };

ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
            ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$CollectionToJson(
            ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$CollectionsFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections()
    ..collection =
        ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections$Collection
            .fromJson(json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$CollectionsToJson(
            ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections
                instance) =>
        <String, dynamic>{
          'collection': instance.collection.toJson(),
        };

ContentByCategory$QueryRoot$Content$ChildContent$ContentPart
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPartFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$ChildContent$ContentPart()
    ..contentType =
        ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentType
            .fromJson(json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) =>
            ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ChildContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) =>
            ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ParentContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentCategories
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentState =
        ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$ContentState
            .fromJson(json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) =>
            ContentByCategory$QueryRoot$Content$ChildContent$ContentPart$Collections
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic>
    _$ContentByCategory$QueryRoot$Content$ChildContent$ContentPartToJson(
            ContentByCategory$QueryRoot$Content$ChildContent$ContentPart
                instance) =>
        <String, dynamic>{
          'content_type': instance.contentType.toJson(),
          'updated_at': instance.updatedAt?.toIso8601String(),
          'name': instance.name,
          'is_shared': instance.isShared,
          'id': instance.id,
          'data': instance.data,
          'created_at': instance.createdAt.toIso8601String(),
          'child_content':
              instance.childContent.map((e) => e.toJson()).toList(),
          'parent_content':
              instance.parentContent.map((e) => e.toJson()).toList(),
          'content_categories':
              instance.contentCategories.map((e) => e.toJson()).toList(),
          'content_state': instance.contentState.toJson(),
          'collections': instance.collections.map((e) => e.toJson()).toList(),
        };

ContentByCategory$QueryRoot$Content$ChildContent
    _$ContentByCategory$QueryRoot$Content$ChildContentFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$ChildContent()
    ..contentPart =
        ContentByCategory$QueryRoot$Content$ChildContent$ContentPart.fromJson(
            json['content_part'] as Map<String, dynamic>)
    ..id = json['id'] as String
    ..index = json['index'] as int;
}

Map<String, dynamic> _$ContentByCategory$QueryRoot$Content$ChildContentToJson(
        ContentByCategory$QueryRoot$Content$ChildContent instance) =>
    <String, dynamic>{
      'content_part': instance.contentPart.toJson(),
      'id': instance.id,
      'index': instance.index,
    };

ContentByCategory$QueryRoot$Content$ParentContent
    _$ContentByCategory$QueryRoot$Content$ParentContentFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic> _$ContentByCategory$QueryRoot$Content$ParentContentToJson(
        ContentByCategory$QueryRoot$Content$ParentContent instance) =>
    <String, dynamic>{
      'id': instance.id,
      'index': instance.index,
      'part_id': instance.partId,
    };

ContentByCategory$QueryRoot$Content$ContentCategories$Category
    _$ContentByCategory$QueryRoot$Content$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$ContentByCategory$QueryRoot$Content$ContentCategories$CategoryToJson(
            ContentByCategory$QueryRoot$Content$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

ContentByCategory$QueryRoot$Content$ContentCategories
    _$ContentByCategory$QueryRoot$Content$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$ContentCategories()
    ..category =
        ContentByCategory$QueryRoot$Content$ContentCategories$Category.fromJson(
            json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$ContentByCategory$QueryRoot$Content$ContentCategoriesToJson(
            ContentByCategory$QueryRoot$Content$ContentCategories instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

ContentByCategory$QueryRoot$Content$ContentState
    _$ContentByCategory$QueryRoot$Content$ContentStateFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic> _$ContentByCategory$QueryRoot$Content$ContentStateToJson(
        ContentByCategory$QueryRoot$Content$ContentState instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };

ContentByCategory$QueryRoot$Content$Collections$Collection$CollectionType
    _$ContentByCategory$QueryRoot$Content$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$ContentByCategory$QueryRoot$Content$Collections$Collection$CollectionTypeToJson(
            ContentByCategory$QueryRoot$Content$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

ContentByCategory$QueryRoot$Content$Collections$Collection
    _$ContentByCategory$QueryRoot$Content$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : ContentByCategory$QueryRoot$Content$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String,
    dynamic> _$ContentByCategory$QueryRoot$Content$Collections$CollectionToJson(
        ContentByCategory$QueryRoot$Content$Collections$Collection instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'collection_type': instance.collectionType?.toJson(),
    };

ContentByCategory$QueryRoot$Content$Collections
    _$ContentByCategory$QueryRoot$Content$CollectionsFromJson(
        Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content$Collections()
    ..collection =
        ContentByCategory$QueryRoot$Content$Collections$Collection.fromJson(
            json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ContentByCategory$QueryRoot$Content$CollectionsToJson(
        ContentByCategory$QueryRoot$Content$Collections instance) =>
    <String, dynamic>{
      'collection': instance.collection.toJson(),
    };

ContentByCategory$QueryRoot$Content
    _$ContentByCategory$QueryRoot$ContentFromJson(Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot$Content()
    ..contentType = ContentByCategory$QueryRoot$Content$ContentType.fromJson(
        json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) => ContentByCategory$QueryRoot$Content$ChildContent.fromJson(
            e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) => ContentByCategory$QueryRoot$Content$ParentContent.fromJson(
            e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            ContentByCategory$QueryRoot$Content$ContentCategories.fromJson(
                e as Map<String, dynamic>))
        .toList()
    ..contentState = ContentByCategory$QueryRoot$Content$ContentState.fromJson(
        json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) => ContentByCategory$QueryRoot$Content$Collections.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$ContentByCategory$QueryRoot$ContentToJson(
        ContentByCategory$QueryRoot$Content instance) =>
    <String, dynamic>{
      'content_type': instance.contentType.toJson(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'name': instance.name,
      'is_shared': instance.isShared,
      'id': instance.id,
      'data': instance.data,
      'created_at': instance.createdAt.toIso8601String(),
      'child_content': instance.childContent.map((e) => e.toJson()).toList(),
      'parent_content': instance.parentContent.map((e) => e.toJson()).toList(),
      'content_categories':
          instance.contentCategories.map((e) => e.toJson()).toList(),
      'content_state': instance.contentState.toJson(),
      'collections': instance.collections.map((e) => e.toJson()).toList(),
    };

ContentByCategory$QueryRoot _$ContentByCategory$QueryRootFromJson(
    Map<String, dynamic> json) {
  return ContentByCategory$QueryRoot()
    ..content = (json['content'] as List<dynamic>)
        .map((e) => ContentByCategory$QueryRoot$Content.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$ContentByCategory$QueryRootToJson(
        ContentByCategory$QueryRoot instance) =>
    <String, dynamic>{
      'content': instance.content.map((e) => e.toJson()).toList(),
    };

ContentByCategoryArguments _$ContentByCategoryArgumentsFromJson(
    Map<String, dynamic> json) {
  return ContentByCategoryArguments(
    categoryID: json['categoryID'] as String,
    contentStateNames: (json['contentStateNames'] as List<dynamic>?)
        ?.map((e) => e as String?)
        .toList(),
    offset: json['offset'] as int?,
    limit: json['limit'] as int?,
  );
}

Map<String, dynamic> _$ContentByCategoryArgumentsToJson(
        ContentByCategoryArguments instance) =>
    <String, dynamic>{
      'categoryID': instance.categoryID,
      'contentStateNames': instance.contentStateNames,
      'offset': instance.offset,
      'limit': instance.limit,
    };
