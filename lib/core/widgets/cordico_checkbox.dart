import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:flutter/material.dart';

class CordicoCheckbox extends StatefulWidget {
  const CordicoCheckbox({
    Key? key,
    required this.onChanged,
    this.value = false,
    this.labelText,
    this.labelWidget,
  }) : super(key: key);

  final bool value;
  final ValueChanged<bool?> onChanged;
  final Widget? labelWidget;
  final String? labelText;

  @override
  State createState() => CordicoCheckboxState();
}

class CordicoCheckboxState extends State<CordicoCheckbox> {
  bool checked = false;

  @override
  void initState() {
    checked = widget.value;
    super.initState();
  }

  @override
  void didUpdateWidget(covariant CordicoCheckbox oldWidget) {
    checked = widget.value;
    super.didUpdateWidget(oldWidget);
  }

  void onCheckedChanged(bool? value) {
    setState(() {
      checked = value!;
      widget.onChanged(checked);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Checkbox(
          value: checked,
          activeColor: ThemeColors.blue.shade500,
          onChanged: onCheckedChanged,
        ),
        Expanded(
          child: widget.labelWidget ??
              Text(
                widget.labelText!,
                style: ThemeStyles.textFieldTextStyle(context),
              ),
        ),
      ],
    );
  }
}
