import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:flutter/material.dart';

class NewBadge extends StatelessWidget {
  const NewBadge({
    Key? key,
    this.numberOfNewItems,
    this.compact = true,
  }) : super(key: key);

  final int? numberOfNewItems;
  final bool compact;

  @override
  Widget build(BuildContext context) {
    final badgeText =
        numberOfNewItems != null ? '${numberOfNewItems!} NEW' : 'NEW';

    return Container(
      padding: compact
          ? const EdgeInsets.symmetric(vertical: 1, horizontal: 2)
          : const EdgeInsets.symmetric(vertical: 2, horizontal: 6),
      color: compact ? ThemeColors.blue.shade500 : ThemeColors.blue.shade400,
      child: Text(
        badgeText,
        style: compact
            ? ThemeStyles.extraSmallParagraph(context)?.copyWith(
                color: ThemeColors.blue.shade50,
                fontWeight: FontWeight.w700,
                fontSize: 9.5,
              )
            : ThemeStyles.extraSmallParagraph(context)?.copyWith(
                color: ThemeColors.blue.shade50, fontWeight: FontWeight.w600),
      ),
    );
  }
}
