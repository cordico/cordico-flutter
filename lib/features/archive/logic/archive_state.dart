// ignore_for_file: cascade_invocations

import 'package:cordico_mobile/core/providers/global_providers.dart';
import 'package:core/core.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class ArchiveState extends StateNotifier<List<ContentQuery$QueryRoot$Content>> {
  ArchiveState(this.ref) : super([]);

  bool _isLoading = false;
  bool get isLoading => _isLoading;

  bool _contentLoaded = false;
  bool get contentLoaded => _contentLoaded;

  final StateNotifierProviderRef ref;

  Future<void> getContent() async {
    _isLoading = true;

    final response = await ref.watch(categoryContentFuture.future);

    if (response != null) {
      state = response.map((e) {
        return ContentQuery$QueryRoot$Content.fromJson(e.toJson());
      }).toList();

      _contentLoaded = true;
    }
    _isLoading = false;
  }
}

final archiveContentProvider =
    StateNotifierProvider<ArchiveState, List<ContentQuery$QueryRoot$Content>>(
        (ref) => ArchiveState(ref)..getContent());

final finalContentListProvider =
    StateProvider<Map<String, String>>((ref) => {});

final categoryContentFuture = FutureProvider<List<ContentModel>?>((ref) async {
  final repository = ref.watch(contentRepositoryProvider);

  final input = ref.watch(finalContentListProvider);
  final id = input['id'];
  final result = await repository.getContentByCategory(id ?? '');
  List<ContentModel>? resultContent;
  result.fold((error) {}, (state) => resultContent = state);
  return resultContent;
});
