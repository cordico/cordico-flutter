const int contentBoxId = 0;
const int contentTypeBoxId = 1;
const int categoryBoxId = 2;
const int contentStateBoxId = 3;

const String kContentTypeBulletinId = '79328b13-18cb-4cea-b575-929293d7d953';
const String kContentTypeAssessmentId = '50fed8de-0677-4ede-98ae-5240fd00aa2f';
const String kContentTypeGuideId = '56966a06-9a81-4bd5-b1d2-56aa958cc3d6';
const String kContentTypeGetHelpId = '44f457c1-bbad-44b2-b64a-2a8fed825620';
const String kContentTypeCovidId = '38f84ee1-b4af-4694-978d-a88e1fa702e6';
const String kContentTypeDepartmentLinkId =
    'd3b92eac-477e-476c-8618-361deeefc98b';
const String kContentTypeEAPId = '819bd441-0347-451b-b65c-f6b59980745a';

const String kContactTypeChaplainId = '42096150-bd8f-4fae-b83a-698f359eb139';
const String kContactTypeTherapistId = 'eb91c38f-48d4-43da-b5b7-99377a8575b1';
const String kContactTypePeerSupportId = '30534eb1-e189-4af4-9f08-0b0203208562';
const String kContactTypeCrisisId = '541e7ac0-15ad-48a2-8965-6875e3abd6e0';

const String kCategoryDrGilmartinId = '142385ff-185c-406f-81ca-0ccba39750d0';
const String kCategoryAdditionalResourcesId =
    '35c80756-4579-48f2-82bb-232fe1d7fc05';

const String kCollectionCordicoFeaturedId =
    '0a831ad9-1129-4f03-b155-8efc5be64aed';
const String kCollectionWhatsNewId = '0d8359cf-322f-4f4a-bcf8-634a5637317a';
const String kCollectionNewsFeedId = 'd041c815-8123-4bd7-abc7-cc66dfb18527';

const String kCollectionTypeBulletin = '56c2085e-b229-49d8-bafb-d6cf10e6350d';
const String kCollectionTypeDepartmentLink =
    '1fb704ed-b1f4-4242-93bb-447dcf0eda09';
const String kCollectionTypeEAP = '722d4499-34b2-4623-89cc-f33d5c1b3421';

const String kCollectionBulletinTypeKey = 'cordico_bulletin';

const String kFeedbackTypeUser = '029cd1b5-a2f4-4ab0-94c4-6865d398bbd8';

const String kSharedPrefContentId = 'liked_content';
const String kCordicoFeedbackEmail = 'feedback@cordico.com';

const String kFeedbackUserIDExample = '39be9911-5f73-4d2a-bdeb-b8b1f882a93e';

abstract class HiveConstants {
  static const int contentBoxId = 0;
  static const int contentTypeBoxId = 1;
  static const int categoryBoxId = 2;
  static const int contentStateBoxId = 3;

  static const String contentBox = 'content';
  static const String contentTypeBox = 'contentType';
  static const String categoryBox = 'category';
  static const String contentStateBox = 'contentState';
  static const String entityVersionBox = 'entityVersion';
  static const String articleBox = 'article';
}
