// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'feedback_type_query.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedbackTypeQuery$QueryRoot$FeedbackType
    _$FeedbackTypeQuery$QueryRoot$FeedbackTypeFromJson(
        Map<String, dynamic> json) {
  return FeedbackTypeQuery$QueryRoot$FeedbackType()
    ..id = json['id'] as String
    ..key = json['key'] as String
    ..name = json['name'] as String
    ..schema = json['schema'];
}

Map<String, dynamic> _$FeedbackTypeQuery$QueryRoot$FeedbackTypeToJson(
        FeedbackTypeQuery$QueryRoot$FeedbackType instance) =>
    <String, dynamic>{
      'id': instance.id,
      'key': instance.key,
      'name': instance.name,
      'schema': instance.schema,
    };

FeedbackTypeQuery$QueryRoot _$FeedbackTypeQuery$QueryRootFromJson(
    Map<String, dynamic> json) {
  return FeedbackTypeQuery$QueryRoot()
    ..feedbackType = (json['feedback_type'] as List<dynamic>)
        .map((e) => FeedbackTypeQuery$QueryRoot$FeedbackType.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$FeedbackTypeQuery$QueryRootToJson(
        FeedbackTypeQuery$QueryRoot instance) =>
    <String, dynamic>{
      'feedback_type': instance.feedbackType.map((e) => e.toJson()).toList(),
    };

FeedbackTypeQueryArguments _$FeedbackTypeQueryArgumentsFromJson(
    Map<String, dynamic> json) {
  return FeedbackTypeQueryArguments(
    limit: json['limit'] as int?,
    offset: json['offset'] as int?,
  );
}

Map<String, dynamic> _$FeedbackTypeQueryArgumentsToJson(
        FeedbackTypeQueryArguments instance) =>
    <String, dynamic>{
      'limit': instance.limit,
      'offset': instance.offset,
    };
