// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'create_one_organization_contact_mutation.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CreateOneOrganizationContactMutation$MutationRoot$CreateOneOrganizationContact
    extends JsonSerializable with EquatableMixin {
  CreateOneOrganizationContactMutation$MutationRoot$CreateOneOrganizationContact();

  factory CreateOneOrganizationContactMutation$MutationRoot$CreateOneOrganizationContact.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneOrganizationContactMutation$MutationRoot$CreateOneOrganizationContactFromJson(
          json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneOrganizationContactMutation$MutationRoot$CreateOneOrganizationContactToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneOrganizationContactMutation$MutationRoot extends JsonSerializable
    with EquatableMixin {
  CreateOneOrganizationContactMutation$MutationRoot();

  factory CreateOneOrganizationContactMutation$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneOrganizationContactMutation$MutationRootFromJson(json);

  late CreateOneOrganizationContactMutation$MutationRoot$CreateOneOrganizationContact
      createOneOrganizationContact;

  @override
  List<Object?> get props => [createOneOrganizationContact];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneOrganizationContactMutation$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateOneOrganizationContactMutationArguments extends JsonSerializable
    with EquatableMixin {
  CreateOneOrganizationContactMutationArguments(
      {this.contactTypeId,
      this.organizationId,
      required this.name,
      required this.title,
      this.data,
      this.isActive});

  @override
  factory CreateOneOrganizationContactMutationArguments.fromJson(
          Map<String, dynamic> json) =>
      _$CreateOneOrganizationContactMutationArgumentsFromJson(json);

  final String? contactTypeId;

  final String? organizationId;

  late String name;

  late String title;

  final dynamic? data;

  final bool? isActive;

  @override
  List<Object?> get props =>
      [contactTypeId, organizationId, name, title, data, isActive];
  @override
  Map<String, dynamic> toJson() =>
      _$CreateOneOrganizationContactMutationArgumentsToJson(this);
}

final CREATE_ONE_ORGANIZATION_CONTACT_MUTATION_MUTATION_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'CreateOneOrganizationContactMutation'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contactTypeId')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'organizationId')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'name')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'title')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'data')),
            type:
                NamedTypeNode(name: NameNode(value: 'Json'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'isActive')),
            type: NamedTypeNode(
                name: NameNode(value: 'Boolean'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'createOneOrganizationContact'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'data'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'contactType'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'connect'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'id'),
                                    value: VariableNode(
                                        name: NameNode(value: 'contactTypeId')))
                              ]))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'name'),
                        value: VariableNode(name: NameNode(value: 'name'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'organization'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'connect'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'id'),
                                    value: VariableNode(
                                        name:
                                            NameNode(value: 'organizationId')))
                              ]))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'title'),
                        value: VariableNode(name: NameNode(value: 'title'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'data'),
                        value: VariableNode(name: NameNode(value: 'data'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'isActive'),
                        value: VariableNode(name: NameNode(value: 'isActive')))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class CreateOneOrganizationContactMutationMutation extends GraphQLQuery<
    CreateOneOrganizationContactMutation$MutationRoot,
    CreateOneOrganizationContactMutationArguments> {
  CreateOneOrganizationContactMutationMutation({required this.variables});

  @override
  final DocumentNode document =
      CREATE_ONE_ORGANIZATION_CONTACT_MUTATION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'CreateOneOrganizationContactMutation';

  @override
  final CreateOneOrganizationContactMutationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  CreateOneOrganizationContactMutation$MutationRoot parse(
          Map<String, dynamic> json) =>
      CreateOneOrganizationContactMutation$MutationRoot.fromJson(json);
}
