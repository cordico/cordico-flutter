// ignore_for_file: lines_longer_than_80_chars

import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/card/cordico_content_card.dart';
import 'package:cordico_mobile/core/widgets/card/hotline_card.dart';
import 'package:cordico_mobile/features/department/views/bulletin_cell.dart';
import 'package:core/graphql/collection/collection_by_id.dart';
import 'package:core/graphql/content/content_by_category.dart';
import 'package:core/graphql/content/content_by_content_type.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/graphql/organization/organization_contact_by_id.dart';
import 'package:flutter/material.dart';

class ContentListView<T> extends StatelessWidget {
  const ContentListView({
    Key? key,
    required this.source,
    this.onItemTap,
    this.isBulletin = false,
    this.padding = EdgeInsets.zero,
    this.header,
    this.isSliver = false,
    this.customItemBuilder,
  }) : super(key: key);

  final List<T> source;
  final Function(int)? onItemTap;
  final bool isBulletin;
  final bool isSliver;
  final EdgeInsets padding;
  final String? header;
  final Widget Function(BuildContext, int)? customItemBuilder;

  @override
  Widget build(BuildContext context) {
    final hasHeader = header != null && header!.isNotEmpty;
    return source.isEmpty
        ? emptyWidget()
        : returnListView(hasHeader: hasHeader);
  }

  Widget returnListView({bool hasHeader = false}) {
    if (isSliver) {
      return SliverList(
        delegate: SliverChildBuilderDelegate(
          customItemBuilder ??
              (context, index) {
                return returnItems(context, index, hasHeader);
              },
          childCount: source.length + (hasHeader ? 1 : 0),
        ),
      );
    }

    return ListView.builder(
      primary: false,
      padding: padding,
      shrinkWrap: true,
      itemCount: source.length + (hasHeader ? 1 : 0),
      itemBuilder: customItemBuilder ??
          (context, index) {
            return returnItems(context, index, hasHeader);
          },
    );
  }

  Widget emptyWidget() {
    const widget = SizedBox(
      height: 150,
      child: Center(
        child: Text(
          'No results.',
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
    if (isSliver) {
      return const SliverToBoxAdapter(child: widget);
    }
    return widget;
  }

  Widget returnItems(BuildContext context, int idx, bool hasHeader) {
    final index = idx - (hasHeader ? 1 : 0);
    if (hasHeader && idx == 0) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(20, 24, 0, 20),
        child: Text(
          header ?? '',
          style: ThemeStyles.sectionTitle(context),
        ),
      );
    }
    if (source
        is List<OrganizationContactById$QueryRoot$OrganizationContactsByPk>) {
      final item = source[index]
          as OrganizationContactById$QueryRoot$OrganizationContactsByPk;
      return HotlineCard(contact: item, isLast: index == source.length - 1);
    } else if (source
        is List<CollectionById$QueryRoot$CollectionByPk$CollectionContents>) {
      final item = source[index]
          as CollectionById$QueryRoot$CollectionByPk$CollectionContents;

      final finalItem =
          ContentQuery$QueryRoot$Content.fromJson(item.content.toJson());

      return CordicoContentCard(
        content: finalItem,
        isLast: index == source.length - 1,
        onPressed: () {
          if (onItemTap != null) {
            onItemTap!(index);
          }
        },
      );
    } else if (source is List<ContentQuery$QueryRoot$Content>) {
      final item = source[index] as ContentQuery$QueryRoot$Content;

      if (isBulletin) {
        return BulletinCell(
          bulletin: item,
          onPressed: () {
            if (onItemTap != null) {
              onItemTap!(index);
            }
          },
          isLast: index == source.length - 1,
        );
      }
      return CordicoContentCard(
        content: item,
        isLast: index == source.length - 1,
        onPressed: () {
          if (onItemTap != null) {
            onItemTap!(index);
          }
        },
      );
    } else if (source is List<ContentByCategory$QueryRoot$Content>) {
      final item = source[index] as ContentByCategory$QueryRoot$Content;
      return CordicoContentCard(
        content: ContentQuery$QueryRoot$Content.fromJson(item.toJson()),
        isLast: index == source.length - 1,
        onPressed: () {
          if (onItemTap != null) {
            onItemTap!(index);
          }
        },
      );
    } else if (source is List<ContentByContentType$QueryRoot$Content>) {
      final item = source[index] as ContentByContentType$QueryRoot$Content;
      return CordicoContentCard(
        content: ContentQuery$QueryRoot$Content.fromJson(item.toJson()),
        isLast: index == source.length - 1,
        onPressed: () {
          if (onItemTap != null) {
            onItemTap!(index);
          }
        },
      );
    }

    return Container();
  }
}
