// ignore_for_file: lines_longer_than_80_chars

import 'package:core/core.dart';
import 'package:core/graphql/asset/asset_by_id.dart';
import 'package:core/graphql/asset/asset_type_query.dart';
import 'package:core/graphql/asset/create_one_organization_asset.dart';
import 'package:core/graphql/asset/delete_one_organization_asset.dart';
import 'package:core/graphql/asset/organization_asset_by_id.dart';
import 'package:core/graphql/asset/organization_asset_by_organization.dart';
import 'package:core/repositories/graphql_repository.dart';
import 'package:core/utils/failure.dart';
import 'package:core/utils/map_extension.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

abstract class IAssetRepository {
  Future<Either<Failure, AssetModel?>> getAssetById(String id,
      {FetchPolicy? fetchPolicy});
  Future<Either<Failure, List<AssetTypeModel>?>> listAssetType();

  Future<Either<Failure, AssetModel?>> createOneAsset(
      {required CreateOneAssetMutationArguments arguments,
      String? authorizationToken});

  //ORGANIZATION ASSET
  Future<Either<Failure, OrganizationAsset?>> getOrganizationAssetById(
      String id,
      {FetchPolicy? fetchPolicy});
  Future<Either<Failure, List<OrganizationAsset>?>> listOrganizationAsset(
      {required OrganizationAssetByOrganizationArguments arguments});
  Future<Either<Failure, AssetModel?>> createOneOrganizationAsset(
      {required CreateOneOrganizationAssetArguments arguments,
      String? authorizationToken});
  Future<Either<Failure, AssetModel?>> deleteOneOrganizationAsset(
      {required DeleteOneOrganizationAssetArguments arguments,
      String? authorizationToken});
}

class AssetRepository extends GraphQLRepository implements IAssetRepository {
  AssetRepository(
      {required String url, required String userUrl, required String uploadUrl,
        String? userToken,})
      : super(url: url, userUrl: userUrl, uploadUrl: uploadUrl,
          token: userToken,);

  @override
  Future<Either<Failure, AssetModel?>> getAssetById(String id,
      {FetchPolicy? fetchPolicy}) async {
    try {
      final query = AssetByIdQuery(variables: AssetByIdArguments(id: id));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<AssetModel>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: fetchPolicy ?? FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = AssetById$QueryRoot.fromJson(response.data!);
        if (root.assetByPk != null) {
          return Right(root.assetByPk);
        }
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<AssetTypeModel>?>> listAssetType() async {
    try {
      final query = AssetTypeQueryQuery();

      final response = await graphqlClient.query<AssetTypeModel>(QueryOptions(
          document: query.document, fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = AssetTypeQuery$QueryRoot.fromJson(response.data!);
        return Right(root.assetType);
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, AssetModel?>> createOneAsset(
      {required CreateOneAssetMutationArguments arguments,
      String? authorizationToken}) async {
    try {
      final query = CreateOneAssetMutationMutation(variables: arguments);

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await uploadGraphQLClient.mutate<AssetModel>(MutationOptions(
          document: query.document,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          variables: variables,
          fetchPolicy: FetchPolicy.noCache));
      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }
      if (response.data != null) {
        final root =
            CreateOneAssetMutation$MutationRoot.fromJson(response.data!);
        final element = await getAssetById(root.createOneAssetWithFile,
            fetchPolicy: FetchPolicy.noCache);
        return element;
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  //ORGANIZATION ASSET
  @override
  Future<Either<Failure, OrganizationAsset?>> getOrganizationAssetById(
      String id,
      {FetchPolicy? fetchPolicy}) async {
    try {
      final query = OrganizationAssetByIdQuery(
          variables: OrganizationAssetByIdArguments(id: id));

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<OrganizationAsset>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: fetchPolicy ?? FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root = OrganizationAssetById$QueryRoot.fromJson(response.data!);
        if (root.organizationAssetByPk != null) {
          return Right(root.organizationAssetByPk!.asset);
        }
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, List<OrganizationAsset>?>> listOrganizationAsset(
      {required OrganizationAssetByOrganizationArguments arguments}) async {
    try {
      final query = OrganizationAssetByOrganizationQuery(variables: arguments);

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await graphqlClient.query<OrganizationAsset>(QueryOptions(
          document: query.document,
          variables: variables,
          fetchPolicy: FetchPolicy.cacheAndNetwork));

      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }

      if (response.data != null) {
        final root =
            OrganizationAssetByOrganization$QueryRoot.fromJson(response.data!);
        return Right(root.organizationAsset
            .map((e) => OrganizationAsset.fromJson(e.asset.toJson()))
            .toList());
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, AssetModel?>> createOneOrganizationAsset(
      {required CreateOneOrganizationAssetArguments arguments,
      String? authorizationToken}) async {
    try {
      final query = CreateOneOrganizationAssetMutation(variables: arguments);

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await uploadGraphQLClient.mutate<AssetModel>(MutationOptions(
          document: query.document,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          variables: variables,
          fetchPolicy: FetchPolicy.noCache));
      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }
      if (response.data != null) {
        //final root =
        //    CreateOneOrganizationAsset$MutationRoot.fromJson(response.data!);
        return const Right(null);
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }

  @override
  Future<Either<Failure, AssetModel?>> deleteOneOrganizationAsset(
      {required DeleteOneOrganizationAssetArguments arguments,
      String? authorizationToken}) async {
    try {
      final query = DeleteOneOrganizationAssetMutation(variables: arguments);

      final variables = query.variables.toJson();
      removeNullAndEmptyParams(variables);

      final response = await uploadGraphQLClient.mutate<AssetModel>(MutationOptions(
          document: query.document,
          context: authorizationToken == null
              ? null
              : Context.fromList([
                  HttpLinkHeaders(
                      headers: {'Authorization': authorizationToken})
                ]),
          variables: variables,
          fetchPolicy: FetchPolicy.noCache));
      if (response.hasException) {
        debugPrint('${response.exception}');
        return Left(ServerFailure(exception: response.exception));
      }
      if (response.data != null) {
        //final root =
        //    DeleteOneOrganizationAsset$MutationRoot.fromJson(response.data!);
        return const Right(null);
      }

      return Left(ServerFailure(exception: response.exception));
    } catch (e) {
      debugPrint('$e');
      return Left(EmptyFailure());
    }
  }
}
