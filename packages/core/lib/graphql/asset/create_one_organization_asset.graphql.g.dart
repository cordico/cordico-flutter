// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'create_one_organization_asset.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset$Returning
    _$CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset$ReturningFromJson(
        Map<String, dynamic> json) {
  return CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset$Returning()
    ..assetId = json['asset_id'] as String
    ..id = json['id'] as String
    ..organizationId = json['organization_id'] as String;
}

Map<String, dynamic>
    _$CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset$ReturningToJson(
            CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset$Returning
                instance) =>
        <String, dynamic>{
          'asset_id': instance.assetId,
          'id': instance.id,
          'organization_id': instance.organizationId,
        };

CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset
    _$CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAssetFromJson(
        Map<String, dynamic> json) {
  return CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset()
    ..affectedRows = json['affected_rows'] as int
    ..returning = (json['returning'] as List<dynamic>)
        .map((e) =>
            CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset$Returning
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic>
    _$CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAssetToJson(
            CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset
                instance) =>
        <String, dynamic>{
          'affected_rows': instance.affectedRows,
          'returning': instance.returning.map((e) => e.toJson()).toList(),
        };

CreateOneOrganizationAsset$MutationRoot
    _$CreateOneOrganizationAsset$MutationRootFromJson(
        Map<String, dynamic> json) {
  return CreateOneOrganizationAsset$MutationRoot()
    ..insertOrganizationAsset = json['insert_organization_asset'] == null
        ? null
        : CreateOneOrganizationAsset$MutationRoot$InsertOrganizationAsset
            .fromJson(
                json['insert_organization_asset'] as Map<String, dynamic>);
}

Map<String, dynamic> _$CreateOneOrganizationAsset$MutationRootToJson(
        CreateOneOrganizationAsset$MutationRoot instance) =>
    <String, dynamic>{
      'insert_organization_asset': instance.insertOrganizationAsset?.toJson(),
    };

CreateOneOrganizationAssetArguments
    _$CreateOneOrganizationAssetArgumentsFromJson(Map<String, dynamic> json) {
  return CreateOneOrganizationAssetArguments(
    assetId: json['assetId'] as String,
    organizationId: json['organizationId'] as String,
  );
}

Map<String, dynamic> _$CreateOneOrganizationAssetArgumentsToJson(
        CreateOneOrganizationAssetArguments instance) =>
    <String, dynamic>{
      'assetId': instance.assetId,
      'organizationId': instance.organizationId,
    };
