// ignore_for_file: implementation_imports

import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/widgets/content_widget.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/models/schema/content_schema.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class PlanGuideDetails extends ConsumerStatefulWidget {
  const PlanGuideDetails({
    Key? key,
    required this.content,
  }) : super(key: key);
  final String content;

  @override
  ConsumerState<PlanGuideDetails> createState() => _PlanGuideDetailsState();
}

class _PlanGuideDetailsState extends ConsumerState<PlanGuideDetails> {
  late ContentSchema mainContentSchema;
  late ContentQuery$QueryRoot$Content? mainContent;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarBrightness: ThemeData.estimateBrightnessForColor(Colors.white),
        statusBarIconBrightness: Brightness.light,
      ),
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          bottom: false,
          child: Builder(builder: (context) {
            return SingleChildScrollView(
              padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewPadding.bottom + 20,
                left: 16,
                right: 16,
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      IconButton(
                        onPressed: () {
                          Navigator.of(context).maybePop();
                        },
                        iconSize: 40,
                        splashRadius: 20,
                        icon: CircleAvatar(
                          radius: 20,
                          backgroundColor: ThemeColors.buttonsGreyColor,
                          child: Icon(Icons.close,
                              color: ThemeColors.blue.shade100),
                        ),
                        padding: EdgeInsets.zero,
                      ),
                    ],
                  ),
                  ContentWidget(
                    content: widget.content,
                    contentType: MimeType.markdown,
                    isDarkMode: false,
                  ),
                ],
              ),
            );
          }),
        ),
      ),
    );
  }
}
