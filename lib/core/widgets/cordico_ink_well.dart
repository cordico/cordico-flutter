import 'package:flutter/material.dart';

class CordicoInkWell extends InkWell {
  const CordicoInkWell({Key? key, required Widget child, VoidCallback? onTap})
      : super(
          key: key,
          highlightColor: Colors.transparent,
          onTap: onTap,
          child: child,
        );
}
