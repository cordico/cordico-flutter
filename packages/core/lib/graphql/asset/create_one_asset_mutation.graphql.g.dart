// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'create_one_asset_mutation.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateOneAssetMutation$MutationRoot
    _$CreateOneAssetMutation$MutationRootFromJson(Map<String, dynamic> json) {
  return CreateOneAssetMutation$MutationRoot()
    ..createOneAssetWithFile = json['createOneAssetWithFile'] as String;
}

Map<String, dynamic> _$CreateOneAssetMutation$MutationRootToJson(
        CreateOneAssetMutation$MutationRoot instance) =>
    <String, dynamic>{
      'createOneAssetWithFile': instance.createOneAssetWithFile,
    };

CreateAssetWithFileInput _$CreateAssetWithFileInputFromJson(
    Map<String, dynamic> json) {
  return CreateAssetWithFileInput(
    assetTypeId: json['assetTypeId'] as String,
    data: json['data'],
    fileName: json['fileName'] as String,
    mimeType: json['mimeType'] as String,
  );
}

Map<String, dynamic> _$CreateAssetWithFileInputToJson(
        CreateAssetWithFileInput instance) =>
    <String, dynamic>{
      'assetTypeId': instance.assetTypeId,
      'data': instance.data,
      'fileName': instance.fileName,
      'mimeType': instance.mimeType,
    };

CreateOneAssetMutationArguments _$CreateOneAssetMutationArgumentsFromJson(
    Map<String, dynamic> json) {
  return CreateOneAssetMutationArguments(
    data:
        CreateAssetWithFileInput.fromJson(json['data'] as Map<String, dynamic>),
    upload: json['upload'],
  );
}

Map<String, dynamic> _$CreateOneAssetMutationArgumentsToJson(
        CreateOneAssetMutationArguments instance) =>
    <String, dynamic>{
      'data': instance.data.toJson(),
      'upload': instance.upload,
    };
