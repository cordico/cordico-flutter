// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'update_one_organization_contact_mutation.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateOneOrganizationContactMutation$MutationRoot$UpdateOneOrganizationContact
    _$UpdateOneOrganizationContactMutation$MutationRoot$UpdateOneOrganizationContactFromJson(
        Map<String, dynamic> json) {
  return UpdateOneOrganizationContactMutation$MutationRoot$UpdateOneOrganizationContact()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$UpdateOneOrganizationContactMutation$MutationRoot$UpdateOneOrganizationContactToJson(
            UpdateOneOrganizationContactMutation$MutationRoot$UpdateOneOrganizationContact
                instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

UpdateOneOrganizationContactMutation$MutationRoot
    _$UpdateOneOrganizationContactMutation$MutationRootFromJson(
        Map<String, dynamic> json) {
  return UpdateOneOrganizationContactMutation$MutationRoot()
    ..updateOneOrganizationContact = json['updateOneOrganizationContact'] ==
            null
        ? null
        : UpdateOneOrganizationContactMutation$MutationRoot$UpdateOneOrganizationContact
            .fromJson(
                json['updateOneOrganizationContact'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UpdateOneOrganizationContactMutation$MutationRootToJson(
        UpdateOneOrganizationContactMutation$MutationRoot instance) =>
    <String, dynamic>{
      'updateOneOrganizationContact':
          instance.updateOneOrganizationContact?.toJson(),
    };

UpdateOneOrganizationContactMutationArguments
    _$UpdateOneOrganizationContactMutationArgumentsFromJson(
        Map<String, dynamic> json) {
  return UpdateOneOrganizationContactMutationArguments(
    id: json['id'] as String?,
    contactTypeId: json['contactTypeId'] as String?,
    organizationId: json['organizationId'] as String?,
    name: json['name'] as String,
    title: json['title'] as String,
    data: json['data'],
    isActive: json['isActive'] as bool?,
  );
}

Map<String, dynamic> _$UpdateOneOrganizationContactMutationArgumentsToJson(
        UpdateOneOrganizationContactMutationArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
      'contactTypeId': instance.contactTypeId,
      'organizationId': instance.organizationId,
      'name': instance.name,
      'title': instance.title,
      'data': instance.data,
      'isActive': instance.isActive,
    };
