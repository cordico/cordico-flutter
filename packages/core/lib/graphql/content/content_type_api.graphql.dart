// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'content_type_api.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class ContentTypeQuery$QueryRoot$ContentType extends JsonSerializable
    with EquatableMixin {
  ContentTypeQuery$QueryRoot$ContentType();

  factory ContentTypeQuery$QueryRoot$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$ContentTypeQuery$QueryRoot$ContentTypeFromJson(json);

  late String id;

  late String name;

  String? description;

  late dynamic schema;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @JsonKey(name: 'creator_id')
  late String creatorId;

  @JsonKey(name: 'updater_id')
  String? updaterId;

  @JsonKey(name: 'parent_type_id')
  String? parentTypeId;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  @JsonKey(name: 'supports_category')
  late bool supportsCategory;

  late String key;

  @override
  List<Object?> get props => [
        id,
        name,
        description,
        schema,
        createdAt,
        updatedAt,
        creatorId,
        updaterId,
        parentTypeId,
        isShared,
        supportsCategory,
        key
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$ContentTypeQuery$QueryRoot$ContentTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ContentTypeQuery$QueryRoot extends JsonSerializable with EquatableMixin {
  ContentTypeQuery$QueryRoot();

  factory ContentTypeQuery$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$ContentTypeQuery$QueryRootFromJson(json);

  @JsonKey(name: 'content_type')
  late List<ContentTypeQuery$QueryRoot$ContentType> contentType;

  @override
  List<Object?> get props => [contentType];
  @override
  Map<String, dynamic> toJson() => _$ContentTypeQuery$QueryRootToJson(this);
}

final CONTENT_TYPE_QUERY_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'ContentTypeQuery'),
      variableDefinitions: [],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'content_type'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'description'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'creator_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updater_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'parent_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'is_shared'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'supports_category'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class ContentTypeQueryQuery
    extends GraphQLQuery<ContentTypeQuery$QueryRoot, JsonSerializable> {
  ContentTypeQueryQuery();

  @override
  final DocumentNode document = CONTENT_TYPE_QUERY_QUERY_DOCUMENT;

  @override
  final String operationName = 'ContentTypeQuery';

  @override
  List<Object?> get props => [document, operationName];
  @override
  ContentTypeQuery$QueryRoot parse(Map<String, dynamic> json) =>
      ContentTypeQuery$QueryRoot.fromJson(json);
}
