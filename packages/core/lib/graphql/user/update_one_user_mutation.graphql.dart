// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'update_one_user_mutation.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class UpdateOneUserMutation$MutationRoot$UpdateOneUser extends JsonSerializable
    with EquatableMixin {
  UpdateOneUserMutation$MutationRoot$UpdateOneUser();

  factory UpdateOneUserMutation$MutationRoot$UpdateOneUser.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateOneUserMutation$MutationRoot$UpdateOneUserFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$UpdateOneUserMutation$MutationRoot$UpdateOneUserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateOneUserMutation$MutationRoot extends JsonSerializable
    with EquatableMixin {
  UpdateOneUserMutation$MutationRoot();

  factory UpdateOneUserMutation$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateOneUserMutation$MutationRootFromJson(json);

  UpdateOneUserMutation$MutationRoot$UpdateOneUser? updateOneUser;

  @override
  List<Object?> get props => [updateOneUser];
  @override
  Map<String, dynamic> toJson() =>
      _$UpdateOneUserMutation$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class JsonFieldUpdateOperationsInput extends JsonSerializable
    with EquatableMixin {
  JsonFieldUpdateOperationsInput({this.kw$set});

  factory JsonFieldUpdateOperationsInput.fromJson(Map<String, dynamic> json) =>
      _$JsonFieldUpdateOperationsInputFromJson(json);

  @JsonKey(name: 'set')
  dynamic? kw$set;

  @override
  List<Object?> get props => [kw$set];
  @override
  Map<String, dynamic> toJson() => _$JsonFieldUpdateOperationsInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateOneUserMutationArguments extends JsonSerializable
    with EquatableMixin {
  UpdateOneUserMutationArguments(
      {this.id, required this.identifier, this.userTypeId, this.data});

  @override
  factory UpdateOneUserMutationArguments.fromJson(Map<String, dynamic> json) =>
      _$UpdateOneUserMutationArgumentsFromJson(json);

  final String? id;

  late String identifier;

  final String? userTypeId;

  final JsonFieldUpdateOperationsInput? data;

  @override
  List<Object?> get props => [id, identifier, userTypeId, data];
  @override
  Map<String, dynamic> toJson() => _$UpdateOneUserMutationArgumentsToJson(this);
}

final UPDATE_ONE_USER_MUTATION_MUTATION_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'UpdateOneUserMutation'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'identifier')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'userTypeId')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'data')),
            type: NamedTypeNode(
                name: NameNode(value: 'JsonFieldUpdateOperationsInput'),
                isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'updateOneUser'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'data'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'data'),
                        value: VariableNode(name: NameNode(value: 'data'))),
                    ObjectFieldNode(
                        name: NameNode(value: 'identifier'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'set'),
                              value: VariableNode(
                                  name: NameNode(value: 'identifier')))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'userType'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'connect'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'id'),
                                    value: VariableNode(
                                        name: NameNode(value: 'userTypeId')))
                              ]))
                        ]))
                  ])),
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'id'),
                        value: VariableNode(name: NameNode(value: 'id')))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class UpdateOneUserMutationMutation extends GraphQLQuery<
    UpdateOneUserMutation$MutationRoot, UpdateOneUserMutationArguments> {
  UpdateOneUserMutationMutation({required this.variables});

  @override
  final DocumentNode document = UPDATE_ONE_USER_MUTATION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'UpdateOneUserMutation';

  @override
  final UpdateOneUserMutationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  UpdateOneUserMutation$MutationRoot parse(Map<String, dynamic> json) =>
      UpdateOneUserMutation$MutationRoot.fromJson(json);
}
