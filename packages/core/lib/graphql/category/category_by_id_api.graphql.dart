// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'category_by_id_api.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CategoryById$QueryRoot$CategoryByPk extends JsonSerializable
    with EquatableMixin {
  CategoryById$QueryRoot$CategoryByPk();

  factory CategoryById$QueryRoot$CategoryByPk.fromJson(
          Map<String, dynamic> json) =>
      _$CategoryById$QueryRoot$CategoryByPkFromJson(json);

  late String id;

  late String name;

  late String description;

  late String key;

  @override
  List<Object?> get props => [id, name, description, key];
  @override
  Map<String, dynamic> toJson() =>
      _$CategoryById$QueryRoot$CategoryByPkToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CategoryById$QueryRoot extends JsonSerializable with EquatableMixin {
  CategoryById$QueryRoot();

  factory CategoryById$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$CategoryById$QueryRootFromJson(json);

  @JsonKey(name: 'category_by_pk')
  CategoryById$QueryRoot$CategoryByPk? categoryByPk;

  @override
  List<Object?> get props => [categoryByPk];
  @override
  Map<String, dynamic> toJson() => _$CategoryById$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CategoryByIdArguments extends JsonSerializable with EquatableMixin {
  CategoryByIdArguments({required this.id});

  @override
  factory CategoryByIdArguments.fromJson(Map<String, dynamic> json) =>
      _$CategoryByIdArgumentsFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$CategoryByIdArgumentsToJson(this);
}

final CATEGORY_BY_ID_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'CategoryById'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'ID'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'category_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'id'),
                  value: VariableNode(name: NameNode(value: 'id')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'description'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class CategoryByIdQuery
    extends GraphQLQuery<CategoryById$QueryRoot, CategoryByIdArguments> {
  CategoryByIdQuery({required this.variables});

  @override
  final DocumentNode document = CATEGORY_BY_ID_QUERY_DOCUMENT;

  @override
  final String operationName = 'CategoryById';

  @override
  final CategoryByIdArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  CategoryById$QueryRoot parse(Map<String, dynamic> json) =>
      CategoryById$QueryRoot.fromJson(json);
}
