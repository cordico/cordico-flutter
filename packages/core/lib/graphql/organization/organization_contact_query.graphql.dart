// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'organization_contact_query.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class OrganizationContactQuery$QueryRoot$OrganizationContacts$ContactType
    extends JsonSerializable with EquatableMixin {
  OrganizationContactQuery$QueryRoot$OrganizationContacts$ContactType();

  factory OrganizationContactQuery$QueryRoot$OrganizationContacts$ContactType.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContactQuery$QueryRoot$OrganizationContacts$ContactTypeFromJson(
          json);

  @JsonKey(name: 'display_name')
  late String displayName;

  late String id;

  late String name;

  @override
  List<Object?> get props => [displayName, id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContactQuery$QueryRoot$OrganizationContacts$ContactTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContactQuery$QueryRoot$OrganizationContacts$Organization
    extends JsonSerializable with EquatableMixin {
  OrganizationContactQuery$QueryRoot$OrganizationContacts$Organization();

  factory OrganizationContactQuery$QueryRoot$OrganizationContacts$Organization.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContactQuery$QueryRoot$OrganizationContacts$OrganizationFromJson(
          json);

  late String name;

  @override
  List<Object?> get props => [name];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContactQuery$QueryRoot$OrganizationContacts$OrganizationToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContactQuery$QueryRoot$OrganizationContacts
    extends JsonSerializable with EquatableMixin {
  OrganizationContactQuery$QueryRoot$OrganizationContacts();

  factory OrganizationContactQuery$QueryRoot$OrganizationContacts.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContactQuery$QueryRoot$OrganizationContactsFromJson(json);

  dynamic? data;

  late String title;

  @JsonKey(name: 'contact_type_id')
  late String contactTypeId;

  @JsonKey(name: 'contact_type')
  late OrganizationContactQuery$QueryRoot$OrganizationContacts$ContactType
      contactType;

  @JsonKey(name: 'organization_id')
  late String organizationId;

  late OrganizationContactQuery$QueryRoot$OrganizationContacts$Organization
      organization;

  late String name;

  @JsonKey(name: 'is_active')
  late bool isActive;

  late String id;

  DateTime? updated;

  late DateTime created;

  @override
  List<Object?> get props => [
        data,
        title,
        contactTypeId,
        contactType,
        organizationId,
        organization,
        name,
        isActive,
        id,
        updated,
        created
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContactQuery$QueryRoot$OrganizationContactsToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContactQuery$QueryRoot extends JsonSerializable
    with EquatableMixin {
  OrganizationContactQuery$QueryRoot();

  factory OrganizationContactQuery$QueryRoot.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContactQuery$QueryRootFromJson(json);

  @JsonKey(name: 'organization_contacts')
  late List<OrganizationContactQuery$QueryRoot$OrganizationContacts>
      organizationContacts;

  @override
  List<Object?> get props => [organizationContacts];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContactQuery$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContactQueryArguments extends JsonSerializable
    with EquatableMixin {
  OrganizationContactQueryArguments({this.offset, this.limit});

  @override
  factory OrganizationContactQueryArguments.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContactQueryArgumentsFromJson(json);

  final int? offset;

  final int? limit;

  @override
  List<Object?> get props => [offset, limit];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContactQueryArgumentsToJson(this);
}

final ORGANIZATION_CONTACT_QUERY_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'OrganizationContactQuery'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'offset')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '0')),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'limit')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '10000')),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'organization_contacts'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'offset'),
                  value: VariableNode(name: NameNode(value: 'offset'))),
              ArgumentNode(
                  name: NameNode(value: 'limit'),
                  value: VariableNode(name: NameNode(value: 'limit')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'title'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'contact_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'contact_type'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'display_name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'organization_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'organization'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'is_active'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class OrganizationContactQueryQuery extends GraphQLQuery<
    OrganizationContactQuery$QueryRoot, OrganizationContactQueryArguments> {
  OrganizationContactQueryQuery({required this.variables});

  @override
  final DocumentNode document = ORGANIZATION_CONTACT_QUERY_QUERY_DOCUMENT;

  @override
  final String operationName = 'OrganizationContactQuery';

  @override
  final OrganizationContactQueryArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  OrganizationContactQuery$QueryRoot parse(Map<String, dynamic> json) =>
      OrganizationContactQuery$QueryRoot.fromJson(json);
}
