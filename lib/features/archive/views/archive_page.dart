// ignore_for_file: lines_longer_than_80_chars

import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/managers/cordico_alert.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/app_bar_provider.dart';
import 'package:cordico_mobile/core/widgets/cordico_elevated_button.dart';
import 'package:cordico_mobile/core/widgets/cordico_search_field.dart';
import 'package:cordico_mobile/core/widgets/drop_down/cordico_filters_widget.dart';
import 'package:cordico_mobile/features/archive/logic/archive_state.dart';
import 'package:cordico_mobile/features/content/logic/content_provider.dart';
import 'package:cordico_mobile/features/department/logic/department_provider.dart';
import 'package:cordico_mobile/features/department/views/content_list_view.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:flutter/material.dart';
import 'package:flutter_segment/flutter_segment.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:vrouter/vrouter.dart';

final loadingIndicatorKey = UniqueKey();

final filtersArchiveQueryProvider = StateProvider<String>((ref) => '');
final filtersArchiveTypesProvider = StateProvider<List<String>>((ref) => []);
final filtersArchiveBulletinTypesProvider =
    StateProvider<List<BulletinType>>((ref) => []);
final filtersArchiveDaysProvider =
    StateProvider<DaysFilter>((ref) => DaysFilter.allDays);

final filteredArchiveContentProvider =
    Provider<List<ContentQuery$QueryRoot$Content>>((ref) {
  final query = ref.watch(filtersArchiveQueryProvider);
  final types = ref.watch(filtersArchiveTypesProvider);
  final daysFilter = ref.watch(filtersArchiveDaysProvider);

  final content = ref.watch(archiveContentProvider);

  return utils.filterContent(
    content: content,
    query: query,
    types: types,
    dayFilter: daysFilter,
  );
});

final filteredBulletinContentProvider =
    Provider<List<ContentQuery$QueryRoot$Content>>((ref) {
  final query = ref.watch(filtersArchiveQueryProvider);
  final types = ref.watch(filtersArchiveTypesProvider);
  final daysFilter = ref.watch(filtersArchiveDaysProvider);
  final bulletinFilter = ref.watch(filtersArchiveBulletinTypesProvider);

  final content = ref.watch(departmentPageStateNotifierProvider);

  return utils.filterContent(
    content: content.bulletin ?? [],
    query: query,
    types: types,
    dayFilter: daysFilter,
    isBulletin: true,
    bulletinFilters: bulletinFilter,
  );
});

final filteredEapContentProvider =
    Provider<List<ContentQuery$QueryRoot$Content>>((ref) {
  final query = ref.watch(filtersArchiveQueryProvider);
  final types = ref.watch(filtersArchiveTypesProvider);
  final daysFilter = ref.watch(filtersArchiveDaysProvider);

  final content = ref.watch(departmentPageStateNotifierProvider);

  return utils.filterContent(
    content: content.eapResources ?? [],
    query: query,
    types: types,
    dayFilter: daysFilter,
    isEAP: true,
  );
});

class ArchivePage extends ConsumerStatefulWidget {
  const ArchivePage({Key? key}) : super(key: key);

  @override
  ConsumerState<ArchivePage> createState() => _ArchivePageState();
}

class _ArchivePageState extends ConsumerState<ArchivePage> {
  TextEditingController textController = TextEditingController();

  late Map<String, String> paramteres;

  Filters filters = Filters(
    contentTypes: [],
    categoriesTypes: [],
    daysFilter: DaysFilter.allDays,
    bulletinTypes: [],
  );

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      ref.watch(finalContentListProvider.state).state =
          context.vRouter.pathParameters;

      ref.watch(filtersArchiveTypesProvider.state).state = [];
      ref.watch(filtersArchiveDaysProvider.state).state = DaysFilter.allDays;
      ref.watch(filtersArchiveQueryProvider.state).state = '';
      ref.watch(filtersArchiveBulletinTypesProvider.state).state = [];
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final title = context.vRouter.pathParameters['title'];
    final type = context.vRouter.pathParameters['type'];

    Segment.screen(screenName: 'Archive', properties: <String, dynamic>{
      'type': type,
      'name': 'Archive',
      'title': title,
    });

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Consumer(builder: (cont, ref, _) {
        return Scaffold(
          appBar: AppBarProvider.getAppBarWithBackIcon(
            context,
            title: Text(
              (title ?? '').isEmpty ? 'Archive (From Home)' : '$title',
              style: ThemeStyles.cardContentTitle(context)
                  ?.copyWith(color: Colors.white),
            ),
          ),
          body: Builder(
            builder: (ctx) {
              paramteres = context.vRouter.pathParameters;
              List<ContentQuery$QueryRoot$Content> listProvider;

              var isLoading = false;
              if (type == 'bulletin') {
                listProvider = ref.watch(filteredBulletinContentProvider);
                isLoading = ref
                    .watch(departmentPageStateNotifierProvider.notifier)
                    .isLoading;
              } else if (type == 'eap') {
                listProvider = ref.watch(filteredEapContentProvider);
                isLoading = ref
                    .watch(departmentPageStateNotifierProvider.notifier)
                    .isLoading;
              } else {
                listProvider = ref.watch(filteredArchiveContentProvider);
                isLoading =
                    ref.watch(archiveContentProvider.notifier).isLoading;
              }

              return isLoading
                  ? const Center(
                      child: Padding(
                        padding: EdgeInsets.all(24),
                        child: CircularProgressIndicator(),
                      ),
                    )
                  : Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(12, 16, 12, 0),
                          child: CordicoSearchField(
                            controller: textController,
                            hintText: 'Search $title',
                            onChanged: (value) {
                              ref.watch(filtersArchiveQueryProvider.state).state =
                                  value;
                            },
                          ),
                        ),
                        _buildResourceTypeFilter(context),
                        Expanded(
                          child:
                              ContentListView<ContentQuery$QueryRoot$Content>(
                            source: listProvider,
                            isBulletin: type == 'bulletin',
                            padding: EdgeInsets.only(
                                bottom:
                                    MediaQuery.of(ctx).viewPadding.bottom + 12),
                            onItemTap: (index) {
                              final co = listProvider[index];
                              ref.watch(contentIdProvider.state).state = co.id;
                              global.goTo(
                                context,
                                content:
                                    ContentQuery$QueryRoot$Content.fromJson(
                                        co.toJson()),
                                index: 0,
                                contentSource: type == 'eap' ? 'eap' : null,
                              );
                            },
                          ),
                        ),
                      ],
                    );
            },
          ),
        );
      }),
    );
  }

  Widget _buildResourceTypeFilter(BuildContext context) {
    final type = context.vRouter.pathParameters['type'];
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(height: 19),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: CordicoElevatedButton(
                color: ThemeColors.gray.shade500,
                hasBorder: true,
                height: 37,
                child: Row(
                  children: [
                    Icon(
                      Icons.filter_list,
                      color: ThemeColors.gray.shade100,
                      size: 19,
                    ),
                    Container(width: 7),
                    Text(
                      'Filters',
                      style: ThemeStyles.contentDescription(context),
                    ),
                    if (filters.filtersApplied != 0)
                      Container(
                        margin: const EdgeInsets.only(left: 8),
                        height: 21,
                        width: 21,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(11),
                        ),
                        child: Center(
                          child: Text(
                            '${filters.filtersApplied}',
                            style: ThemeStyles.contentDescription(context)!
                                .copyWith(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                  ],
                ),
                onPressed: () async {
                  final res = await CordicoAlert.showContentFilters(
                    context,
                    filters: filters,
                    filterByContent: type != 'bulletin',
                    filterByDay: type == 'bulletin',
                    filterByBulletinType: type == 'bulletin',
                  );

                  if (res != null) {
                    setState(() => filters = res);

                    ref.watch(filtersArchiveTypesProvider.state).state =
                        res.contentTypesIds;

                    ref.watch(filtersArchiveDaysProvider.state).state =
                        res.daysFilter ?? DaysFilter.allDays;

                    ref.watch(filtersArchiveBulletinTypesProvider.state).state =
                        res.bulletinTypes ?? [];
                  }
                },
              ),
            ),
          ],
        ),
        Container(height: 19),
        Container(
          width: double.infinity,
          height: 1,
          color: ThemeColors.gray.shade700,
        )
      ],
    );
  }
}
