import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/images.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:core/core.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/utils/content_utils.dart';
import 'package:flutter/material.dart';

class ArticleItemListItem extends StatelessWidget {
  const ArticleItemListItem({
    Key? key,
    required this.content,
    required this.contentSchema,
    required this.type,
    this.onItemTap,
    this.isRead = false,
  }) : super(key: key);

  final ContentQuery$QueryRoot$Content? content;
  final ContentSchema contentSchema;
  final String type;
  final Function()? onItemTap;

  final bool isRead;

  String? get title {
    var title = '';
    if (content != null &&
        content?.data != null &&
        (content?.data as Map).containsKey('title')) {
      title = (content?.data as Map)['title'] as String;
    }

    return title;
  }

  String? get description {
    var description = '';
    if (content != null &&
        content?.data != null &&
        (content?.data as Map).containsKey('summary')) {
      description = (content?.data as Map)['summary'] as String;
    }
    return description;
  }

  String? get coverImageUrl {
    String? imageUrl;
    if (content != null &&
        content?.data != null &&
        (content?.data as Map).containsKey('cover_image_url')) {
      imageUrl = (content?.data as Map)['cover_image_url'] as String;
    }
    return imageUrl;
  }

  String? get imageUrl {
    String? imageUrl;
    if (content != null &&
        content?.data != null &&
        (content?.data as Map).containsKey('imageUrl')) {
      imageUrl = (content?.data as Map)['imageUrl'] as String;
    }
    return imageUrl;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: onItemTap,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
        child: Row(
          children: [
            _getArticleItemIcon(context, content),
            Container(width: 20),
            Expanded(
              child: _buildArticleItemDescription(context),
            )
          ],
        ),
      ),
    );
  }

  Widget _getArticleItemIcon(
      BuildContext context, ContentQuery$QueryRoot$Content? article) {
    if (isRead == true) {
      return Image.asset(Images.icComplete, height: 24, width: 24);
    }

    if (type == 'Text') {
      return Image.asset(Images.icText, height: 24, width: 24);
    }

    if (type == 'Audio') {
      return Image.asset(Images.icAudio, height: 24, width: 24);
    }

    return Image.asset(Images.icVideo, height: 24, width: 24);
  }

  Widget _buildArticleItemDescription(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title ?? '',
          style: ThemeStyles.cardContentTitle(context)?.copyWith(
            color: isRead ? ThemeColors.gray.shade300 : Colors.white,
          ),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
        _buildFooter(context),
      ],
    );
  }

  Widget _buildFooter(BuildContext context) {
    var stringContent = '';
    if (type.isNotEmpty) {
      stringContent = type;
    }

    if (type != 'Video' && type != 'Audio') {
      if (stringContent.isNotEmpty) {
        stringContent += ' \u00B7 ';
      }

      stringContent +=
          'Reading Time ${ContentUtils.readingTimeFromContentPart(content!)}';
    }

    return Padding(
      padding: const EdgeInsets.only(top: 1),
      child: Text(
        stringContent,
        style: ThemeStyles.smallParagraph(context)
            ?.copyWith(fontSize: 11, color: ThemeColors.gray.shade300),
      ),
    );
  }
}
