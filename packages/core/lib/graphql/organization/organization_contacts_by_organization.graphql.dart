// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'organization_contacts_by_organization.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$ContactType
    extends JsonSerializable with EquatableMixin {
  OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$ContactType();

  factory OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$ContactType.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$ContactTypeFromJson(
          json);

  @JsonKey(name: 'display_name')
  late String displayName;

  late String id;

  late String name;

  @override
  List<Object?> get props => [displayName, id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$ContactTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$Organization
    extends JsonSerializable with EquatableMixin {
  OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$Organization();

  factory OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$Organization.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$OrganizationFromJson(
          json);

  late String name;

  @override
  List<Object?> get props => [name];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$OrganizationToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContactsByOrganization$QueryRoot$OrganizationContacts
    extends JsonSerializable with EquatableMixin {
  OrganizationContactsByOrganization$QueryRoot$OrganizationContacts();

  factory OrganizationContactsByOrganization$QueryRoot$OrganizationContacts.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContactsByOrganization$QueryRoot$OrganizationContactsFromJson(
          json);

  dynamic? data;

  late String title;

  @JsonKey(name: 'contact_type_id')
  late String contactTypeId;

  @JsonKey(name: 'contact_type')
  late OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$ContactType
      contactType;

  @JsonKey(name: 'organization_id')
  late String organizationId;

  late OrganizationContactsByOrganization$QueryRoot$OrganizationContacts$Organization
      organization;

  late String name;

  @JsonKey(name: 'is_active')
  late bool isActive;

  late String id;

  DateTime? updated;

  late DateTime created;

  @override
  List<Object?> get props => [
        data,
        title,
        contactTypeId,
        contactType,
        organizationId,
        organization,
        name,
        isActive,
        id,
        updated,
        created
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContactsByOrganization$QueryRoot$OrganizationContactsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContactsByOrganization$QueryRoot extends JsonSerializable
    with EquatableMixin {
  OrganizationContactsByOrganization$QueryRoot();

  factory OrganizationContactsByOrganization$QueryRoot.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContactsByOrganization$QueryRootFromJson(json);

  @JsonKey(name: 'organization_contacts')
  late List<OrganizationContactsByOrganization$QueryRoot$OrganizationContacts>
      organizationContacts;

  @override
  List<Object?> get props => [organizationContacts];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContactsByOrganization$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationContactsByOrganizationArguments extends JsonSerializable
    with EquatableMixin {
  OrganizationContactsByOrganizationArguments(
      {required this.organizationID, this.isActive});

  @override
  factory OrganizationContactsByOrganizationArguments.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationContactsByOrganizationArgumentsFromJson(json);

  late String organizationID;

  final List<bool>? isActive;

  @override
  List<Object?> get props => [organizationID, isActive];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationContactsByOrganizationArgumentsToJson(this);
}

final ORGANIZATION_CONTACTS_BY_ORGANIZATION_QUERY_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'OrganizationContactsByOrganization'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'organizationID')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'isActive')),
            type: ListTypeNode(
                type: NamedTypeNode(
                    name: NameNode(value: 'Boolean'), isNonNull: true),
                isNonNull: false),
            defaultValue: DefaultValueNode(
                value: ListValueNode(values: [BooleanValueNode(value: true)])),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'organization_contacts'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'organization_id'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: '_eq'),
                              value: VariableNode(
                                  name: NameNode(value: 'organizationID')))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'is_active'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: '_in'),
                              value: VariableNode(
                                  name: NameNode(value: 'isActive')))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'title'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'contact_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'contact_type'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'display_name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'organization_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'organization'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'is_active'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class OrganizationContactsByOrganizationQuery extends GraphQLQuery<
    OrganizationContactsByOrganization$QueryRoot,
    OrganizationContactsByOrganizationArguments> {
  OrganizationContactsByOrganizationQuery({required this.variables});

  @override
  final DocumentNode document =
      ORGANIZATION_CONTACTS_BY_ORGANIZATION_QUERY_DOCUMENT;

  @override
  final String operationName = 'OrganizationContactsByOrganization';

  @override
  final OrganizationContactsByOrganizationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  OrganizationContactsByOrganization$QueryRoot parse(
          Map<String, dynamic> json) =>
      OrganizationContactsByOrganization$QueryRoot.fromJson(json);
}
