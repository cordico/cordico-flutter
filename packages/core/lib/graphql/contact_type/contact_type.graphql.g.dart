// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'contact_type.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContactTypeQuery$QueryRoot$ContactType
    _$ContactTypeQuery$QueryRoot$ContactTypeFromJson(
        Map<String, dynamic> json) {
  return ContactTypeQuery$QueryRoot$ContactType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..displayName = json['display_name'] as String
    ..schema = json['schema'];
}

Map<String, dynamic> _$ContactTypeQuery$QueryRoot$ContactTypeToJson(
        ContactTypeQuery$QueryRoot$ContactType instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'display_name': instance.displayName,
      'schema': instance.schema,
    };

ContactTypeQuery$QueryRoot _$ContactTypeQuery$QueryRootFromJson(
    Map<String, dynamic> json) {
  return ContactTypeQuery$QueryRoot()
    ..contactType = (json['contact_type'] as List<dynamic>)
        .map((e) => ContactTypeQuery$QueryRoot$ContactType.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$ContactTypeQuery$QueryRootToJson(
        ContactTypeQuery$QueryRoot instance) =>
    <String, dynamic>{
      'contact_type': instance.contactType.map((e) => e.toJson()).toList(),
    };
