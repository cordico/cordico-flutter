// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'feedback_type_query.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class FeedbackTypeQuery$QueryRoot$FeedbackType extends JsonSerializable
    with EquatableMixin {
  FeedbackTypeQuery$QueryRoot$FeedbackType();

  factory FeedbackTypeQuery$QueryRoot$FeedbackType.fromJson(
          Map<String, dynamic> json) =>
      _$FeedbackTypeQuery$QueryRoot$FeedbackTypeFromJson(json);

  late String id;

  late String key;

  late String name;

  dynamic? schema;

  @override
  List<Object?> get props => [id, key, name, schema];
  @override
  Map<String, dynamic> toJson() =>
      _$FeedbackTypeQuery$QueryRoot$FeedbackTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FeedbackTypeQuery$QueryRoot extends JsonSerializable with EquatableMixin {
  FeedbackTypeQuery$QueryRoot();

  factory FeedbackTypeQuery$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$FeedbackTypeQuery$QueryRootFromJson(json);

  @JsonKey(name: 'feedback_type')
  late List<FeedbackTypeQuery$QueryRoot$FeedbackType> feedbackType;

  @override
  List<Object?> get props => [feedbackType];
  @override
  Map<String, dynamic> toJson() => _$FeedbackTypeQuery$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FeedbackTypeQueryArguments extends JsonSerializable with EquatableMixin {
  FeedbackTypeQueryArguments({this.limit, this.offset});

  @override
  factory FeedbackTypeQueryArguments.fromJson(Map<String, dynamic> json) =>
      _$FeedbackTypeQueryArgumentsFromJson(json);

  final int? limit;

  final int? offset;

  @override
  List<Object?> get props => [limit, offset];
  @override
  Map<String, dynamic> toJson() => _$FeedbackTypeQueryArgumentsToJson(this);
}

final FEEDBACK_TYPE_QUERY_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'FeedbackTypeQuery'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'limit')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '10000')),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'offset')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '0')),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'feedback_type'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'offset'),
                  value: VariableNode(name: NameNode(value: 'offset'))),
              ArgumentNode(
                  name: NameNode(value: 'limit'),
                  value: VariableNode(name: NameNode(value: 'limit')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class FeedbackTypeQueryQuery extends GraphQLQuery<FeedbackTypeQuery$QueryRoot,
    FeedbackTypeQueryArguments> {
  FeedbackTypeQueryQuery({required this.variables});

  @override
  final DocumentNode document = FEEDBACK_TYPE_QUERY_QUERY_DOCUMENT;

  @override
  final String operationName = 'FeedbackTypeQuery';

  @override
  final FeedbackTypeQueryArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  FeedbackTypeQuery$QueryRoot parse(Map<String, dynamic> json) =>
      FeedbackTypeQuery$QueryRoot.fromJson(json);
}
