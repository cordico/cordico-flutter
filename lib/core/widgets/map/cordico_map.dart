// ignore_for_file: use_late_for_private_fields_and_variables, prefer_const_constructors, lines_longer_than_80_chars, use_setters_to_change_properties, unused_field, empty_catches, unawaited_futures

import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/widgets/map/marker_location.dart';
import 'package:core/core.dart';
import 'package:core/graphql/organization/organization_contact_by_id.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class CordicoMap extends StatefulWidget {
  const CordicoMap({
    Key? key,
    required this.therapistsContactList,
    this.onContactTap,
    this.showSingleOnMap = false,
  }) : super(key: key);

  final List<OrganizationContactModel> therapistsContactList;

  final Function(OrganizationContactModel)? onContactTap;

  final bool showSingleOnMap;

  @override
  State<StatefulWidget> createState() => CordicoMapState();
}

class CordicoMapState extends State<CordicoMap> {
  GoogleMapController? _controller;

  List<OrganizationContactModel> contacts = [];

  List<Marker> _markers = [];
  Set<Marker> _markersApplied = {};

  bool isLoadingPosition = false;

  LatLngBounds? currentBound;

  CameraPosition? _userCameraPosition = CameraPosition(
    target: LatLng(37.42796133580664, -97.085749655962),
    zoom: 1,
  );

  @override
  void didUpdateWidget(covariant CordicoMap oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (oldWidget.therapistsContactList != widget.therapistsContactList) {
      contacts = widget.therapistsContactList;
      _addCustomMarkers(contacts);
    }
  }

  @override
  void initState() {
    super.initState();

    contacts = widget.therapistsContactList;

    if (widget.showSingleOnMap) {
      checkPermissionChangedAndUpdateUserLocation(addPins: true);
    } else {
      checkPermissionChangedAndUpdateUserLocation(
          addPins: true, moveCamera: true);
    }
  }

  Future checkPermissionChangedAndUpdateUserLocation({
    bool moveCamera = false,
    bool addPins = false,
  }) async {
    /*final newPermission = await Geolocator.checkPermission();

    final update = newPermission != global.locationPermission &&
        newPermission != LocationPermission.denied &&
        newPermission != LocationPermission.deniedForever;*/

    await _setUserPosition(
      moveCamera: moveCamera,
      addPins: addPins,
    );
  }

  Future<Position?> determinePosition({bool showingPopup = false}) async {
    global.locationPermission = await Geolocator.checkPermission();
    if (global.locationPermission == LocationPermission.deniedForever) {
      if (showingPopup) {
        //showLocationPermissionPopup();
      }

      return null;
    } else if (global.locationPermission == LocationPermission.denied) {
      global.locationPermission = await Geolocator.requestPermission();
      if (global.locationPermission != LocationPermission.denied &&
          global.locationPermission != LocationPermission.deniedForever) {
        return Geolocator.getCurrentPosition();
      }

      return null;
    } else {
      return Geolocator.getCurrentPosition();
    }
  }

  Future<void> _setUserPosition({
    bool moveCamera = false,
    bool addPins = false,
    bool showingPopup = false,
  }) async {
    setState(() => isLoadingPosition = true);
    final position = await determinePosition(showingPopup: showingPopup);
    setState(() => isLoadingPosition = false);

    if (position != null) {
      setState(() {
        _userCameraPosition = CameraPosition(
          target: LatLng(position.latitude, position.longitude),
          zoom: 9,
        );
      });
    } else {
      if (global.organizationSchema != null &&
          global.organizationSchema!.latitude != null &&
          global.organizationSchema!.longitude != null) {
        setState(
          () => _userCameraPosition = CameraPosition(
            target: LatLng(global.organizationSchema!.latitude!.toDouble(),
                global.organizationSchema!.longitude!.toDouble()),
            zoom: 9,
          ),
        );
      }
    }
    if (addPins) {
      await _addCustomMarkers(contacts);
    }

    if (moveCamera) {
      _moveCameraToUserPosition();
    }
  }

  void _moveCameraToUserPosition() {
    if (_controller != null && _userCameraPosition != null) {
      _controller?.animateCamera(
        CameraUpdate.newCameraPosition(
          _userCameraPosition!,
        ),
      );
    }
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller = controller;
  }

  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      compassEnabled: false,
      mapToolbarEnabled: false,
      initialCameraPosition: _userCameraPosition!,
      myLocationEnabled: true,
      onMapCreated: _onMapCreated,
      onTap: (_) => FocusScope.of(context).unfocus(),
      markers: _markersApplied, //_getMapMarkers(locations),
    );
  }

  void moveCameraToPosition(CameraPosition position) {
    if (_controller != null) {
      _controller?.animateCamera(
        CameraUpdate.newCameraPosition(position),
      );
    }
  }

  Future<void> _addCustomMarkers(
      List<OrganizationContactModel> contacts) async {
    _markers = List.from(<Marker>[]);

    for (final contact in contacts) {
      try {
        addSingleStoreMarker(contact: contact);
      } catch (e) {
        debugPrint('IMAGE ERROR: $e');
      }
    }
    // await Future.forEach(contacts, (element) async {
    //   final e = element
    //       as OrganizationContactById$QueryRoot$OrganizationContactsByPk?;

    //   if (e != null) {
    //     try {
    //       await addSingleStoreMarker(contact: e);
    //     } catch (e) {}
    //   }
    // });

    if (widget.showSingleOnMap) {
      await _controller?.animateCamera(
        CameraUpdate.newCameraPosition(
            CameraPosition(target: _markers.first.position, zoom: 9)),
      );
    }

    //setState(() => _markersApplied = Set.from(_markers.toSet()));

    // final cameraPosition =
    //     CameraPosition(target: _markers.first.position, zoom: 14);
    // _moveCameraToPosition(cameraPosition);
  }

  Future<void> addSingleStoreMarker(
      {required OrganizationContactById$QueryRoot$OrganizationContactsByPk
          contact,
      double size = 50}) async {
    final contactData = OrganizationContactSchema.fromJson(
        contact.data as Map<String, dynamic>);

    BitmapDescriptor? bitmap;

    if (contactData.profileImageUrl != null &&
        contactData.profileImageUrl?.isNotEmpty == true) {
      try {
        bitmap =
            await utils.getMarkerIcon(contactData.profileImageUrl, size: size);
      } catch (e) {}
    }

    //final i = locations.indexWhere((s) => s.id == location.id);

    if (contactData.address != null) {
      final coordinates = LatLng(
        (contactData.address?.latitude ?? 0).toDouble(),
        (contactData.address?.longitude ?? 0).toDouble(),
      );

      final markerLocation =
          MarkerLocation(id: contact.name, position: coordinates);

      _markers.add(
        Marker(
          markerId: MarkerId(markerLocation.id),
          position: markerLocation.position,
          onTap: () async {
            if (widget.onContactTap != null) {
              widget.onContactTap!(contact);
            }
          },
          icon: bitmap ?? BitmapDescriptor.defaultMarker,
        ),
      );
    }

    setState(() => _markersApplied = Set.from(_markers.toSet()));
    if (widget.showSingleOnMap) {
      await _controller?.animateCamera(
        CameraUpdate.newCameraPosition(
            CameraPosition(target: _markers.first.position, zoom: 9)),
      );
    }
  }

  void moveCameraToShowCurrentBound(LatLng target) {
    currentBound = getMarkersBounds(_userCameraPosition!.target, target);
    final u2 = CameraUpdate.newLatLngBounds(currentBound!, 100);
    _controller?.animateCamera(u2);
  }

  LatLngBounds getMarkersBounds(LatLng source, LatLng target) {
    LatLngBounds bound;
    if (target.latitude > source.latitude &&
        target.longitude > source.longitude) {
      bound = LatLngBounds(southwest: source, northeast: target);
    } else if (target.longitude > source.longitude) {
      bound = LatLngBounds(
        southwest: LatLng(target.latitude, source.longitude),
        northeast: LatLng(source.latitude, target.longitude),
      );
    } else if (target.latitude > source.latitude) {
      bound = LatLngBounds(
        southwest: LatLng(source.latitude, target.longitude),
        northeast: LatLng(target.latitude, source.longitude),
      );
    } else {
      bound = LatLngBounds(southwest: target, northeast: source);
    }
    return bound;
  }
}
