// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'login_mutation.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class LoginMutation$MutationRoot$Login$User extends JsonSerializable
    with EquatableMixin {
  LoginMutation$MutationRoot$Login$User();

  factory LoginMutation$MutationRoot$Login$User.fromJson(
          Map<String, dynamic> json) =>
      _$LoginMutation$MutationRoot$Login$UserFromJson(json);

  late String userTypeId;

  String? userServiceId;

  late String identifier;

  late String id;

  dynamic? data;

  late DateTime createdAt;

  @override
  List<Object?> get props =>
      [userTypeId, userServiceId, identifier, id, data, createdAt];
  @override
  Map<String, dynamic> toJson() =>
      _$LoginMutation$MutationRoot$Login$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class LoginMutation$MutationRoot$Login extends JsonSerializable
    with EquatableMixin {
  LoginMutation$MutationRoot$Login();

  factory LoginMutation$MutationRoot$Login.fromJson(
          Map<String, dynamic> json) =>
      _$LoginMutation$MutationRoot$LoginFromJson(json);

  late String token;

  late LoginMutation$MutationRoot$Login$User user;

  @override
  List<Object?> get props => [token, user];
  @override
  Map<String, dynamic> toJson() =>
      _$LoginMutation$MutationRoot$LoginToJson(this);
}

@JsonSerializable(explicitToJson: true)
class LoginMutation$MutationRoot extends JsonSerializable with EquatableMixin {
  LoginMutation$MutationRoot();

  factory LoginMutation$MutationRoot.fromJson(Map<String, dynamic> json) =>
      _$LoginMutation$MutationRootFromJson(json);

  LoginMutation$MutationRoot$Login? login;

  @override
  List<Object?> get props => [login];
  @override
  Map<String, dynamic> toJson() => _$LoginMutation$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AuthInput extends JsonSerializable with EquatableMixin {
  AuthInput({required this.identifier, required this.password});

  factory AuthInput.fromJson(Map<String, dynamic> json) =>
      _$AuthInputFromJson(json);

  late String identifier;

  late String password;

  @override
  List<Object?> get props => [identifier, password];
  @override
  Map<String, dynamic> toJson() => _$AuthInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class LoginMutationArguments extends JsonSerializable with EquatableMixin {
  LoginMutationArguments({this.input});

  @override
  factory LoginMutationArguments.fromJson(Map<String, dynamic> json) =>
      _$LoginMutationArgumentsFromJson(json);

  final AuthInput? input;

  @override
  List<Object?> get props => [input];
  @override
  Map<String, dynamic> toJson() => _$LoginMutationArgumentsToJson(this);
}

final LOGIN_MUTATION_MUTATION_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'LoginMutation'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'input')),
            type: NamedTypeNode(
                name: NameNode(value: 'AuthInput'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'login'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'input'),
                  value: VariableNode(name: NameNode(value: 'input')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'token'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'user'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'userTypeId'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'userServiceId'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'identifier'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'data'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'createdAt'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ]))
            ]))
      ]))
]);

class LoginMutationMutation
    extends GraphQLQuery<LoginMutation$MutationRoot, LoginMutationArguments> {
  LoginMutationMutation({required this.variables});

  @override
  final DocumentNode document = LOGIN_MUTATION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'LoginMutation';

  @override
  final LoginMutationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  LoginMutation$MutationRoot parse(Map<String, dynamic> json) =>
      LoginMutation$MutationRoot.fromJson(json);
}
