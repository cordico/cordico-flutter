// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'delete_one_collection.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class DeleteOneCollection$MutationRoot$DeleteOneCollection
    extends JsonSerializable with EquatableMixin {
  DeleteOneCollection$MutationRoot$DeleteOneCollection();

  factory DeleteOneCollection$MutationRoot$DeleteOneCollection.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneCollection$MutationRoot$DeleteOneCollectionFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneCollection$MutationRoot$DeleteOneCollectionToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeleteOneCollection$MutationRoot extends JsonSerializable
    with EquatableMixin {
  DeleteOneCollection$MutationRoot();

  factory DeleteOneCollection$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$DeleteOneCollection$MutationRootFromJson(json);

  DeleteOneCollection$MutationRoot$DeleteOneCollection? deleteOneCollection;

  @override
  List<Object?> get props => [deleteOneCollection];
  @override
  Map<String, dynamic> toJson() =>
      _$DeleteOneCollection$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class DeleteOneCollectionArguments extends JsonSerializable
    with EquatableMixin {
  DeleteOneCollectionArguments({required this.id});

  @override
  factory DeleteOneCollectionArguments.fromJson(Map<String, dynamic> json) =>
      _$DeleteOneCollectionArgumentsFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$DeleteOneCollectionArgumentsToJson(this);
}

final DELETE_ONE_COLLECTION_MUTATION_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'DeleteOneCollection'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'deleteOneCollection'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'id'),
                        value: VariableNode(name: NameNode(value: 'id')))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class DeleteOneCollectionMutation extends GraphQLQuery<
    DeleteOneCollection$MutationRoot, DeleteOneCollectionArguments> {
  DeleteOneCollectionMutation({required this.variables});

  @override
  final DocumentNode document = DELETE_ONE_COLLECTION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'DeleteOneCollection';

  @override
  final DeleteOneCollectionArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  DeleteOneCollection$MutationRoot parse(Map<String, dynamic> json) =>
      DeleteOneCollection$MutationRoot.fromJson(json);
}
