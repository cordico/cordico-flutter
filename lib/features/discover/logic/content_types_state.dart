import 'package:cordico_mobile/features/discover/logic/content_types_holder.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'content_types_state.freezed.dart';

extension ContentGetters on ContentTypesState {
  bool get isLoading => this is _Loading;
}

@freezed
class ContentTypesState with _$ContentTypesState {
  /// Data is present state
  const factory ContentTypesState.data({required ContentTypesHolder content}) =
      _Data;

  /// Initial/default state
  const factory ContentTypesState.initial() = _Initial;

  /// Data is loading state
  const factory ContentTypesState.loading() = _Loading;

  /// Error when loading data state
  const factory ContentTypesState.error([String? message]) = _Error;
}
