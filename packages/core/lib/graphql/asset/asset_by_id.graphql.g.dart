// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'asset_by_id.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AssetById$QueryRoot$AssetByPk _$AssetById$QueryRoot$AssetByPkFromJson(
    Map<String, dynamic> json) {
  return AssetById$QueryRoot$AssetByPk()
    ..assetTypeId = json['asset_type_id'] as String
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..creatorId = json['creator_id'] as String
    ..data = json['data']
    ..fileName = json['file_name'] as String
    ..hash = json['hash'] as String?
    ..id = json['id'] as String
    ..mimeType = json['mime_type'] as String
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..updaterId = json['updater_id'] as String?
    ..url = json['url'] as String?;
}

Map<String, dynamic> _$AssetById$QueryRoot$AssetByPkToJson(
        AssetById$QueryRoot$AssetByPk instance) =>
    <String, dynamic>{
      'asset_type_id': instance.assetTypeId,
      'created_at': instance.createdAt.toIso8601String(),
      'creator_id': instance.creatorId,
      'data': instance.data,
      'file_name': instance.fileName,
      'hash': instance.hash,
      'id': instance.id,
      'mime_type': instance.mimeType,
      'updated_at': instance.updatedAt?.toIso8601String(),
      'updater_id': instance.updaterId,
      'url': instance.url,
    };

AssetById$QueryRoot _$AssetById$QueryRootFromJson(Map<String, dynamic> json) {
  return AssetById$QueryRoot()
    ..assetByPk = json['asset_by_pk'] == null
        ? null
        : AssetById$QueryRoot$AssetByPk.fromJson(
            json['asset_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$AssetById$QueryRootToJson(
        AssetById$QueryRoot instance) =>
    <String, dynamic>{
      'asset_by_pk': instance.assetByPk?.toJson(),
    };

AssetByIdArguments _$AssetByIdArgumentsFromJson(Map<String, dynamic> json) {
  return AssetByIdArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$AssetByIdArgumentsToJson(AssetByIdArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
