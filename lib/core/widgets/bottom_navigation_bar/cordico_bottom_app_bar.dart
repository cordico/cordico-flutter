import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/bottom_navigation_bar/cordico_bottom_app_bar_item.dart';
import 'package:flutter/material.dart';

class CordicoBottomAppBar extends StatefulWidget {
  const CordicoBottomAppBar({
    Key? key,
    required this.items,
    required this.onTabSelected,
    required this.selectedIndex,
  }) : super(key: key);

  final List<CordicoBottomAppBarItem> items;
  final ValueChanged<int> onTabSelected;
  final int selectedIndex;

  @override
  State<StatefulWidget> createState() => CordicoAppBarState();
}

class CordicoAppBarState extends State<CordicoBottomAppBar> {
  static const double _appBarHeight = 60;
  static const double _iconHeight = 25;
  int _selectedIndex = 0;

  @override
  void initState() {
    _selectedIndex = widget.selectedIndex;

    global.tabBarState = this;
    super.initState();
  }

  @override
  void didUpdateWidget(covariant CordicoBottomAppBar oldWidget) {
    _selectedIndex = widget.selectedIndex;
    super.didUpdateWidget(oldWidget);
  }

  void updateIndex(int index) {
    widget.onTabSelected(index);
  }

  @override
  Widget build(BuildContext context) {
    final items = List<Widget>.generate(widget.items.length, (int index) {
      return _buildTabItem(
        item: widget.items[index],
        index: index,
        onPressed: updateIndex,
      );
    });

    return BottomAppBar(
      color: ThemeColors.gray.shade600,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: items,
      ),
    );
  }

  List<Widget> _buildIconChildren(CordicoBottomAppBarItem item) {
    return <Widget>[
      SizedBox(
        height: _iconHeight,
        child: Align(
          child: item.icon,
        ),
      ),
      const SizedBox(height: 4),
      FittedBox(
        fit: BoxFit.fitWidth,
        child: Text(
          item.text!,
          style: ThemeStyles.extraSmallParagraph(context)?.copyWith(
            color: item.textColor ?? ThemeColors.blue.shade50,
            letterSpacing: 1,
          ),
        ),
      )
    ];
  }

  Widget _buildTabItem({
    required CordicoBottomAppBarItem item,
    required int index,
    required ValueChanged<int> onPressed,
  }) {
    return Expanded(
      child: SizedBox(
        height: _appBarHeight,
        child: Material(
          type: MaterialType.transparency,
          child: InkWell(
              onTap: () => onPressed(index),
              child: Container(
                color: index == _selectedIndex
                    ? Colors.white.withOpacity(0.1)
                    : Colors.transparent,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: _buildIconChildren(item),
                ),
              )),
        ),
      ),
    );
  }
}
