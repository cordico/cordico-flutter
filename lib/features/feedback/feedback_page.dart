// ignore_for_file: lines_longer_than_80_chars, implementation_imports

import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/managers/cordico_alert.dart';
import 'package:cordico_mobile/core/providers/global_providers.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/images.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/app_bar_provider.dart';
import 'package:cordico_mobile/core/widgets/cordico_elevated_button.dart';
import 'package:cordico_mobile/core/widgets/m_text_field.dart';
import 'package:core/core.dart';
import 'package:core/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_segment/flutter_segment.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:vrouter/src/core/extended_context.dart';

class FeedbackPage extends ConsumerStatefulWidget {
  const FeedbackPage({Key? key}) : super(key: key);

  @override
  ConsumerState<FeedbackPage> createState() => _FeedbackPageState();
}

class _FeedbackPageState extends ConsumerState<FeedbackPage> {
  final TextEditingController _textEditingController = TextEditingController();
  bool isLoading = false;

  FocusNode focus = FocusNode();

  bool hasFocus = false;

  @override
  void initState() {
    super.initState();
    focus.addListener(focusListener);
  }

  void focusListener() {
    if (focus.hasFocus != hasFocus) {
      hasFocus = focus.hasFocus;
      setState(() {});
    }
  }

  @override
  void dispose() {
    focus.removeListener(focusListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Segment.screen(
        screenName: 'Feedback',
        properties: <String, dynamic>{'type': 'screen', 'name': 'Feedback'});

    return AbsorbPointer(
      absorbing: isLoading,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBarProvider.getAppBarWithBackIcon(
            context,
            backgroundColor: ThemeColors.gray.shade900,
            title: Text(
              'Feedback for Cordico',
              style: ThemeStyles.cardContentTitle(context)
                  ?.copyWith(color: Colors.white),
            ),
          ),
          body: Stack(
            children: [
              GestureDetector(
                //onTap: () => FocusScope.of(context).unfocus(),
                child: Column(
                  children: [
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            const SizedBox(height: 24),
                            Container(
                              margin:
                                  const EdgeInsets.symmetric(horizontal: 13),
                              child: Text(
                                'Please tell Cordico about your experience using our new wellness application. \n\nThe information we gather will help Cordico build a better app experience and your feedback will remain completely anonymous.',
                                style: ThemeStyles.cardTitle(context)
                                    ?.copyWith(fontSize: 16),
                                textAlign: TextAlign.start,
                              ),
                            ),
                            const SizedBox(height: 24),
                            Container(
                              margin:
                                  const EdgeInsets.symmetric(horizontal: 13),
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 14, vertical: 19),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                                color: ThemeColors.gray.shade800,
                                border: Border.all(
                                  color: hasFocus
                                      ? ThemeColors.blue.shade500
                                      : Colors.transparent,
                                ),
                              ),
                              child: MTextField(
                                controller: _textEditingController,
                                cap: TextCapitalization.sentences,
                                tfPadding: EdgeInsets.zero,
                                maxLength: 1500,
                                focusNode: focus,
                                minLines: 5,
                                autofocus: true,
                                maxLines: null,
                                showCounter: true,
                                onChanged: (val) {
                                  setState(() {});
                                },
                                hint: 'Add your feedback here...',
                                lineVisible: false,
                              ),
                            ),
                            const SizedBox(height: 32),
                            const SizedBox(height: 32),
                          ],
                        ),
                      ),
                    ),
                    SafeArea(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 12, bottom: 13, left: 28, right: 28),
                        child: CordicoElevatedButton(
                          isLoading: isLoading,
                          onPressed: _textEditingController.text.length < 5
                              ? null
                              : () async {
                                  focus.unfocus();

                                  setState(() => isLoading = true);

                                  final res = await ref
                                      .watch(feedbackRepositoryProvider)
                                      .createFeedback(
                                        arguments:
                                            CreateOneFeedbackMutationArguments(
                                          subject: 'Cordico Feedback',
                                          body: _textEditingController.text
                                              .trim(),
                                          data: <String, dynamic>{
                                            'subject': 'Cordico Feedback'
                                          },
                                          feedback_type_id: kFeedbackTypeUser,
                                          creatorId:
                                              global.globalUser?.id ?? '',
                                        ),
                                      );

                                  await Future.delayed(
                                      const Duration(milliseconds: 2000),
                                      () {});
                                  setState(() => isLoading = false);

                                  await res.fold((l) {
                                    Future.delayed(
                                      const Duration(milliseconds: 300),
                                      () => Navigator.of(context).maybePop(),
                                    );
                                  }, (r) async {
                                    await CordicoAlert.showContentAlert(
                                      context,
                                      image: Images.checkIcon,
                                      action: () {
                                        Navigator.of(context).maybePop();
                                      },
                                      actionTitle: 'Back to Home',
                                      message:
                                          'Thank you fot taking the time to give us the feedback.\nFeel free in any moment to give feedback to us from the “Feedback for Cordico” button that you can find in the Home screen (you have to scroll all the way down.',
                                      title: 'Feedback Sent',
                                    );
                                    Future.delayed(
                                      const Duration(milliseconds: 300),
                                      () => context.vRouter.historyGo(-2),
                                    );
                                  });
                                },
                          height: 38,
                          text: 'Send Feedback',
                          textStyle:
                              ThemeStyles.elevatedButtonTextStyle(context)
                                  ?.copyWith(
                            color: ThemeColors.blue.shade50,
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              if (isLoading)
                Positioned.fill(
                  child: Container(
                    color: Colors.black26,
                    child: Center(
                      child: Container(
                        padding: const EdgeInsets.all(16),
                        decoration: BoxDecoration(
                          color: ThemeColors.gray.shade700,
                          borderRadius: BorderRadius.circular(12),
                          boxShadow: const [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 12,
                            )
                          ],
                        ),
                        child: const CircularProgressIndicator(),
                      ),
                    ),
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }
}
