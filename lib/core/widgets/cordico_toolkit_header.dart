import 'dart:ui' as ui;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/images.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:flutter/material.dart';

class CordicoToolkitHeader extends StatelessWidget {
  const CordicoToolkitHeader({
    Key? key,
    required this.maxHeight,
    required this.minHeight,
    required this.organizationName,
    required this.organizationImageUrl,
    required this.organizationCoverUrl,
    this.onSettingsTap,
  }) : super(key: key);

  final double minHeight;
  final double maxHeight;
  final String organizationName;
  final String organizationImageUrl;
  final String organizationCoverUrl;
  final Function()? onSettingsTap;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final expandRatio = _calculateExpandRatio(context, constraints);
        final animation = AlwaysStoppedAnimation(expandRatio);

        final topPadding = MediaQuery.of(context).viewPadding.top;

        final value =
            utils.calculateAlpha(animation.value * 60, zeroOpacityOffset: 0) *
                50;

        return Stack(
          fit: StackFit.expand,
          children: [
            Positioned(
              bottom: 10 + (60 - value) + topPadding,
              left: 0,
              right: 0,
              child: SizedBox(
                height: maxHeight,
                child: CachedNetworkImage(
                  imageUrl: organizationCoverUrl,
                  useOldImageOnUrlChange: true,
                  alignment: Alignment.topCenter,
                  fit: BoxFit.cover,
                  imageBuilder: (context, image) {
                    return Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: image, alignment: Alignment.topCenter),
                      ),
                    );
                  },
                  placeholder: (context, url) => Container(),
                  errorWidget: (context, url, dynamic error) => Container(
                    color: ThemeColors.blue.shade50,
                  ),
                ),
              ),
            ),
            _buildCordicoLogo(animation),
            _buildOrganizationLogo(context, animation, constraints),
            _buildOrganizationName(context, animation, constraints),
            Positioned(
              top: 17 + MediaQuery.of(context).viewPadding.top,
              right: 12,
              child: GestureDetector(
                onTap: onSettingsTap,
                child: Container(
                  clipBehavior: Clip.antiAlias,
                  decoration: BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(20)),
                  height: 40,
                  width: 40,
                  child: BackdropFilter(
                    filter: ui.ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                    child: Container(
                      clipBehavior: Clip.antiAlias,
                      decoration: BoxDecoration(
                        color: Colors.black.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(100),
                      ),
                      child: Align(
                        child: Image.asset(
                          Images.settingsIcon,
                          height: 24,
                          width: 24,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  double _calculateExpandRatio(
      BuildContext context, BoxConstraints constraints) {
    var expandRatio = (constraints.maxHeight -
            minHeight -
            MediaQuery.of(context).padding.top) /
        (maxHeight - minHeight);

    if (expandRatio > 1.0) expandRatio = 1.0;
    if (expandRatio < 0.0) expandRatio = 0.0;

    return expandRatio;
  }

  Positioned _buildCordicoLogo(
    Animation<double> animation,
  ) {
    final positionSize =
        SizeTween(end: const Size(53, 20), begin: const Size(-83, 20))
            .evaluate(animation);

    return Positioned(
      top: positionSize?.width,
      left: positionSize?.height,
      child: Image.asset(
        Images.icLogoBig,
        width: 78,
      ),
    );
  }

  Positioned _buildOrganizationLogo(
    BuildContext context,
    Animation<double> animation,
    BoxConstraints constraints,
  ) {
    final topPadding = MediaQuery.of(context).viewPadding.top;
    final positionSize = SizeTween(
            end: Size(110, (constraints.maxWidth / 2) - 106),
            begin: Size(20 + topPadding, 14))
        .evaluate(animation);

    final imageSize =
        SizeTween(end: const Size(212, 212), begin: const Size(53, 53))
            .evaluate(animation);

    final blurPadding = utils.calculateAlpha(animation.value * 40,
            zeroOpacityOffset: 0, fullOpacityOffset: 40) *
        40;

    return Positioned(
      top: (positionSize?.width ?? 0) - 5.0,
      left: positionSize?.height,
      child: Container(
        clipBehavior: Clip.antiAlias,
        decoration: BoxDecoration(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular((imageSize?.height ?? 200) / 2.0),
        ),
        width: imageSize?.width,
        height: imageSize?.height,
        child: BackdropFilter(
          filter: ui.ImageFilter.blur(sigmaX: 10, sigmaY: 10),
          child: Container(
            clipBehavior: Clip.antiAlias,
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0),
              borderRadius: BorderRadius.circular(100),
            ),
            margin: EdgeInsets.all(blurPadding),
            child: CachedNetworkImage(
              imageUrl: organizationImageUrl,
              fit: BoxFit.cover,
              placeholder: (context, url) => Container(
                color: ThemeColors.blue.shade50,
              ),
              imageBuilder: (context, image) {
                return Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(image: image),
                  ),
                );
              },
              errorWidget: (context, url, dynamic error) => Container(
                color: ThemeColors.blue.shade50,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Positioned _buildOrganizationName(
    BuildContext context,
    Animation<double> animation,
    BoxConstraints constraints,
  ) {
    final topPadding = MediaQuery.of(context).viewPadding.top;
    final positionSize = SizeTween(
            end: Size(332, (constraints.maxWidth / 2) - 82),
            begin: Size(20 + topPadding, 83))
        .evaluate(animation);

    return Positioned(
      top: positionSize?.width,
      left: positionSize?.height,
      child: Opacity(
        opacity: 1 -
            utils.calculateAlpha(animation.value,
                zeroOpacityOffset: 0, fullOpacityOffset: 0.4),
        child: Container(
          constraints: const BoxConstraints(maxWidth: 300),
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(100)),
            color: ThemeColors.gray.shade600,
          ),
          padding: const EdgeInsets.fromLTRB(16, 10, 16, 8),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.asset(Images.departmentIcon, height: 24, width: 24),
              Container(width: 10),
              Flexible(
                child: Text(
                  organizationName,
                  style: ThemeStyles.sectionTitle(context)?.copyWith(
                    color: Colors.white,
                    fontSize: 21,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
