// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'update_one_content_mutation.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class UpdateOneContentMutation$MutationRoot$UpdateOneContent
    extends JsonSerializable with EquatableMixin {
  UpdateOneContentMutation$MutationRoot$UpdateOneContent();

  factory UpdateOneContentMutation$MutationRoot$UpdateOneContent.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateOneContentMutation$MutationRoot$UpdateOneContentFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$UpdateOneContentMutation$MutationRoot$UpdateOneContentToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateOneContentMutation$MutationRoot extends JsonSerializable
    with EquatableMixin {
  UpdateOneContentMutation$MutationRoot();

  factory UpdateOneContentMutation$MutationRoot.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateOneContentMutation$MutationRootFromJson(json);

  UpdateOneContentMutation$MutationRoot$UpdateOneContent? updateOneContent;

  @override
  List<Object?> get props => [updateOneContent];
  @override
  Map<String, dynamic> toJson() =>
      _$UpdateOneContentMutation$MutationRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateOneContentMutationArguments extends JsonSerializable
    with EquatableMixin {
  UpdateOneContentMutationArguments(
      {this.id,
      this.contentStateId,
      this.contentTypeId,
      required this.name,
      this.data,
      this.isShared});

  @override
  factory UpdateOneContentMutationArguments.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateOneContentMutationArgumentsFromJson(json);

  final String? id;

  final String? contentStateId;

  final String? contentTypeId;

  late String name;

  final dynamic? data;

  final bool? isShared;

  @override
  List<Object?> get props =>
      [id, contentStateId, contentTypeId, name, data, isShared];
  @override
  Map<String, dynamic> toJson() =>
      _$UpdateOneContentMutationArgumentsToJson(this);
}

final UPDATE_ONE_CONTENT_MUTATION_MUTATION_DOCUMENT =
    DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'UpdateOneContentMutation'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentStateId')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentTypeId')),
            type: NamedTypeNode(
                name: NameNode(value: 'String'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'name')),
            type:
                NamedTypeNode(name: NameNode(value: 'String'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'data')),
            type:
                NamedTypeNode(name: NameNode(value: 'Json'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'isShared')),
            type: NamedTypeNode(
                name: NameNode(value: 'Boolean'), isNonNull: false),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'updateOneContent'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'where'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'id'),
                        value: VariableNode(name: NameNode(value: 'id')))
                  ])),
              ArgumentNode(
                  name: NameNode(value: 'data'),
                  value: ObjectValueNode(fields: [
                    ObjectFieldNode(
                        name: NameNode(value: 'contentState'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'connect'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'id'),
                                    value: VariableNode(
                                        name:
                                            NameNode(value: 'contentStateId')))
                              ]))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'contentType'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'connect'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'id'),
                                    value: VariableNode(
                                        name: NameNode(value: 'contentTypeId')))
                              ]))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'data'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'set'),
                              value:
                                  VariableNode(name: NameNode(value: 'data')))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'name'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'set'),
                              value:
                                  VariableNode(name: NameNode(value: 'name')))
                        ])),
                    ObjectFieldNode(
                        name: NameNode(value: 'isShared'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'set'),
                              value: VariableNode(
                                  name: NameNode(value: 'isShared')))
                        ]))
                  ]))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class UpdateOneContentMutationMutation extends GraphQLQuery<
    UpdateOneContentMutation$MutationRoot, UpdateOneContentMutationArguments> {
  UpdateOneContentMutationMutation({required this.variables});

  @override
  final DocumentNode document = UPDATE_ONE_CONTENT_MUTATION_MUTATION_DOCUMENT;

  @override
  final String operationName = 'UpdateOneContentMutation';

  @override
  final UpdateOneContentMutationArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  UpdateOneContentMutation$MutationRoot parse(Map<String, dynamic> json) =>
      UpdateOneContentMutation$MutationRoot.fromJson(json);
}
