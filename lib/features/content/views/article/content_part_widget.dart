// ignore_for_file: implementation_imports

import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/audio_player/cordico_audio_player.dart';
import 'package:cordico_mobile/core/widgets/brightcove_player.dart';
import 'package:cordico_mobile/core/widgets/card/cordico_card.dart';
import 'package:cordico_mobile/core/widgets/content_widget.dart';
import 'package:cordico_mobile/core/widgets/cordico_video_player.dart';
import 'package:cordico_mobile/core/widgets/vimeo_player.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/models/schema/content_schema.dart';
import 'package:core/utils/content_utils.dart';
import 'package:core/utils/map_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_segment/flutter_segment.dart';
import 'package:vrouter/src/core/extended_context.dart';

class ContentPartWidget extends StatefulWidget {
  const ContentPartWidget({
    Key? key,
    required this.content,
    required this.mainContent,
    this.nextContent,
    required this.index,
    required this.pageController,
    this.updatePage,
  }) : super(key: key);

  final ContentQuery$QueryRoot$Content? mainContent;
  final ContentQuery$QueryRoot$Content? content;
  final ContentQuery$QueryRoot$Content? nextContent;
  final PageController pageController;
  final int index;
  final Function()? updatePage;

  @override
  State<ContentPartWidget> createState() => _ContentPartWidgetState();
}

class _ContentPartWidgetState extends State<ContentPartWidget> {
  late ContentSchema contentSchema;
  late ContentSchema? nextContentSchema;
  late ContentSchema mainContentSchema;

  @override
  void initState() {
    super.initState();

    final readContentParts =
        global.prefs.getStringList(widget.mainContent?.id ?? '') ?? [];

    if (!readContentParts.contains(widget.content?.id)) {
      readContentParts.add(widget.content?.id ?? '');

      global.prefs
          .setStringList(widget.mainContent?.id ?? '', readContentParts);

      if (widget.updatePage != null) {
        widget.updatePage!();
      }
    }

    if (widget.content != null) {
      removeNullAndEmptyParams(widget.content!.data as Map<String, dynamic>);
      contentSchema =
          ContentSchema.fromJson(widget.content!.data as Map<String, dynamic>);
    }

    if (widget.mainContent != null) {
      removeNullAndEmptyParams(
          widget.mainContent!.data as Map<String, dynamic>);
      mainContentSchema = ContentSchema.fromJson(
          widget.mainContent!.data as Map<String, dynamic>);
    }

    if (widget.nextContent != null) {
      removeNullAndEmptyParams(
          widget.nextContent!.data as Map<String, dynamic>);
      nextContentSchema = ContentSchema.fromJson(
          widget.nextContent!.data as Map<String, dynamic>);
    }
  }

  @override
  Widget build(BuildContext context) {
    return _buildContentScrollView(
      context,
      pageController: widget.pageController,
      mainContent: widget.mainContent,
      nextPart: widget.nextContent,
      content: widget.content,
      index: widget.index,
    );
  }

  Widget _buildContentScrollView(
    BuildContext context, {
    required PageController pageController,
    ContentQuery$QueryRoot$Content? mainContent,
    required ContentQuery$QueryRoot$Content? content,
    required int index,
    ContentQuery$QueryRoot$Content? nextPart,
  }) {
    Segment.track(eventName: 'View Content Part', properties: <String, dynamic>{
      'part_id': content!.id,
      'type': content.contentType.name,
      'name': content.name,
      'index': index
    });

    //videoUrl; //'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4';
    //audioUrl; //'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-2.mp3';
    // check for BrightCove video...
    Widget? videoPlayerChild;
    if ((contentSchema.videoUrl1 != null &&
            contentSchema.videoUrl1!.isNotEmpty) ||
        (contentSchema.videoUrl != null &&
            contentSchema.videoUrl!.isNotEmpty)) {
      final url = contentSchema.videoUrl!;

      Segment.track(
          eventName: 'Video Content',
          properties: <String, dynamic>{'type': content.name, 'url': url});

      if (url.contains('players.brightcove.net')) {
        videoPlayerChild = BrightcovePlayer(url: url);
      } else if (url.contains('player.vimeo.com') ||
          url.contains('www.youtube.com')) {
        videoPlayerChild = VimeoPlayer(url: url);
      } else {
        videoPlayerChild = CordicoVideoPlayer(
            videoUrl: contentSchema.videoUrl1 ?? contentSchema.videoUrl ?? '');
      }
    }

    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 17, 20, 0),
              child: _buildHeader(
                context,
                content: content,
                mainContent: mainContent,
              ),
            ),
            if (videoPlayerChild != null)
              Padding(
                padding: const EdgeInsets.only(top: 18),
                child: videoPlayerChild,
              ),
            if (contentSchema.audioUrl != null &&
                contentSchema.audioUrl!.isNotEmpty)
              Padding(
                padding: const EdgeInsets.fromLTRB(22, 37, 22, 0),
                child: CordicoAudioPlayer(url: contentSchema.audioUrl ?? ''),
              ),
            Padding(
              padding: contentSchema.videoUrl1 != null ||
                      contentSchema.audioUrl != null
                  ? const EdgeInsets.fromLTRB(20, 30, 20, 0)
                  : const EdgeInsets.fromLTRB(20, 56, 20, 0),
              child: ContentWidget(
                content: contentSchema.body ?? '',
                contentType: contentSchema.bodyMimeType == null ||
                        contentSchema.bodyMimeType == MimeType.markdown
                    ? MimeType.markdown
                    : MimeType.html,
                isDarkMode: false,
              ),
            ),
            if (nextPart != null)
              _buildNextContentCard(context, pageController, nextPart)
            else
              Container(),
          ],
        ),
      ),
    );
  }

  Widget _buildHeader(
    BuildContext context, {
    ContentQuery$QueryRoot$Content? content,
    ContentQuery$QueryRoot$Content? mainContent,
  }) {
    final contentTypeName = content?.contentType.name;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          children: [
            const Padding(
              padding: EdgeInsets.only(right: 8),
              child: CircleAvatar(
                backgroundColor: Color(0xFF26CD54),
                radius: 5,
              ),
            ),
            Expanded(
              child: Text(
                (contentTypeName ?? '').toUpperCase(),
                style: ThemeStyles.cardCategory(context)?.copyWith(
                  color: ThemeColors.gray.shade500,
                ),
              ),
            ),
            Container(width: 8),
            IconButton(
              onPressed: () {
                context.vRouter.historyBack();
              },
              iconSize: 40,
              splashRadius: 20,
              icon: CircleAvatar(
                radius: 20,
                backgroundColor: ThemeColors.buttonsGreyColor,
                child: Icon(Icons.close, color: ThemeColors.blue.shade100),
              ),
              padding: EdgeInsets.zero,
            ),
          ],
        ),
        Text(
          mainContentSchema.title ?? '',
          style: ThemeStyles.bulletinCardTitle(context)?.copyWith(
            color: ThemeColors.gray.shade500,
          ),
        ),
      ],
    );
  }

  Widget _buildNextContentCard(
    BuildContext context,
    PageController pageController,
    ContentQuery$QueryRoot$Content? content,
  ) {
    return Padding(
      padding: const EdgeInsets.only(top: 15, left: 20, right: 20),
      child: CordicoCard(
        onTap: () {
          if (pageController.hasClients) {
            pageController.animateToPage(widget.index + 1,
                duration: const Duration(milliseconds: 400),
                curve: Curves.easeInOut);
          }
        },
        cornerRadius: 16,
        borderSide: BorderSide(color: ThemeColors.blue.shade200),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'NEXT CONTENT',
                style: ThemeStyles.extraSmallParagraph(context)?.copyWith(
                  color: ThemeColors.blue.shade200,
                ),
              ),
              Container(height: 4),
              Text(
                widget.nextContent?.name ?? '',
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: ThemeStyles.cardContentTitle(context)?.copyWith(
                  color: ThemeColors.blue.shade500,
                ),
              ),
              Container(height: 2),
              Row(
                children: [
                  Flexible(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 9),
                      child: Text(
                        (nextContentSchema?.title ?? '').trim(),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: ThemeStyles.smallParagraph(context)?.copyWith(
                          color: ThemeColors.blue.shade400,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Text(
                      // ignore: lines_longer_than_80_chars
                      'Reading Time ${ContentUtils.readingTimeFromContentPart(content!)}',
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.end,
                      maxLines: 1,
                      style: ThemeStyles.extraSmallParagraph(context)?.copyWith(
                        color: ThemeColors.blue.shade400,
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
