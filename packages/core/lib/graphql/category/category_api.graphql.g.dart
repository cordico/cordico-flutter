// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'category_api.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoryQuery$QueryRoot$Category _$CategoryQuery$QueryRoot$CategoryFromJson(
    Map<String, dynamic> json) {
  return CategoryQuery$QueryRoot$Category()
    ..id = json['id'] as String
    ..name = json['name'] as String
    ..description = json['description'] as String
    ..key = json['key'] as String;
}

Map<String, dynamic> _$CategoryQuery$QueryRoot$CategoryToJson(
        CategoryQuery$QueryRoot$Category instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'key': instance.key,
    };

CategoryQuery$QueryRoot _$CategoryQuery$QueryRootFromJson(
    Map<String, dynamic> json) {
  return CategoryQuery$QueryRoot()
    ..category = (json['category'] as List<dynamic>)
        .map((e) => CategoryQuery$QueryRoot$Category.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$CategoryQuery$QueryRootToJson(
        CategoryQuery$QueryRoot instance) =>
    <String, dynamic>{
      'category': instance.category.map((e) => e.toJson()).toList(),
    };
