// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'organization_contact_by_id.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrganizationContactById$QueryRoot$OrganizationContactsByPk$ContactType
    _$OrganizationContactById$QueryRoot$OrganizationContactsByPk$ContactTypeFromJson(
        Map<String, dynamic> json) {
  return OrganizationContactById$QueryRoot$OrganizationContactsByPk$ContactType()
    ..displayName = json['display_name'] as String
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$OrganizationContactById$QueryRoot$OrganizationContactsByPk$ContactTypeToJson(
            OrganizationContactById$QueryRoot$OrganizationContactsByPk$ContactType
                instance) =>
        <String, dynamic>{
          'display_name': instance.displayName,
          'id': instance.id,
          'name': instance.name,
        };

OrganizationContactById$QueryRoot$OrganizationContactsByPk$Organization
    _$OrganizationContactById$QueryRoot$OrganizationContactsByPk$OrganizationFromJson(
        Map<String, dynamic> json) {
  return OrganizationContactById$QueryRoot$OrganizationContactsByPk$Organization()
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$OrganizationContactById$QueryRoot$OrganizationContactsByPk$OrganizationToJson(
            OrganizationContactById$QueryRoot$OrganizationContactsByPk$Organization
                instance) =>
        <String, dynamic>{
          'name': instance.name,
        };

OrganizationContactById$QueryRoot$OrganizationContactsByPk
    _$OrganizationContactById$QueryRoot$OrganizationContactsByPkFromJson(
        Map<String, dynamic> json) {
  return OrganizationContactById$QueryRoot$OrganizationContactsByPk()
    ..data = json['data']
    ..title = json['title'] as String
    ..contactTypeId = json['contact_type_id'] as String
    ..contactType =
        OrganizationContactById$QueryRoot$OrganizationContactsByPk$ContactType
            .fromJson(json['contact_type'] as Map<String, dynamic>)
    ..organizationId = json['organization_id'] as String
    ..organization =
        OrganizationContactById$QueryRoot$OrganizationContactsByPk$Organization
            .fromJson(json['organization'] as Map<String, dynamic>)
    ..name = json['name'] as String
    ..isActive = json['is_active'] as bool
    ..id = json['id'] as String
    ..updated = json['updated'] == null
        ? null
        : DateTime.parse(json['updated'] as String)
    ..created = DateTime.parse(json['created'] as String);
}

Map<String,
    dynamic> _$OrganizationContactById$QueryRoot$OrganizationContactsByPkToJson(
        OrganizationContactById$QueryRoot$OrganizationContactsByPk instance) =>
    <String, dynamic>{
      'data': instance.data,
      'title': instance.title,
      'contact_type_id': instance.contactTypeId,
      'contact_type': instance.contactType.toJson(),
      'organization_id': instance.organizationId,
      'organization': instance.organization.toJson(),
      'name': instance.name,
      'is_active': instance.isActive,
      'id': instance.id,
      'updated': instance.updated?.toIso8601String(),
      'created': instance.created.toIso8601String(),
    };

OrganizationContactById$QueryRoot _$OrganizationContactById$QueryRootFromJson(
    Map<String, dynamic> json) {
  return OrganizationContactById$QueryRoot()
    ..organizationContactsByPk = json['organization_contacts_by_pk'] == null
        ? null
        : OrganizationContactById$QueryRoot$OrganizationContactsByPk.fromJson(
            json['organization_contacts_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$OrganizationContactById$QueryRootToJson(
        OrganizationContactById$QueryRoot instance) =>
    <String, dynamic>{
      'organization_contacts_by_pk':
          instance.organizationContactsByPk?.toJson(),
    };

OrganizationContactByIdArguments _$OrganizationContactByIdArgumentsFromJson(
    Map<String, dynamic> json) {
  return OrganizationContactByIdArguments(
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$OrganizationContactByIdArgumentsToJson(
        OrganizationContactByIdArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
