# kratos_api.model.UiNodeImageAttributes

## Load the model package
```dart
import 'package:kratos_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**height** | **int** | Height of the image | [optional] 
**id** | **String** | A unique identifier | [optional] 
**nodeType** | **String** |  | [optional] 
**src** | **String** | The image's source URL.  format: uri | [optional] 
**width** | **int** | Width of the image | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


