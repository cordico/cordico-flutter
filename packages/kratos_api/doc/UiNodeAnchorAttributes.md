# kratos_api.model.UiNodeAnchorAttributes

## Load the model package
```dart
import 'package:kratos_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**href** | **String** | The link's href (destination) URL.  format: uri | [optional] 
**id** | **String** | A unique identifier | [optional] 
**nodeType** | **String** |  | [optional] 
**title** | [**UiText**](UiText.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


