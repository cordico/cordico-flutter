import 'package:core/models/schema/content_schema.dart';
import 'package:json_annotation/json_annotation.dart';
part 'organization_contact_schema.g.dart';

@JsonSerializable(explicitToJson: true)
class OrganizationContactSchema extends JsonSerializable {
  OrganizationContactSchema();

  factory OrganizationContactSchema.fromJson(Map<String, dynamic> json) =>
      _$OrganizationContactSchemaFromJson(json);

  String? email;
  String? title;

  NameSchemaContainer? name;

  AddressSchema? address;

  SummarySchemaContainer? summary;

  @JsonKey(name: 'contact_phone')
  String? contactPhone;

  @JsonKey(name: 'profile_image_url')
  String? profileImageUrl;

  @JsonKey(name: 'action_buttons')
  List<ActionButtonSchema>? actionButtons;

  @override
  Map<String, dynamic> toJson() => _$OrganizationContactSchemaToJson(this);

  static OrganizationContactSchema fromJSON(Map<String, dynamic> json) {
    return OrganizationContactSchema.fromJson(json);
  }
}

@JsonSerializable(explicitToJson: true)
class AddressSchema {
  AddressSchema();

  factory AddressSchema.fromJson(Map<String, dynamic> json) =>
      _$AddressSchemaFromJson(json);

  String? zip;
  String? city;
  String? state;
  String? street;
  num? latitude;
  num? longitude;

  Map<String, dynamic> toJson() => _$AddressSchemaToJson(this);

  static AddressSchema fromJSON(Map<String, dynamic> json) {
    return AddressSchema.fromJson(json);
  }
}

@JsonSerializable(explicitToJson: true)
class NameSchemaContainer {
  NameSchemaContainer();

  factory NameSchemaContainer.fromJson(Map<String, dynamic> json) =>
      _$NameSchemaContainerFromJson(json);

  @JsonKey(name: 'last_name')
  String? lastName;

  @JsonKey(name: 'first_name')
  String? firstName;

  @JsonKey(name: 'display_name')
  String? displayName;

  Map<String, dynamic> toJson() => _$NameSchemaContainerToJson(this);

  static NameSchemaContainer fromJSON(Map<String, dynamic> json) {
    return NameSchemaContainer.fromJson(json);
  }
}

@JsonSerializable(explicitToJson: true)
class SummarySchemaContainer {
  SummarySchemaContainer();

  factory SummarySchemaContainer.fromJson(Map<String, dynamic> json) =>
      _$SummarySchemaContainerFromJson(json);

  @JsonKey(name: 'body')
  String? body;

  @JsonKey(
      name: 'body_mime_type',
      disallowNullValue: true,
      unknownEnumValue: MimeType.markdown)
  MimeType? bodyMimeType;

  Map<String, dynamic> toJson() => _$SummarySchemaContainerToJson(this);

  static SummarySchemaContainer fromJSON(Map<String, dynamic> json) {
    return SummarySchemaContainer.fromJson(json);
  }
}
