// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'department_page_holder.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DepartmentPageHolder _$DepartmentPageHolderFromJson(Map<String, dynamic> json) {
  return DepartmentPageHolder()
    ..bulletin = (json['bulletin'] as List<dynamic>?)
        ?.map((e) =>
            ContentQuery$QueryRoot$Content.fromJson(e as Map<String, dynamic>))
        .toList()
    ..gallery = (json['gallery'] as List<dynamic>?)
        ?.map((e) =>
            ContentQuery$QueryRoot$Content.fromJson(e as Map<String, dynamic>))
        .toList()
    ..departmentResources = (json['departmentResources'] as List<dynamic>?)
        ?.map((e) =>
            ContentQuery$QueryRoot$Content.fromJson(e as Map<String, dynamic>))
        .toList()
    ..eapResources = (json['eapResources'] as List<dynamic>?)
        ?.map((e) =>
            ContentQuery$QueryRoot$Content.fromJson(e as Map<String, dynamic>))
        .toList()
    ..organization = json['organization'] == null
        ? null
        : OrganizationByID$QueryRoot$OrganizationByPk.fromJson(
            json['organization'] as Map<String, dynamic>);
}

Map<String, dynamic> _$DepartmentPageHolderToJson(
        DepartmentPageHolder instance) =>
    <String, dynamic>{
      'bulletin': instance.bulletin?.map((e) => e.toJson()).toList(),
      'gallery': instance.gallery?.map((e) => e.toJson()).toList(),
      'departmentResources':
          instance.departmentResources?.map((e) => e.toJson()).toList(),
      'eapResources': instance.eapResources?.map((e) => e.toJson()).toList(),
      'organization': instance.organization?.toJson(),
    };
