// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'collection_content_by_id.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Collection$CollectionType();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Collection
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Collection();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$CollectionFromJson(
          json);

  late String id;

  late String name;

  @JsonKey(name: 'collection_type')
  CollectionContentById$QueryRoot$CollectionContentByPk$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [id, name, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentType
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentType();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentTypeFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentType
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentType();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentTypeFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  String? description;

  @override
  List<Object?> get props => [name, id, isShared, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ChildContent
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ChildContent();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ChildContentFromJson(
          json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ChildContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ParentContent
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ParentContent();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ParentContentFromJson(
          json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ParentContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories$Category();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategoriesFromJson(
          json);

  late CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories$Category
      category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategoriesToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentState
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentState();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentStateFromJson(
          json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentStateToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection$CollectionType();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$CollectionFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$CollectionsFromJson(
          json);

  late CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections$Collection
      collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$CollectionsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPartFromJson(
          json);

  @JsonKey(name: 'content_type')
  late CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentType
      contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<
          CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ChildContent>
      childContent;

  @JsonKey(name: 'parent_content')
  late List<
          CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ParentContent>
      parentContent;

  @JsonKey(name: 'content_categories')
  late List<
          CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$ContentState
      contentState;

  late List<
          CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart$Collections>
      collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPartToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContentFromJson(
          json);

  @JsonKey(name: 'content_part')
  late CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent$ContentPart
      contentPart;

  late String id;

  late int index;

  @override
  List<Object?> get props => [contentPart, id, index];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$ParentContent
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$ParentContent();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$ParentContent.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ParentContentFromJson(
          json);

  late String id;

  late int index;

  @JsonKey(name: 'part_id')
  late String partId;

  @override
  List<Object?> get props => [id, index, partId];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ParentContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories$Category
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories$Category();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories$Category.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories$CategoryFromJson(
          json);

  late String name;

  late String id;

  late String description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories$CategoryToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategoriesFromJson(
          json);

  late CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories$Category
      category;

  @override
  List<Object?> get props => [category];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategoriesToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentState
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentState();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentState.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentStateFromJson(
          json);

  late String id;

  late String name;

  @override
  List<Object?> get props => [id, name];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentStateToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection$CollectionType
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection$CollectionType();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection$CollectionType.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection$CollectionTypeFromJson(
          json);

  late String name;

  late String id;

  String? description;

  @override
  List<Object?> get props => [name, id, description];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection$CollectionTypeToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$CollectionFromJson(
          json);

  late String name;

  late String id;

  @JsonKey(name: 'collection_type')
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection$CollectionType?
      collectionType;

  @override
  List<Object?> get props => [name, id, collectionType];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$CollectionToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$CollectionsFromJson(
          json);

  late CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections$Collection
      collection;

  @override
  List<Object?> get props => [collection];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$Content$CollectionsToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk$Content
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk$Content();

  factory CollectionContentById$QueryRoot$CollectionContentByPk$Content.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$ContentFromJson(
          json);

  @JsonKey(name: 'content_type')
  late CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentType
      contentType;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  late String name;

  @JsonKey(name: 'is_shared')
  late bool isShared;

  late String id;

  late dynamic data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'child_content')
  late List<
          CollectionContentById$QueryRoot$CollectionContentByPk$Content$ChildContent>
      childContent;

  @JsonKey(name: 'parent_content')
  late List<
          CollectionContentById$QueryRoot$CollectionContentByPk$Content$ParentContent>
      parentContent;

  @JsonKey(name: 'content_categories')
  late List<
          CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentCategories>
      contentCategories;

  @JsonKey(name: 'content_state')
  late CollectionContentById$QueryRoot$CollectionContentByPk$Content$ContentState
      contentState;

  late List<
          CollectionContentById$QueryRoot$CollectionContentByPk$Content$Collections>
      collections;

  @override
  List<Object?> get props => [
        contentType,
        updatedAt,
        name,
        isShared,
        id,
        data,
        createdAt,
        childContent,
        parentContent,
        contentCategories,
        contentState,
        collections
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPk$ContentToJson(
          this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot$CollectionContentByPk
    extends JsonSerializable with EquatableMixin {
  CollectionContentById$QueryRoot$CollectionContentByPk();

  factory CollectionContentById$QueryRoot$CollectionContentByPk.fromJson(
          Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRoot$CollectionContentByPkFromJson(json);

  late CollectionContentById$QueryRoot$CollectionContentByPk$Collection
      collection;

  late String id;

  late CollectionContentById$QueryRoot$CollectionContentByPk$Content content;

  @override
  List<Object?> get props => [collection, id, content];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRoot$CollectionContentByPkToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentById$QueryRoot extends JsonSerializable
    with EquatableMixin {
  CollectionContentById$QueryRoot();

  factory CollectionContentById$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$CollectionContentById$QueryRootFromJson(json);

  @JsonKey(name: 'collection_content_by_pk')
  CollectionContentById$QueryRoot$CollectionContentByPk? collectionContentByPk;

  @override
  List<Object?> get props => [collectionContentByPk];
  @override
  Map<String, dynamic> toJson() =>
      _$CollectionContentById$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CollectionContentByIdArguments extends JsonSerializable
    with EquatableMixin {
  CollectionContentByIdArguments(
      {required this.id,
      this.contentStateNames,
      this.contentOffset,
      this.contentLimit});

  @override
  factory CollectionContentByIdArguments.fromJson(Map<String, dynamic> json) =>
      _$CollectionContentByIdArgumentsFromJson(json);

  late String id;

  final List<String?>? contentStateNames;

  final int? contentOffset;

  final int? contentLimit;

  @override
  List<Object?> get props =>
      [id, contentStateNames, contentOffset, contentLimit];
  @override
  Map<String, dynamic> toJson() => _$CollectionContentByIdArgumentsToJson(this);
}

final COLLECTION_CONTENT_BY_ID_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'CollectionContentById'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentStateNames')),
            type: ListTypeNode(
                type: NamedTypeNode(
                    name: NameNode(value: 'String'), isNonNull: false),
                isNonNull: false),
            defaultValue: DefaultValueNode(
                value: ListValueNode(values: [
              StringValueNode(value: 'Published', isBlock: false)
            ])),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentOffset')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '0')),
            directives: []),
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'contentLimit')),
            type: NamedTypeNode(name: NameNode(value: 'Int'), isNonNull: false),
            defaultValue: DefaultValueNode(value: IntValueNode(value: '10000')),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'collection_content_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'id'),
                  value: VariableNode(name: NameNode(value: 'id')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'collection'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'collection_type'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'description'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ]))
                  ])),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'content'),
                  alias: null,
                  arguments: [
                    ArgumentNode(
                        name: NameNode(value: 'offset'),
                        value: VariableNode(
                            name: NameNode(value: 'contentOffset'))),
                    ArgumentNode(
                        name: NameNode(value: 'limit'),
                        value: VariableNode(
                            name: NameNode(value: 'contentLimit'))),
                    ArgumentNode(
                        name: NameNode(value: 'where'),
                        value: ObjectValueNode(fields: [
                          ObjectFieldNode(
                              name: NameNode(value: 'content_state'),
                              value: ObjectValueNode(fields: [
                                ObjectFieldNode(
                                    name: NameNode(value: 'name'),
                                    value: ObjectValueNode(fields: [
                                      ObjectFieldNode(
                                          name: NameNode(value: '_in'),
                                          value: VariableNode(
                                              name: NameNode(
                                                  value: 'contentStateNames')))
                                    ]))
                              ]))
                        ]))
                  ],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'content_type'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'is_shared'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'description'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ])),
                    FieldNode(
                        name: NameNode(value: 'updated_at'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'name'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'is_shared'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'id'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'data'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'created_at'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null),
                    FieldNode(
                        name: NameNode(value: 'child_content'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'content_part'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'content_type'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'is_shared'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'description'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ])),
                                FieldNode(
                                    name: NameNode(value: 'updated_at'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'is_shared'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'data'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'created_at'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'child_content'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ])),
                                FieldNode(
                                    name: NameNode(value: 'parent_content'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'index'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'part_id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ])),
                                FieldNode(
                                    name: NameNode(value: 'content_categories'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'category'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'name'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'description'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null)
                                          ]))
                                    ])),
                                FieldNode(
                                    name: NameNode(value: 'content_state'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ])),
                                FieldNode(
                                    name: NameNode(value: 'collections'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'collection'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet:
                                              SelectionSetNode(selections: [
                                            FieldNode(
                                                name: NameNode(value: 'name'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(value: 'id'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: null),
                                            FieldNode(
                                                name: NameNode(
                                                    value: 'collection_type'),
                                                alias: null,
                                                arguments: [],
                                                directives: [],
                                                selectionSet: SelectionSetNode(
                                                    selections: [
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'name'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value: 'id'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null),
                                                      FieldNode(
                                                          name: NameNode(
                                                              value:
                                                                  'description'),
                                                          alias: null,
                                                          arguments: [],
                                                          directives: [],
                                                          selectionSet: null)
                                                    ]))
                                          ]))
                                    ]))
                              ])),
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'index'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ])),
                    FieldNode(
                        name: NameNode(value: 'parent_content'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'index'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'part_id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ])),
                    FieldNode(
                        name: NameNode(value: 'content_categories'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'category'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'description'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null)
                              ]))
                        ])),
                    FieldNode(
                        name: NameNode(value: 'content_state'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'id'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null),
                          FieldNode(
                              name: NameNode(value: 'name'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: null)
                        ])),
                    FieldNode(
                        name: NameNode(value: 'collections'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: SelectionSetNode(selections: [
                          FieldNode(
                              name: NameNode(value: 'collection'),
                              alias: null,
                              arguments: [],
                              directives: [],
                              selectionSet: SelectionSetNode(selections: [
                                FieldNode(
                                    name: NameNode(value: 'name'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'id'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: null),
                                FieldNode(
                                    name: NameNode(value: 'collection_type'),
                                    alias: null,
                                    arguments: [],
                                    directives: [],
                                    selectionSet: SelectionSetNode(selections: [
                                      FieldNode(
                                          name: NameNode(value: 'name'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'id'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null),
                                      FieldNode(
                                          name: NameNode(value: 'description'),
                                          alias: null,
                                          arguments: [],
                                          directives: [],
                                          selectionSet: null)
                                    ]))
                              ]))
                        ]))
                  ]))
            ]))
      ]))
]);

class CollectionContentByIdQuery extends GraphQLQuery<
    CollectionContentById$QueryRoot, CollectionContentByIdArguments> {
  CollectionContentByIdQuery({required this.variables});

  @override
  final DocumentNode document = COLLECTION_CONTENT_BY_ID_QUERY_DOCUMENT;

  @override
  final String operationName = 'CollectionContentById';

  @override
  final CollectionContentByIdArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  CollectionContentById$QueryRoot parse(Map<String, dynamic> json) =>
      CollectionContentById$QueryRoot.fromJson(json);
}
