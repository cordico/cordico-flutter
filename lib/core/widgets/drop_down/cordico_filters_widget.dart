// ignore_for_file: file_names

import 'package:cordico_mobile/core/managers/coordico_manager.dart';
import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/cordico_elevated_button.dart';
import 'package:cordico_mobile/core/widgets/drop_down/cordico_drop_down_item.dart';
import 'package:cordico_mobile/features/discover/logic/content_types_provider.dart';
import 'package:cordico_mobile/features/toolkit/logic/categories_state_notifier.dart';
import 'package:core/core.dart';
import 'package:core/graphql/content/content_type_by_id.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class CordicoFiltersWidget extends StatefulWidget {
  const CordicoFiltersWidget({
    Key? key,
    this.dismissable = true,
    required this.filters,
    this.filterByCategory = false,
    this.filterByDay = false,
    this.filterByContent = true,
    this.filterByBulletinType = false,
    this.hideReset = false,
  }) : super(key: key);

  final bool dismissable;
  final bool filterByContent;
  final bool filterByCategory;
  final bool filterByDay;
  final bool filterByBulletinType;
  final bool hideReset;
  final Filters filters;

  @override
  _CordicoFiltersWidgetState createState() => _CordicoFiltersWidgetState();
}

class _CordicoFiltersWidgetState extends State<CordicoFiltersWidget> {
  late Filters filters;
  late List<ContentTypeModel> contentTypes;
  late List<CategoryModel> categoriesTypes;
  late List<DaysFilter> daysFilters;
  late List<BulletinType> bulletinTypesFilters;

  bool get allSelected {
    var allFiltersLength = 0;

    if (widget.filterByContent) {
      allFiltersLength += contentTypes.length;
    }

    if (widget.filterByCategory) {
      allFiltersLength += categoriesTypes.length;
    }

    if (widget.filterByBulletinType) {
      allFiltersLength += bulletinTypesFilters.length;
    }

    return filters.filtersApplied == allFiltersLength;
  }

  @override
  void initState() {
    super.initState();

    filters = widget.filters;

    contentTypes = [];
    categoriesTypes = [];

    daysFilters = [
      DaysFilter.allDays,
      DaysFilter.last7Days,
      DaysFilter.last30Days,
      DaysFilter.last60Days,
    ];

    bulletinTypesFilters = [
      BulletinType.announcement,
      BulletinType.event,
      BulletinType.workshop,
    ];
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => widget.dismissable,
      child: AlertDialog(
        backgroundColor: ThemeColors.gray.shade700,
        content: Container(
          clipBehavior: Clip.hardEdge,
          width: MediaQuery.of(context).size.width,
          decoration: const BoxDecoration(),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                constraints: BoxConstraints(
                    maxHeight: MediaQuery.of(context).size.height * 0.6),
                child: Scrollbar(
                  child: ListView(
                    physics: const ClampingScrollPhysics(),
                    shrinkWrap: true,
                    children: [
                      if (widget.filterByCategory)
                        _buildCategoriesItems(context),
                      if (widget.filterByContent)
                        _buildContentTypesItems(context),
                      if (widget.filterByDay) _buildDaysItems(context),
                      if (widget.filterByBulletinType)
                        _buildBulletinTypesItems(context)
                    ],
                  ),
                ),
              ),
              actionButtons(),
            ],
          ),
        ),
        contentPadding: const EdgeInsets.only(left: 13, right: 13),
        insetPadding: const EdgeInsets.only(left: 12, right: 12),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6),
        ),
      ),
    );
  }

  Widget actionButtons() {
    return SizedBox(
      height: 60,
      child: Row(
        children: [
          if (widget.hideReset == false)
            Expanded(
              child: CordicoElevatedButton(
                height: 32,
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Reset Filters',
                      style: ThemeStyles.contentDescription(context)?.copyWith(
                        color: ThemeColors.blue.shade500,
                        fontWeight: FontWeight.w500,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
                onPressed: () {
                  filters.clearAll();
                  setState(() {});
                },
              ),
            ),
          if (widget.hideReset == false) const SizedBox(width: 12),
          Expanded(
            child: CordicoElevatedButton(
              height: 32,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Apply Filters',
                    style: ThemeStyles.contentDescription(context)?.copyWith(
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
              onPressed: () {
                Navigator.of(context).maybePop(filters);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCategoriesItems(BuildContext context) {
    return Consumer(
        builder: (BuildContext context, WidgetRef ref, Widget? child) {
      final state = ref.watch(categoriesNotifierProvider);

      return Builder(builder: (context) {
        categoriesTypes = List.from(state.categories ?? <CategoryModel>[]);
        final items = state.categories?.map((item) {
              final isSelected = filters.categoriesTypes.contains(item);
              return Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: CordicoDropDownItem(
                  item: item.name,
                  isSelected: isSelected,
                  colordBadge: global.colorFromCategoryModel(item),
                  onSelected: (_) {
                    if (isSelected) {
                      filters.categoriesTypes
                          .removeWhere((element) => element.id == item.id);
                    } else {
                      filters.categoriesTypes.add(item);
                    }
                    setState(() {});
                  },
                ),
              );
            }).toList() ??
            [];
        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 16, bottom: 16),
              child: Text(
                'Welness Toolkit',
                style: ThemeStyles.contentDescription(context)
                    ?.copyWith(color: ThemeColors.gray.shade200),
              ),
            ),
            ...items,
          ],
        );
      });
    });
  }

  Widget _buildContentTypesItems(BuildContext context) {
    return Consumer(
        builder: (BuildContext context, WidgetRef ref, Widget? child) {
      final state = ref.watch(contentTypesStateNotifierProvider);

      return Builder(builder: (context) {
        if (state.contentTypes != null) {
          contentTypes = List.from(state.contentTypes!);
        }

        final items = (state.contentTypes ?? []).map((item) {
          final isSelected = filters.contentTypes.contains(item);
          return Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: CordicoDropDownItem(
              item: item.name,
              isSelected: isSelected,
              onSelected: (_) {
                if (isSelected) {
                  filters.contentTypes.remove(item);
                } else {
                  filters.contentTypes.add(item);
                }
                setState(() {});
              },
            ),
          );
        }).toList();
        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 16, bottom: 16),
              child: Text(
                'Content Types',
                style: ThemeStyles.contentDescription(context)
                    ?.copyWith(color: ThemeColors.gray.shade200),
              ),
            ),
            ...items,
          ],
        );
      });
    });
  }

  Widget _buildDaysItems(BuildContext context) {
    final items = daysFilters.map((item) {
      final isSelected = filters.daysFilter == item;
      return Padding(
        padding: const EdgeInsets.only(bottom: 8),
        child: CordicoDropDownItem(
          item: item.getName(),
          isRounded: true,
          isSelected: isSelected,
          onSelected: (_) {
            filters.daysFilter = item;
            setState(() {});
          },
        ),
      );
    }).toList();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 16, bottom: 16),
          child: Text(
            'Days',
            style: ThemeStyles.contentDescription(context)
                ?.copyWith(color: ThemeColors.gray.shade200),
          ),
        ),
        ...items,
      ],
    );
  }

  Widget _buildBulletinTypesItems(BuildContext context) {
    final items = bulletinTypesFilters.map((item) {
      final isSelected = filters.bulletinTypes?.contains(item) ?? false;
      return Padding(
        padding: const EdgeInsets.only(bottom: 8),
        child: CordicoDropDownItem(
          item: item.getName(),
          isSelected: isSelected,
          onSelected: (_) {
            if (isSelected) {
              filters.bulletinTypes?.remove(item);
            } else {
              filters.bulletinTypes?.add(item);
            }

            setState(() {});
          },
        ),
      );
    }).toList();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 16, bottom: 16),
          child: Text(
            'Bulletin Type',
            style: ThemeStyles.contentDescription(context)
                ?.copyWith(color: ThemeColors.gray.shade200),
          ),
        ),
        ...items,
      ],
    );
  }
}

class Filters {
  Filters({
    required this.categoriesTypes,
    required this.contentTypes,
    this.daysFilter,
    this.bulletinTypes,
  });

  List<String> get contentTypesIds {
    return contentTypes.map((e) => e.id).toList();
  }

  List<String> get categoriesTypesIds {
    return categoriesTypes.map((e) => e.id).toList();
  }

  int get filtersApplied {
    var count = 0;
    count += contentTypes.length;
    count += categoriesTypes.length;
    if (bulletinTypes != null) {
      count += bulletinTypes!.length;
    }
    return count;
  }

  void clearAll() {
    contentTypes.clear();
    categoriesTypes.clear();
    daysFilter = DaysFilter.allDays;

    if (bulletinTypes != null) {
      bulletinTypes!.clear();
    }
  }

  List<ContentTypeById$QueryRoot$ContentTypeByPk> contentTypes;
  List<CategoryModel> categoriesTypes;
  DaysFilter? daysFilter;
  List<BulletinType>? bulletinTypes;
}

enum DaysFilter {
  allDays,
  last7Days,
  last30Days,
  last60Days,
}

extension DaysFiltersName on DaysFilter {
  String getName() {
    switch (this) {
      case DaysFilter.allDays:
        return 'All Days';
      case DaysFilter.last7Days:
        return 'Last 7 Days';
      case DaysFilter.last30Days:
        return 'Last 30 Days';
      case DaysFilter.last60Days:
        return 'Last 60 Days';
      default:
        return '';
    }
  }

  int getFilterDays() {
    switch (this) {
      case DaysFilter.allDays:
        return 0;
      case DaysFilter.last7Days:
        return 7;
      case DaysFilter.last30Days:
        return 30;
      case DaysFilter.last60Days:
        return 60;
      default:
        return 0;
    }
  }
}

enum BulletinType {
  announcement,
  workshop,
  event,
}

extension BulletinUtils on BulletinType {
  BulletinType? getType(String name) {
    switch (name.trim().toLowerCase()) {
      case 'announcement':
        return BulletinType.announcement;
      case 'workshop':
        return BulletinType.workshop;
      case 'event':
        return BulletinType.event;
      default:
        return null;
    }
  }

  String getName() {
    switch (this) {
      case BulletinType.announcement:
        return 'Announcement';
      case BulletinType.workshop:
        return 'Workshop';
      case BulletinType.event:
        return 'Event';
      default:
        return '';
    }
  }
}

class BulletinHelper {
  static BulletinType? getType(String name) {
    switch (name.trim().toLowerCase()) {
      case 'announcement':
        return BulletinType.announcement;
      case 'workshop':
        return BulletinType.workshop;
      case 'event':
        return BulletinType.event;
      default:
        return null;
    }
  }
}
