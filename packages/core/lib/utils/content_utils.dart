// ignore_for_file: avoid_function_literals_in_foreach_calls

import 'package:core/graphql/content/content_query.dart';
import 'package:core/models/schema/content_schema.dart';
import 'package:core/utils/constants.dart';
import 'package:core/utils/map_extension.dart';

abstract class ContentUtils {
  static int wordCount(String text) {
    final regExp = RegExp(r"\w+(\'\w+)?");
    return regExp.allMatches(text).length;
  }

  static double readingTime(String text) {
    final wordCount = ContentUtils.wordCount(text);
    return wordCount / 230.0;
  }

  static String readingTimeString(String text) {
    final minutes = ContentUtils.readingTime(text);

    final milliseconds = minutes.toInt() * 60 * 1000;
    final duration = Duration(milliseconds: milliseconds);
    if (duration.inDays > 0) {
      return '${duration.inDays} days';
    }

    if (duration.inHours > 0) {
      return '${duration.inHours} hr';
    }

    if (duration.inMinutes > 0) {
      return '${duration.inMinutes} ${duration.inMinutes > 1 ? 'Mins' : 'Min'}';
    }

    return '${duration.inSeconds} sec';
  }

  static String readingTimeFromContentPart(
      ContentQuery$QueryRoot$Content content) {
    var body = '';

    if (content.data != null) {
      removeNullAndEmptyParams(content.data as Map<String, dynamic>);
      final contentSchema =
          ContentSchema.fromJson(content.data as Map<String, dynamic>);
      body = contentSchema.body ?? '';
    }
    final buffer = StringBuffer(body);
    return ContentUtils.readingTimeString(buffer.toString());
  }

  static String readingTimeFromContent(ContentQuery$QueryRoot$Content content) {
    var body = '';

    if (content.childContent.isNotEmpty) {
      content.childContent.forEach((element) {
        removeNullAndEmptyParams(
            element.contentPart.data as Map<String, dynamic>);
        final contentSchema = ContentSchema.fromJson(
            element.contentPart.data as Map<String, dynamic>);

        if (contentSchema.body != null && contentSchema.body!.isNotEmpty) {
          body += ' ${contentSchema.body}';
        }
      });
    }

    final buffer = StringBuffer(body);
    return ContentUtils.readingTimeString(buffer.toString());
  }

  static bool evaluateIsNew(ContentQuery$QueryRoot$Content content) {
    final today = DateTime.now();
    final diff = today.difference(content.updatedAt ?? content.createdAt);
    //GUIDE IS CONSIDERED NEW IF HAS BEEN PUBLISHED WITHIN A DAY
    return diff.inDays < 1;
  }

  static String returnPartsCount(
      {required ContentQuery$QueryRoot$Content content}) {
    final count = content.childContent.length;
    if (content.contentType.id == kContentTypeGuideId) {
      return '$count ${count > 1 ? 'Chapters' : 'Chapter'}';
    } else if (content.contentType.id == kContentTypeAssessmentId) {
      return '$count ${count > 1 ? 'Questions' : 'Question'}';
    } else if (content.contentType.id == kContentTypeBulletinId) {
    } else if (content.contentType.id == kContentTypeCovidId) {}

    return '';
  }
}
