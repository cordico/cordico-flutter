// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'collection_by_id.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CollectionById$QueryRoot$CollectionByPk$CollectionType
    _$CollectionById$QueryRoot$CollectionByPk$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionTypeToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionType instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionById$QueryRoot$CollectionByPk$OrganizationCollections$Organization
    _$CollectionById$QueryRoot$CollectionByPk$OrganizationCollections$OrganizationFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$OrganizationCollections$Organization()
    ..name = json['name'] as String
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$OrganizationCollections$OrganizationToJson(
            CollectionById$QueryRoot$CollectionByPk$OrganizationCollections$Organization
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
        };

CollectionById$QueryRoot$CollectionByPk$OrganizationCollections
    _$CollectionById$QueryRoot$CollectionByPk$OrganizationCollectionsFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$OrganizationCollections()
    ..organization =
        CollectionById$QueryRoot$CollectionByPk$OrganizationCollections$Organization
            .fromJson(json['organization'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$OrganizationCollectionsToJson(
            CollectionById$QueryRoot$CollectionByPk$OrganizationCollections
                instance) =>
        <String, dynamic>{
          'organization': instance.organization.toJson(),
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentType
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentTypeToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'is_shared': instance.isShared,
          'description': instance.description,
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentType
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentTypeToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'is_shared': instance.isShared,
          'description': instance.description,
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ChildContent
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ChildContentFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ChildContent()
    ..id = json['id'] as String;
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ChildContentToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ChildContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ParentContent
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ParentContentFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ParentContentToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ParentContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'index': instance.index,
          'part_id': instance.partId,
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$CategoryToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories()
    ..category =
        CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories$Category
            .fromJson(json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategoriesToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories
                instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentState
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentStateFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentStateToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentState
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionTypeToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$CollectionToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$CollectionsFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections()
    ..collection =
        CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections$Collection
            .fromJson(json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$CollectionsToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections
                instance) =>
        <String, dynamic>{
          'collection': instance.collection.toJson(),
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPartFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart()
    ..contentType =
        CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentType
            .fromJson(json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) =>
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ChildContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) =>
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ParentContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentCategories
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentState =
        CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$ContentState
            .fromJson(json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) =>
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart$Collections
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPartToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart
                instance) =>
        <String, dynamic>{
          'content_type': instance.contentType.toJson(),
          'updated_at': instance.updatedAt?.toIso8601String(),
          'name': instance.name,
          'is_shared': instance.isShared,
          'id': instance.id,
          'data': instance.data,
          'created_at': instance.createdAt.toIso8601String(),
          'child_content':
              instance.childContent.map((e) => e.toJson()).toList(),
          'parent_content':
              instance.parentContent.map((e) => e.toJson()).toList(),
          'content_categories':
              instance.contentCategories.map((e) => e.toJson()).toList(),
          'content_state': instance.contentState.toJson(),
          'collections': instance.collections.map((e) => e.toJson()).toList(),
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContentFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent()
    ..contentPart =
        CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent$ContentPart
            .fromJson(json['content_part'] as Map<String, dynamic>)
    ..id = json['id'] as String
    ..index = json['index'] as int;
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContentToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent
                instance) =>
        <String, dynamic>{
          'content_part': instance.contentPart.toJson(),
          'id': instance.id,
          'index': instance.index,
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ParentContent
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ParentContentFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ParentContent()
    ..id = json['id'] as String
    ..index = json['index'] as int
    ..partId = json['part_id'] as String;
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ParentContentToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ParentContent
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'index': instance.index,
          'part_id': instance.partId,
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories$Category
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories$CategoryFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories$Category()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories$CategoryToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories$Category
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategoriesFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories()
    ..category =
        CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories$Category
            .fromJson(json['category'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategoriesToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories
                instance) =>
        <String, dynamic>{
          'category': instance.category.toJson(),
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentState
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentStateFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentState()
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentStateToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentState
                instance) =>
        <String, dynamic>{
          'id': instance.id,
          'name': instance.name,
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection$CollectionType
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection$CollectionTypeFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection$CollectionType()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..description = json['description'] as String?;
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection$CollectionTypeToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection$CollectionType
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'description': instance.description,
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$CollectionFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection()
    ..name = json['name'] as String
    ..id = json['id'] as String
    ..collectionType = json['collection_type'] == null
        ? null
        : CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection$CollectionType
            .fromJson(json['collection_type'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$CollectionToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection
                instance) =>
        <String, dynamic>{
          'name': instance.name,
          'id': instance.id,
          'collection_type': instance.collectionType?.toJson(),
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$CollectionsFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections()
    ..collection =
        CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections$Collection
            .fromJson(json['collection'] as Map<String, dynamic>);
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$CollectionsToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections
                instance) =>
        <String, dynamic>{
          'collection': instance.collection.toJson(),
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$ContentFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content()
    ..contentType =
        CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentType
            .fromJson(json['content_type'] as Map<String, dynamic>)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..name = json['name'] as String
    ..isShared = json['is_shared'] as bool
    ..id = json['id'] as String
    ..data = json['data']
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..childContent = (json['child_content'] as List<dynamic>)
        .map((e) =>
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ChildContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..parentContent = (json['parent_content'] as List<dynamic>)
        .map((e) =>
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ParentContent
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentCategories = (json['content_categories'] as List<dynamic>)
        .map((e) =>
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentCategories
                .fromJson(e as Map<String, dynamic>))
        .toList()
    ..contentState =
        CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$ContentState
            .fromJson(json['content_state'] as Map<String, dynamic>)
    ..collections = (json['collections'] as List<dynamic>)
        .map((e) =>
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content$Collections
                .fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic>
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContents$ContentToJson(
            CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content
                instance) =>
        <String, dynamic>{
          'content_type': instance.contentType.toJson(),
          'updated_at': instance.updatedAt?.toIso8601String(),
          'name': instance.name,
          'is_shared': instance.isShared,
          'id': instance.id,
          'data': instance.data,
          'created_at': instance.createdAt.toIso8601String(),
          'child_content':
              instance.childContent.map((e) => e.toJson()).toList(),
          'parent_content':
              instance.parentContent.map((e) => e.toJson()).toList(),
          'content_categories':
              instance.contentCategories.map((e) => e.toJson()).toList(),
          'content_state': instance.contentState.toJson(),
          'collections': instance.collections.map((e) => e.toJson()).toList(),
        };

CollectionById$QueryRoot$CollectionByPk$CollectionContents
    _$CollectionById$QueryRoot$CollectionByPk$CollectionContentsFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk$CollectionContents()
    ..id = json['id'] as String
    ..contentId = json['content_id'] as String
    ..collectionId = json['collection_id'] as String
    ..content =
        CollectionById$QueryRoot$CollectionByPk$CollectionContents$Content
            .fromJson(json['content'] as Map<String, dynamic>);
}

Map<String,
    dynamic> _$CollectionById$QueryRoot$CollectionByPk$CollectionContentsToJson(
        CollectionById$QueryRoot$CollectionByPk$CollectionContents instance) =>
    <String, dynamic>{
      'id': instance.id,
      'content_id': instance.contentId,
      'collection_id': instance.collectionId,
      'content': instance.content.toJson(),
    };

CollectionById$QueryRoot$CollectionByPk
    _$CollectionById$QueryRoot$CollectionByPkFromJson(
        Map<String, dynamic> json) {
  return CollectionById$QueryRoot$CollectionByPk()
    ..id = json['id'] as String
    ..isShared = json['is_shared'] as bool
    ..name = json['name'] as String
    ..title = json['title'] as String
    ..key = json['key'] as String?
    ..createdAt = DateTime.parse(json['created_at'] as String)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..collectionTypeId = json['collection_type_id'] as String?
    ..collectionType = json['collection_type'] == null
        ? null
        : CollectionById$QueryRoot$CollectionByPk$CollectionType.fromJson(
            json['collection_type'] as Map<String, dynamic>)
    ..organizationCollections =
        (json['organization_collections'] as List<dynamic>)
            .map((e) =>
                CollectionById$QueryRoot$CollectionByPk$OrganizationCollections
                    .fromJson(e as Map<String, dynamic>))
            .toList()
    ..collectionContents = (json['collection_contents'] as List<dynamic>)
        .map((e) =>
            CollectionById$QueryRoot$CollectionByPk$CollectionContents.fromJson(
                e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$CollectionById$QueryRoot$CollectionByPkToJson(
        CollectionById$QueryRoot$CollectionByPk instance) =>
    <String, dynamic>{
      'id': instance.id,
      'is_shared': instance.isShared,
      'name': instance.name,
      'title': instance.title,
      'key': instance.key,
      'created_at': instance.createdAt.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'collection_type_id': instance.collectionTypeId,
      'collection_type': instance.collectionType?.toJson(),
      'organization_collections':
          instance.organizationCollections.map((e) => e.toJson()).toList(),
      'collection_contents':
          instance.collectionContents.map((e) => e.toJson()).toList(),
    };

CollectionById$QueryRoot _$CollectionById$QueryRootFromJson(
    Map<String, dynamic> json) {
  return CollectionById$QueryRoot()
    ..collectionByPk = json['collection_by_pk'] == null
        ? null
        : CollectionById$QueryRoot$CollectionByPk.fromJson(
            json['collection_by_pk'] as Map<String, dynamic>);
}

Map<String, dynamic> _$CollectionById$QueryRootToJson(
        CollectionById$QueryRoot instance) =>
    <String, dynamic>{
      'collection_by_pk': instance.collectionByPk?.toJson(),
    };

CollectionByIdArguments _$CollectionByIdArgumentsFromJson(
    Map<String, dynamic> json) {
  return CollectionByIdArguments(
    id: json['id'] as String,
    contentStateNames: (json['contentStateNames'] as List<dynamic>?)
        ?.map((e) => e as String?)
        .toList(),
  );
}

Map<String, dynamic> _$CollectionByIdArgumentsToJson(
        CollectionByIdArguments instance) =>
    <String, dynamic>{
      'id': instance.id,
      'contentStateNames': instance.contentStateNames,
    };
