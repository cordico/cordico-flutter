// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'oc_by_oid_ctid.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OCByOIdCtId$QueryRoot$OrganizationContacts$ContactType
    _$OCByOIdCtId$QueryRoot$OrganizationContacts$ContactTypeFromJson(
        Map<String, dynamic> json) {
  return OCByOIdCtId$QueryRoot$OrganizationContacts$ContactType()
    ..displayName = json['display_name'] as String
    ..id = json['id'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$OCByOIdCtId$QueryRoot$OrganizationContacts$ContactTypeToJson(
            OCByOIdCtId$QueryRoot$OrganizationContacts$ContactType instance) =>
        <String, dynamic>{
          'display_name': instance.displayName,
          'id': instance.id,
          'name': instance.name,
        };

OCByOIdCtId$QueryRoot$OrganizationContacts$Organization
    _$OCByOIdCtId$QueryRoot$OrganizationContacts$OrganizationFromJson(
        Map<String, dynamic> json) {
  return OCByOIdCtId$QueryRoot$OrganizationContacts$Organization()
    ..name = json['name'] as String;
}

Map<String, dynamic>
    _$OCByOIdCtId$QueryRoot$OrganizationContacts$OrganizationToJson(
            OCByOIdCtId$QueryRoot$OrganizationContacts$Organization instance) =>
        <String, dynamic>{
          'name': instance.name,
        };

OCByOIdCtId$QueryRoot$OrganizationContacts
    _$OCByOIdCtId$QueryRoot$OrganizationContactsFromJson(
        Map<String, dynamic> json) {
  return OCByOIdCtId$QueryRoot$OrganizationContacts()
    ..data = json['data']
    ..title = json['title'] as String
    ..contactTypeId = json['contact_type_id'] as String
    ..contactType =
        OCByOIdCtId$QueryRoot$OrganizationContacts$ContactType.fromJson(
            json['contact_type'] as Map<String, dynamic>)
    ..organizationId = json['organization_id'] as String
    ..organization =
        OCByOIdCtId$QueryRoot$OrganizationContacts$Organization.fromJson(
            json['organization'] as Map<String, dynamic>)
    ..name = json['name'] as String
    ..isActive = json['is_active'] as bool
    ..id = json['id'] as String
    ..updated = json['updated'] == null
        ? null
        : DateTime.parse(json['updated'] as String)
    ..created = DateTime.parse(json['created'] as String);
}

Map<String, dynamic> _$OCByOIdCtId$QueryRoot$OrganizationContactsToJson(
        OCByOIdCtId$QueryRoot$OrganizationContacts instance) =>
    <String, dynamic>{
      'data': instance.data,
      'title': instance.title,
      'contact_type_id': instance.contactTypeId,
      'contact_type': instance.contactType.toJson(),
      'organization_id': instance.organizationId,
      'organization': instance.organization.toJson(),
      'name': instance.name,
      'is_active': instance.isActive,
      'id': instance.id,
      'updated': instance.updated?.toIso8601String(),
      'created': instance.created.toIso8601String(),
    };

OCByOIdCtId$QueryRoot _$OCByOIdCtId$QueryRootFromJson(
    Map<String, dynamic> json) {
  return OCByOIdCtId$QueryRoot()
    ..organizationContacts = (json['organization_contacts'] as List<dynamic>)
        .map((e) => OCByOIdCtId$QueryRoot$OrganizationContacts.fromJson(
            e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$OCByOIdCtId$QueryRootToJson(
        OCByOIdCtId$QueryRoot instance) =>
    <String, dynamic>{
      'organization_contacts':
          instance.organizationContacts.map((e) => e.toJson()).toList(),
    };

OCByOIdCtIdArguments _$OCByOIdCtIdArgumentsFromJson(Map<String, dynamic> json) {
  return OCByOIdCtIdArguments(
    organizationID: json['organizationID'] as String,
    contact_type_id: json['contact_type_id'] as String,
    isActive:
        (json['isActive'] as List<dynamic>?)?.map((e) => e as bool).toList(),
    offset: json['offset'] as int?,
    limit: json['limit'] as int?,
  );
}

Map<String, dynamic> _$OCByOIdCtIdArgumentsToJson(
        OCByOIdCtIdArguments instance) =>
    <String, dynamic>{
      'organizationID': instance.organizationID,
      'contact_type_id': instance.contact_type_id,
      'isActive': instance.isActive,
      'offset': instance.offset,
      'limit': instance.limit,
    };
