// ignore_for_file: avoid_function_literals_in_foreach_calls

import 'package:core/core.dart';

class AssessmentResult {
  AssessmentResult({
    this.assessmentID,
    this.assessmentScoresResult,
  });
  String? assessmentID;
  List<AssessmentScoresResult>? assessmentScoresResult;
}

class AssessmentScoresResult {
  AssessmentScoresResult();
  String? scaleKey;
  
  //THIS CONTAINS THE ASSESSMENT QUESTION ID AS KEY 
  //AND THE SCORE ASSIGNED TO IT AS VALUE
  Map<String, int>? scores;
  OutcomeSchema? outcome;

  int get getFinalScore {
    var score = 0;
    if (scores == null) {
      return score;
    }
    scores?.entries.forEach((element) {
      score += element.value;
    });
    return score;
  }

  ScoreRangeSchema? get getScoreRange {
    if (outcome == null) {
      return null;
    }
    final ranges = outcome!.scoreRanges?.where((e) {
      return getFinalScore >= (e.startScore ?? 0).toInt() &&
          getFinalScore <= (e.endScore ?? 0).toInt();
    }).toList();

    if (ranges != null && ranges.isNotEmpty) {
      return ranges.first;
    }

    return null;
  }
}
