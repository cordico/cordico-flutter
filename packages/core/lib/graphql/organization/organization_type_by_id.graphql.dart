// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'organization_type_by_id.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class OrganizationTypeByID$QueryRoot$OrganizationTypeByPk
    extends JsonSerializable with EquatableMixin {
  OrganizationTypeByID$QueryRoot$OrganizationTypeByPk();

  factory OrganizationTypeByID$QueryRoot$OrganizationTypeByPk.fromJson(
          Map<String, dynamic> json) =>
      _$OrganizationTypeByID$QueryRoot$OrganizationTypeByPkFromJson(json);

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  @JsonKey(name: 'creator_id')
  late String creatorId;

  String? description;

  late String id;

  late String key;

  late String name;

  late dynamic schema;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @JsonKey(name: 'updater_id')
  String? updaterId;

  @override
  List<Object?> get props => [
        createdAt,
        creatorId,
        description,
        id,
        key,
        name,
        schema,
        updatedAt,
        updaterId
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$OrganizationTypeByID$QueryRoot$OrganizationTypeByPkToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationTypeByID$QueryRoot extends JsonSerializable
    with EquatableMixin {
  OrganizationTypeByID$QueryRoot();

  factory OrganizationTypeByID$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$OrganizationTypeByID$QueryRootFromJson(json);

  @JsonKey(name: 'organization_type_by_pk')
  OrganizationTypeByID$QueryRoot$OrganizationTypeByPk? organizationTypeByPk;

  @override
  List<Object?> get props => [organizationTypeByPk];
  @override
  Map<String, dynamic> toJson() => _$OrganizationTypeByID$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class OrganizationTypeByIDArguments extends JsonSerializable
    with EquatableMixin {
  OrganizationTypeByIDArguments({required this.id});

  @override
  factory OrganizationTypeByIDArguments.fromJson(Map<String, dynamic> json) =>
      _$OrganizationTypeByIDArgumentsFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$OrganizationTypeByIDArgumentsToJson(this);
}

final ORGANIZATION_TYPE_BY_I_D_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'OrganizationTypeByID'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'organization_type_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'id'),
                  value: VariableNode(name: NameNode(value: 'id')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'creator_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'description'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'key'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'schema'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updater_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class OrganizationTypeByIDQuery extends GraphQLQuery<
    OrganizationTypeByID$QueryRoot, OrganizationTypeByIDArguments> {
  OrganizationTypeByIDQuery({required this.variables});

  @override
  final DocumentNode document = ORGANIZATION_TYPE_BY_I_D_QUERY_DOCUMENT;

  @override
  final String operationName = 'OrganizationTypeByID';

  @override
  final OrganizationTypeByIDArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  OrganizationTypeByID$QueryRoot parse(Map<String, dynamic> json) =>
      OrganizationTypeByID$QueryRoot.fromJson(json);
}
