// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'user_by_id.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class UserById$QueryRoot$UserByPk extends JsonSerializable with EquatableMixin {
  UserById$QueryRoot$UserByPk();

  factory UserById$QueryRoot$UserByPk.fromJson(Map<String, dynamic> json) =>
      _$UserById$QueryRoot$UserByPkFromJson(json);

  dynamic? data;

  @JsonKey(name: 'created_at')
  late DateTime createdAt;

  late String id;

  late String identifier;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @JsonKey(name: 'user_service_id')
  String? userServiceId;

  @JsonKey(name: 'user_type_id')
  late String userTypeId;

  @override
  List<Object?> get props =>
      [data, createdAt, id, identifier, updatedAt, userServiceId, userTypeId];
  @override
  Map<String, dynamic> toJson() => _$UserById$QueryRoot$UserByPkToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UserById$QueryRoot extends JsonSerializable with EquatableMixin {
  UserById$QueryRoot();

  factory UserById$QueryRoot.fromJson(Map<String, dynamic> json) =>
      _$UserById$QueryRootFromJson(json);

  @JsonKey(name: 'user_by_pk')
  UserById$QueryRoot$UserByPk? userByPk;

  @override
  List<Object?> get props => [userByPk];
  @override
  Map<String, dynamic> toJson() => _$UserById$QueryRootToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UserByIdArguments extends JsonSerializable with EquatableMixin {
  UserByIdArguments({required this.id});

  @override
  factory UserByIdArguments.fromJson(Map<String, dynamic> json) =>
      _$UserByIdArgumentsFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() => _$UserByIdArgumentsToJson(this);
}

final USER_BY_ID_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'UserById'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'id')),
            type: NamedTypeNode(name: NameNode(value: 'uuid'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'user_by_pk'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'id'),
                  value: VariableNode(name: NameNode(value: 'id')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'data'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'created_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'identifier'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'updated_at'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'user_service_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'user_type_id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class UserByIdQuery
    extends GraphQLQuery<UserById$QueryRoot, UserByIdArguments> {
  UserByIdQuery({required this.variables});

  @override
  final DocumentNode document = USER_BY_ID_QUERY_DOCUMENT;

  @override
  final String operationName = 'UserById';

  @override
  final UserByIdArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  UserById$QueryRoot parse(Map<String, dynamic> json) =>
      UserById$QueryRoot.fromJson(json);
}
