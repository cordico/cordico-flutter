import 'package:cordico_mobile/core/theme/colors.dart';
import 'package:cordico_mobile/core/theme/styles.dart';
import 'package:cordico_mobile/core/widgets/badge/new_badge.dart';
import 'package:cordico_mobile/core/widgets/badge/urgent_badge.dart';

import 'package:cordico_mobile/core/widgets/card/cordico_card.dart';
import 'package:core/core.dart';
import 'package:core/graphql/content/content_query.dart';
import 'package:core/utils/content_utils.dart';
import 'package:core/utils/map_extension.dart';
import 'package:flutter/material.dart';

class BulletinCell extends StatefulWidget {
  const BulletinCell({
    Key? key,
    required this.bulletin,
    this.onPressed,
    this.isLast = false,
  }) : super(key: key);

  final ContentQuery$QueryRoot$Content bulletin;
  final Function()? onPressed;
  final bool isLast;

  @override
  State<BulletinCell> createState() => _BulletinCellState();
}

class _BulletinCellState extends State<BulletinCell> {
  ContentSchema? contentSchema;

  late ContentQuery$QueryRoot$Content bulletin;
  @override
  void initState() {
    super.initState();

    bulletin = widget.bulletin;

    removeNullAndEmptyParams(widget.bulletin.data as Map<String, dynamic>);
    contentSchema =
        ContentSchema.fromJson(widget.bulletin.data as Map<String, dynamic>);
  }

  @override
  void didUpdateWidget(covariant BulletinCell oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (oldWidget.bulletin != widget.bulletin) {
      bulletin = widget.bulletin;
      removeNullAndEmptyParams(widget.bulletin.data as Map<String, dynamic>);
      contentSchema =
          ContentSchema.fromJson(widget.bulletin.data as Map<String, dynamic>);
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: widget.isLast ? 0 : 10),
      child: CordicoCard(
        cornerRadius: 0,
        onTap: widget.onPressed,
        color: ThemeColors.gray.shade900,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 18),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              _buildCategoryHeader(context),
              //if (isLargePicture && picture != null) _buildBigPicture(context)
              //if (videoUrl != null) _buildVideoPlayer(context),
              _buildBulletinContent(context),
              _buildFooter(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCategoryHeader(BuildContext context) {
    final isNew = ContentUtils.evaluateIsNew(widget.bulletin);
    return Container(
      constraints: const BoxConstraints(minHeight: 16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Flexible(
            child: Text(
              (contentSchema?.type ?? '').toUpperCase(),
              style: ThemeStyles.cardCategory(context),
            ),
          ),
          if (isNew)
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 8),
              child: NewBadge(),
            ),
          if (contentSchema?.isUrgent == true)
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 8),
              child: UrgentBadge(),
            ),
        ],
      ),
    );
  }

  Widget _buildBulletinContent(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            contentSchema?.title ?? '',
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: ThemeStyles.bulletinCardTitle(context),
          ),
          const SizedBox(height: 4),
          Text(
            contentSchema?.summary ?? '',
            maxLines: 5,
            overflow: TextOverflow.ellipsis,
            style: ThemeStyles.contentDescription(context),
          ),
        ],
      ),
    );
  }

  Widget _buildFooter(BuildContext context) {
    var content = '';
    if ((contentSchema?.author ?? '').isNotEmpty) {
      content = '${contentSchema?.author ?? ''}  \u00B7  ';
    }

    final formattedDate = DateExtensionCore.formatDate(
        date: widget.bulletin.createdAt, format: 'MMM dd, yyyy  \u00B7  hh:mm');

    content += '$formattedDate';

    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: Row(
        children: [
          Expanded(
            child: Text(
              content,
              style: ThemeStyles.smallParagraph(context),
            ),
          )
        ],
      ),
    );
  }
}
